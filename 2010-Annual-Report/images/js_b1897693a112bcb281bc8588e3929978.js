// $Id: jquery.js,v 1.12.2.3 2008/06/25 09:38:39 goba Exp $ 

/*
 * jQuery 1.2.6 - New Wave Javascript
 *
 * Copyright (c) 2008 John Resig (jquery.com)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * Date: 2008-05-24 14:22:17 -0400 (Sat, 24 May 2008)
 * Rev: 5685
 */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(H(){J w=1b.4M,3m$=1b.$;J D=1b.4M=1b.$=H(a,b){I 2B D.17.5j(a,b)};J u=/^[^<]*(<(.|\\s)+>)[^>]*$|^#(\\w+)$/,62=/^.[^:#\\[\\.]*$/,12;D.17=D.44={5j:H(d,b){d=d||S;G(d.16){7[0]=d;7.K=1;I 7}G(1j d=="23"){J c=u.2D(d);G(c&&(c[1]||!b)){G(c[1])d=D.4h([c[1]],b);N{J a=S.61(c[3]);G(a){G(a.2v!=c[3])I D().2q(d);I D(a)}d=[]}}N I D(b).2q(d)}N G(D.1D(d))I D(S)[D.17.27?"27":"43"](d);I 7.6Y(D.2d(d))},5w:"1.2.6",8G:H(){I 7.K},K:0,3p:H(a){I a==12?D.2d(7):7[a]},2I:H(b){J a=D(b);a.5n=7;I a},6Y:H(a){7.K=0;2p.44.1p.1w(7,a);I 7},P:H(a,b){I D.P(7,a,b)},5i:H(b){J a=-1;I D.2L(b&&b.5w?b[0]:b,7)},1K:H(c,a,b){J d=c;G(c.1q==56)G(a===12)I 7[0]&&D[b||"1K"](7[0],c);N{d={};d[c]=a}I 7.P(H(i){R(c 1n d)D.1K(b?7.V:7,c,D.1i(7,d[c],b,i,c))})},1g:H(b,a){G((b==\'2h\'||b==\'1Z\')&&3d(a)<0)a=12;I 7.1K(b,a,"2a")},1r:H(b){G(1j b!="49"&&b!=U)I 7.4E().3v((7[0]&&7[0].2z||S).5F(b));J a="";D.P(b||7,H(){D.P(7.3t,H(){G(7.16!=8)a+=7.16!=1?7.76:D.17.1r([7])})});I a},5z:H(b){G(7[0])D(b,7[0].2z).5y().39(7[0]).2l(H(){J a=7;1B(a.1x)a=a.1x;I a}).3v(7);I 7},8Y:H(a){I 7.P(H(){D(7).6Q().5z(a)})},8R:H(a){I 7.P(H(){D(7).5z(a)})},3v:H(){I 7.3W(19,M,Q,H(a){G(7.16==1)7.3U(a)})},6F:H(){I 7.3W(19,M,M,H(a){G(7.16==1)7.39(a,7.1x)})},6E:H(){I 7.3W(19,Q,Q,H(a){7.1d.39(a,7)})},5q:H(){I 7.3W(19,Q,M,H(a){7.1d.39(a,7.2H)})},3l:H(){I 7.5n||D([])},2q:H(b){J c=D.2l(7,H(a){I D.2q(b,a)});I 7.2I(/[^+>] [^+>]/.11(b)||b.1h("..")>-1?D.4r(c):c)},5y:H(e){J f=7.2l(H(){G(D.14.1f&&!D.4n(7)){J a=7.6o(M),5h=S.3h("1v");5h.3U(a);I D.4h([5h.4H])[0]}N I 7.6o(M)});J d=f.2q("*").5c().P(H(){G(7[E]!=12)7[E]=U});G(e===M)7.2q("*").5c().P(H(i){G(7.16==3)I;J c=D.L(7,"3w");R(J a 1n c)R(J b 1n c[a])D.W.1e(d[i],a,c[a][b],c[a][b].L)});I f},1E:H(b){I 7.2I(D.1D(b)&&D.3C(7,H(a,i){I b.1k(a,i)})||D.3g(b,7))},4Y:H(b){G(b.1q==56)G(62.11(b))I 7.2I(D.3g(b,7,M));N b=D.3g(b,7);J a=b.K&&b[b.K-1]!==12&&!b.16;I 7.1E(H(){I a?D.2L(7,b)<0:7!=b})},1e:H(a){I 7.2I(D.4r(D.2R(7.3p(),1j a==\'23\'?D(a):D.2d(a))))},3F:H(a){I!!a&&D.3g(a,7).K>0},7T:H(a){I 7.3F("."+a)},6e:H(b){G(b==12){G(7.K){J c=7[0];G(D.Y(c,"2A")){J e=c.64,63=[],15=c.15,2V=c.O=="2A-2V";G(e<0)I U;R(J i=2V?e:0,2f=2V?e+1:15.K;i<2f;i++){J d=15[i];G(d.2W){b=D.14.1f&&!d.at.2x.an?d.1r:d.2x;G(2V)I b;63.1p(b)}}I 63}N I(7[0].2x||"").1o(/\\r/g,"")}I 12}G(b.1q==4L)b+=\'\';I 7.P(H(){G(7.16!=1)I;G(b.1q==2p&&/5O|5L/.11(7.O))7.4J=(D.2L(7.2x,b)>=0||D.2L(7.34,b)>=0);N G(D.Y(7,"2A")){J a=D.2d(b);D("9R",7).P(H(){7.2W=(D.2L(7.2x,a)>=0||D.2L(7.1r,a)>=0)});G(!a.K)7.64=-1}N 7.2x=b})},2K:H(a){I a==12?(7[0]?7[0].4H:U):7.4E().3v(a)},7b:H(a){I 7.5q(a).21()},79:H(i){I 7.3s(i,i+1)},3s:H(){I 7.2I(2p.44.3s.1w(7,19))},2l:H(b){I 7.2I(D.2l(7,H(a,i){I b.1k(a,i,a)}))},5c:H(){I 7.1e(7.5n)},L:H(d,b){J a=d.1R(".");a[1]=a[1]?"."+a[1]:"";G(b===12){J c=7.5C("9z"+a[1]+"!",[a[0]]);G(c===12&&7.K)c=D.L(7[0],d);I c===12&&a[1]?7.L(a[0]):c}N I 7.1P("9u"+a[1]+"!",[a[0],b]).P(H(){D.L(7,d,b)})},3b:H(a){I 7.P(H(){D.3b(7,a)})},3W:H(g,f,h,d){J e=7.K>1,3x;I 7.P(H(){G(!3x){3x=D.4h(g,7.2z);G(h)3x.9o()}J b=7;G(f&&D.Y(7,"1T")&&D.Y(3x[0],"4F"))b=7.3H("22")[0]||7.3U(7.2z.3h("22"));J c=D([]);D.P(3x,H(){J a=e?D(7).5y(M)[0]:7;G(D.Y(a,"1m"))c=c.1e(a);N{G(a.16==1)c=c.1e(D("1m",a).21());d.1k(b,a)}});c.P(6T)})}};D.17.5j.44=D.17;H 6T(i,a){G(a.4d)D.3Y({1a:a.4d,31:Q,1O:"1m"});N D.5u(a.1r||a.6O||a.4H||"");G(a.1d)a.1d.37(a)}H 1z(){I+2B 8J}D.1l=D.17.1l=H(){J b=19[0]||{},i=1,K=19.K,4x=Q,15;G(b.1q==8I){4x=b;b=19[1]||{};i=2}G(1j b!="49"&&1j b!="H")b={};G(K==i){b=7;--i}R(;i<K;i++)G((15=19[i])!=U)R(J c 1n 15){J a=b[c],2w=15[c];G(b===2w)6M;G(4x&&2w&&1j 2w=="49"&&!2w.16)b[c]=D.1l(4x,a||(2w.K!=U?[]:{}),2w);N G(2w!==12)b[c]=2w}I b};J E="4M"+1z(),6K=0,5r={},6G=/z-?5i|8B-?8A|1y|6B|8v-?1Z/i,3P=S.3P||{};D.1l({8u:H(a){1b.$=3m$;G(a)1b.4M=w;I D},1D:H(a){I!!a&&1j a!="23"&&!a.Y&&a.1q!=2p&&/^[\\s[]?H/.11(a+"")},4n:H(a){I a.1C&&!a.1c||a.2j&&a.2z&&!a.2z.1c},5u:H(a){a=D.3k(a);G(a){J b=S.3H("6w")[0]||S.1C,1m=S.3h("1m");1m.O="1r/4t";G(D.14.1f)1m.1r=a;N 1m.3U(S.5F(a));b.39(1m,b.1x);b.37(1m)}},Y:H(b,a){I b.Y&&b.Y.2r()==a.2r()},1Y:{},L:H(c,d,b){c=c==1b?5r:c;J a=c[E];G(!a)a=c[E]=++6K;G(d&&!D.1Y[a])D.1Y[a]={};G(b!==12)D.1Y[a][d]=b;I d?D.1Y[a][d]:a},3b:H(c,b){c=c==1b?5r:c;J a=c[E];G(b){G(D.1Y[a]){2U D.1Y[a][b];b="";R(b 1n D.1Y[a])1X;G(!b)D.3b(c)}}N{1U{2U c[E]}1V(e){G(c.5l)c.5l(E)}2U D.1Y[a]}},P:H(d,a,c){J e,i=0,K=d.K;G(c){G(K==12){R(e 1n d)G(a.1w(d[e],c)===Q)1X}N R(;i<K;)G(a.1w(d[i++],c)===Q)1X}N{G(K==12){R(e 1n d)G(a.1k(d[e],e,d[e])===Q)1X}N R(J b=d[0];i<K&&a.1k(b,i,b)!==Q;b=d[++i]){}}I d},1i:H(b,a,c,i,d){G(D.1D(a))a=a.1k(b,i);I a&&a.1q==4L&&c=="2a"&&!6G.11(d)?a+"2X":a},1F:{1e:H(c,b){D.P((b||"").1R(/\\s+/),H(i,a){G(c.16==1&&!D.1F.3T(c.1F,a))c.1F+=(c.1F?" ":"")+a})},21:H(c,b){G(c.16==1)c.1F=b!=12?D.3C(c.1F.1R(/\\s+/),H(a){I!D.1F.3T(b,a)}).6s(" "):""},3T:H(b,a){I D.2L(a,(b.1F||b).6r().1R(/\\s+/))>-1}},6q:H(b,c,a){J e={};R(J d 1n c){e[d]=b.V[d];b.V[d]=c[d]}a.1k(b);R(J d 1n c)b.V[d]=e[d]},1g:H(d,e,c){G(e=="2h"||e=="1Z"){J b,3X={30:"5x",5g:"1G",18:"3I"},35=e=="2h"?["5e","6k"]:["5G","6i"];H 5b(){b=e=="2h"?d.8f:d.8c;J a=0,2C=0;D.P(35,H(){a+=3d(D.2a(d,"57"+7,M))||0;2C+=3d(D.2a(d,"2C"+7+"4b",M))||0});b-=29.83(a+2C)}G(D(d).3F(":4j"))5b();N D.6q(d,3X,5b);I 29.2f(0,b)}I D.2a(d,e,c)},2a:H(f,l,k){J e,V=f.V;H 3E(b){G(!D.14.2k)I Q;J a=3P.54(b,U);I!a||a.52("3E")==""}G(l=="1y"&&D.14.1f){e=D.1K(V,"1y");I e==""?"1":e}G(D.14.2G&&l=="18"){J d=V.50;V.50="0 7Y 7W";V.50=d}G(l.1I(/4i/i))l=y;G(!k&&V&&V[l])e=V[l];N G(3P.54){G(l.1I(/4i/i))l="4i";l=l.1o(/([A-Z])/g,"-$1").3y();J c=3P.54(f,U);G(c&&!3E(f))e=c.52(l);N{J g=[],2E=[],a=f,i=0;R(;a&&3E(a);a=a.1d)2E.6h(a);R(;i<2E.K;i++)G(3E(2E[i])){g[i]=2E[i].V.18;2E[i].V.18="3I"}e=l=="18"&&g[2E.K-1]!=U?"2F":(c&&c.52(l))||"";R(i=0;i<g.K;i++)G(g[i]!=U)2E[i].V.18=g[i]}G(l=="1y"&&e=="")e="1"}N G(f.4g){J h=l.1o(/\\-(\\w)/g,H(a,b){I b.2r()});e=f.4g[l]||f.4g[h];G(!/^\\d+(2X)?$/i.11(e)&&/^\\d/.11(e)){J j=V.1A,66=f.65.1A;f.65.1A=f.4g.1A;V.1A=e||0;e=V.aM+"2X";V.1A=j;f.65.1A=66}}I e},4h:H(l,h){J k=[];h=h||S;G(1j h.3h==\'12\')h=h.2z||h[0]&&h[0].2z||S;D.P(l,H(i,d){G(!d)I;G(d.1q==4L)d+=\'\';G(1j d=="23"){d=d.1o(/(<(\\w+)[^>]*?)\\/>/g,H(b,a,c){I c.1I(/^(aK|4f|7E|aG|4T|7A|aB|3n|az|ay|av)$/i)?b:a+"></"+c+">"});J f=D.3k(d).3y(),1v=h.3h("1v");J e=!f.1h("<au")&&[1,"<2A 7w=\'7w\'>","</2A>"]||!f.1h("<ar")&&[1,"<7v>","</7v>"]||f.1I(/^<(aq|22|am|ak|ai)/)&&[1,"<1T>","</1T>"]||!f.1h("<4F")&&[2,"<1T><22>","</22></1T>"]||(!f.1h("<af")||!f.1h("<ad"))&&[3,"<1T><22><4F>","</4F></22></1T>"]||!f.1h("<7E")&&[2,"<1T><22></22><7q>","</7q></1T>"]||D.14.1f&&[1,"1v<1v>","</1v>"]||[0,"",""];1v.4H=e[1]+d+e[2];1B(e[0]--)1v=1v.5T;G(D.14.1f){J g=!f.1h("<1T")&&f.1h("<22")<0?1v.1x&&1v.1x.3t:e[1]=="<1T>"&&f.1h("<22")<0?1v.3t:[];R(J j=g.K-1;j>=0;--j)G(D.Y(g[j],"22")&&!g[j].3t.K)g[j].1d.37(g[j]);G(/^\\s/.11(d))1v.39(h.5F(d.1I(/^\\s*/)[0]),1v.1x)}d=D.2d(1v.3t)}G(d.K===0&&(!D.Y(d,"3V")&&!D.Y(d,"2A")))I;G(d[0]==12||D.Y(d,"3V")||d.15)k.1p(d);N k=D.2R(k,d)});I k},1K:H(d,f,c){G(!d||d.16==3||d.16==8)I 12;J e=!D.4n(d),40=c!==12,1f=D.14.1f;f=e&&D.3X[f]||f;G(d.2j){J g=/5Q|4d|V/.11(f);G(f=="2W"&&D.14.2k)d.1d.64;G(f 1n d&&e&&!g){G(40){G(f=="O"&&D.Y(d,"4T")&&d.1d)7p"O a3 a1\'t 9V 9U";d[f]=c}G(D.Y(d,"3V")&&d.7i(f))I d.7i(f).76;I d[f]}G(1f&&e&&f=="V")I D.1K(d.V,"9T",c);G(40)d.9Q(f,""+c);J h=1f&&e&&g?d.4G(f,2):d.4G(f);I h===U?12:h}G(1f&&f=="1y"){G(40){d.6B=1;d.1E=(d.1E||"").1o(/7f\\([^)]*\\)/,"")+(3r(c)+\'\'=="9L"?"":"7f(1y="+c*7a+")")}I d.1E&&d.1E.1h("1y=")>=0?(3d(d.1E.1I(/1y=([^)]*)/)[1])/7a)+\'\':""}f=f.1o(/-([a-z])/9H,H(a,b){I b.2r()});G(40)d[f]=c;I d[f]},3k:H(a){I(a||"").1o(/^\\s+|\\s+$/g,"")},2d:H(b){J a=[];G(b!=U){J i=b.K;G(i==U||b.1R||b.4I||b.1k)a[0]=b;N 1B(i)a[--i]=b[i]}I a},2L:H(b,a){R(J i=0,K=a.K;i<K;i++)G(a[i]===b)I i;I-1},2R:H(a,b){J i=0,T,2S=a.K;G(D.14.1f){1B(T=b[i++])G(T.16!=8)a[2S++]=T}N 1B(T=b[i++])a[2S++]=T;I a},4r:H(a){J c=[],2o={};1U{R(J i=0,K=a.K;i<K;i++){J b=D.L(a[i]);G(!2o[b]){2o[b]=M;c.1p(a[i])}}}1V(e){c=a}I c},3C:H(c,a,d){J b=[];R(J i=0,K=c.K;i<K;i++)G(!d!=!a(c[i],i))b.1p(c[i]);I b},2l:H(d,a){J c=[];R(J i=0,K=d.K;i<K;i++){J b=a(d[i],i);G(b!=U)c[c.K]=b}I c.7d.1w([],c)}});J v=9B.9A.3y();D.14={5B:(v.1I(/.+(?:9y|9x|9w|9v)[\\/: ]([\\d.]+)/)||[])[1],2k:/75/.11(v),2G:/2G/.11(v),1f:/1f/.11(v)&&!/2G/.11(v),42:/42/.11(v)&&!/(9s|75)/.11(v)};J y=D.14.1f?"7o":"72";D.1l({71:!D.14.1f||S.70=="6Z",3X:{"R":"9n","9k":"1F","4i":y,72:y,7o:y,9h:"9f",9e:"9d",9b:"99"}});D.P({6W:H(a){I a.1d},97:H(a){I D.4S(a,"1d")},95:H(a){I D.3a(a,2,"2H")},91:H(a){I D.3a(a,2,"4l")},8Z:H(a){I D.4S(a,"2H")},8X:H(a){I D.4S(a,"4l")},8W:H(a){I D.5v(a.1d.1x,a)},8V:H(a){I D.5v(a.1x)},6Q:H(a){I D.Y(a,"8U")?a.8T||a.8S.S:D.2d(a.3t)}},H(c,d){D.17[c]=H(b){J a=D.2l(7,d);G(b&&1j b=="23")a=D.3g(b,a);I 7.2I(D.4r(a))}});D.P({6P:"3v",8Q:"6F",39:"6E",8P:"5q",8O:"7b"},H(c,b){D.17[c]=H(){J a=19;I 7.P(H(){R(J i=0,K=a.K;i<K;i++)D(a[i])[b](7)})}});D.P({8N:H(a){D.1K(7,a,"");G(7.16==1)7.5l(a)},8M:H(a){D.1F.1e(7,a)},8L:H(a){D.1F.21(7,a)},8K:H(a){D.1F[D.1F.3T(7,a)?"21":"1e"](7,a)},21:H(a){G(!a||D.1E(a,[7]).r.K){D("*",7).1e(7).P(H(){D.W.21(7);D.3b(7)});G(7.1d)7.1d.37(7)}},4E:H(){D(">*",7).21();1B(7.1x)7.37(7.1x)}},H(a,b){D.17[a]=H(){I 7.P(b,19)}});D.P(["6N","4b"],H(i,c){J b=c.3y();D.17[b]=H(a){I 7[0]==1b?D.14.2G&&S.1c["5t"+c]||D.14.2k&&1b["5s"+c]||S.70=="6Z"&&S.1C["5t"+c]||S.1c["5t"+c]:7[0]==S?29.2f(29.2f(S.1c["4y"+c],S.1C["4y"+c]),29.2f(S.1c["2i"+c],S.1C["2i"+c])):a==12?(7.K?D.1g(7[0],b):U):7.1g(b,a.1q==56?a:a+"2X")}});H 25(a,b){I a[0]&&3r(D.2a(a[0],b,M),10)||0}J C=D.14.2k&&3r(D.14.5B)<8H?"(?:[\\\\w*3m-]|\\\\\\\\.)":"(?:[\\\\w\\8F-\\8E*3m-]|\\\\\\\\.)",6L=2B 4v("^>\\\\s*("+C+"+)"),6J=2B 4v("^("+C+"+)(#)("+C+"+)"),6I=2B 4v("^([#.]?)("+C+"*)");D.1l({6H:{"":H(a,i,m){I m[2]=="*"||D.Y(a,m[2])},"#":H(a,i,m){I a.4G("2v")==m[2]},":":{8D:H(a,i,m){I i<m[3]-0},8C:H(a,i,m){I i>m[3]-0},3a:H(a,i,m){I m[3]-0==i},79:H(a,i,m){I m[3]-0==i},3o:H(a,i){I i==0},3S:H(a,i,m,r){I i==r.K-1},6D:H(a,i){I i%2==0},6C:H(a,i){I i%2},"3o-4u":H(a){I a.1d.3H("*")[0]==a},"3S-4u":H(a){I D.3a(a.1d.5T,1,"4l")==a},"8z-4u":H(a){I!D.3a(a.1d.5T,2,"4l")},6W:H(a){I a.1x},4E:H(a){I!a.1x},8y:H(a,i,m){I(a.6O||a.8x||D(a).1r()||"").1h(m[3])>=0},4j:H(a){I"1G"!=a.O&&D.1g(a,"18")!="2F"&&D.1g(a,"5g")!="1G"},1G:H(a){I"1G"==a.O||D.1g(a,"18")=="2F"||D.1g(a,"5g")=="1G"},8w:H(a){I!a.3R},3R:H(a){I a.3R},4J:H(a){I a.4J},2W:H(a){I a.2W||D.1K(a,"2W")},1r:H(a){I"1r"==a.O},5O:H(a){I"5O"==a.O},5L:H(a){I"5L"==a.O},5p:H(a){I"5p"==a.O},3Q:H(a){I"3Q"==a.O},5o:H(a){I"5o"==a.O},6A:H(a){I"6A"==a.O},6z:H(a){I"6z"==a.O},2s:H(a){I"2s"==a.O||D.Y(a,"2s")},4T:H(a){I/4T|2A|6y|2s/i.11(a.Y)},3T:H(a,i,m){I D.2q(m[3],a).K},8t:H(a){I/h\\d/i.11(a.Y)},8s:H(a){I D.3C(D.3O,H(b){I a==b.T}).K}}},6x:[/^(\\[) *@?([\\w-]+) *([!*$^~=]*) *(\'?"?)(.*?)\\4 *\\]/,/^(:)([\\w-]+)\\("?\'?(.*?(\\(.*?\\))?[^(]*?)"?\'?\\)/,2B 4v("^([:.#]*)("+C+"+)")],3g:H(a,c,b){J d,1t=[];1B(a&&a!=d){d=a;J f=D.1E(a,c,b);a=f.t.1o(/^\\s*,\\s*/,"");1t=b?c=f.r:D.2R(1t,f.r)}I 1t},2q:H(t,o){G(1j t!="23")I[t];G(o&&o.16!=1&&o.16!=9)I[];o=o||S;J d=[o],2o=[],3S,Y;1B(t&&3S!=t){J r=[];3S=t;t=D.3k(t);J l=Q,3j=6L,m=3j.2D(t);G(m){Y=m[1].2r();R(J i=0;d[i];i++)R(J c=d[i].1x;c;c=c.2H)G(c.16==1&&(Y=="*"||c.Y.2r()==Y))r.1p(c);d=r;t=t.1o(3j,"");G(t.1h(" ")==0)6M;l=M}N{3j=/^([>+~])\\s*(\\w*)/i;G((m=3j.2D(t))!=U){r=[];J k={};Y=m[2].2r();m=m[1];R(J j=0,3i=d.K;j<3i;j++){J n=m=="~"||m=="+"?d[j].2H:d[j].1x;R(;n;n=n.2H)G(n.16==1){J g=D.L(n);G(m=="~"&&k[g])1X;G(!Y||n.Y.2r()==Y){G(m=="~")k[g]=M;r.1p(n)}G(m=="+")1X}}d=r;t=D.3k(t.1o(3j,""));l=M}}G(t&&!l){G(!t.1h(",")){G(o==d[0])d.4s();2o=D.2R(2o,d);r=d=[o];t=" "+t.6v(1,t.K)}N{J h=6J;J m=h.2D(t);G(m){m=[0,m[2],m[3],m[1]]}N{h=6I;m=h.2D(t)}m[2]=m[2].1o(/\\\\/g,"");J f=d[d.K-1];G(m[1]=="#"&&f&&f.61&&!D.4n(f)){J p=f.61(m[2]);G((D.14.1f||D.14.2G)&&p&&1j p.2v=="23"&&p.2v!=m[2])p=D(\'[@2v="\'+m[2]+\'"]\',f)[0];d=r=p&&(!m[3]||D.Y(p,m[3]))?[p]:[]}N{R(J i=0;d[i];i++){J a=m[1]=="#"&&m[3]?m[3]:m[1]!=""||m[0]==""?"*":m[2];G(a=="*"&&d[i].Y.3y()=="49")a="3n";r=D.2R(r,d[i].3H(a))}G(m[1]==".")r=D.5m(r,m[2]);G(m[1]=="#"){J e=[];R(J i=0;r[i];i++)G(r[i].4G("2v")==m[2]){e=[r[i]];1X}r=e}d=r}t=t.1o(h,"")}}G(t){J b=D.1E(t,r);d=r=b.r;t=D.3k(b.t)}}G(t)d=[];G(d&&o==d[0])d.4s();2o=D.2R(2o,d);I 2o},5m:H(r,m,a){m=" "+m+" ";J c=[];R(J i=0;r[i];i++){J b=(" "+r[i].1F+" ").1h(m)>=0;G(!a&&b||a&&!b)c.1p(r[i])}I c},1E:H(t,r,h){J d;1B(t&&t!=d){d=t;J p=D.6x,m;R(J i=0;p[i];i++){m=p[i].2D(t);G(m){t=t.8r(m[0].K);m[2]=m[2].1o(/\\\\/g,"");1X}}G(!m)1X;G(m[1]==":"&&m[2]=="4Y")r=62.11(m[3])?D.1E(m[3],r,M).r:D(r).4Y(m[3]);N G(m[1]==".")r=D.5m(r,m[2],h);N G(m[1]=="["){J g=[],O=m[3];R(J i=0,3i=r.K;i<3i;i++){J a=r[i],z=a[D.3X[m[2]]||m[2]];G(z==U||/5Q|4d|2W/.11(m[2]))z=D.1K(a,m[2])||\'\';G((O==""&&!!z||O=="="&&z==m[5]||O=="!="&&z!=m[5]||O=="^="&&z&&!z.1h(m[5])||O=="$="&&z.6v(z.K-m[5].K)==m[5]||(O=="*="||O=="~=")&&z.1h(m[5])>=0)^h)g.1p(a)}r=g}N G(m[1]==":"&&m[2]=="3a-4u"){J e={},g=[],11=/(-?)(\\d*)n((?:\\+|-)?\\d*)/.2D(m[3]=="6D"&&"2n"||m[3]=="6C"&&"2n+1"||!/\\D/.11(m[3])&&"8q+"+m[3]||m[3]),3o=(11[1]+(11[2]||1))-0,d=11[3]-0;R(J i=0,3i=r.K;i<3i;i++){J j=r[i],1d=j.1d,2v=D.L(1d);G(!e[2v]){J c=1;R(J n=1d.1x;n;n=n.2H)G(n.16==1)n.4q=c++;e[2v]=M}J b=Q;G(3o==0){G(j.4q==d)b=M}N G((j.4q-d)%3o==0&&(j.4q-d)/3o>=0)b=M;G(b^h)g.1p(j)}r=g}N{J f=D.6H[m[1]];G(1j f=="49")f=f[m[2]];G(1j f=="23")f=6u("Q||H(a,i){I "+f+";}");r=D.3C(r,H(a,i){I f(a,i,m,r)},h)}}I{r:r,t:t}},4S:H(b,c){J a=[],1t=b[c];1B(1t&&1t!=S){G(1t.16==1)a.1p(1t);1t=1t[c]}I a},3a:H(a,e,c,b){e=e||1;J d=0;R(;a;a=a[c])G(a.16==1&&++d==e)1X;I a},5v:H(n,a){J r=[];R(;n;n=n.2H){G(n.16==1&&n!=a)r.1p(n)}I r}});D.W={1e:H(f,i,g,e){G(f.16==3||f.16==8)I;G(D.14.1f&&f.4I)f=1b;G(!g.24)g.24=7.24++;G(e!=12){J h=g;g=7.3M(h,H(){I h.1w(7,19)});g.L=e}J j=D.L(f,"3w")||D.L(f,"3w",{}),1H=D.L(f,"1H")||D.L(f,"1H",H(){G(1j D!="12"&&!D.W.5k)I D.W.1H.1w(19.3L.T,19)});1H.T=f;D.P(i.1R(/\\s+/),H(c,b){J a=b.1R(".");b=a[0];g.O=a[1];J d=j[b];G(!d){d=j[b]={};G(!D.W.2t[b]||D.W.2t[b].4p.1k(f)===Q){G(f.3K)f.3K(b,1H,Q);N G(f.6t)f.6t("4o"+b,1H)}}d[g.24]=g;D.W.26[b]=M});f=U},24:1,26:{},21:H(e,h,f){G(e.16==3||e.16==8)I;J i=D.L(e,"3w"),1L,5i;G(i){G(h==12||(1j h=="23"&&h.8p(0)=="."))R(J g 1n i)7.21(e,g+(h||""));N{G(h.O){f=h.2y;h=h.O}D.P(h.1R(/\\s+/),H(b,a){J c=a.1R(".");a=c[0];G(i[a]){G(f)2U i[a][f.24];N R(f 1n i[a])G(!c[1]||i[a][f].O==c[1])2U i[a][f];R(1L 1n i[a])1X;G(!1L){G(!D.W.2t[a]||D.W.2t[a].4A.1k(e)===Q){G(e.6p)e.6p(a,D.L(e,"1H"),Q);N G(e.6n)e.6n("4o"+a,D.L(e,"1H"))}1L=U;2U i[a]}}})}R(1L 1n i)1X;G(!1L){J d=D.L(e,"1H");G(d)d.T=U;D.3b(e,"3w");D.3b(e,"1H")}}},1P:H(h,c,f,g,i){c=D.2d(c);G(h.1h("!")>=0){h=h.3s(0,-1);J a=M}G(!f){G(7.26[h])D("*").1e([1b,S]).1P(h,c)}N{G(f.16==3||f.16==8)I 12;J b,1L,17=D.1D(f[h]||U),W=!c[0]||!c[0].32;G(W){c.6h({O:h,2J:f,32:H(){},3J:H(){},4C:1z()});c[0][E]=M}c[0].O=h;G(a)c[0].6m=M;J d=D.L(f,"1H");G(d)b=d.1w(f,c);G((!17||(D.Y(f,\'a\')&&h=="4V"))&&f["4o"+h]&&f["4o"+h].1w(f,c)===Q)b=Q;G(W)c.4s();G(i&&D.1D(i)){1L=i.1w(f,b==U?c:c.7d(b));G(1L!==12)b=1L}G(17&&g!==Q&&b!==Q&&!(D.Y(f,\'a\')&&h=="4V")){7.5k=M;1U{f[h]()}1V(e){}}7.5k=Q}I b},1H:H(b){J a,1L,38,5f,4m;b=19[0]=D.W.6l(b||1b.W);38=b.O.1R(".");b.O=38[0];38=38[1];5f=!38&&!b.6m;4m=(D.L(7,"3w")||{})[b.O];R(J j 1n 4m){J c=4m[j];G(5f||c.O==38){b.2y=c;b.L=c.L;1L=c.1w(7,19);G(a!==Q)a=1L;G(1L===Q){b.32();b.3J()}}}I a},6l:H(b){G(b[E]==M)I b;J d=b;b={8o:d};J c="8n 8m 8l 8k 2s 8j 47 5d 6j 5E 8i L 8h 8g 4K 2y 5a 59 8e 8b 58 6f 8a 88 4k 87 86 84 6d 2J 4C 6c O 82 81 35".1R(" ");R(J i=c.K;i;i--)b[c[i]]=d[c[i]];b[E]=M;b.32=H(){G(d.32)d.32();d.80=Q};b.3J=H(){G(d.3J)d.3J();d.7Z=M};b.4C=b.4C||1z();G(!b.2J)b.2J=b.6d||S;G(b.2J.16==3)b.2J=b.2J.1d;G(!b.4k&&b.4K)b.4k=b.4K==b.2J?b.6c:b.4K;G(b.58==U&&b.5d!=U){J a=S.1C,1c=S.1c;b.58=b.5d+(a&&a.2e||1c&&1c.2e||0)-(a.6b||0);b.6f=b.6j+(a&&a.2c||1c&&1c.2c||0)-(a.6a||0)}G(!b.35&&((b.47||b.47===0)?b.47:b.5a))b.35=b.47||b.5a;G(!b.59&&b.5E)b.59=b.5E;G(!b.35&&b.2s)b.35=(b.2s&1?1:(b.2s&2?3:(b.2s&4?2:0)));I b},3M:H(a,b){b.24=a.24=a.24||b.24||7.24++;I b},2t:{27:{4p:H(){55();I},4A:H(){I}},3D:{4p:H(){G(D.14.1f)I Q;D(7).2O("53",D.W.2t.3D.2y);I M},4A:H(){G(D.14.1f)I Q;D(7).4e("53",D.W.2t.3D.2y);I M},2y:H(a){G(F(a,7))I M;a.O="3D";I D.W.1H.1w(7,19)}},3N:{4p:H(){G(D.14.1f)I Q;D(7).2O("51",D.W.2t.3N.2y);I M},4A:H(){G(D.14.1f)I Q;D(7).4e("51",D.W.2t.3N.2y);I M},2y:H(a){G(F(a,7))I M;a.O="3N";I D.W.1H.1w(7,19)}}}};D.17.1l({2O:H(c,a,b){I c=="4X"?7.2V(c,a,b):7.P(H(){D.W.1e(7,c,b||a,b&&a)})},2V:H(d,b,c){J e=D.W.3M(c||b,H(a){D(7).4e(a,e);I(c||b).1w(7,19)});I 7.P(H(){D.W.1e(7,d,e,c&&b)})},4e:H(a,b){I 7.P(H(){D.W.21(7,a,b)})},1P:H(c,a,b){I 7.P(H(){D.W.1P(c,a,7,M,b)})},5C:H(c,a,b){I 7[0]&&D.W.1P(c,a,7[0],Q,b)},2m:H(b){J c=19,i=1;1B(i<c.K)D.W.3M(b,c[i++]);I 7.4V(D.W.3M(b,H(a){7.4Z=(7.4Z||0)%i;a.32();I c[7.4Z++].1w(7,19)||Q}))},7X:H(a,b){I 7.2O(\'3D\',a).2O(\'3N\',b)},27:H(a){55();G(D.2Q)a.1k(S,D);N D.3A.1p(H(){I a.1k(7,D)});I 7}});D.1l({2Q:Q,3A:[],27:H(){G(!D.2Q){D.2Q=M;G(D.3A){D.P(D.3A,H(){7.1k(S)});D.3A=U}D(S).5C("27")}}});J x=Q;H 55(){G(x)I;x=M;G(S.3K&&!D.14.2G)S.3K("69",D.27,Q);G(D.14.1f&&1b==1S)(H(){G(D.2Q)I;1U{S.1C.7V("1A")}1V(3e){3B(19.3L,0);I}D.27()})();G(D.14.2G)S.3K("69",H(){G(D.2Q)I;R(J i=0;i<S.4W.K;i++)G(S.4W[i].3R){3B(19.3L,0);I}D.27()},Q);G(D.14.2k){J a;(H(){G(D.2Q)I;G(S.3f!="68"&&S.3f!="1J"){3B(19.3L,0);I}G(a===12)a=D("V, 7A[7U=7S]").K;G(S.4W.K!=a){3B(19.3L,0);I}D.27()})()}D.W.1e(1b,"43",D.27)}D.P(("7R,7Q,43,85,4y,4X,4V,7P,"+"7O,7N,89,53,51,7M,2A,"+"5o,7L,7K,8d,3e").1R(","),H(i,b){D.17[b]=H(a){I a?7.2O(b,a):7.1P(b)}});J F=H(a,c){J b=a.4k;1B(b&&b!=c)1U{b=b.1d}1V(3e){b=c}I b==c};D(1b).2O("4X",H(){D("*").1e(S).4e()});D.17.1l({67:D.17.43,43:H(g,d,c){G(1j g!=\'23\')I 7.67(g);J e=g.1h(" ");G(e>=0){J i=g.3s(e,g.K);g=g.3s(0,e)}c=c||H(){};J f="2P";G(d)G(D.1D(d)){c=d;d=U}N{d=D.3n(d);f="6g"}J h=7;D.3Y({1a:g,O:f,1O:"2K",L:d,1J:H(a,b){G(b=="1W"||b=="7J")h.2K(i?D("<1v/>").3v(a.4U.1o(/<1m(.|\\s)*?\\/1m>/g,"")).2q(i):a.4U);h.P(c,[a.4U,b,a])}});I 7},aL:H(){I D.3n(7.7I())},7I:H(){I 7.2l(H(){I D.Y(7,"3V")?D.2d(7.aH):7}).1E(H(){I 7.34&&!7.3R&&(7.4J||/2A|6y/i.11(7.Y)||/1r|1G|3Q/i.11(7.O))}).2l(H(i,c){J b=D(7).6e();I b==U?U:b.1q==2p?D.2l(b,H(a,i){I{34:c.34,2x:a}}):{34:c.34,2x:b}}).3p()}});D.P("7H,7G,7F,7D,7C,7B".1R(","),H(i,o){D.17[o]=H(f){I 7.2O(o,f)}});J B=1z();D.1l({3p:H(d,b,a,c){G(D.1D(b)){a=b;b=U}I D.3Y({O:"2P",1a:d,L:b,1W:a,1O:c})},aE:H(b,a){I D.3p(b,U,a,"1m")},aD:H(c,b,a){I D.3p(c,b,a,"3z")},aC:H(d,b,a,c){G(D.1D(b)){a=b;b={}}I D.3Y({O:"6g",1a:d,L:b,1W:a,1O:c})},aA:H(a){D.1l(D.60,a)},60:{1a:5Z.5Q,26:M,O:"2P",2T:0,7z:"4R/x-ax-3V-aw",7x:M,31:M,L:U,5Y:U,3Q:U,4Q:{2N:"4R/2N, 1r/2N",2K:"1r/2K",1m:"1r/4t, 4R/4t",3z:"4R/3z, 1r/4t",1r:"1r/as",4w:"*/*"}},4z:{},3Y:H(s){s=D.1l(M,s,D.1l(M,{},D.60,s));J g,2Z=/=\\?(&|$)/g,1u,L,O=s.O.2r();G(s.L&&s.7x&&1j s.L!="23")s.L=D.3n(s.L);G(s.1O=="4P"){G(O=="2P"){G(!s.1a.1I(2Z))s.1a+=(s.1a.1I(/\\?/)?"&":"?")+(s.4P||"7u")+"=?"}N G(!s.L||!s.L.1I(2Z))s.L=(s.L?s.L+"&":"")+(s.4P||"7u")+"=?";s.1O="3z"}G(s.1O=="3z"&&(s.L&&s.L.1I(2Z)||s.1a.1I(2Z))){g="4P"+B++;G(s.L)s.L=(s.L+"").1o(2Z,"="+g+"$1");s.1a=s.1a.1o(2Z,"="+g+"$1");s.1O="1m";1b[g]=H(a){L=a;1W();1J();1b[g]=12;1U{2U 1b[g]}1V(e){}G(i)i.37(h)}}G(s.1O=="1m"&&s.1Y==U)s.1Y=Q;G(s.1Y===Q&&O=="2P"){J j=1z();J k=s.1a.1o(/(\\?|&)3m=.*?(&|$)/,"$ap="+j+"$2");s.1a=k+((k==s.1a)?(s.1a.1I(/\\?/)?"&":"?")+"3m="+j:"")}G(s.L&&O=="2P"){s.1a+=(s.1a.1I(/\\?/)?"&":"?")+s.L;s.L=U}G(s.26&&!D.4O++)D.W.1P("7H");J n=/^(?:\\w+:)?\\/\\/([^\\/?#]+)/;G(s.1O=="1m"&&O=="2P"&&n.11(s.1a)&&n.2D(s.1a)[1]!=5Z.al){J i=S.3H("6w")[0];J h=S.3h("1m");h.4d=s.1a;G(s.7t)h.aj=s.7t;G(!g){J l=Q;h.ah=h.ag=H(){G(!l&&(!7.3f||7.3f=="68"||7.3f=="1J")){l=M;1W();1J();i.37(h)}}}i.3U(h);I 12}J m=Q;J c=1b.7s?2B 7s("ae.ac"):2B 7r();G(s.5Y)c.6R(O,s.1a,s.31,s.5Y,s.3Q);N c.6R(O,s.1a,s.31);1U{G(s.L)c.4B("ab-aa",s.7z);G(s.5S)c.4B("a9-5R-a8",D.4z[s.1a]||"a7, a6 a5 a4 5N:5N:5N a2");c.4B("X-9Z-9Y","7r");c.4B("9W",s.1O&&s.4Q[s.1O]?s.4Q[s.1O]+", */*":s.4Q.4w)}1V(e){}G(s.7m&&s.7m(c,s)===Q){s.26&&D.4O--;c.7l();I Q}G(s.26)D.W.1P("7B",[c,s]);J d=H(a){G(!m&&c&&(c.3f==4||a=="2T")){m=M;G(f){7k(f);f=U}1u=a=="2T"&&"2T"||!D.7j(c)&&"3e"||s.5S&&D.7h(c,s.1a)&&"7J"||"1W";G(1u=="1W"){1U{L=D.6X(c,s.1O,s.9S)}1V(e){1u="5J"}}G(1u=="1W"){J b;1U{b=c.5I("7g-5R")}1V(e){}G(s.5S&&b)D.4z[s.1a]=b;G(!g)1W()}N D.5H(s,c,1u);1J();G(s.31)c=U}};G(s.31){J f=4I(d,13);G(s.2T>0)3B(H(){G(c){c.7l();G(!m)d("2T")}},s.2T)}1U{c.9P(s.L)}1V(e){D.5H(s,c,U,e)}G(!s.31)d();H 1W(){G(s.1W)s.1W(L,1u);G(s.26)D.W.1P("7C",[c,s])}H 1J(){G(s.1J)s.1J(c,1u);G(s.26)D.W.1P("7F",[c,s]);G(s.26&&!--D.4O)D.W.1P("7G")}I c},5H:H(s,a,b,e){G(s.3e)s.3e(a,b,e);G(s.26)D.W.1P("7D",[a,s,e])},4O:0,7j:H(a){1U{I!a.1u&&5Z.9O=="5p:"||(a.1u>=7e&&a.1u<9N)||a.1u==7c||a.1u==9K||D.14.2k&&a.1u==12}1V(e){}I Q},7h:H(a,c){1U{J b=a.5I("7g-5R");I a.1u==7c||b==D.4z[c]||D.14.2k&&a.1u==12}1V(e){}I Q},6X:H(a,c,b){J d=a.5I("9J-O"),2N=c=="2N"||!c&&d&&d.1h("2N")>=0,L=2N?a.9I:a.4U;G(2N&&L.1C.2j=="5J")7p"5J";G(b)L=b(L,c);G(c=="1m")D.5u(L);G(c=="3z")L=6u("("+L+")");I L},3n:H(a){J s=[];G(a.1q==2p||a.5w)D.P(a,H(){s.1p(3u(7.34)+"="+3u(7.2x))});N R(J j 1n a)G(a[j]&&a[j].1q==2p)D.P(a[j],H(){s.1p(3u(j)+"="+3u(7))});N s.1p(3u(j)+"="+3u(D.1D(a[j])?a[j]():a[j]));I s.6s("&").1o(/%20/g,"+")}});D.17.1l({1N:H(c,b){I c?7.2g({1Z:"1N",2h:"1N",1y:"1N"},c,b):7.1E(":1G").P(H(){7.V.18=7.5D||"";G(D.1g(7,"18")=="2F"){J a=D("<"+7.2j+" />").6P("1c");7.V.18=a.1g("18");G(7.V.18=="2F")7.V.18="3I";a.21()}}).3l()},1M:H(b,a){I b?7.2g({1Z:"1M",2h:"1M",1y:"1M"},b,a):7.1E(":4j").P(H(){7.5D=7.5D||D.1g(7,"18");7.V.18="2F"}).3l()},78:D.17.2m,2m:H(a,b){I D.1D(a)&&D.1D(b)?7.78.1w(7,19):a?7.2g({1Z:"2m",2h:"2m",1y:"2m"},a,b):7.P(H(){D(7)[D(7).3F(":1G")?"1N":"1M"]()})},9G:H(b,a){I 7.2g({1Z:"1N"},b,a)},9F:H(b,a){I 7.2g({1Z:"1M"},b,a)},9E:H(b,a){I 7.2g({1Z:"2m"},b,a)},9D:H(b,a){I 7.2g({1y:"1N"},b,a)},9M:H(b,a){I 7.2g({1y:"1M"},b,a)},9C:H(c,a,b){I 7.2g({1y:a},c,b)},2g:H(k,j,i,g){J h=D.77(j,i,g);I 7[h.36===Q?"P":"36"](H(){G(7.16!=1)I Q;J f=D.1l({},h),p,1G=D(7).3F(":1G"),46=7;R(p 1n k){G(k[p]=="1M"&&1G||k[p]=="1N"&&!1G)I f.1J.1k(7);G(p=="1Z"||p=="2h"){f.18=D.1g(7,"18");f.33=7.V.33}}G(f.33!=U)7.V.33="1G";f.45=D.1l({},k);D.P(k,H(c,a){J e=2B D.28(46,f,c);G(/2m|1N|1M/.11(a))e[a=="2m"?1G?"1N":"1M":a](k);N{J b=a.6r().1I(/^([+-]=)?([\\d+-.]+)(.*)$/),2b=e.1t(M)||0;G(b){J d=3d(b[2]),2M=b[3]||"2X";G(2M!="2X"){46.V[c]=(d||1)+2M;2b=((d||1)/e.1t(M))*2b;46.V[c]=2b+2M}G(b[1])d=((b[1]=="-="?-1:1)*d)+2b;e.3G(2b,d,2M)}N e.3G(2b,a,"")}});I M})},36:H(a,b){G(D.1D(a)||(a&&a.1q==2p)){b=a;a="28"}G(!a||(1j a=="23"&&!b))I A(7[0],a);I 7.P(H(){G(b.1q==2p)A(7,a,b);N{A(7,a).1p(b);G(A(7,a).K==1)b.1k(7)}})},9X:H(b,c){J a=D.3O;G(b)7.36([]);7.P(H(){R(J i=a.K-1;i>=0;i--)G(a[i].T==7){G(c)a[i](M);a.7n(i,1)}});G(!c)7.5A();I 7}});J A=H(b,c,a){G(b){c=c||"28";J q=D.L(b,c+"36");G(!q||a)q=D.L(b,c+"36",D.2d(a))}I q};D.17.5A=H(a){a=a||"28";I 7.P(H(){J q=A(7,a);q.4s();G(q.K)q[0].1k(7)})};D.1l({77:H(b,a,c){J d=b&&b.1q==a0?b:{1J:c||!c&&a||D.1D(b)&&b,2u:b,41:c&&a||a&&a.1q!=9t&&a};d.2u=(d.2u&&d.2u.1q==4L?d.2u:D.28.5K[d.2u])||D.28.5K.74;d.5M=d.1J;d.1J=H(){G(d.36!==Q)D(7).5A();G(D.1D(d.5M))d.5M.1k(7)};I d},41:{73:H(p,n,b,a){I b+a*p},5P:H(p,n,b,a){I((-29.9r(p*29.9q)/2)+0.5)*a+b}},3O:[],48:U,28:H(b,c,a){7.15=c;7.T=b;7.1i=a;G(!c.3Z)c.3Z={}}});D.28.44={4D:H(){G(7.15.2Y)7.15.2Y.1k(7.T,7.1z,7);(D.28.2Y[7.1i]||D.28.2Y.4w)(7);G(7.1i=="1Z"||7.1i=="2h")7.T.V.18="3I"},1t:H(a){G(7.T[7.1i]!=U&&7.T.V[7.1i]==U)I 7.T[7.1i];J r=3d(D.1g(7.T,7.1i,a));I r&&r>-9p?r:3d(D.2a(7.T,7.1i))||0},3G:H(c,b,d){7.5V=1z();7.2b=c;7.3l=b;7.2M=d||7.2M||"2X";7.1z=7.2b;7.2S=7.4N=0;7.4D();J e=7;H t(a){I e.2Y(a)}t.T=7.T;D.3O.1p(t);G(D.48==U){D.48=4I(H(){J a=D.3O;R(J i=0;i<a.K;i++)G(!a[i]())a.7n(i--,1);G(!a.K){7k(D.48);D.48=U}},13)}},1N:H(){7.15.3Z[7.1i]=D.1K(7.T.V,7.1i);7.15.1N=M;7.3G(0,7.1t());G(7.1i=="2h"||7.1i=="1Z")7.T.V[7.1i]="9m";D(7.T).1N()},1M:H(){7.15.3Z[7.1i]=D.1K(7.T.V,7.1i);7.15.1M=M;7.3G(7.1t(),0)},2Y:H(a){J t=1z();G(a||t>7.15.2u+7.5V){7.1z=7.3l;7.2S=7.4N=1;7.4D();7.15.45[7.1i]=M;J b=M;R(J i 1n 7.15.45)G(7.15.45[i]!==M)b=Q;G(b){G(7.15.18!=U){7.T.V.33=7.15.33;7.T.V.18=7.15.18;G(D.1g(7.T,"18")=="2F")7.T.V.18="3I"}G(7.15.1M)7.T.V.18="2F";G(7.15.1M||7.15.1N)R(J p 1n 7.15.45)D.1K(7.T.V,p,7.15.3Z[p])}G(b)7.15.1J.1k(7.T);I Q}N{J n=t-7.5V;7.4N=n/7.15.2u;7.2S=D.41[7.15.41||(D.41.5P?"5P":"73")](7.4N,n,0,1,7.15.2u);7.1z=7.2b+((7.3l-7.2b)*7.2S);7.4D()}I M}};D.1l(D.28,{5K:{9l:9j,9i:7e,74:9g},2Y:{2e:H(a){a.T.2e=a.1z},2c:H(a){a.T.2c=a.1z},1y:H(a){D.1K(a.T.V,"1y",a.1z)},4w:H(a){a.T.V[a.1i]=a.1z+a.2M}}});D.17.2i=H(){J b=0,1S=0,T=7[0],3q;G(T)ao(D.14){J d=T.1d,4a=T,1s=T.1s,1Q=T.2z,5U=2k&&3r(5B)<9c&&!/9a/i.11(v),1g=D.2a,3c=1g(T,"30")=="3c";G(T.7y){J c=T.7y();1e(c.1A+29.2f(1Q.1C.2e,1Q.1c.2e),c.1S+29.2f(1Q.1C.2c,1Q.1c.2c));1e(-1Q.1C.6b,-1Q.1C.6a)}N{1e(T.5X,T.5W);1B(1s){1e(1s.5X,1s.5W);G(42&&!/^t(98|d|h)$/i.11(1s.2j)||2k&&!5U)2C(1s);G(!3c&&1g(1s,"30")=="3c")3c=M;4a=/^1c$/i.11(1s.2j)?4a:1s;1s=1s.1s}1B(d&&d.2j&&!/^1c|2K$/i.11(d.2j)){G(!/^96|1T.*$/i.11(1g(d,"18")))1e(-d.2e,-d.2c);G(42&&1g(d,"33")!="4j")2C(d);d=d.1d}G((5U&&(3c||1g(4a,"30")=="5x"))||(42&&1g(4a,"30")!="5x"))1e(-1Q.1c.5X,-1Q.1c.5W);G(3c)1e(29.2f(1Q.1C.2e,1Q.1c.2e),29.2f(1Q.1C.2c,1Q.1c.2c))}3q={1S:1S,1A:b}}H 2C(a){1e(D.2a(a,"6V",M),D.2a(a,"6U",M))}H 1e(l,t){b+=3r(l,10)||0;1S+=3r(t,10)||0}I 3q};D.17.1l({30:H(){J a=0,1S=0,3q;G(7[0]){J b=7.1s(),2i=7.2i(),4c=/^1c|2K$/i.11(b[0].2j)?{1S:0,1A:0}:b.2i();2i.1S-=25(7,\'94\');2i.1A-=25(7,\'aF\');4c.1S+=25(b,\'6U\');4c.1A+=25(b,\'6V\');3q={1S:2i.1S-4c.1S,1A:2i.1A-4c.1A}}I 3q},1s:H(){J a=7[0].1s;1B(a&&(!/^1c|2K$/i.11(a.2j)&&D.1g(a,\'30\')==\'93\'))a=a.1s;I D(a)}});D.P([\'5e\',\'5G\'],H(i,b){J c=\'4y\'+b;D.17[c]=H(a){G(!7[0])I;I a!=12?7.P(H(){7==1b||7==S?1b.92(!i?a:D(1b).2e(),i?a:D(1b).2c()):7[c]=a}):7[0]==1b||7[0]==S?46[i?\'aI\':\'aJ\']||D.71&&S.1C[c]||S.1c[c]:7[0][c]}});D.P(["6N","4b"],H(i,b){J c=i?"5e":"5G",4f=i?"6k":"6i";D.17["5s"+b]=H(){I 7[b.3y()]()+25(7,"57"+c)+25(7,"57"+4f)};D.17["90"+b]=H(a){I 7["5s"+b]()+25(7,"2C"+c+"4b")+25(7,"2C"+4f+"4b")+(a?25(7,"6S"+c)+25(7,"6S"+4f):0)}})})();',62,669,'|||||||this|||||||||||||||||||||||||||||||||||if|function|return|var|length|data|true|else|type|each|false|for|document|elem|null|style|event||nodeName|||test|undefined||browser|options|nodeType|fn|display|arguments|url|window|body|parentNode|add|msie|css|indexOf|prop|typeof|call|extend|script|in|replace|push|constructor|text|offsetParent|cur|status|div|apply|firstChild|opacity|now|left|while|documentElement|isFunction|filter|className|hidden|handle|match|complete|attr|ret|hide|show|dataType|trigger|doc|split|top|table|try|catch|success|break|cache|height||remove|tbody|string|guid|num|global|ready|fx|Math|curCSS|start|scrollTop|makeArray|scrollLeft|max|animate|width|offset|tagName|safari|map|toggle||done|Array|find|toUpperCase|button|special|duration|id|copy|value|handler|ownerDocument|select|new|border|exec|stack|none|opera|nextSibling|pushStack|target|html|inArray|unit|xml|bind|GET|isReady|merge|pos|timeout|delete|one|selected|px|step|jsre|position|async|preventDefault|overflow|name|which|queue|removeChild|namespace|insertBefore|nth|removeData|fixed|parseFloat|error|readyState|multiFilter|createElement|rl|re|trim|end|_|param|first|get|results|parseInt|slice|childNodes|encodeURIComponent|append|events|elems|toLowerCase|json|readyList|setTimeout|grep|mouseenter|color|is|custom|getElementsByTagName|block|stopPropagation|addEventListener|callee|proxy|mouseleave|timers|defaultView|password|disabled|last|has|appendChild|form|domManip|props|ajax|orig|set|easing|mozilla|load|prototype|curAnim|self|charCode|timerId|object|offsetChild|Width|parentOffset|src|unbind|br|currentStyle|clean|float|visible|relatedTarget|previousSibling|handlers|isXMLDoc|on|setup|nodeIndex|unique|shift|javascript|child|RegExp|_default|deep|scroll|lastModified|teardown|setRequestHeader|timeStamp|update|empty|tr|getAttribute|innerHTML|setInterval|checked|fromElement|Number|jQuery|state|active|jsonp|accepts|application|dir|input|responseText|click|styleSheets|unload|not|lastToggle|outline|mouseout|getPropertyValue|mouseover|getComputedStyle|bindReady|String|padding|pageX|metaKey|keyCode|getWH|andSelf|clientX|Left|all|visibility|container|index|init|triggered|removeAttribute|classFilter|prevObject|submit|file|after|windowData|inner|client|globalEval|sibling|jquery|absolute|clone|wrapAll|dequeue|version|triggerHandler|oldblock|ctrlKey|createTextNode|Top|handleError|getResponseHeader|parsererror|speeds|checkbox|old|00|radio|swing|href|Modified|ifModified|lastChild|safari2|startTime|offsetTop|offsetLeft|username|location|ajaxSettings|getElementById|isSimple|values|selectedIndex|runtimeStyle|rsLeft|_load|loaded|DOMContentLoaded|clientTop|clientLeft|toElement|srcElement|val|pageY|POST|unshift|Bottom|clientY|Right|fix|exclusive|detachEvent|cloneNode|removeEventListener|swap|toString|join|attachEvent|eval|substr|head|parse|textarea|reset|image|zoom|odd|even|before|prepend|exclude|expr|quickClass|quickID|uuid|quickChild|continue|Height|textContent|appendTo|contents|open|margin|evalScript|borderTopWidth|borderLeftWidth|parent|httpData|setArray|CSS1Compat|compatMode|boxModel|cssFloat|linear|def|webkit|nodeValue|speed|_toggle|eq|100|replaceWith|304|concat|200|alpha|Last|httpNotModified|getAttributeNode|httpSuccess|clearInterval|abort|beforeSend|splice|styleFloat|throw|colgroup|XMLHttpRequest|ActiveXObject|scriptCharset|callback|fieldset|multiple|processData|getBoundingClientRect|contentType|link|ajaxSend|ajaxSuccess|ajaxError|col|ajaxComplete|ajaxStop|ajaxStart|serializeArray|notmodified|keypress|keydown|change|mouseup|mousedown|dblclick|focus|blur|stylesheet|hasClass|rel|doScroll|black|hover|solid|cancelBubble|returnValue|wheelDelta|view|round|shiftKey|resize|screenY|screenX|relatedNode|mousemove|prevValue|originalTarget|offsetHeight|keyup|newValue|offsetWidth|eventPhase|detail|currentTarget|cancelable|bubbles|attrName|attrChange|altKey|originalEvent|charAt|0n|substring|animated|header|noConflict|line|enabled|innerText|contains|only|weight|font|gt|lt|uFFFF|u0128|size|417|Boolean|Date|toggleClass|removeClass|addClass|removeAttr|replaceAll|insertAfter|prependTo|wrap|contentWindow|contentDocument|iframe|children|siblings|prevAll|wrapInner|nextAll|outer|prev|scrollTo|static|marginTop|next|inline|parents|able|cellSpacing|adobeair|cellspacing|522|maxLength|maxlength|readOnly|400|readonly|fast|600|class|slow|1px|htmlFor|reverse|10000|PI|cos|compatible|Function|setData|ie|ra|it|rv|getData|userAgent|navigator|fadeTo|fadeIn|slideToggle|slideUp|slideDown|ig|responseXML|content|1223|NaN|fadeOut|300|protocol|send|setAttribute|option|dataFilter|cssText|changed|be|Accept|stop|With|Requested|Object|can|GMT|property|1970|Jan|01|Thu|Since|If|Type|Content|XMLHTTP|th|Microsoft|td|onreadystatechange|onload|cap|charset|colg|host|tfoot|specified|with|1_|thead|leg|plain|attributes|opt|embed|urlencoded|www|area|hr|ajaxSetup|meta|post|getJSON|getScript|marginLeft|img|elements|pageYOffset|pageXOffset|abbr|serialize|pixelLeft'.split('|'),0,{}));
// $Id: drupal.js,v 1.41.2.4 2009/07/21 08:59:10 goba Exp $

var Drupal = Drupal || { 'settings': {}, 'behaviors': {}, 'themes': {}, 'locale': {} };

/**
 * Set the variable that indicates if JavaScript behaviors should be applied
 */
Drupal.jsEnabled = document.getElementsByTagName && document.createElement && document.createTextNode && document.documentElement && document.getElementById;

/**
 * Attach all registered behaviors to a page element.
 *
 * Behaviors are event-triggered actions that attach to page elements, enhancing
 * default non-Javascript UIs. Behaviors are registered in the Drupal.behaviors
 * object as follows:
 * @code
 *    Drupal.behaviors.behaviorName = function () {
 *      ...
 *    };
 * @endcode
 *
 * Drupal.attachBehaviors is added below to the jQuery ready event and so
 * runs on initial page load. Developers implementing AHAH/AJAX in their
 * solutions should also call this function after new page content has been
 * loaded, feeding in an element to be processed, in order to attach all
 * behaviors to the new content.
 *
 * Behaviors should use a class in the form behaviorName-processed to ensure
 * the behavior is attached only once to a given element. (Doing so enables
 * the reprocessing of given elements, which may be needed on occasion despite
 * the ability to limit behavior attachment to a particular element.)
 *
 * @param context
 *   An element to attach behaviors to. If none is given, the document element
 *   is used.
 */
Drupal.attachBehaviors = function(context) {
  context = context || document;
  if (Drupal.jsEnabled) {
    // Execute all of them.
    jQuery.each(Drupal.behaviors, function() {
      this(context);
    });
  }
};

/**
 * Encode special characters in a plain-text string for display as HTML.
 */
Drupal.checkPlain = function(str) {
  str = String(str);
  var replace = { '&': '&amp;', '"': '&quot;', '<': '&lt;', '>': '&gt;' };
  for (var character in replace) {
    var regex = new RegExp(character, 'g');
    str = str.replace(regex, replace[character]);
  }
  return str;
};

/**
 * Translate strings to the page language or a given language.
 *
 * See the documentation of the server-side t() function for further details.
 *
 * @param str
 *   A string containing the English string to translate.
 * @param args
 *   An object of replacements pairs to make after translation. Incidences
 *   of any key in this array are replaced with the corresponding value.
 *   Based on the first character of the key, the value is escaped and/or themed:
 *    - !variable: inserted as is
 *    - @variable: escape plain text to HTML (Drupal.checkPlain)
 *    - %variable: escape text and theme as a placeholder for user-submitted
 *      content (checkPlain + Drupal.theme('placeholder'))
 * @return
 *   The translated string.
 */
Drupal.t = function(str, args) {
  // Fetch the localized version of the string.
  if (Drupal.locale.strings && Drupal.locale.strings[str]) {
    str = Drupal.locale.strings[str];
  }

  if (args) {
    // Transform arguments before inserting them
    for (var key in args) {
      switch (key.charAt(0)) {
        // Escaped only
        case '@':
          args[key] = Drupal.checkPlain(args[key]);
        break;
        // Pass-through
        case '!':
          break;
        // Escaped and placeholder
        case '%':
        default:
          args[key] = Drupal.theme('placeholder', args[key]);
          break;
      }
      str = str.replace(key, args[key]);
    }
  }
  return str;
};

/**
 * Format a string containing a count of items.
 *
 * This function ensures that the string is pluralized correctly. Since Drupal.t() is
 * called by this function, make sure not to pass already-localized strings to it.
 *
 * See the documentation of the server-side format_plural() function for further details.
 *
 * @param count
 *   The item count to display.
 * @param singular
 *   The string for the singular case. Please make sure it is clear this is
 *   singular, to ease translation (e.g. use "1 new comment" instead of "1 new").
 *   Do not use @count in the singular string.
 * @param plural
 *   The string for the plural case. Please make sure it is clear this is plural,
 *   to ease translation. Use @count in place of the item count, as in "@count
 *   new comments".
 * @param args
 *   An object of replacements pairs to make after translation. Incidences
 *   of any key in this array are replaced with the corresponding value.
 *   Based on the first character of the key, the value is escaped and/or themed:
 *    - !variable: inserted as is
 *    - @variable: escape plain text to HTML (Drupal.checkPlain)
 *    - %variable: escape text and theme as a placeholder for user-submitted
 *      content (checkPlain + Drupal.theme('placeholder'))
 *   Note that you do not need to include @count in this array.
 *   This replacement is done automatically for the plural case.
 * @return
 *   A translated string.
 */
Drupal.formatPlural = function(count, singular, plural, args) {
  var args = args || {};
  args['@count'] = count;
  // Determine the index of the plural form.
  var index = Drupal.locale.pluralFormula ? Drupal.locale.pluralFormula(args['@count']) : ((args['@count'] == 1) ? 0 : 1);

  if (index == 0) {
    return Drupal.t(singular, args);
  }
  else if (index == 1) {
    return Drupal.t(plural, args);
  }
  else {
    args['@count['+ index +']'] = args['@count'];
    delete args['@count'];
    return Drupal.t(plural.replace('@count', '@count['+ index +']'));
  }
};

/**
 * Generate the themed representation of a Drupal object.
 *
 * All requests for themed output must go through this function. It examines
 * the request and routes it to the appropriate theme function. If the current
 * theme does not provide an override function, the generic theme function is
 * called.
 *
 * For example, to retrieve the HTML that is output by theme_placeholder(text),
 * call Drupal.theme('placeholder', text).
 *
 * @param func
 *   The name of the theme function to call.
 * @param ...
 *   Additional arguments to pass along to the theme function.
 * @return
 *   Any data the theme function returns. This could be a plain HTML string,
 *   but also a complex object.
 */
Drupal.theme = function(func) {
  for (var i = 1, args = []; i < arguments.length; i++) {
    args.push(arguments[i]);
  }

  return (Drupal.theme[func] || Drupal.theme.prototype[func]).apply(this, args);
};

/**
 * Parse a JSON response.
 *
 * The result is either the JSON object, or an object with 'status' 0 and 'data' an error message.
 */
Drupal.parseJson = function (data) {
  if ((data.substring(0, 1) != '{') && (data.substring(0, 1) != '[')) {
    return { status: 0, data: data.length ? data : Drupal.t('Unspecified error') };
  }
  return eval('(' + data + ');');
};

/**
 * Freeze the current body height (as minimum height). Used to prevent
 * unnecessary upwards scrolling when doing DOM manipulations.
 */
Drupal.freezeHeight = function () {
  Drupal.unfreezeHeight();
  var div = document.createElement('div');
  $(div).css({
    position: 'absolute',
    top: '0px',
    left: '0px',
    width: '1px',
    height: $('body').css('height')
  }).attr('id', 'freeze-height');
  $('body').append(div);
};

/**
 * Unfreeze the body height
 */
Drupal.unfreezeHeight = function () {
  $('#freeze-height').remove();
};

/**
 * Wrapper around encodeURIComponent() which avoids Apache quirks (equivalent of
 * drupal_urlencode() in PHP). This function should only be used on paths, not
 * on query string arguments.
 */
Drupal.encodeURIComponent = function (item, uri) {
  uri = uri || location.href;
  item = encodeURIComponent(item).replace(/%2F/g, 'index.html');
  return (uri.indexOf('?q=') != -1) ? item : item.replace(/%26/g, '%2526').replace(/%23/g, '%2523').replace(/\/\//g, '/%252F');
};

/**
 * Get the text selection in a textarea.
 */
Drupal.getSelection = function (element) {
  if (typeof(element.selectionStart) != 'number' && document.selection) {
    // The current selection
    var range1 = document.selection.createRange();
    var range2 = range1.duplicate();
    // Select all text.
    range2.moveToElementText(element);
    // Now move 'dummy' end point to end point of original range.
    range2.setEndPoint('EndToEnd', range1);
    // Now we can calculate start and end points.
    var start = range2.text.length - range1.text.length;
    var end = start + range1.text.length;
    return { 'start': start, 'end': end };
  }
  return { 'start': element.selectionStart, 'end': element.selectionEnd };
};

/**
 * Build an error message from ahah response.
 */
Drupal.ahahError = function(xmlhttp, uri) {
  if (xmlhttp.status == 200) {
    if (jQuery.trim($(xmlhttp.responseText).text())) {
      var message = Drupal.t("An error occurred. \n@uri\n@text", {'@uri': uri, '@text': xmlhttp.responseText });
    }
    else {
      var message = Drupal.t("An error occurred. \n@uri\n(no information available).", {'@uri': uri, '@text': xmlhttp.responseText });
    }
  }
  else {
    var message = Drupal.t("An HTTP error @status occurred. \n@uri", {'@uri': uri, '@status': xmlhttp.status });
  }
  return message;
}

// Global Killswitch on the <html> element
if (Drupal.jsEnabled) {
  // Global Killswitch on the <html> element
  $(document.documentElement).addClass('js');
  // 'js enabled' cookie
  document.cookie = 'has_js%3d1%3b%20path%3d/index.html';
  // Attach all behaviors.
  $(document).ready(function() {
    Drupal.attachBehaviors(this);
  });
}

/**
 * The default themes.
 */
Drupal.theme.prototype = {

  /**
   * Formats text for emphasized display in a placeholder inside a sentence.
   *
   * @param str
   *   The text to format (plain-text).
   * @return
   *   The formatted text (html).
   */
  placeholder: function(str) {
    return '<em>' + Drupal.checkPlain(str) + '</em>';
  }
};
;


function submit_url(){
    var count = 0;
    $('.calendar-calendar .styled .form-checkbox').each(function(){
            count++;
    });
} 

$(document).ready(function(){
     $(".parent :checkbox").click(function(){

     parentid= $(this).attr('id');   

        if($('.parent :checkbox').is(':checked')){
            $('.'+parentid+':checkbox').attr('checked', true)
        }
        if(!$('.parent :checkbox').is(':checked')){
            $('.'+parentid+':checkbox').attr('checked', false)
        }
     });

     $(".clildterm :checkbox").click(function(){
     var ccount=0;
     var pcount=0;

        childclass= $(this).attr('class').split(" ");

        $('.'+childclass[1]+':checkbox').each(function(){
            ccount++;
    });
      $('.'+childclass[1]+':checkbox:checked').each(function(){
            pcount++;
    });
   if(ccount==pcount){
       $('#'+childclass[1]+':checkbox').attr('checked', true);
   }else{
        $('#'+childclass[1]+':checkbox').attr('checked', false);
   }
       

     });
  
});

;
function sitehelper_load_block(year){
	ajax_url = '/sitehelper-date-block';
	$.ajax({
		    type: "GET",
			url: ajax_url,
			data: 'year='+year,
			async: false,
			beforeSend: function () {
			
		    },
			success: function(data){
			lhref = $("block", data).text();
			//alert(lhref);
			$('#show_year_block').html(lhref);
		    },
			error: function(msg, xx, yy){
			alert("connection error. " + msg);
                        alert (xx);
                        alert(yy);
		    }
		});
}

function deactivate_all()
{
	$('.year_tab li').each(function(){
		$(this).removeClass("default_year");
	});
}

$(document).ready(function (){
	$('.year_tab a').click(function(){
		year = $(this).html();
		deactivate_all();
		$(this).parent().addClass("default_year");
		sitehelper_load_block(year);
		return false;
	});
	
	$("#loading-newsroom").ajaxStart(function(){
		// $(this).show();
		$(this).css("display", "block");
	});
	
	$("#loading-newsroom").ajaxStop(function(){
		// $(this).hide();
		$(this).css("display", "none");
	});
});

;
/* $Id: lightbox_video.js,v 1.1.4.20 2010/09/21 17:57:22 snpower Exp $ */

/**
 * Lightbox video
 * @author
 *   Stella Power, <http://drupal.org/user/66894>
 */

var Lightvideo = {

  // startVideo()
  startVideo: function (href) {
    if (Lightvideo.checkKnownVideos(href)) {
      return;
    }
    else if (href.match(/\.mov$/i)) {
      if (navigator.plugins && navigator.plugins.length) {
        Lightbox.modalHTML ='<object id="qtboxMovie" type="video/quicktime" codebase="http://www.apple.com/qtactivex/qtplugin.cab" data="'+href+'" width="'+Lightbox.modalWidth+'" height="'+Lightbox.modalHeight+'"><param name="allowFullScreen" value="true"></param><param name="src" value="'+href+'" /><param name="scale" value="aspect" /><param name="controller" value="true" /><param name="autoplay" value="true" /><param name="bgcolor" value="#000000" /><param name="enablejavascript" value="true" /></object>';
      } else {
        Lightbox.modalHTML = '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="'+Lightbox.modalWidth+'" height="'+Lightbox.modalHeight+'" id="qtboxMovie"><param name="allowFullScreen" value="true"></param><param name="src" value="'+href+'" /><param name="scale" value="aspect" /><param name="controller" value="true" /><param name="autoplay" value="true" /><param name="bgcolor" value="#000000" /><param name="enablejavascript" value="true" /></object>';
      }
    }
    else if (href.match(/\.wmv$/i) || href.match(/\.asx$/i)) {
      Lightbox.modalHTML = '<object NAME="Player" WIDTH="'+Lightbox.modalWidth+'" HEIGHT="'+Lightbox.modalHeight+'" align="left" hspace="0" type="application/x-oleobject" CLASSID="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6"><param name="allowFullScreen" value="true"></param><param NAME="URL" VALUE="'+href+'"></param><param NAME="AUTOSTART" VALUE="true"></param><param name="showControls" value="true"></param><embed WIDTH="'+Lightbox.modalWidth+'" HEIGHT="'+Lightbox.modalHeight+'" align="left" hspace="0" SRC="'+href+'" TYPE="application/x-oleobject" AUTOSTART="false"></embed></object>';
    }
    else {
      Lightbox.videoId = href;
      variables = '';
      if (!href.match(/\.swf$/i)) {
        href = Lightbox.flvPlayer + '?file=' + href;
        if (Lightbox.flvFlashvars.length) {
          variables = Lightbox.flvFlashvars;
        }
      }

      Lightvideo.createEmbed(href, "flvplayer", "#ffffff", variables);
    }
  },

  // createEmbed()
  createEmbed: function(href, id, color, variables) {
    var bgcolor = 'bgcolor="' + color + '"';
    var flashvars = '';
    if (variables) {
      flashvars = 'flashvars="' + variables + '"';

    }
    Lightbox.modalHTML = '<embed type="application/x-shockwave-flash" ' +
      'src="' + href + '" ' +
      'id="' + id + '" name="' + id + '" ' + bgcolor + ' ' +
      'quality="high" wmode="transparent" ' + flashvars + ' ' +
      'height="' + Lightbox.modalHeight + '" ' +
      'width="' + Lightbox.modalWidth + '" ' +
      'allowfullscreen="true" ' +
      '></embed>';
  },


  // checkKnownVideos()
  checkKnownVideos: function(href) {
    if (Lightvideo.checkYouTubeVideo(href) || Lightvideo.checkGoogleVideo(href) ||
      Lightvideo.checkMySpaceVideo(href) || Lightvideo.checkLiveVideo(href) ||
      Lightvideo.checkMetacafeVideo(href) ||
      Lightvideo.checkIFilmSpikeVideo(href)
      ) {
      return true;
    }
    return false;
  },


  // checkYouTubeVideo()
  checkYouTubeVideo: function(href) {
    var patterns = [
      'youtube.com/v/([^"&]+)',
      'youtube.com/watch\\?v=([^"&]+)',
      'youtube.com/\\?v=([^"&]+)'
      ];

    for (var i = 0; i < patterns.length; i++) {
      var pattern = new RegExp(patterns[i], "i");
      var results = pattern.exec(href);
      if (results !== null) {
        Lightbox.videoId = results[1];
        var href = "http://www.youtube.com/v/"+Lightbox.videoId;
        var variables = 'fs=1';
        if (Lightbox.flvFlashvars.length) {
          variables = variables + '&' + Lightbox.flvFlashvars;
          href = href + '&' + variables;
        }
        Lightvideo.createEmbed(href, "flvvideo", "#ffffff", variables);
        return true;
      }
    }
    return false;
  },

  // checkGoogleVideo()
  checkGoogleVideo: function(href) {
    var patterns = [
      'http://video.google.[a-z]{2,4}/googleplayer.swf\\?docId=(-?\\d*)',
      'http://video.google.[a-z]{2,4}/videoplay\\?docid=([^&]*)&',
      'http://video.google.[a-z]{2,4}/videoplay\\?docid=(.*)'
      ];

    for (var i = 0; i < patterns.length; i++) {
      var pattern = new RegExp(patterns[i], "i");
      var results = pattern.exec(href);
      if (results !== null) {
        Lightbox.videoId = results[1];
        var href = "../video.google.com/googleplayer2797.swf?docId="+Lightbox.videoId+"&hl=en";
        var variables = 'fs=true';
        if (Lightbox.flvFlashvars.length) {
          variables = variables + '&' + Lightbox.flvFlashvars;
          href = href + '&' + variables;
        }
        Lightvideo.createEmbed(href, "flvvideo", "#ffffff", variables);
        return true;
      }
    }
    return false;
  },

  // checkMetacafeVideo()
  checkMetacafeVideo: function(href) {
    var patterns = [
      'metacafe.com/watch/(\.[^/]*)/(\.[^/]*)/',
      'metacafe.com/watch/(\.[^/]*)/(\.*)',
      'metacafe.com/fplayer/(\.[^/]*)/(\.[^.]*).'
      ];

    for (var i = 0; i < patterns.length; i++) {
      var pattern = new RegExp(patterns[i], "i");
      var results = pattern.exec(href);
      if (results !== null) {
        Lightbox.videoId = results[1];
        Lightvideo.createEmbed("http://www.metacafe.com/fplayer/"+Lightbox.videoId+"/.swf", "flvvideo", "#ffffff");
        return true;
      }
    }
    return false;
  },

  // checkIFilmSpikeVideo()
  checkIFilmSpikeVideo: function(href) {
    var patterns = [
      'spike.com/video/[^/&"]*?/(\\d+)',
      'ifilm.com/video/[^/&"]*?/(\\d+)',
      'spike.com/video/([^/&"]*)',
      'ifilm.com/video/([^/&"]*)'
      ];

    for (var i = 0; i < patterns.length; i++) {
      var pattern = new RegExp(patterns[i], "i");
      var results = pattern.exec(href);
      if (results !== null) {
        Lightbox.videoId = results[1];
        Lightvideo.createEmbed("http://www.spike.com/efp", "flvvideo", "#000", "flvbaseclip="+Lightbox.videoId+"&amp;");
        return true;
      }
    }
    return false;
  },

  // checkMySpaceVideo()
  checkMySpaceVideo: function(href) {
    var patterns = [
      'src="myspace.com/index.cfm\\?fuseaction=vids.individual&videoid=([^&"]+)',
      'myspace.com/index.cfm\\?fuseaction=vids.individual&videoid=([^&"]+)',
      'src="myspacetv.com/index.cfm\\?fuseaction=vids.individual&videoid=([^&"]+)"',
      'myspacetv.com/index.cfm\\?fuseaction=vids.individual&videoid=([^&"]+)'
      ];

    for (var i = 0; i < patterns.length; i++) {
      var pattern = new RegExp(patterns[i], "i");
      var results = pattern.exec(href);
      if (results !== null) {
        Lightbox.videoId = results[1];
        Lightvideo.createEmbed("../lads.myspace.com/videos/vplayer.swf", "flvvideo", "#ffffff", "m="+Lightbox.videoId);
        return true;
      }
    }
    return false;
  },

  // checkLiveVideo()
  checkLiveVideo: function(href) {
    var patterns = [
      'livevideo.com/flvplayer/embed/([^"]*)"',
      'livevideo.com/video/[^/]*?/([^/]*)/',
      'livevideo.com/video/([^/]*)/'
      ];

    for (var i = 0; i < patterns.length; i++) {
      var pattern = new RegExp(patterns[i], "i");
      var results = pattern.exec(href);
      if (results !== null) {
        Lightbox.videoId = results[1];
        Lightvideo.createEmbed("http://www.livevideo.com/flvplayer/embed/"+Lightbox.videoId, "flvvideo", "#ffffff");
        return true;
      }
    }
    return false;
  }

};
;
/* $Id: lightbox.js,v 1.5.2.6.2.136 2010/09/24 08:39:40 snpower Exp $ */

/**
 * jQuery Lightbox
 * @author
 *   Stella Power, <http://drupal.org/user/66894>
 *
 * Based on Lightbox v2.03.3 by Lokesh Dhakar
 * <http://www.huddletogether.com/projects/lightbox2/>
 * Also partially based on the jQuery Lightbox by Warren Krewenki
 *   <http://warren.mesozen.com>
 *
 * Permission has been granted to Mark Ashmead & other Drupal Lightbox2 module
 * maintainers to distribute this file via Drupal.org
 * Under GPL license.
 *
 * Slideshow, iframe and video functionality added by Stella Power.
 */

var Lightbox = {
  auto_modal : false,
  overlayOpacity : 0.8, // Controls transparency of shadow overlay.
  overlayColor : '000', // Controls colour of shadow overlay.
  disableCloseClick : true,
  // Controls the order of the lightbox resizing animation sequence.
  resizeSequence: 0, // 0: simultaneous, 1: width then height, 2: height then width.
  resizeSpeed: 'normal', // Controls the speed of the lightbox resizing animation.
  fadeInSpeed: 'normal', // Controls the speed of the image appearance.
  slideDownSpeed: 'slow', // Controls the speed of the image details appearance.
  minWidth: 240,
  borderSize : 10,
  boxColor : 'fff',
  fontColor : '000',
  topPosition : '',
  infoHeight: 20,
  alternative_layout : false,
  imageArray : [],
  imageNum : null,
  total : 0,
  activeImage : null,
  inprogress : false,
  disableResize : false,
  disableZoom : false,
  isZoomedIn : false,
  rtl : false,
  loopItems : false,
  keysClose : ['c', 'x', 27],
  keysPrevious : ['p', 37],
  keysNext : ['n', 39],
  keysZoom : ['z'],
  keysPlayPause : [32],

  // Slideshow options.
  slideInterval : 5000, // In milliseconds.
  showPlayPause : true,
  autoStart : true,
  autoExit : true,
  pauseOnNextClick : false, // True to pause the slideshow when the "Next" button is clicked.
  pauseOnPrevClick : true, // True to pause the slideshow when the "Prev" button is clicked.
  slideIdArray : [],
  slideIdCount : 0,
  isSlideshow : false,
  isPaused : false,
  loopSlides : false,

  // Iframe options.
  isLightframe : false,
  iframe_width : 600,
  iframe_height : 400,
  iframe_border : 1,

  // Video and modal options.
  enableVideo : false,
  flvPlayer : '/flvplayer.swf',
  flvFlashvars : '',
  isModal : false,
  isVideo : false,
  videoId : false,
  modalWidth : 400,
  modalHeight : 400,
  modalHTML : null,


  // initialize()
  // Constructor runs on completion of the DOM loading.
  // The function inserts html at the bottom of the page which is used
  // to display the shadow overlay and the image container.
  initialize: function() {

    var s = Drupal.settings.lightbox2;
    Lightbox.overlayOpacity = s.overlay_opacity;
    Lightbox.overlayColor = s.overlay_color;
    Lightbox.disableCloseClick = s.disable_close_click;
    Lightbox.resizeSequence = s.resize_sequence;
    Lightbox.resizeSpeed = s.resize_speed;
    Lightbox.fadeInSpeed = s.fade_in_speed;
    Lightbox.slideDownSpeed = s.slide_down_speed;
    Lightbox.borderSize = s.border_size;
    Lightbox.boxColor = s.box_color;
    Lightbox.fontColor = s.font_color;
    Lightbox.topPosition = s.top_position;
    Lightbox.rtl = s.rtl;
    Lightbox.loopItems = s.loop_items;
    Lightbox.keysClose = s.keys_close.split(" ");
    Lightbox.keysPrevious = s.keys_previous.split(" ");
    Lightbox.keysNext = s.keys_next.split(" ");
    Lightbox.keysZoom = s.keys_zoom.split(" ");
    Lightbox.keysPlayPause = s.keys_play_pause.split(" ");
    Lightbox.disableResize = s.disable_resize;
    Lightbox.disableZoom = s.disable_zoom;
    Lightbox.slideInterval = s.slideshow_interval;
    Lightbox.showPlayPause = s.show_play_pause;
    Lightbox.showCaption = s.show_caption;
    Lightbox.autoStart = s.slideshow_automatic_start;
    Lightbox.autoExit = s.slideshow_automatic_exit;
    Lightbox.pauseOnNextClick = s.pause_on_next_click;
    Lightbox.pauseOnPrevClick = s.pause_on_previous_click;
    Lightbox.loopSlides = s.loop_slides;
    Lightbox.alternative_layout = s.use_alt_layout;
    Lightbox.iframe_width = s.iframe_width;
    Lightbox.iframe_height = s.iframe_height;
    Lightbox.iframe_border = s.iframe_border;
    Lightbox.enableVideo = s.enable_video;
    if (s.enable_video) {
      Lightbox.flvPlayer = s.flvPlayer;
      Lightbox.flvFlashvars = s.flvFlashvars;
    }

    // Make the lightbox divs.
    var layout_class = (s.use_alt_layout ? 'lightbox2-alt-layout' : 'lightbox2-orig-layout');
    var output = '<div id="lightbox2-overlay" style="display: none;"></div>\
      <div id="lightbox" style="display: none;" class="' + layout_class + '">\
        <div id="outerImageContainer"></div>\
        <div id="imageDataContainer" class="clearfix">\
          <div id="imageData"></div>\
        </div>\
      </div>';
    var loading = '<div id="loading"><a href="#" id="loadingLink"></a></div>';
    var modal = '<div id="modalContainer" style="display: none;"></div>';
    var frame = '<div id="frameContainer" style="display: none;"></div>';
    var imageContainer = '<div id="imageContainer" style="display: none;"></div>';
    var details = '<div id="imageDetails"></div>';
    var bottomNav = '<div id="bottomNav"></div>';
    var image = '<img id="lightboxImage" alt="" />';
    var hoverNav = '<div id="hoverNav"><a id="prevLink" href="#"></a><a id="nextLink" href="#"></a></div>';
    var frameNav = '<div id="frameHoverNav"><a id="framePrevLink" href="#"></a><a id="frameNextLink" href="#"></a></div>';
    var hoverNav = '<div id="hoverNav"><a id="prevLink" title="' + Drupal.t('Previous') + '" href="#"></a><a id="nextLink" title="' + Drupal.t('Next') + '" href="#"></a></div>';
    var frameNav = '<div id="frameHoverNav"><a id="framePrevLink" title="' + Drupal.t('Previous') + '" href="#"></a><a id="frameNextLink" title="' + Drupal.t('Next') + '" href="#"></a></div>';
    var caption = '<span id="caption"></span>';
    var numberDisplay = '<span id="numberDisplay"></span>';
    var close = '<a id="bottomNavClose" title="' + Drupal.t('Close') + '" href="#"></a>';
    var zoom = '<a id="bottomNavZoom" href="#"></a>';
    var zoomOut = '<a id="bottomNavZoomOut" href="#"></a>';
    var pause = '<a id="lightshowPause" title="' + Drupal.t('Pause Slideshow') + '" href="#" style="display: none;"></a>';
    var play = '<a id="lightshowPlay" title="' + Drupal.t('Play Slideshow') + '" href="#" style="display: none;"></a>';

    $("body").append(output);
    $('#outerImageContainer').append(modal + frame + imageContainer + loading);
    if (!s.use_alt_layout) {
      $('#imageContainer').append(image + hoverNav);
      $('#imageData').append(details + bottomNav);
      $('#imageDetails').append(caption + numberDisplay);
      $('#bottomNav').append(frameNav + close + zoom + zoomOut + pause + play);
    }
    else {
      $('#outerImageContainer').append(bottomNav);
      $('#imageContainer').append(image);
      $('#bottomNav').append(close + zoom + zoomOut);
      $('#imageData').append(hoverNav + details);
      $('#imageDetails').append(caption + numberDisplay + pause + play);
    }

    // Setup onclick handlers.
    if (Lightbox.disableCloseClick) {
      $('#lightbox2-overlay').click(function() { Lightbox.end(); return false; } ).hide();
    }
    $('#loadingLink, #bottomNavClose').click(function() { Lightbox.end('forceClose'); return false; } );
    $('#prevLink, #framePrevLink').click(function() { Lightbox.changeData(Lightbox.activeImage - 1); return false; } );
    $('#nextLink, #frameNextLink').click(function() { Lightbox.changeData(Lightbox.activeImage + 1); return false; } );
    $('#bottomNavZoom').click(function() { Lightbox.changeData(Lightbox.activeImage, true); return false; } );
    $('#bottomNavZoomOut').click(function() { Lightbox.changeData(Lightbox.activeImage, false); return false; } );
    $('#lightshowPause').click(function() { Lightbox.togglePlayPause("lightshowPause", "lightshowPlay"); return false; } );
    $('#lightshowPlay').click(function() { Lightbox.togglePlayPause("lightshowPlay", "lightshowPause"); return false; } );

    // Fix positioning.
    $('#prevLink, #nextLink, #framePrevLink, #frameNextLink').css({ 'paddingTop': Lightbox.borderSize + 'px'});
    $('#imageContainer, #frameContainer, #modalContainer').css({ 'padding': Lightbox.borderSize + 'px'});
    $('#outerImageContainer, #imageDataContainer, #bottomNavClose').css({'backgroundColor': '#' + Lightbox.boxColor, 'color': '#'+Lightbox.fontColor});
    if (Lightbox.alternative_layout) {
      $('#bottomNavZoom, #bottomNavZoomOut').css({'bottom': Lightbox.borderSize + 'px', 'right': Lightbox.borderSize + 'px'});
    }
    else if (Lightbox.rtl == 1 && $.browser.msie) {
      $('#bottomNavZoom, #bottomNavZoomOut').css({'left': '0px'});
    }

    // Force navigation links to always be displayed
    if (s.force_show_nav) {
      $('#prevLink, #nextLink').addClass("force_show_nav");
    }

  },

  // initList()
  // Loops through anchor tags looking for 'lightbox', 'lightshow' and
  // 'lightframe', etc, references and applies onclick events to appropriate
  // links. You can rerun after dynamically adding images w/ajax.
  initList : function(context) {

    if (context == undefined || context == null) {
      context = document;
    }

    // Attach lightbox to any links with rel 'lightbox', 'lightshow' or
    // 'lightframe', etc.
    $("a[rel^='lightbox']:not(.lightbox-processed), area[rel^='lightbox']:not(.lightbox-processed)", context).addClass('lightbox-processed').click(function(e) {
      if (Lightbox.disableCloseClick) {
        $('#lightbox').unbind('click');
        $('#lightbox').click(function() { Lightbox.end('forceClose'); } );
      }
      Lightbox.start(this, false, false, false, false);
      if (e.preventDefault) { e.preventDefault(); }
      return false;
    });
    $("a[rel^='lightshow']:not(.lightbox-processed), area[rel^='lightshow']:not(.lightbox-processed)", context).addClass('lightbox-processed').click(function(e) {
      if (Lightbox.disableCloseClick) {
        $('#lightbox').unbind('click');
        $('#lightbox').click(function() { Lightbox.end('forceClose'); } );
      }
      Lightbox.start(this, true, false, false, false);
      if (e.preventDefault) { e.preventDefault(); }
      return false;
    });
    $("a[rel^='lightframe']:not(.lightbox-processed), area[rel^='lightframe']:not(.lightbox-processed)", context).addClass('lightbox-processed').click(function(e) {
      if (Lightbox.disableCloseClick) {
        $('#lightbox').unbind('click');
        $('#lightbox').click(function() { Lightbox.end('forceClose'); } );
      }
      Lightbox.start(this, false, true, false, false);
      if (e.preventDefault) { e.preventDefault(); }
      return false;
    });
    if (Lightbox.enableVideo) {
      $("a[rel^='lightvideo']:not(.lightbox-processed), area[rel^='lightvideo']:not(.lightbox-processed)", context).addClass('lightbox-processed').click(function(e) {
        if (Lightbox.disableCloseClick) {
          $('#lightbox').unbind('click');
          $('#lightbox').click(function() { Lightbox.end('forceClose'); } );
        }
        Lightbox.start(this, false, false, true, false);
        if (e.preventDefault) { e.preventDefault(); }
        return false;
      });
    }
    $("a[rel^='lightmodal']:not(.lightbox-processed), area[rel^='lightmodal']:not(.lightbox-processed)", context).addClass('lightbox-processed').click(function(e) {
      $('#lightbox').unbind('click');
      // Add classes from the link to the lightbox div - don't include lightbox-processed
      $('#lightbox').addClass($(this).attr('class'));
      $('#lightbox').removeClass('lightbox-processed');
      Lightbox.start(this, false, false, false, true);
      if (e.preventDefault) { e.preventDefault(); }
      return false;
    });
    $("#lightboxAutoModal:not(.lightbox-processed)", context).addClass('lightbox-processed').click(function(e) {
      Lightbox.auto_modal = true;
      $('#lightbox').unbind('click');
      Lightbox.start(this, false, false, false, true);
      if (e.preventDefault) { e.preventDefault(); }
      return false;
    });
  },

  // start()
  // Display overlay and lightbox. If image is part of a set, add siblings to
  // imageArray.
  start: function(imageLink, slideshow, lightframe, lightvideo, lightmodal) {

    Lightbox.isPaused = !Lightbox.autoStart;

    // Replaces hideSelectBoxes() and hideFlash() calls in original lightbox2.
    Lightbox.toggleSelectsFlash('hide');

    // Stretch overlay to fill page and fade in.
    var arrayPageSize = Lightbox.getPageSize();
    $("#lightbox2-overlay").hide().css({
      'width': '100%',
      'zIndex': '10090',
      'height': arrayPageSize[1] + 'px',
      'backgroundColor' : '#' + Lightbox.overlayColor
    });
    // Detect OS X FF2 opacity + flash issue.
    if (lightvideo && this.detectMacFF2()) {
      $("#lightbox2-overlay").removeClass("overlay_default");
      $("#lightbox2-overlay").addClass("overlay_macff2");
      $("#lightbox2-overlay").css({'opacity' : null});
    }
    else {
      $("#lightbox2-overlay").removeClass("overlay_macff2");
      $("#lightbox2-overlay").addClass("overlay_default");
      $("#lightbox2-overlay").css({'opacity' : Lightbox.overlayOpacity});
    }
    $("#lightbox2-overlay").fadeIn(Lightbox.fadeInSpeed);


    Lightbox.isSlideshow = slideshow;
    Lightbox.isLightframe = lightframe;
    Lightbox.isVideo = lightvideo;
    Lightbox.isModal = lightmodal;
    Lightbox.imageArray = [];
    Lightbox.imageNum = 0;

    var anchors = $(imageLink.tagName);
    var anchor = null;
    var rel_parts = Lightbox.parseRel(imageLink);
    var rel = rel_parts["rel"];
    var rel_group = rel_parts["group"];
    var title = (rel_parts["title"] ? rel_parts["title"] : imageLink.title);
    var rel_style = null;
    var i = 0;

    if (rel_parts["flashvars"]) {
      Lightbox.flvFlashvars = Lightbox.flvFlashvars + '&' + rel_parts["flashvars"];
    }

    // Set the title for image alternative text.
    var alt = imageLink.title;
    if (!alt) {
      var img = $(imageLink).find("img");
      if (img && $(img).attr("alt")) {
        alt = $(img).attr("alt");
      }
      else {
        alt = title;
      }
    }

    if ($(imageLink).attr('id') == 'lightboxAutoModal') {
      rel_style = rel_parts["style"];
      Lightbox.imageArray.push(['#lightboxAutoModal > *', title, alt, rel_style, 1]);
    }
    else {
      // Handle lightbox images with no grouping.
      if ((rel == 'lightbox' || rel == 'lightshow') && !rel_group) {
        Lightbox.imageArray.push([imageLink.href, title, alt]);
      }

      // Handle other items with no grouping.
      else if (!rel_group) {
        rel_style = rel_parts["style"];
        Lightbox.imageArray.push([imageLink.href, title, alt, rel_style]);
      }

      // Handle grouped items.
      else {

        // Loop through anchors and add them to imageArray.
        for (i = 0; i < anchors.length; i++) {
          anchor = anchors[i];
          if (anchor.href && typeof(anchor.href) == "string" && $(anchor).attr('rel')) {
            var rel_data = Lightbox.parseRel(anchor);
            var anchor_title = (rel_data["title"] ? rel_data["title"] : anchor.title);
            img_alt = anchor.title;
            if (!img_alt) {
              var anchor_img = $(anchor).find("img");
              if (anchor_img && $(anchor_img).attr("alt")) {
                img_alt = $(anchor_img).attr("alt");
              }
              else {
                img_alt = title;
              }
            }
            if (rel_data["rel"] == rel) {
              if (rel_data["group"] == rel_group) {
                if (Lightbox.isLightframe || Lightbox.isModal || Lightbox.isVideo) {
                  rel_style = rel_data["style"];
                }
                Lightbox.imageArray.push([anchor.href, anchor_title, img_alt, rel_style]);
              }
            }
          }
        }

        // Remove duplicates.
        for (i = 0; i < Lightbox.imageArray.length; i++) {
          for (j = Lightbox.imageArray.length-1; j > i; j--) {
            if (Lightbox.imageArray[i][0] == Lightbox.imageArray[j][0]) {
              Lightbox.imageArray.splice(j,1);
            }
          }
        }
        while (Lightbox.imageArray[Lightbox.imageNum][0] != imageLink.href) {
          Lightbox.imageNum++;
        }
      }
    }

    if (Lightbox.isSlideshow && Lightbox.showPlayPause && Lightbox.isPaused) {
      $('#lightshowPlay').show();
      $('#lightshowPause').hide();
    }

    // Calculate top and left offset for the lightbox.
    var arrayPageScroll = Lightbox.getPageScroll();
    var lightboxTop = arrayPageScroll[1] + (Lightbox.topPosition == '' ? (arrayPageSize[3] / 10) : Lightbox.topPosition) * 1;
    var lightboxLeft = arrayPageScroll[0];
    $('#frameContainer, #modalContainer, #lightboxImage').hide();
    $('#hoverNav, #prevLink, #nextLink, #frameHoverNav, #framePrevLink, #frameNextLink').hide();
    $('#imageDataContainer, #numberDisplay, #bottomNavZoom, #bottomNavZoomOut').hide();
    $('#outerImageContainer').css({'width': '250px', 'height': '250px'});
    $('#lightbox').css({
      'zIndex': '10500',
      'top': lightboxTop + 'px',
      'left': lightboxLeft + 'px'
    }).show();

    Lightbox.total = Lightbox.imageArray.length;
    Lightbox.changeData(Lightbox.imageNum);
  },

  // changeData()
  // Hide most elements and preload image in preparation for resizing image
  // container.
  changeData: function(imageNum, zoomIn) {

    if (Lightbox.inprogress === false) {
      if (Lightbox.total > 1 && ((Lightbox.isSlideshow && Lightbox.loopSlides) || (!Lightbox.isSlideshow && Lightbox.loopItems))) {
        if (imageNum >= Lightbox.total) imageNum = 0;
        if (imageNum < 0) imageNum = Lightbox.total - 1;
      }

      if (Lightbox.isSlideshow) {
        for (var i = 0; i < Lightbox.slideIdCount; i++) {
          window.clearTimeout(Lightbox.slideIdArray[i]);
        }
      }
      Lightbox.inprogress = true;
      Lightbox.activeImage = imageNum;

      if (Lightbox.disableResize && !Lightbox.isSlideshow) {
        zoomIn = true;
      }
      Lightbox.isZoomedIn = zoomIn;


      // Hide elements during transition.
      $('#loading').css({'zIndex': '10500'}).show();
      if (!Lightbox.alternative_layout) {
        $('#imageContainer').hide();
      }
      $('#frameContainer, #modalContainer, #lightboxImage').hide();
      $('#hoverNav, #prevLink, #nextLink, #frameHoverNav, #framePrevLink, #frameNextLink').hide();
      $('#imageDataContainer, #numberDisplay, #bottomNavZoom, #bottomNavZoomOut').hide();

      // Preload image content, but not iframe pages.
      if (!Lightbox.isLightframe && !Lightbox.isVideo && !Lightbox.isModal) {
        $("#lightbox #imageDataContainer").removeClass('lightbox2-alt-layout-data');
        imgPreloader = new Image();
        imgPreloader.onerror = function() { Lightbox.imgNodeLoadingError(this); };

        imgPreloader.onload = function() {
          var photo = document.getElementById('lightboxImage');
          photo.src = Lightbox.imageArray[Lightbox.activeImage][0];
          photo.alt = Lightbox.imageArray[Lightbox.activeImage][2];

          var imageWidth = imgPreloader.width;
          var imageHeight = imgPreloader.height;

          // Resize code.
          var arrayPageSize = Lightbox.getPageSize();
          var targ = { w:arrayPageSize[2] - (Lightbox.borderSize * 2), h:arrayPageSize[3] - (Lightbox.borderSize * 6) - (Lightbox.infoHeight * 4) - (arrayPageSize[3] / 10) };
          var orig = { w:imgPreloader.width, h:imgPreloader.height };

          // Image is very large, so show a smaller version of the larger image
          // with zoom button.
          if (zoomIn !== true) {
            var ratio = 1.0; // Shrink image with the same aspect.
            $('#bottomNavZoomOut, #bottomNavZoom').hide();
            if ((orig.w >= targ.w || orig.h >= targ.h) && orig.h && orig.w) {
              ratio = ((targ.w / orig.w) < (targ.h / orig.h)) ? targ.w / orig.w : targ.h / orig.h;
              if (!Lightbox.disableZoom && !Lightbox.isSlideshow) {
                $('#bottomNavZoom').css({'zIndex': '10500'}).show();
              }
            }

            imageWidth  = Math.floor(orig.w * ratio);
            imageHeight = Math.floor(orig.h * ratio);
          }

          else {
            $('#bottomNavZoom').hide();
            // Only display zoom out button if the image is zoomed in already.
            if ((orig.w >= targ.w || orig.h >= targ.h) && orig.h && orig.w) {
              // Only display zoom out button if not a slideshow and if the
              // buttons aren't disabled.
              if (!Lightbox.disableResize && Lightbox.isSlideshow === false && !Lightbox.disableZoom) {
                $('#bottomNavZoomOut').css({'zIndex': '10500'}).show();
              }
            }
          }

          photo.style.width = (imageWidth) + 'px';
          photo.style.height = (imageHeight) + 'px';
          Lightbox.resizeContainer(imageWidth, imageHeight);

          // Clear onLoad, IE behaves irratically with animated gifs otherwise.
          imgPreloader.onload = function() {};
        };

        imgPreloader.src = Lightbox.imageArray[Lightbox.activeImage][0];
        imgPreloader.alt = Lightbox.imageArray[Lightbox.activeImage][2];
      }

      // Set up frame size, etc.
      else if (Lightbox.isLightframe) {
        $("#lightbox #imageDataContainer").addClass('lightbox2-alt-layout-data');
        var src = Lightbox.imageArray[Lightbox.activeImage][0];
        $('#frameContainer').html('<iframe id="lightboxFrame" style="display: none;" src="'+src+'"></iframe>');

        // Enable swf support in Gecko browsers.
        if ($.browser.mozilla && src.indexOf('.swf') != -1) {
          setTimeout(function () {
            document.getElementById("lightboxFrame").src = Lightbox.imageArray[Lightbox.activeImage][0];
          }, 1000);
        }

        if (!Lightbox.iframe_border) {
          $('#lightboxFrame').css({'border': 'none'});
          $('#lightboxFrame').attr('frameborder', '0');
        }
        var iframe = document.getElementById('lightboxFrame');
        var iframeStyles = Lightbox.imageArray[Lightbox.activeImage][3];
        iframe = Lightbox.setStyles(iframe, iframeStyles);
        Lightbox.resizeContainer(parseInt(iframe.width, 10), parseInt(iframe.height, 10));
      }
      else if (Lightbox.isVideo || Lightbox.isModal) {
        $("#lightbox #imageDataContainer").addClass('lightbox2-alt-layout-data');
        var container = document.getElementById('modalContainer');
        var modalStyles = Lightbox.imageArray[Lightbox.activeImage][3];
        container = Lightbox.setStyles(container, modalStyles);
        if (Lightbox.isVideo) {
          Lightbox.modalHeight =  parseInt(container.height, 10) - 10;
          Lightbox.modalWidth =  parseInt(container.width, 10) - 10;
          Lightvideo.startVideo(Lightbox.imageArray[Lightbox.activeImage][0]);
        }
        Lightbox.resizeContainer(parseInt(container.width, 10), parseInt(container.height, 10));
      }
    }
  },

  // imgNodeLoadingError()
  imgNodeLoadingError: function(image) {
    var s = Drupal.settings.lightbox2;
    var original_image = Lightbox.imageArray[Lightbox.activeImage][0];
    if (s.display_image_size !== "") {
      original_image = original_image.replace(new RegExp("."+s.display_image_size), "");
    }
    Lightbox.imageArray[Lightbox.activeImage][0] = original_image;
    image.onerror = function() { Lightbox.imgLoadingError(image); };
    image.src = original_image;
  },

  // imgLoadingError()
  imgLoadingError: function(image) {
    var s = Drupal.settings.lightbox2;
    Lightbox.imageArray[Lightbox.activeImage][0] = s.default_image;
    image.src = s.default_image;
  },

  // resizeContainer()
  resizeContainer: function(imgWidth, imgHeight) {

    imgWidth = (imgWidth < Lightbox.minWidth ? Lightbox.minWidth : imgWidth);

    this.widthCurrent = $('#outerImageContainer').width();
    this.heightCurrent = $('#outerImageContainer').height();

    var widthNew = (imgWidth  + (Lightbox.borderSize * 2));
    var heightNew = (imgHeight  + (Lightbox.borderSize * 2));

    // Scalars based on change from old to new.
    this.xScale = ( widthNew / this.widthCurrent) * 100;
    this.yScale = ( heightNew / this.heightCurrent) * 100;

    // Calculate size difference between new and old image, and resize if
    // necessary.
    wDiff = this.widthCurrent - widthNew;
    hDiff = this.heightCurrent - heightNew;

    $('#modalContainer').css({'width': imgWidth, 'height': imgHeight});
    // Detect animation sequence.
    if (Lightbox.resizeSequence) {
      var animate1 = {width: widthNew};
      var animate2 = {height: heightNew};
      if (Lightbox.resizeSequence == 2) {
        animate1 = {height: heightNew};
        animate2 = {width: widthNew};
      }
      $('#outerImageContainer').animate(animate1, Lightbox.resizeSpeed).animate(animate2, Lightbox.resizeSpeed, 'linear', function() { Lightbox.showData(); });
    }
    // Simultaneous.
    else {
      $('#outerImageContainer').animate({'width': widthNew, 'height': heightNew}, Lightbox.resizeSpeed, 'linear', function() { Lightbox.showData(); });
    }

    // If new and old image are same size and no scaling transition is necessary
    // do a quick pause to prevent image flicker.
    if ((hDiff === 0) && (wDiff === 0)) {
      if ($.browser.msie) {
        Lightbox.pause(250);
      }
      else {
        Lightbox.pause(100);
      }
    }

    var s = Drupal.settings.lightbox2;
    if (!s.use_alt_layout) {
      $('#prevLink, #nextLink').css({'height': imgHeight + 'px'});
    }
    $('#imageDataContainer').css({'width': widthNew + 'px'});
  },

  // showData()
  // Display image and begin preloading neighbors.
  showData: function() {
    $('#loading').hide();

    if (Lightbox.isLightframe || Lightbox.isVideo || Lightbox.isModal) {
      Lightbox.updateDetails();
      if (Lightbox.isLightframe) {
        $('#frameContainer').show();
        if ($.browser.safari || Lightbox.fadeInSpeed === 0) {
          $('#lightboxFrame').css({'zIndex': '10500'}).show();
        }
        else {
          $('#lightboxFrame').css({'zIndex': '10500'}).fadeIn(Lightbox.fadeInSpeed);
        }
      }
      else {
        if (Lightbox.isVideo) {
          $("#modalContainer").html(Lightbox.modalHTML).click(function(){return false;}).css('zIndex', '10500').show();
        }
        else {
          var src = unescape(Lightbox.imageArray[Lightbox.activeImage][0]);
          if (Lightbox.imageArray[Lightbox.activeImage][4]) {
            $(src).appendTo("#modalContainer");
            $('#modalContainer').css({'zIndex': '10500'}).show();
          }
          else {
            // Use a callback to show the new image, otherwise you get flicker.
            $("#modalContainer").hide().load(src, function () {$('#modalContainer').css({'zIndex': '10500'}).show();});
          }
          $('#modalContainer').unbind('click');
        }
        // This might be needed in the Lightframe section above.
        //$('#modalContainer').css({'zIndex': '10500'}).show();
      }
    }

    // Handle display of image content.
    else {
      $('#imageContainer').show();
      if ($.browser.safari || Lightbox.fadeInSpeed === 0) {
        $('#lightboxImage').css({'zIndex': '10500'}).show();
      }
      else {
        $('#lightboxImage').css({'zIndex': '10500'}).fadeIn(Lightbox.fadeInSpeed);
      }
      Lightbox.updateDetails();
      this.preloadNeighborImages();
    }
    Lightbox.inprogress = false;

    // Slideshow specific stuff.
    if (Lightbox.isSlideshow) {
      if (!Lightbox.loopSlides && Lightbox.activeImage == (Lightbox.total - 1)) {
        if (Lightbox.autoExit) {
          Lightbox.slideIdArray[Lightbox.slideIdCount++] = setTimeout(function () {Lightbox.end('slideshow');}, Lightbox.slideInterval);
        }
      }
      else {
        if (!Lightbox.isPaused && Lightbox.total > 1) {
          Lightbox.slideIdArray[Lightbox.slideIdCount++] = setTimeout(function () {Lightbox.changeData(Lightbox.activeImage + 1);}, Lightbox.slideInterval);
        }
      }
      if (Lightbox.showPlayPause && Lightbox.total > 1 && !Lightbox.isPaused) {
        $('#lightshowPause').show();
        $('#lightshowPlay').hide();
      }
      else if (Lightbox.showPlayPause && Lightbox.total > 1) {
        $('#lightshowPause').hide();
        $('#lightshowPlay').show();
      }
    }

    // Adjust the page overlay size.
    var arrayPageSize = Lightbox.getPageSize();
    var arrayPageScroll = Lightbox.getPageScroll();
    var pageHeight = arrayPageSize[1];
    if (Lightbox.isZoomedIn && arrayPageSize[1] > arrayPageSize[3]) {
      var lightboxTop = (Lightbox.topPosition == '' ? (arrayPageSize[3] / 10) : Lightbox.topPosition) * 1;
      pageHeight = pageHeight + arrayPageScroll[1] + lightboxTop;
    }
    $('#lightbox2-overlay').css({'height': pageHeight + 'px', 'width': arrayPageSize[0] + 'px'});

    // Gecko browsers (e.g. Firefox, SeaMonkey, etc) don't handle pdfs as
    // expected.
    if ($.browser.mozilla) {
      if (Lightbox.imageArray[Lightbox.activeImage][0].indexOf(".pdf") != -1) {
        setTimeout(function () {
          document.getElementById("lightboxFrame").src = Lightbox.imageArray[Lightbox.activeImage][0];
        }, 1000);
      }
    }
  },

  // updateDetails()
  // Display caption, image number, and bottom nav.
  updateDetails: function() {

    $("#imageDataContainer").hide();

    var s = Drupal.settings.lightbox2;

    if (s.show_caption) {
      var caption = Lightbox.filterXSS(Lightbox.imageArray[Lightbox.activeImage][1]);
      if (!caption) caption = '';
      $('#caption').html(caption).css({'zIndex': '10500'}).show();
    }

    // If image is part of set display 'Image x of x'.
    var numberDisplay = null;
    if (s.image_count && Lightbox.total > 1) {
      var currentImage = Lightbox.activeImage + 1;
      if (!Lightbox.isLightframe && !Lightbox.isModal && !Lightbox.isVideo) {
        numberDisplay = s.image_count.replace(/\!current/, currentImage).replace(/\!total/, Lightbox.total);
      }
      else if (Lightbox.isVideo) {
        numberDisplay = s.video_count.replace(/\!current/, currentImage).replace(/\!total/, Lightbox.total);
      }
      else {
        numberDisplay = s.page_count.replace(/\!current/, currentImage).replace(/\!total/, Lightbox.total);
      }
      $('#numberDisplay').html(numberDisplay).css({'zIndex': '10500'}).show();
    }
    else {
      $('#numberDisplay').hide();
    }

    $("#imageDataContainer").hide().slideDown(Lightbox.slideDownSpeed, function() {
      $("#bottomNav").show();
    });
    if (Lightbox.rtl == 1) {
      $("#bottomNav").css({'float': 'left'});
    }
    Lightbox.updateNav();
  },

  // updateNav()
  // Display appropriate previous and next hover navigation.
  updateNav: function() {

    $('#hoverNav').css({'zIndex': '10500'}).show();
    var prevLink = '#prevLink';
    var nextLink = '#nextLink';

    // Slideshow is separated as we need to show play / pause button.
    if (Lightbox.isSlideshow) {
      if ((Lightbox.total > 1 && Lightbox.loopSlides) || Lightbox.activeImage !== 0) {
        $(prevLink).css({'zIndex': '10500'}).show().click(function() {
          if (Lightbox.pauseOnPrevClick) {
            Lightbox.togglePlayPause("lightshowPause", "lightshowPlay");
          }
          Lightbox.changeData(Lightbox.activeImage - 1); return false;
        });
      }
      else {
        $(prevLink).hide();
      }

      // If not last image in set, display next image button.
      if ((Lightbox.total > 1 && Lightbox.loopSlides) || Lightbox.activeImage != (Lightbox.total - 1)) {
        $(nextLink).css({'zIndex': '10500'}).show().click(function() {
          if (Lightbox.pauseOnNextClick) {
            Lightbox.togglePlayPause("lightshowPause", "lightshowPlay");
          }
          Lightbox.changeData(Lightbox.activeImage + 1); return false;
        });
      }
      // Safari browsers need to have hide() called again.
      else {
        $(nextLink).hide();
      }
    }

    // All other types of content.
    else {

      if ((Lightbox.isLightframe || Lightbox.isModal || Lightbox.isVideo) && !Lightbox.alternative_layout) {
        $('#frameHoverNav').css({'zIndex': '10500'}).show();
        $('#hoverNav').css({'zIndex': '10500'}).hide();
        prevLink = '#framePrevLink';
        nextLink = '#frameNextLink';
      }

      // If not first image in set, display prev image button.
      if ((Lightbox.total > 1 && Lightbox.loopItems) || Lightbox.activeImage !== 0) {
        // Unbind any other click handlers, otherwise this adds a new click handler
        // each time the arrow is clicked.
        $(prevLink).css({'zIndex': '10500'}).show().unbind().click(function() {
          Lightbox.changeData(Lightbox.activeImage - 1); return false;
        });
      }
      // Safari browsers need to have hide() called again.
      else {
        $(prevLink).hide();
      }

      // If not last image in set, display next image button.
      if ((Lightbox.total > 1 && Lightbox.loopItems) || Lightbox.activeImage != (Lightbox.total - 1)) {
        // Unbind any other click handlers, otherwise this adds a new click handler
        // each time the arrow is clicked.
        $(nextLink).css({'zIndex': '10500'}).show().unbind().click(function() {
          Lightbox.changeData(Lightbox.activeImage + 1); return false;
        });
      }
      // Safari browsers need to have hide() called again.
      else {
        $(nextLink).hide();
      }
    }

    // Don't enable keyboard shortcuts so forms will work.
    if (!Lightbox.isModal) {
      this.enableKeyboardNav();
    }
  },


  // enableKeyboardNav()
  enableKeyboardNav: function() {
    $(document).bind("keydown", this.keyboardAction);
  },

  // disableKeyboardNav()
  disableKeyboardNav: function() {
    $(document).unbind("keydown", this.keyboardAction);
  },

  // keyboardAction()
  keyboardAction: function(e) {
    if (e === null) { // IE.
      keycode = event.keyCode;
      escapeKey = 27;
    }
    else { // Mozilla.
      keycode = e.keyCode;
      escapeKey = e.DOM_VK_ESCAPE;
    }

    key = String.fromCharCode(keycode).toLowerCase();

    // Close lightbox.
    if (Lightbox.checkKey(Lightbox.keysClose, key, keycode)) {
      Lightbox.end('forceClose');
    }
    // Display previous image (p, <-).
    else if (Lightbox.checkKey(Lightbox.keysPrevious, key, keycode)) {
      if ((Lightbox.total > 1 && ((Lightbox.isSlideshow && Lightbox.loopSlides) || (!Lightbox.isSlideshow && Lightbox.loopItems))) || Lightbox.activeImage !== 0) {
        Lightbox.changeData(Lightbox.activeImage - 1);
      }

    }
    // Display next image (n, ->).
    else if (Lightbox.checkKey(Lightbox.keysNext, key, keycode)) {
      if ((Lightbox.total > 1 && ((Lightbox.isSlideshow && Lightbox.loopSlides) || (!Lightbox.isSlideshow && Lightbox.loopItems))) || Lightbox.activeImage != (Lightbox.total - 1)) {
        Lightbox.changeData(Lightbox.activeImage + 1);
      }
    }
    // Zoom in.
    else if (Lightbox.checkKey(Lightbox.keysZoom, key, keycode) && !Lightbox.disableResize && !Lightbox.disableZoom && !Lightbox.isSlideshow && !Lightbox.isLightframe) {
      if (Lightbox.isZoomedIn) {
        Lightbox.changeData(Lightbox.activeImage, false);
      }
      else if (!Lightbox.isZoomedIn) {
        Lightbox.changeData(Lightbox.activeImage, true);
      }
      return false;
    }
    // Toggle play / pause (space).
    else if (Lightbox.checkKey(Lightbox.keysPlayPause, key, keycode) && Lightbox.isSlideshow) {

      if (Lightbox.isPaused) {
        Lightbox.togglePlayPause("lightshowPlay", "lightshowPause");
      }
      else {
        Lightbox.togglePlayPause("lightshowPause", "lightshowPlay");
      }
      return false;
    }
  },

  preloadNeighborImages: function() {

    if ((Lightbox.total - 1) > Lightbox.activeImage) {
      preloadNextImage = new Image();
      preloadNextImage.src = Lightbox.imageArray[Lightbox.activeImage + 1][0];
    }
    if (Lightbox.activeImage > 0) {
      preloadPrevImage = new Image();
      preloadPrevImage.src = Lightbox.imageArray[Lightbox.activeImage - 1][0];
    }

  },

  end: function(caller) {
    var closeClick = (caller == 'slideshow' ? false : true);
    if (Lightbox.isSlideshow && Lightbox.isPaused && !closeClick) {
      return;
    }
    // To prevent double clicks on navigation links.
    if (Lightbox.inprogress === true && caller != 'forceClose') {
      return;
    }
    Lightbox.disableKeyboardNav();
    $('#lightbox').hide();
    $("#lightbox2-overlay").fadeOut();
    Lightbox.isPaused = true;
    Lightbox.inprogress = false;
    // Replaces calls to showSelectBoxes() and showFlash() in original
    // lightbox2.
    Lightbox.toggleSelectsFlash('visible');
    if (Lightbox.isSlideshow) {
      for (var i = 0; i < Lightbox.slideIdCount; i++) {
        window.clearTimeout(Lightbox.slideIdArray[i]);
      }
      $('#lightshowPause, #lightshowPlay').hide();
    }
    else if (Lightbox.isLightframe) {
      $('#frameContainer').empty().hide();
    }
    else if (Lightbox.isVideo || Lightbox.isModal) {
      if (!Lightbox.auto_modal) {
        $('#modalContainer').hide().html("");
      }
      Lightbox.auto_modal = false;
    }
  },


  // getPageScroll()
  // Returns array with x,y page scroll values.
  // Core code from - quirksmode.com.
  getPageScroll : function() {

    var xScroll, yScroll;

    if (self.pageYOffset || self.pageXOffset) {
      yScroll = self.pageYOffset;
      xScroll = self.pageXOffset;
    }
    else if (document.documentElement && (document.documentElement.scrollTop || document.documentElement.scrollLeft)) {  // Explorer 6 Strict.
      yScroll = document.documentElement.scrollTop;
      xScroll = document.documentElement.scrollLeft;
    }
    else if (document.body) {// All other Explorers.
      yScroll = document.body.scrollTop;
      xScroll = document.body.scrollLeft;
    }

    arrayPageScroll = [xScroll,yScroll];
    return arrayPageScroll;
  },

  // getPageSize()
  // Returns array with page width, height and window width, height.
  // Core code from - quirksmode.com.
  // Edit for Firefox by pHaez.

  getPageSize : function() {

    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
      xScroll = window.innerWidth + window.scrollMaxX;
      yScroll = window.innerHeight + window.scrollMaxY;
    }
    else if (document.body.scrollHeight > document.body.offsetHeight) { // All but Explorer Mac.
      xScroll = document.body.scrollWidth;
      yScroll = document.body.scrollHeight;
    }
    else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari.
      xScroll = document.body.offsetWidth;
      yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;

    if (self.innerHeight) { // All except Explorer.
      if (document.documentElement.clientWidth) {
        windowWidth = document.documentElement.clientWidth;
      }
      else {
        windowWidth = self.innerWidth;
      }
      windowHeight = self.innerHeight;
    }
    else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode.
      windowWidth = document.documentElement.clientWidth;
      windowHeight = document.documentElement.clientHeight;
    }
    else if (document.body) { // Other Explorers.
      windowWidth = document.body.clientWidth;
      windowHeight = document.body.clientHeight;
    }
    // For small pages with total height less than height of the viewport.
    if (yScroll < windowHeight) {
      pageHeight = windowHeight;
    }
    else {
      pageHeight = yScroll;
    }
    // For small pages with total width less than width of the viewport.
    if (xScroll < windowWidth) {
      pageWidth = xScroll;
    }
    else {
      pageWidth = windowWidth;
    }
    arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
    return arrayPageSize;
  },


  // pause(numberMillis)
  pause : function(ms) {
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while (curDate - date < ms);
  },


  // toggleSelectsFlash()
  // Hide / unhide select lists and flash objects as they appear above the
  // lightbox in some browsers.
  toggleSelectsFlash: function (state) {
    if (state == 'visible') {
      $("select.lightbox_hidden, embed.lightbox_hidden, object.lightbox_hidden").show();
    }
    else if (state == 'hide') {
      $("select:visible, embed:visible, object:visible").not('#lightboxAutoModal select, #lightboxAutoModal embed, #lightboxAutoModal object').addClass("lightbox_hidden");
      $("select.lightbox_hidden, embed.lightbox_hidden, object.lightbox_hidden").hide();
    }
  },


  // parseRel()
  parseRel: function (link) {
    var parts = [];
    parts["rel"] = parts["title"] = parts["group"] = parts["style"] = parts["flashvars"] = null;
    if (!$(link).attr('rel')) return parts;
    parts["rel"] = $(link).attr('rel').match(/\w+/)[0];

    if ($(link).attr('rel').match(/\[(.*)\]/)) {
      var info = $(link).attr('rel').match(/\[(.*?)\]/)[1].split('|');
      parts["group"] = info[0];
      parts["style"] = info[1];
      if (parts["style"] != undefined && parts["style"].match(/flashvars:\s?(.*?);/)) {
        parts["flashvars"] = parts["style"].match(/flashvars:\s?(.*?);/)[1];
      }
    }
    if ($(link).attr('rel').match(/\[.*\]\[(.*)\]/)) {
      parts["title"] = $(link).attr('rel').match(/\[.*\]\[(.*)\]/)[1];
    }
    return parts;
  },

  // setStyles()
  setStyles: function(item, styles) {
    item.width = Lightbox.iframe_width;
    item.height = Lightbox.iframe_height;
    item.scrolling = "auto";

    if (!styles) return item;
    var stylesArray = styles.split(';');
    for (var i = 0; i< stylesArray.length; i++) {
      if (stylesArray[i].indexOf('width:') >= 0) {
        var w = stylesArray[i].replace('width:', '');
        item.width = jQuery.trim(w);
      }
      else if (stylesArray[i].indexOf('height:') >= 0) {
        var h = stylesArray[i].replace('height:', '');
        item.height = jQuery.trim(h);
      }
      else if (stylesArray[i].indexOf('scrolling:') >= 0) {
        var scrolling = stylesArray[i].replace('scrolling:', '');
        item.scrolling = jQuery.trim(scrolling);
      }
      else if (stylesArray[i].indexOf('overflow:') >= 0) {
        var overflow = stylesArray[i].replace('overflow:', '');
        item.overflow = jQuery.trim(overflow);
      }
    }
    return item;
  },


  // togglePlayPause()
  // Hide the pause / play button as appropriate.  If pausing the slideshow also
  // clear the timers, otherwise move onto the next image.
  togglePlayPause: function(hideId, showId) {
    if (Lightbox.isSlideshow && hideId == "lightshowPause") {
      for (var i = 0; i < Lightbox.slideIdCount; i++) {
        window.clearTimeout(Lightbox.slideIdArray[i]);
      }
    }
    $('#' + hideId).hide();
    $('#' + showId).show();

    if (hideId == "lightshowPlay") {
      Lightbox.isPaused = false;
      if (!Lightbox.loopSlides && Lightbox.activeImage == (Lightbox.total - 1)) {
        Lightbox.end();
      }
      else if (Lightbox.total > 1) {
        Lightbox.changeData(Lightbox.activeImage + 1);
      }
    }
    else {
      Lightbox.isPaused = true;
    }
  },

  triggerLightbox: function (rel_type, rel_group) {
    if (rel_type.length) {
      if (rel_group && rel_group.length) {
        $("a[rel^='" + rel_type +"\[" + rel_group + "\]'], area[rel^='" + rel_type +"\[" + rel_group + "\]']").eq(0).trigger("click");
      }
      else {
        $("a[rel^='" + rel_type +"'], area[rel^='" + rel_type +"']").eq(0).trigger("click");
      }
    }
  },

  detectMacFF2: function() {
    var ua = navigator.userAgent.toLowerCase();
    if (/firefox[\/\s](\d+\.\d+)/.test(ua)) {
      var ffversion = new Number(RegExp.$1);
      if (ffversion < 3 && ua.indexOf('mac') != -1) {
        return true;
      }
    }
    return false;
  },

  checkKey: function(keys, key, code) {
    return (jQuery.inArray(key, keys) != -1 || jQuery.inArray(String(code), keys) != -1);
  },

  filterXSS: function(str, allowed_tags) {
    var output = "";
    $.ajax({
      url: Drupal.settings.basePath + 'system/lightbox2/filter-xss',
      data: {
        'string' : str,
        'allowed_tags' : allowed_tags
      },
      type: "POST",
      async: false,
      dataType:  "json",
      success: function(data) {
        output = data;
      }
    });
    return output;
  }

};

// Initialize the lightbox.
Drupal.behaviors.initLightbox = function (context) {
  $('body:not(.lightbox-processed)', context).addClass('lightbox-processed').each(function() {
    Lightbox.initialize();
    return false; // Break the each loop.
  });

  // Attach lightbox to any links with lightbox rels.
  Lightbox.initList(context);
  $('#lightboxAutoModal', context).triggerHandler('click');
};

;
/* $Id: jquery.hoverIntent.minified.js,v 1.1 2010/02/12 12:53:13 mehrpadin Exp $ */
/**
* hoverIntent r5 // 2007.03.27 // jQuery 1.1.2+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
*
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne <brian@cherne.net>
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);;
/* $Id: jquery.bgiframe.min.js,v 1.1 2010/02/15 13:38:03 mehrpadin Exp $ */
/* Copyright (c) 2006 Brandon Aaron (http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * $LastChangedDate: 2007-06-19 20:25:28 -0500 (Tue, 19 Jun 2007) $
 * $Rev: 2111 $
 *
 * Version 2.1
 */
(function($){$.fn.bgIframe=$.fn.bgiframe=function(s){if($.browser.msie&&parseInt($.browser.version)<=6){s=$.extend({top:'auto',left:'auto',width:'auto',height:'auto',opacity:true,src:'javascript:false;'},s||{});var prop=function(n){return n&&n.constructor==Number?n+'px':n;},html='<iframe class="bgiframe"frameborder="0"tabindex="-1"src="'+s.src+'"'+'style="display:block;position:absolute;z-index:-1;'+(s.opacity!==false?'filter:Alpha(Opacity=\'0\');':'')+'top:'+(s.top=='auto'?'expression(((parseInt(this.parentNode.currentStyle.borderTopWidth)||0)*-1)+\'px\')':prop(s.top))+';'+'left:'+(s.left=='auto'?'expression(((parseInt(this.parentNode.currentStyle.borderLeftWidth)||0)*-1)+\'px\')':prop(s.left))+';'+'width:'+(s.width=='auto'?'expression(this.parentNode.offsetWidth+\'px\')':prop(s.width))+';'+'height:'+(s.height=='auto'?'expression(this.parentNode.offsetHeight+\'px\')':prop(s.height))+';'+'"/>';return this.each(function(){if($('> iframe.bgiframe',this).length==0)this.insertBefore(document.createElement(html),this.firstChild);});}return this;};if(!$.browser.version)$.browser.version=navigator.userAgent.toLowerCase().match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/)[1];})(jQuery);;
/* $Id: superfish.js,v 1.1 2010/02/12 12:53:13 mehrpadin Exp $ */
/*
 * Superfish v1.4.8 - jQuery menu widget
 * Copyright (c) 2008 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * CHANGELOG: http://users.tpg.com.au/j_birch/plugins/superfish/changelog.txt
 */

;(function($){
  $.fn.superfish = function(op){

    var sf = $.fn.superfish,
      c = sf.c,
      $arrow = $(['<span class="',c.arrowClass,'"> &#187;</span>'].join('')),
      over = function(){
        var $$ = $(this), menu = getMenu($$);
        clearTimeout(menu.sfTimer);
        $$.showSuperfishUl().siblings().hideSuperfishUl();
      },
      out = function(){
        var $$ = $(this), menu = getMenu($$), o = sf.op;
        clearTimeout(menu.sfTimer);
        menu.sfTimer=setTimeout(function(){
          o.retainPath=($.inArray($$[0],o.$path)>-1);
          $$.hideSuperfishUl();
          if (o.$path.length && $$.parents(['li.',o.hoverClass].join('')).length<1){over.call(o.$path);}
        },o.delay);
      },
      getMenu = function($menu){
        var menu = $menu.parents(['ul.',c.menuClass,':first'].join(''))[0];
        sf.op = sf.o[menu.serial];
        return menu;
      },
      addArrow = function($a){ $a.addClass(c.anchorClass).append($arrow.clone()); };

    return this.each(function() {
      var s = this.serial = sf.o.length;
      var o = $.extend({},sf.defaults,op);
      o.$path = $('li.'+o.pathClass,this).slice(0,o.pathLevels).each(function(){
        $(this).addClass([o.hoverClass,c.bcClass].join(' '))
          .filter('li:has(ul)').removeClass(o.pathClass);
      });
      sf.o[s] = sf.op = o;

      $('li:has(ul)',this)[($.fn.hoverIntent && !o.disableHI) ? 'hoverIntent' : 'hover'](over,out).each(function() {
        if (o.autoArrows) addArrow( $('>a:first-child',this) );
      })
      .not('.'+c.bcClass)
        .hideSuperfishUl();

      var $a = $('a',this);
      $a.each(function(i){
        var $li = $a.eq(i).parents('li');
        $a.eq(i).focus(function(){over.call($li);}).blur(function(){out.call($li);});
      });
      o.onInit.call(this);

    }).each(function() {
      var menuClasses = [c.menuClass];
      if (sf.op.dropShadows  && !($.browser.msie && $.browser.version < 7)) menuClasses.push(c.shadowClass);
      $(this).addClass(menuClasses.join(' '));
    });
  };

  var sf = $.fn.superfish;
  sf.o = [];
  sf.op = {};
  sf.IE7fix = function(){
    var o = sf.op;
    if ($.browser.msie && $.browser.version > 6 && o.dropShadows && o.animation.opacity!=undefined)
      this.toggleClass(sf.c.shadowClass+'-off');
    };
  sf.c = {
    bcClass  : 'sf-breadcrumb',
    menuClass: 'sf-js-enabled',
    anchorClass : 'sf-with-ul',
    arrowClass  : 'sf-sub-indicator',
    shadowClass : 'sf-shadow'
  };
  sf.defaults = {
    hoverClass  : 'sfHover',
    pathClass  : 'overideThisToUse',
    pathLevels  : 1,
    delay : 800,
    animation  : {opacity:'show'},
    speed : 'normal',
    autoArrows  : true,
    dropShadows : true,
    disableHI  : false, // true disables hoverIntent detection
    onInit : function(){}, // callback functions
    onBeforeShow: function(){},
    onShow : function(){},
    onHide : function(){}
  };
  $.fn.extend({
    hideSuperfishUl : function(){
      var o = sf.op,
        not = (o.retainPath===true) ? o.$path : '';
      o.retainPath = false;
      var $ul = $(['li.',o.hoverClass].join(''),this).add(this).not(not).removeClass(o.hoverClass)
          .find('>ul').hide().css('visibility','hidden');
      o.onHide.call($ul);
      return this;
    },
    showSuperfishUl : function(){
      var o = sf.op,
        sh = sf.c.shadowClass+'-off',
        $ul = this.addClass(o.hoverClass)
          .find('>ul:hidden').css('visibility','visible');
      sf.IE7fix.call($ul);
      o.onBeforeShow.call($ul);
      $ul.animate(o.animation,o.speed,function(){ sf.IE7fix.call($ul); o.onShow.call($ul); });
      return this;
    }
  });

})(jQuery);
;
/* $Id: supersubs.js,v 1.1 2010/02/12 12:53:13 mehrpadin Exp $ */
/*
 * Supersubs v0.2b - jQuery plugin
 * Copyright (c) 2008 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 *
 * This plugin automatically adjusts submenu widths of suckerfish-style menus to that of
 * their longest list item children. If you use this, please expect bugs and report them
 * to the jQuery Google Group with the word 'Superfish' in the subject line.
 *
 */

;(function($){ // $ will refer to jQuery within this closure

  $.fn.supersubs = function(options){
    var opts = $.extend({}, $.fn.supersubs.defaults, options);
    // return original object to support chaining
    return this.each(function() {
      // cache selections
      var $$ = $(this);
      // support metadata
      var o = $.meta ? $.extend({}, opts, $$.data()) : opts;
      // get the font size of menu.
      // .css('fontSize') returns various results cross-browser, so measure an em dash instead
      var fontsize = $('<li id="menu-fontsize">&#8212;</li>').css({
        'padding' : 0,
        'position' : 'absolute',
        'top' : '-999em',
        'width' : 'auto'
      }).appendTo($$).width(); //clientWidth is faster, but was incorrect here
      // remove em dash
      $('#menu-fontsize').remove();
      // cache all ul elements
      $ULs = $$.find('ul');
      // loop through each ul in menu
      $ULs.each(function(i) {
        // cache this ul
        var $ul = $ULs.eq(i);
        // get all (li) children of this ul
        var $LIs = $ul.children();
        // get all anchor grand-children
        var $As = $LIs.children('a');
        // force content to one line and save current float property
        var liFloat = $LIs.css('white-space','nowrap').css('float');
        // remove width restrictions and floats so elements remain vertically stacked
        var emWidth = $ul.add($LIs).add($As).css({
          'float' : 'none',
          'width'  : 'auto'
        })
        // this ul will now be shrink-wrapped to longest li due to position:absolute
        // so save its width as ems. Clientwidth is 2 times faster than .width() - thanks Dan Switzer
        .end().end()[0].clientWidth / fontsize;
        // add more width to ensure lines don't turn over at certain sizes in various browsers
        emWidth += o.extraWidth;
        // restrict to at least minWidth and at most maxWidth
        if (emWidth > o.maxWidth)    { emWidth = o.maxWidth; }
        else if (emWidth < o.minWidth)  { emWidth = o.minWidth; }
        emWidth += 'em';
        // set ul to width in ems
        $ul.css('width',emWidth);
        // restore li floats to avoid IE bugs
        // set li width to full width of this ul
        // revert white-space to normal
        $LIs.css({
          'float' : liFloat,
          'width' : '100%',
          'white-space' : 'normal'
        })
        // update offset position of descendant ul to reflect new width of parent
        .each(function(){
          var $childUl = $('>ul',this);
          var offsetDirection = $childUl.css('left')!==undefined ? 'left' : 'right';
          $childUl.css(offsetDirection,emWidth);
        });
      });

    });
  };
  // expose defaults
  $.fn.supersubs.defaults = {
    minWidth: 9, // requires em unit.
    maxWidth: 25, // requires em unit.
    extraWidth: 0 // extra width can ensure lines don't sometimes turn over due to slight browser differences in how they round-off values
  };

})(jQuery); // plugin code ends
;
// $Id: video.js,v 1.1.2.6 2010/09/22 17:32:55 heshanmw Exp $

/**
 * @file
 * Adds some show/hide to the admin form to make the UXP easier.
 *
 */

$(document).ready(function() {
  //lets see if we have any jmedia movies
  if($.fn.media) {
    $('.jmedia').media();
  }
	
  video_hide_all_options();
  $("input[name='vid_convertor']").change(function() {
    video_hide_all_options();
  });

  // change metadata options
  video_hide_all__metadata_options();
  $("input[name='vid_metadata']").change(function() {
    video_hide_all__metadata_options();
  });

  $('.video_select').each(function() {
    var ext = $(this).attr('rel');
    $('select', this).change(function() {
      if($(this).val() == 'video_play_flv') {
        $('#flv_player_'+ext).show();
      } else {
        $('#flv_player_'+ext).hide();
      }
    });
    if($('select', this).val() == 'video_play_flv') {
      $('#flv_player_'+ext).show();
    }
  });
	
  if(Drupal.settings.video) {
    $.fn.media.defaults.flvPlayer = Drupal.settings.video.flvplayer;

  }
	
  //lets setup our colorbox videos
  $('.video-box').each(function() {
    var url = $(this).attr('href');
    var data = $(this).metadata();
    var width = data.width;
    var height= data.height;
    var player = Drupal.settings.video.player; //player can be either jwplayer or flowplayer.
    $(this).colorbox({
      html: '<a id="video-overlay" href="'+url+'" style="height:'+height+'; width:'+width+'; display: block;"></a>',
      onComplete:function() {
        if(player == 'flowplayer') {
          flowplayer("video-overlay", Drupal.settings.video.flvplayer, {
            clip: {
              autoPlay: Drupal.settings.video.autoplay,
              autoBuffering: Drupal.settings.video.autobuffer
            }
          });
        } else {
          $('#video-overlay').media({
            flashvars: {
              autostart: Drupal.settings.video.autoplay
              },
            width:width,
            height:height
          });
        }
      }
    });
  });
});

function video_hide_all_options() {
  $("input[name='vid_convertor']").each(function() {
    var id = $(this).val();
    $('#'+id).hide();
    if ($(this).is(':checked')) {
      $('#' + id).show();
    }
  });
}

function videoftp_thumbnail_change() {
  // Add handlers for the video thumbnail radio buttons to update the large thumbnail onchange.
  $(".video-thumbnails input").each(function() {
    var path = $(this).val();
    if($(this).is(':checked')) {
      var holder = $(this).attr('rel');
      $('.'+holder+' img').attr('src', Drupal.settings.basePath + path);
    }
  });

}

function video_hide_all__metadata_options() {
  $("input[name='vid_metadata']").each(function() {
    var id = $(this).val();
    $('#'+id).hide();
    if ($(this).is(':checked')) {
      $('#' + id).show();
    }
  });
}
;
/*
 * jQuery Cycle Plugin (with Transition Definitions)
 * Examples and documentation at: http://jquery.malsup.com/cycle/
 * Copyright (c) 2007-2010 M. Alsup
 * Version: 2.88 (08-JUN-2010)
 * Dual licensed under the MIT and GPL licenses.
 * http://jquery.malsup.com/license.html
 * Requires: jQuery v1.2.6 or later
 */
(function($){var ver="2.88";if($.support==undefined){$.support={opacity:!($.browser.msie)};}function debug(s){if($.fn.cycle.debug){log(s);}}function log(){if(window.console&&window.console.log){window.console.log("[cycle] "+Array.prototype.join.call(arguments," "));}}$.fn.cycle=function(options,arg2){var o={s:this.selector,c:this.context};if(this.length===0&&options!="stop"){if(!$.isReady&&o.s){log("DOM not ready, queuing slideshow");$(function(){$(o.s,o.c).cycle(options,arg2);});return this;}log("terminating; zero elements found by selector"+($.isReady?"":" (DOM not ready)"));return this;}return this.each(function(){var opts=handleArguments(this,options,arg2);if(opts===false){return;}opts.updateActivePagerLink=opts.updateActivePagerLink||$.fn.cycle.updateActivePagerLink;if(this.cycleTimeout){clearTimeout(this.cycleTimeout);}this.cycleTimeout=this.cyclePause=0;var $cont=$(this);var $slides=opts.slideExpr?$(opts.slideExpr,this):$cont.children();var els=$slides.get();if(els.length<2){log("terminating; too few slides: "+els.length);return;}var opts2=buildOptions($cont,$slides,els,opts,o);if(opts2===false){return;}var startTime=opts2.continuous?10:getTimeout(els[opts2.currSlide],els[opts2.nextSlide],opts2,!opts2.rev);if(startTime){startTime+=(opts2.delay||0);if(startTime<10){startTime=10;}debug("first timeout: "+startTime);this.cycleTimeout=setTimeout(function(){go(els,opts2,0,(!opts2.rev&&!opts.backwards));},startTime);}});};function handleArguments(cont,options,arg2){if(cont.cycleStop==undefined){cont.cycleStop=0;}if(options===undefined||options===null){options={};}if(options.constructor==String){switch(options){case"destroy":case"stop":var opts=$(cont).data("cycle.opts");if(!opts){return false;}cont.cycleStop++;if(cont.cycleTimeout){clearTimeout(cont.cycleTimeout);}cont.cycleTimeout=0;$(cont).removeData("cycle.opts");if(options=="destroy"){destroy(opts);}return false;case"toggle":cont.cyclePause=(cont.cyclePause===1)?0:1;checkInstantResume(cont.cyclePause,arg2,cont);return false;case"pause":cont.cyclePause=1;return false;case"resume":cont.cyclePause=0;checkInstantResume(false,arg2,cont);return false;case"prev":case"next":var opts=$(cont).data("cycle.opts");if(!opts){log('options not found, "prev/next" ignored');return false;}$.fn.cycle[options](opts);return false;default:options={fx:options};}return options;}else{if(options.constructor==Number){var num=options;options=$(cont).data("cycle.opts");if(!options){log("options not found, can not advance slide");return false;}if(num<0||num>=options.elements.length){log("invalid slide index: "+num);return false;}options.nextSlide=num;if(cont.cycleTimeout){clearTimeout(cont.cycleTimeout);cont.cycleTimeout=0;}if(typeof arg2=="string"){options.oneTimeFx=arg2;}go(options.elements,options,1,num>=options.currSlide);return false;}}return options;function checkInstantResume(isPaused,arg2,cont){if(!isPaused&&arg2===true){var options=$(cont).data("cycle.opts");if(!options){log("options not found, can not resume");return false;}if(cont.cycleTimeout){clearTimeout(cont.cycleTimeout);cont.cycleTimeout=0;}go(options.elements,options,1,(!opts.rev&&!opts.backwards));}}}function removeFilter(el,opts){if(!$.support.opacity&&opts.cleartype&&el.style.filter){try{el.style.removeAttribute("filter");}catch(smother){}}}function destroy(opts){if(opts.next){$(opts.next).unbind(opts.prevNextEvent);}if(opts.prev){$(opts.prev).unbind(opts.prevNextEvent);}if(opts.pager||opts.pagerAnchorBuilder){$.each(opts.pagerAnchors||[],function(){this.unbind().remove();});}opts.pagerAnchors=null;if(opts.destroy){opts.destroy(opts);}}function buildOptions($cont,$slides,els,options,o){var opts=$.extend({},$.fn.cycle.defaults,options||{},$.metadata?$cont.metadata():$.meta?$cont.data():{});if(opts.autostop){opts.countdown=opts.autostopCount||els.length;}var cont=$cont[0];$cont.data("cycle.opts",opts);opts.$cont=$cont;opts.stopCount=cont.cycleStop;opts.elements=els;opts.before=opts.before?[opts.before]:[];opts.after=opts.after?[opts.after]:[];opts.after.unshift(function(){opts.busy=0;});if(!$.support.opacity&&opts.cleartype){opts.after.push(function(){removeFilter(this,opts);});}if(opts.continuous){opts.after.push(function(){go(els,opts,0,(!opts.rev&&!opts.backwards));});}saveOriginalOpts(opts);if(!$.support.opacity&&opts.cleartype&&!opts.cleartypeNoBg){clearTypeFix($slides);}if($cont.css("position")=="static"){$cont.css("position","relative");}if(opts.width){$cont.width(opts.width);}if(opts.height&&opts.height!="auto"){$cont.height(opts.height);}if(opts.startingSlide){opts.startingSlide=parseInt(opts.startingSlide);}else{if(opts.backwards){opts.startingSlide=els.length-1;}}if(opts.random){opts.randomMap=[];for(var i=0;i<els.length;i++){opts.randomMap.push(i);}opts.randomMap.sort(function(a,b){return Math.random()-0.5;});opts.randomIndex=1;opts.startingSlide=opts.randomMap[1];}else{if(opts.startingSlide>=els.length){opts.startingSlide=0;}}opts.currSlide=opts.startingSlide||0;var first=opts.startingSlide;$slides.css({position:"absolute",top:0,left:0}).hide().each(function(i){var z;if(opts.backwards){z=first?i<=first?els.length+(i-first):first-i:els.length-i;}else{z=first?i>=first?els.length-(i-first):first-i:els.length-i;}$(this).css("z-index",z);});$(els[first]).css("opacity",1).show();removeFilter(els[first],opts);if(opts.fit&&opts.width){$slides.width(opts.width);}if(opts.fit&&opts.height&&opts.height!="auto"){$slides.height(opts.height);}var reshape=opts.containerResize&&!$cont.innerHeight();if(reshape){var maxw=0,maxh=0;for(var j=0;j<els.length;j++){var $e=$(els[j]),e=$e[0],w=$e.outerWidth(),h=$e.outerHeight();if(!w){w=e.offsetWidth||e.width||$e.attr("width");}if(!h){h=e.offsetHeight||e.height||$e.attr("height");}maxw=w>maxw?w:maxw;maxh=h>maxh?h:maxh;}if(maxw>0&&maxh>0){$cont.css({width:maxw+"px",height:maxh+"px"});}}if(opts.pause){$cont.hover(function(){this.cyclePause++;},function(){this.cyclePause--;});}if(supportMultiTransitions(opts)===false){return false;}var requeue=false;options.requeueAttempts=options.requeueAttempts||0;$slides.each(function(){var $el=$(this);this.cycleH=(opts.fit&&opts.height)?opts.height:($el.height()||this.offsetHeight||this.height||$el.attr("height")||0);this.cycleW=(opts.fit&&opts.width)?opts.width:($el.width()||this.offsetWidth||this.width||$el.attr("width")||0);if($el.is("img")){var loadingIE=($.browser.msie&&this.cycleW==28&&this.cycleH==30&&!this.complete);var loadingFF=($.browser.mozilla&&this.cycleW==34&&this.cycleH==19&&!this.complete);var loadingOp=($.browser.opera&&((this.cycleW==42&&this.cycleH==19)||(this.cycleW==37&&this.cycleH==17))&&!this.complete);var loadingOther=(this.cycleH==0&&this.cycleW==0&&!this.complete);if(loadingIE||loadingFF||loadingOp||loadingOther){if(o.s&&opts.requeueOnImageNotLoaded&&++options.requeueAttempts<100){log(options.requeueAttempts," - img slide not loaded, requeuing slideshow: ",this.src,this.cycleW,this.cycleH);setTimeout(function(){$(o.s,o.c).cycle(options);},opts.requeueTimeout);requeue=true;return false;}else{log("could not determine size of image: "+this.src,this.cycleW,this.cycleH);}}}return true;});if(requeue){return false;}opts.cssBefore=opts.cssBefore||{};opts.animIn=opts.animIn||{};opts.animOut=opts.animOut||{};$slides.not(":eq("+first+")").css(opts.cssBefore);if(opts.cssFirst){$($slides[first]).css(opts.cssFirst);}if(opts.timeout){opts.timeout=parseInt(opts.timeout);if(opts.speed.constructor==String){opts.speed=$.fx.speeds[opts.speed]||parseInt(opts.speed);}if(!opts.sync){opts.speed=opts.speed/2;}var buffer=opts.fx=="shuffle"?500:250;while((opts.timeout-opts.speed)<buffer){opts.timeout+=opts.speed;}}if(opts.easing){opts.easeIn=opts.easeOut=opts.easing;}if(!opts.speedIn){opts.speedIn=opts.speed;}if(!opts.speedOut){opts.speedOut=opts.speed;}opts.slideCount=els.length;opts.currSlide=opts.lastSlide=first;if(opts.random){if(++opts.randomIndex==els.length){opts.randomIndex=0;}opts.nextSlide=opts.randomMap[opts.randomIndex];}else{if(opts.backwards){opts.nextSlide=opts.startingSlide==0?(els.length-1):opts.startingSlide-1;}else{opts.nextSlide=opts.startingSlide>=(els.length-1)?0:opts.startingSlide+1;}}if(!opts.multiFx){var init=$.fn.cycle.transitions[opts.fx];if($.isFunction(init)){init($cont,$slides,opts);}else{if(opts.fx!="custom"&&!opts.multiFx){log("unknown transition: "+opts.fx,"; slideshow terminating");return false;}}}var e0=$slides[first];if(opts.before.length){opts.before[0].apply(e0,[e0,e0,opts,true]);}if(opts.after.length>1){opts.after[1].apply(e0,[e0,e0,opts,true]);}if(opts.next){$(opts.next).bind(opts.prevNextEvent,function(){return advance(opts,opts.rev?-1:1);});}if(opts.prev){$(opts.prev).bind(opts.prevNextEvent,function(){return advance(opts,opts.rev?1:-1);});}if(opts.pager||opts.pagerAnchorBuilder){buildPager(els,opts);}exposeAddSlide(opts,els);return opts;}function saveOriginalOpts(opts){opts.original={before:[],after:[]};opts.original.cssBefore=$.extend({},opts.cssBefore);opts.original.cssAfter=$.extend({},opts.cssAfter);opts.original.animIn=$.extend({},opts.animIn);opts.original.animOut=$.extend({},opts.animOut);$.each(opts.before,function(){opts.original.before.push(this);});$.each(opts.after,function(){opts.original.after.push(this);});}function supportMultiTransitions(opts){var i,tx,txs=$.fn.cycle.transitions;if(opts.fx.indexOf(",")>0){opts.multiFx=true;opts.fxs=opts.fx.replace(/\s*/g,"").split(",");for(i=0;i<opts.fxs.length;i++){var fx=opts.fxs[i];tx=txs[fx];if(!tx||!txs.hasOwnProperty(fx)||!$.isFunction(tx)){log("discarding unknown transition: ",fx);opts.fxs.splice(i,1);i--;}}if(!opts.fxs.length){log("No valid transitions named; slideshow terminating.");return false;}}else{if(opts.fx=="all"){opts.multiFx=true;opts.fxs=[];for(p in txs){tx=txs[p];if(txs.hasOwnProperty(p)&&$.isFunction(tx)){opts.fxs.push(p);}}}}if(opts.multiFx&&opts.randomizeEffects){var r1=Math.floor(Math.random()*20)+30;for(i=0;i<r1;i++){var r2=Math.floor(Math.random()*opts.fxs.length);opts.fxs.push(opts.fxs.splice(r2,1)[0]);}debug("randomized fx sequence: ",opts.fxs);}return true;}function exposeAddSlide(opts,els){opts.addSlide=function(newSlide,prepend){var $s=$(newSlide),s=$s[0];if(!opts.autostopCount){opts.countdown++;}els[prepend?"unshift":"push"](s);if(opts.els){opts.els[prepend?"unshift":"push"](s);}opts.slideCount=els.length;$s.css("position","absolute");$s[prepend?"prependTo":"appendTo"](opts.$cont);if(prepend){opts.currSlide++;opts.nextSlide++;}if(!$.support.opacity&&opts.cleartype&&!opts.cleartypeNoBg){clearTypeFix($s);}if(opts.fit&&opts.width){$s.width(opts.width);}if(opts.fit&&opts.height&&opts.height!="auto"){$slides.height(opts.height);}s.cycleH=(opts.fit&&opts.height)?opts.height:$s.height();s.cycleW=(opts.fit&&opts.width)?opts.width:$s.width();$s.css(opts.cssBefore);if(opts.pager||opts.pagerAnchorBuilder){$.fn.cycle.createPagerAnchor(els.length-1,s,$(opts.pager),els,opts);}if($.isFunction(opts.onAddSlide)){opts.onAddSlide($s);}else{$s.hide();}};}$.fn.cycle.resetState=function(opts,fx){fx=fx||opts.fx;opts.before=[];opts.after=[];opts.cssBefore=$.extend({},opts.original.cssBefore);opts.cssAfter=$.extend({},opts.original.cssAfter);opts.animIn=$.extend({},opts.original.animIn);opts.animOut=$.extend({},opts.original.animOut);opts.fxFn=null;$.each(opts.original.before,function(){opts.before.push(this);});$.each(opts.original.after,function(){opts.after.push(this);});var init=$.fn.cycle.transitions[fx];if($.isFunction(init)){init(opts.$cont,$(opts.elements),opts);}};function go(els,opts,manual,fwd){if(manual&&opts.busy&&opts.manualTrump){debug("manualTrump in go(), stopping active transition");$(els).stop(true,true);opts.busy=false;}if(opts.busy){debug("transition active, ignoring new tx request");return;}var p=opts.$cont[0],curr=els[opts.currSlide],next=els[opts.nextSlide];if(p.cycleStop!=opts.stopCount||p.cycleTimeout===0&&!manual){return;}if(!manual&&!p.cyclePause&&!opts.bounce&&((opts.autostop&&(--opts.countdown<=0))||(opts.nowrap&&!opts.random&&opts.nextSlide<opts.currSlide))){if(opts.end){opts.end(opts);}return;}var changed=false;if((manual||!p.cyclePause)&&(opts.nextSlide!=opts.currSlide)){changed=true;var fx=opts.fx;curr.cycleH=curr.cycleH||$(curr).height();curr.cycleW=curr.cycleW||$(curr).width();next.cycleH=next.cycleH||$(next).height();next.cycleW=next.cycleW||$(next).width();if(opts.multiFx){if(opts.lastFx==undefined||++opts.lastFx>=opts.fxs.length){opts.lastFx=0;}fx=opts.fxs[opts.lastFx];opts.currFx=fx;}if(opts.oneTimeFx){fx=opts.oneTimeFx;opts.oneTimeFx=null;}$.fn.cycle.resetState(opts,fx);if(opts.before.length){$.each(opts.before,function(i,o){if(p.cycleStop!=opts.stopCount){return;}o.apply(next,[curr,next,opts,fwd]);});}var after=function(){$.each(opts.after,function(i,o){if(p.cycleStop!=opts.stopCount){return;}o.apply(next,[curr,next,opts,fwd]);});};debug("tx firing; currSlide: "+opts.currSlide+"; nextSlide: "+opts.nextSlide);opts.busy=1;if(opts.fxFn){opts.fxFn(curr,next,opts,after,fwd,manual&&opts.fastOnEvent);}else{if($.isFunction($.fn.cycle[opts.fx])){$.fn.cycle[opts.fx](curr,next,opts,after,fwd,manual&&opts.fastOnEvent);}else{$.fn.cycle.custom(curr,next,opts,after,fwd,manual&&opts.fastOnEvent);}}}if(changed||opts.nextSlide==opts.currSlide){opts.lastSlide=opts.currSlide;if(opts.random){opts.currSlide=opts.nextSlide;if(++opts.randomIndex==els.length){opts.randomIndex=0;}opts.nextSlide=opts.randomMap[opts.randomIndex];if(opts.nextSlide==opts.currSlide){opts.nextSlide=(opts.currSlide==opts.slideCount-1)?0:opts.currSlide+1;}}else{if(opts.backwards){var roll=(opts.nextSlide-1)<0;if(roll&&opts.bounce){opts.backwards=!opts.backwards;opts.nextSlide=1;opts.currSlide=0;}else{opts.nextSlide=roll?(els.length-1):opts.nextSlide-1;opts.currSlide=roll?0:opts.nextSlide+1;}}else{var roll=(opts.nextSlide+1)==els.length;if(roll&&opts.bounce){opts.backwards=!opts.backwards;opts.nextSlide=els.length-2;opts.currSlide=els.length-1;}else{opts.nextSlide=roll?0:opts.nextSlide+1;opts.currSlide=roll?els.length-1:opts.nextSlide-1;}}}}if(changed&&opts.pager){opts.updateActivePagerLink(opts.pager,opts.currSlide,opts.activePagerClass);}var ms=0;if(opts.timeout&&!opts.continuous){ms=getTimeout(els[opts.currSlide],els[opts.nextSlide],opts,fwd);}else{if(opts.continuous&&p.cyclePause){ms=10;}}if(ms>0){p.cycleTimeout=setTimeout(function(){go(els,opts,0,(!opts.rev&&!opts.backwards));},ms);}}$.fn.cycle.updateActivePagerLink=function(pager,currSlide,clsName){$(pager).each(function(){$(this).children().removeClass(clsName).eq(currSlide).addClass(clsName);});};function getTimeout(curr,next,opts,fwd){if(opts.timeoutFn){var t=opts.timeoutFn.call(curr,curr,next,opts,fwd);while((t-opts.speed)<250){t+=opts.speed;}debug("calculated timeout: "+t+"; speed: "+opts.speed);if(t!==false){return t;}}return opts.timeout;}$.fn.cycle.next=function(opts){advance(opts,opts.rev?-1:1);};$.fn.cycle.prev=function(opts){advance(opts,opts.rev?1:-1);};function advance(opts,val){var els=opts.elements;var p=opts.$cont[0],timeout=p.cycleTimeout;if(timeout){clearTimeout(timeout);p.cycleTimeout=0;}if(opts.random&&val<0){opts.randomIndex--;if(--opts.randomIndex==-2){opts.randomIndex=els.length-2;}else{if(opts.randomIndex==-1){opts.randomIndex=els.length-1;}}opts.nextSlide=opts.randomMap[opts.randomIndex];}else{if(opts.random){opts.nextSlide=opts.randomMap[opts.randomIndex];}else{opts.nextSlide=opts.currSlide+val;if(opts.nextSlide<0){if(opts.nowrap){return false;}opts.nextSlide=els.length-1;}else{if(opts.nextSlide>=els.length){if(opts.nowrap){return false;}opts.nextSlide=0;}}}}var cb=opts.onPrevNextEvent||opts.prevNextClick;if($.isFunction(cb)){cb(val>0,opts.nextSlide,els[opts.nextSlide]);}go(els,opts,1,val>=0);return false;}function buildPager(els,opts){var $p=$(opts.pager);$.each(els,function(i,o){$.fn.cycle.createPagerAnchor(i,o,$p,els,opts);});opts.updateActivePagerLink(opts.pager,opts.startingSlide,opts.activePagerClass);}$.fn.cycle.createPagerAnchor=function(i,el,$p,els,opts){var a;if($.isFunction(opts.pagerAnchorBuilder)){a=opts.pagerAnchorBuilder(i,el);debug("pagerAnchorBuilder("+i+", el) returned: "+a);}else{a='<a href="#">'+(i+1)+"</a>";}if(!a){return;}var $a=$(a);if($a.parents("body").length===0){var arr=[];if($p.length>1){$p.each(function(){var $clone=$a.clone(true);$(this).append($clone);arr.push($clone[0]);});$a=$(arr);}else{$a.appendTo($p);}}opts.pagerAnchors=opts.pagerAnchors||[];opts.pagerAnchors.push($a);$a.bind(opts.pagerEvent,function(e){e.preventDefault();opts.nextSlide=i;var p=opts.$cont[0],timeout=p.cycleTimeout;if(timeout){clearTimeout(timeout);p.cycleTimeout=0;}var cb=opts.onPagerEvent||opts.pagerClick;if($.isFunction(cb)){cb(opts.nextSlide,els[opts.nextSlide]);}go(els,opts,1,opts.currSlide<i);});if(!/^click/.test(opts.pagerEvent)&&!opts.allowPagerClickBubble){$a.bind("click.cycle",function(){return false;});}if(opts.pauseOnPagerHover){$a.hover(function(){opts.$cont[0].cyclePause++;},function(){opts.$cont[0].cyclePause--;});}};$.fn.cycle.hopsFromLast=function(opts,fwd){var hops,l=opts.lastSlide,c=opts.currSlide;if(fwd){hops=c>l?c-l:opts.slideCount-l;}else{hops=c<l?l-c:l+opts.slideCount-c;}return hops;};function clearTypeFix($slides){debug("applying clearType background-color hack");function hex(s){s=parseInt(s).toString(16);return s.length<2?"0"+s:s;}function getBg(e){for(;e&&e.nodeName.toLowerCase()!="html";e=e.parentNode){var v=$.css(e,"background-color");if(v.indexOf("rgb")>=0){var rgb=v.match(/\d+/g);return"#"+hex(rgb[0])+hex(rgb[1])+hex(rgb[2]);}if(v&&v!="transparent"){return v;}}return"#ffffff";}$slides.each(function(){$(this).css("background-color",getBg(this));});}$.fn.cycle.commonReset=function(curr,next,opts,w,h,rev){$(opts.elements).not(curr).hide();opts.cssBefore.opacity=1;opts.cssBefore.display="block";if(w!==false&&next.cycleW>0){opts.cssBefore.width=next.cycleW;}if(h!==false&&next.cycleH>0){opts.cssBefore.height=next.cycleH;}opts.cssAfter=opts.cssAfter||{};opts.cssAfter.display="none";$(curr).css("zIndex",opts.slideCount+(rev===true?1:0));$(next).css("zIndex",opts.slideCount+(rev===true?0:1));};$.fn.cycle.custom=function(curr,next,opts,cb,fwd,speedOverride){var $l=$(curr),$n=$(next);var speedIn=opts.speedIn,speedOut=opts.speedOut,easeIn=opts.easeIn,easeOut=opts.easeOut;$n.css(opts.cssBefore);if(speedOverride){if(typeof speedOverride=="number"){speedIn=speedOut=speedOverride;}else{speedIn=speedOut=1;}easeIn=easeOut=null;}var fn=function(){$n.animate(opts.animIn,speedIn,easeIn,cb);};$l.animate(opts.animOut,speedOut,easeOut,function(){if(opts.cssAfter){$l.css(opts.cssAfter);}if(!opts.sync){fn();}});if(opts.sync){fn();}};$.fn.cycle.transitions={fade:function($cont,$slides,opts){$slides.not(":eq("+opts.currSlide+")").css("opacity",0);opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);opts.cssBefore.opacity=0;});opts.animIn={opacity:1};opts.animOut={opacity:0};opts.cssBefore={top:0,left:0};}};$.fn.cycle.ver=function(){return ver;};$.fn.cycle.defaults={fx:"fade",timeout:4000,timeoutFn:null,continuous:0,speed:1000,speedIn:null,speedOut:null,next:null,prev:null,onPrevNextEvent:null,prevNextEvent:"click.cycle",pager:null,onPagerEvent:null,pagerEvent:"click.cycle",allowPagerClickBubble:false,pagerAnchorBuilder:null,before:null,after:null,end:null,easing:null,easeIn:null,easeOut:null,shuffle:null,animIn:null,animOut:null,cssBefore:null,cssAfter:null,fxFn:null,height:"auto",startingSlide:0,sync:1,random:0,fit:0,containerResize:1,pause:0,pauseOnPagerHover:0,autostop:0,autostopCount:0,delay:0,slideExpr:null,cleartype:!$.support.opacity,cleartypeNoBg:false,nowrap:0,fastOnEvent:0,randomizeEffects:1,rev:0,manualTrump:true,requeueOnImageNotLoaded:true,requeueTimeout:250,activePagerClass:"activeSlide",updateActivePagerLink:null,backwards:false};})(jQuery);
/*
 * jQuery Cycle Plugin Transition Definitions
 * This script is a plugin for the jQuery Cycle Plugin
 * Examples and documentation at: http://malsup.com/jquery/cycle/
 * Copyright (c) 2007-2010 M. Alsup
 * Version:	 2.72
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */
(function($){$.fn.cycle.transitions.none=function($cont,$slides,opts){opts.fxFn=function(curr,next,opts,after){$(next).show();$(curr).hide();after();};};$.fn.cycle.transitions.scrollUp=function($cont,$slides,opts){$cont.css("overflow","hidden");opts.before.push($.fn.cycle.commonReset);var h=$cont.height();opts.cssBefore={top:h,left:0};opts.cssFirst={top:0};opts.animIn={top:0};opts.animOut={top:-h};};$.fn.cycle.transitions.scrollDown=function($cont,$slides,opts){$cont.css("overflow","hidden");opts.before.push($.fn.cycle.commonReset);var h=$cont.height();opts.cssFirst={top:0};opts.cssBefore={top:-h,left:0};opts.animIn={top:0};opts.animOut={top:h};};$.fn.cycle.transitions.scrollLeft=function($cont,$slides,opts){$cont.css("overflow","hidden");opts.before.push($.fn.cycle.commonReset);var w=$cont.width();opts.cssFirst={left:0};opts.cssBefore={left:w,top:0};opts.animIn={left:0};opts.animOut={left:0-w};};$.fn.cycle.transitions.scrollRight=function($cont,$slides,opts){$cont.css("overflow","hidden");opts.before.push($.fn.cycle.commonReset);var w=$cont.width();opts.cssFirst={left:0};opts.cssBefore={left:-w,top:0};opts.animIn={left:0};opts.animOut={left:w};};$.fn.cycle.transitions.scrollHorz=function($cont,$slides,opts){$cont.css("overflow","hidden").width();opts.before.push(function(curr,next,opts,fwd){$.fn.cycle.commonReset(curr,next,opts);opts.cssBefore.left=fwd?(next.cycleW-1):(1-next.cycleW);opts.animOut.left=fwd?-curr.cycleW:curr.cycleW;});opts.cssFirst={left:0};opts.cssBefore={top:0};opts.animIn={left:0};opts.animOut={top:0};};$.fn.cycle.transitions.scrollVert=function($cont,$slides,opts){$cont.css("overflow","hidden");opts.before.push(function(curr,next,opts,fwd){$.fn.cycle.commonReset(curr,next,opts);opts.cssBefore.top=fwd?(1-next.cycleH):(next.cycleH-1);opts.animOut.top=fwd?curr.cycleH:-curr.cycleH;});opts.cssFirst={top:0};opts.cssBefore={left:0};opts.animIn={top:0};opts.animOut={left:0};};$.fn.cycle.transitions.slideX=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$(opts.elements).not(curr).hide();$.fn.cycle.commonReset(curr,next,opts,false,true);opts.animIn.width=next.cycleW;});opts.cssBefore={left:0,top:0,width:0};opts.animIn={width:"show"};opts.animOut={width:0};};$.fn.cycle.transitions.slideY=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$(opts.elements).not(curr).hide();$.fn.cycle.commonReset(curr,next,opts,true,false);opts.animIn.height=next.cycleH;});opts.cssBefore={left:0,top:0,height:0};opts.animIn={height:"show"};opts.animOut={height:0};};$.fn.cycle.transitions.shuffle=function($cont,$slides,opts){var i,w=$cont.css("overflow","visible").width();$slides.css({left:0,top:0});opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,true,true);});if(!opts.speedAdjusted){opts.speed=opts.speed/2;opts.speedAdjusted=true;}opts.random=0;opts.shuffle=opts.shuffle||{left:-w,top:15};opts.els=[];for(i=0;i<$slides.length;i++){opts.els.push($slides[i]);}for(i=0;i<opts.currSlide;i++){opts.els.push(opts.els.shift());}opts.fxFn=function(curr,next,opts,cb,fwd){var $el=fwd?$(curr):$(next);$(next).css(opts.cssBefore);var count=opts.slideCount;$el.animate(opts.shuffle,opts.speedIn,opts.easeIn,function(){var hops=$.fn.cycle.hopsFromLast(opts,fwd);for(var k=0;k<hops;k++){fwd?opts.els.push(opts.els.shift()):opts.els.unshift(opts.els.pop());}if(fwd){for(var i=0,len=opts.els.length;i<len;i++){$(opts.els[i]).css("z-index",len-i+count);}}else{var z=$(curr).css("z-index");$el.css("z-index",parseInt(z)+1+count);}$el.animate({left:0,top:0},opts.speedOut,opts.easeOut,function(){$(fwd?this:curr).hide();if(cb){cb();}});});};opts.cssBefore={display:"block",opacity:1,top:0,left:0};};$.fn.cycle.transitions.turnUp=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,false);opts.cssBefore.top=next.cycleH;opts.animIn.height=next.cycleH;});opts.cssFirst={top:0};opts.cssBefore={left:0,height:0};opts.animIn={top:0};opts.animOut={height:0};};$.fn.cycle.transitions.turnDown=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,false);opts.animIn.height=next.cycleH;opts.animOut.top=curr.cycleH;});opts.cssFirst={top:0};opts.cssBefore={left:0,top:0,height:0};opts.animOut={height:0};};$.fn.cycle.transitions.turnLeft=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,true);opts.cssBefore.left=next.cycleW;opts.animIn.width=next.cycleW;});opts.cssBefore={top:0,width:0};opts.animIn={left:0};opts.animOut={width:0};};$.fn.cycle.transitions.turnRight=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,true);opts.animIn.width=next.cycleW;opts.animOut.left=curr.cycleW;});opts.cssBefore={top:0,left:0,width:0};opts.animIn={left:0};opts.animOut={width:0};};$.fn.cycle.transitions.zoom=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,false,true);opts.cssBefore.top=next.cycleH/2;opts.cssBefore.left=next.cycleW/2;opts.animIn={top:0,left:0,width:next.cycleW,height:next.cycleH};opts.animOut={width:0,height:0,top:curr.cycleH/2,left:curr.cycleW/2};});opts.cssFirst={top:0,left:0};opts.cssBefore={width:0,height:0};};$.fn.cycle.transitions.fadeZoom=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,false);opts.cssBefore.left=next.cycleW/2;opts.cssBefore.top=next.cycleH/2;opts.animIn={top:0,left:0,width:next.cycleW,height:next.cycleH};});opts.cssBefore={width:0,height:0};opts.animOut={opacity:0};};$.fn.cycle.transitions.blindX=function($cont,$slides,opts){var w=$cont.css("overflow","hidden").width();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);opts.animIn.width=next.cycleW;opts.animOut.left=curr.cycleW;});opts.cssBefore={left:w,top:0};opts.animIn={left:0};opts.animOut={left:w};};$.fn.cycle.transitions.blindY=function($cont,$slides,opts){var h=$cont.css("overflow","hidden").height();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);opts.animIn.height=next.cycleH;opts.animOut.top=curr.cycleH;});opts.cssBefore={top:h,left:0};opts.animIn={top:0};opts.animOut={top:h};};$.fn.cycle.transitions.blindZ=function($cont,$slides,opts){var h=$cont.css("overflow","hidden").height();var w=$cont.width();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);opts.animIn.height=next.cycleH;opts.animOut.top=curr.cycleH;});opts.cssBefore={top:h,left:w};opts.animIn={top:0,left:0};opts.animOut={top:h,left:w};};$.fn.cycle.transitions.growX=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,true);opts.cssBefore.left=this.cycleW/2;opts.animIn={left:0,width:this.cycleW};opts.animOut={left:0};});opts.cssBefore={width:0,top:0};};$.fn.cycle.transitions.growY=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,false);opts.cssBefore.top=this.cycleH/2;opts.animIn={top:0,height:this.cycleH};opts.animOut={top:0};});opts.cssBefore={height:0,left:0};};$.fn.cycle.transitions.curtainX=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,true,true);opts.cssBefore.left=next.cycleW/2;opts.animIn={left:0,width:this.cycleW};opts.animOut={left:curr.cycleW/2,width:0};});opts.cssBefore={top:0,width:0};};$.fn.cycle.transitions.curtainY=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,false,true);opts.cssBefore.top=next.cycleH/2;opts.animIn={top:0,height:next.cycleH};opts.animOut={top:curr.cycleH/2,height:0};});opts.cssBefore={left:0,height:0};};$.fn.cycle.transitions.cover=function($cont,$slides,opts){var d=opts.direction||"left";var w=$cont.css("overflow","hidden").width();var h=$cont.height();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);if(d=="right"){opts.cssBefore.left=-w;}else{if(d=="up"){opts.cssBefore.top=h;}else{if(d=="down"){opts.cssBefore.top=-h;}else{opts.cssBefore.left=w;}}}});opts.animIn={left:0,top:0};opts.animOut={opacity:1};opts.cssBefore={top:0,left:0};};$.fn.cycle.transitions.uncover=function($cont,$slides,opts){var d=opts.direction||"left";var w=$cont.css("overflow","hidden").width();var h=$cont.height();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,true,true);if(d=="right"){opts.animOut.left=w;}else{if(d=="up"){opts.animOut.top=-h;}else{if(d=="down"){opts.animOut.top=h;}else{opts.animOut.left=-w;}}}});opts.animIn={left:0,top:0};opts.animOut={opacity:1};opts.cssBefore={top:0,left:0};};$.fn.cycle.transitions.toss=function($cont,$slides,opts){var w=$cont.css("overflow","visible").width();var h=$cont.height();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,true,true);if(!opts.animOut.left&&!opts.animOut.top){opts.animOut={left:w*2,top:-h/2,opacity:0};}else{opts.animOut.opacity=0;}});opts.cssBefore={left:0,top:0};opts.animIn={left:0};};$.fn.cycle.transitions.wipe=function($cont,$slides,opts){var w=$cont.css("overflow","hidden").width();var h=$cont.height();opts.cssBefore=opts.cssBefore||{};var clip;if(opts.clip){if(/l2r/.test(opts.clip)){clip="rect(0px 0px "+h+"px 0px)";}else{if(/r2l/.test(opts.clip)){clip="rect(0px "+w+"px "+h+"px "+w+"px)";}else{if(/t2b/.test(opts.clip)){clip="rect(0px "+w+"px 0px 0px)";}else{if(/b2t/.test(opts.clip)){clip="rect("+h+"px "+w+"px "+h+"px 0px)";}else{if(/zoom/.test(opts.clip)){var top=parseInt(h/2);var left=parseInt(w/2);clip="rect("+top+"px "+left+"px "+top+"px "+left+"px)";}}}}}}opts.cssBefore.clip=opts.cssBefore.clip||clip||"rect(0px 0px 0px 0px)";var d=opts.cssBefore.clip.match(/(\d+)/g);var t=parseInt(d[0]),r=parseInt(d[1]),b=parseInt(d[2]),l=parseInt(d[3]);opts.before.push(function(curr,next,opts){if(curr==next){return;}var $curr=$(curr),$next=$(next);$.fn.cycle.commonReset(curr,next,opts,true,true,false);opts.cssAfter.display="block";var step=1,count=parseInt((opts.speedIn/13))-1;(function f(){var tt=t?t-parseInt(step*(t/count)):0;var ll=l?l-parseInt(step*(l/count)):0;var bb=b<h?b+parseInt(step*((h-b)/count||1)):h;var rr=r<w?r+parseInt(step*((w-r)/count||1)):w;$next.css({clip:"rect("+tt+"px "+rr+"px "+bb+"px "+ll+"px)"});(step++<=count)?setTimeout(f,13):$curr.css("display","none");})();});opts.cssBefore={display:"block",opacity:1,top:0,left:0};opts.animIn={left:0};opts.animOut={left:0};};})(jQuery);;
// $Id: views_slideshow.js,v 1.1.2.1.2.39 2010/07/01 03:29:08 redndahead Exp $

/**
 *  @file
 *  A simple jQuery SingleFrame Div Slideshow Rotator.
 */

/**
 * This will set our initial behavior, by starting up each individual slideshow.
 */
Drupal.behaviors.viewsSlideshowSingleFrame = function (context) {
  $('.views_slideshow_singleframe_main:not(.viewsSlideshowSingleFrame-processed)', context).addClass('viewsSlideshowSingleFrame-processed').each(function() {
    var fullId = '#' + $(this).attr('id');
    var settings = Drupal.settings.viewsSlideshowSingleFrame[fullId];
    settings.targetId = '#' + $(fullId + " :first").attr('id');
    settings.paused = false;

    settings.opts = {
      speed:settings.speed,
      timeout:parseInt(settings.timeout),
      delay:parseInt(settings.delay),
      sync:settings.sync==1,
      random:settings.random==1,
      pause:false,
      allowPagerClickBubble:(settings.pager_hover==1 || settings.pager_click_to_page),
      prev:(settings.controls > 0)?'#views_slideshow_singleframe_prev_' + settings.vss_id:null,
      next:(settings.controls > 0)?'#views_slideshow_singleframe_next_' + settings.vss_id:null,
      pager:(settings.pager > 0)?'#views_slideshow_singleframe_pager_' + settings.vss_id:null,
      nowrap:parseInt(settings.nowrap),
      pagerAnchorBuilder: function(idx, slide) {
        var classes = 'pager-item pager-num-' + (idx+1);
        if (idx == 0) {
          classes += ' first';
        }
        if ($(slide).siblings().length == idx) {
          classes += ' last';
        }

        if (idx % 2) {
          classes += ' odd';
        }
        else {
          classes += ' even';
        }
        
        var theme = 'viewsSlideshowPager' + settings.pager_type;
        return Drupal.theme.prototype[theme] ? Drupal.theme(theme, classes, idx, slide, settings) : '';
      },
      after:function(curr, next, opts) {
        // Used for Image Counter.
        if (settings.image_count) {
          $('#views_slideshow_singleframe_image_count_' + settings.vss_id + ' span.num').html(opts.currSlide + 1);
          $('#views_slideshow_singleframe_image_count_' + settings.vss_id + ' span.total').html(opts.slideCount);
        }
      },
      before:function(curr, next, opts) {
        // Remember last slide.
        if (settings.remember_slide) {
          createCookie(settings.vss_id, opts.currSlide + 1, settings.remember_slide_days);
        }

        // Make variable height.
        if (settings.fixed_height == 0) {
          //get the height of the current slide
          var $ht = $(this).height();
          //set the container's height to that of the current slide
          $(this).parent().animate({height: $ht});
        }
      },
      cleartype:(settings.ie.cleartype == 'true')? true : false,
      cleartypeNoBg:(settings.ie.cleartypenobg == 'true')? true : false
    }
    
    // Set the starting slide if we are supposed to remember the slide
    if (settings.remember_slide) {
      var startSlide = readCookie(settings.vss_id);
      if (startSlide == null) {
        startSlide = 0;
      }
      settings.opts.startingSlide =  startSlide;
    }

    if (settings.pager_hover == 1) {
      settings.opts.pagerEvent = 'mouseover';
      settings.opts.pauseOnPagerHover = true;
    }

    if (settings.effect == 'none') {
      settings.opts.speed = 1;
    }
    else {
      settings.opts.fx = settings.effect;
    }

    // Pause on hover.
    if (settings.pause == 1) {
      $('#views_slideshow_singleframe_teaser_section_' + settings.vss_id).hover(function() {
        $(settings.targetId).cycle('pause');
      }, function() {
        if (settings.paused == false) {
          $(settings.targetId).cycle('resume');
        }
      });
    }

    // Pause on clicking of the slide.
    if (settings.pause_on_click == 1) {
      $('#views_slideshow_singleframe_teaser_section_' + settings.vss_id).click(function() { 
        viewsSlideshowSingleFramePause(settings);
      });
    }

    // Add additional settings.
		if (settings.advanced != "\n") {
      var advanced = settings.advanced.split("\n");
      for (i=0; i<advanced.length; i++) {
        var prop = '';
        var value = '';
        var property = advanced[i].split(":");
        for (j=0; j<property.length; j++) {
          if (j == 0) {
            prop = property[j];
          }
          else if (j == 1) {
            value = property[j];
          }
          else {
            value += ":" + property[j];
          }
        }

        // Need to evaluate so true, false and numerics aren't a string.
        if (value == 'true' || value == 'false' || IsNumeric(value)) {
          value = eval(value);
        }
        else {
          // Parse strings into functions.
          var func = value.match(/function\s*\((.*?)\)\s*\{(.*)\}/i);
          if (func) {
            value = new Function(func[1].match(/(\w+)/g), func[2]);
          }
        }
	
        // Call both functions if prop was set previously.
        if (typeof(value) == "function" && prop in settings.opts) {
          var callboth = function(before_func, new_func) {
            return function() {
              before_func.apply(null, arguments);
              new_func.apply(null, arguments);
            };
          };
          settings.opts[prop] = callboth(settings.opts[prop], value);
        }
        else {
          settings.opts[prop] = value;
        }
      }
    }
    
    $(settings.targetId).cycle(settings.opts);

    // Start Paused
    if (settings.start_paused) {
      viewsSlideshowSingleFramePause(settings);
    }
    
    // Pause if hidden.
    if (settings.pause_when_hidden) {
      var checkPause = function(settings) {
        // If the slideshow is visible and it is paused then resume.
        // otherwise if the slideshow is not visible and it is not paused then
        // pause it.
        var visible = viewsSlideshowSingleFrameIsVisible(settings.targetId, settings.pause_when_hidden_type, settings.amount_allowed_visible);
        if (visible && settings.paused) {
          viewsSlideshowSingleFrameResume(settings);
        }
        else if (!visible && !settings.paused) {
          viewsSlideshowSingleFramePause(settings);
        }
      }
     
      // Check when scrolled.
      $(window).scroll(function() {
       checkPause(settings);
      });
      
      // Check when the window is resized.
      $(window).resize(function() {
        checkPause(settings);
      });
    }

    // Show image count for people who have js enabled.
    $('#views_slideshow_singleframe_image_count_' + settings.vss_id).show();

    if (settings.controls > 0) {
      // Show controls for people who have js enabled browsers.
      $('#views_slideshow_singleframe_controls_' + settings.vss_id).show();
      
      $('#views_slideshow_singleframe_playpause_' + settings.vss_id).click(function(e) {
      	if (settings.paused) {
      	  viewsSlideshowSingleFrameResume(settings);
      	}
      	else {
      	  viewsSlideshowSingleFramePause(settings);
      	}
        e.preventDefault();
      });
    }
  });
}

// Pause the slideshow 
viewsSlideshowSingleFramePause = function (settings) {
  //make Resume translatable
  var resume = Drupal.t('Resume');

  $(settings.targetId).cycle('pause');
  if (settings.controls > 0) {
    $('#views_slideshow_singleframe_playpause_' + settings.vss_id)
      .addClass('views_slideshow_singleframe_play')
      .addClass('views_slideshow_play')
      .removeClass('views_slideshow_singleframe_pause')
      .removeClass('views_slideshow_pause')
      .text(resume);
  }
  settings.paused = true;
}

// Resume the slideshow
viewsSlideshowSingleFrameResume = function (settings) {
  $(settings.targetId).cycle('resume');
  if (settings.controls > 0) {
    $('#views_slideshow_singleframe_playpause_' + settings.vss_id)
      .addClass('views_slideshow_singleframe_pause')
      .addClass('views_slideshow_pause')
      .removeClass('views_slideshow_singleframe_play')
      .removeClass('views_slideshow_play')
      .text('Pause');
  }
  settings.paused = false;
}

Drupal.theme.prototype.viewsSlideshowPagerThumbnails = function (classes, idx, slide, settings) {
  var href = '#';
  if (settings.pager_click_to_page) {
    href = $(slide).find('a').attr('href');
  }
  return '<div class="' + classes + '"><a href="' + href + '"><img src="' + $(slide).find('img').attr('src') + '" /></a></div>';
}

Drupal.theme.prototype.viewsSlideshowPagerNumbered = function (classes, idx, slide, settings) {
  var href = '#';
  if (settings.pager_click_to_page) {
    href = $(slide).find('a').attr('href');
  }
  return '<div class="' + classes + '"><a href="' + href + '">' + (idx+1) + '</a></div>';
}

// Verify that the value is a number.
function IsNumeric(sText) {
  var ValidChars = "0123456789";
  var IsNumber=true;
  var Char;

  for (var i=0; i < sText.length && IsNumber == true; i++) { 
    Char = sText.charAt(i); 
    if (ValidChars.indexOf(Char) == -1) {
      IsNumber = false;
    }
  }
  return IsNumber;
}

/**
 * Cookie Handling Functions
 */
function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else {
    var expires = "";
  }
  document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length,c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  createCookie(name,"",-1);
}

/**
 * Checks to see if the slide is visible enough.
 * elem = element to check.
 * type = The way to calculate how much is visible.
 * amountVisible = amount that should be visible. Either in percent or px. If
 *                it's not defined then all of the slide must be visible.
 *
 * Returns true or false
 */
function viewsSlideshowSingleFrameIsVisible(elem, type, amountVisible) {
  // Get the top and bottom of the window;
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var docViewLeft = $(window).scrollLeft();
  var docViewRight = docViewLeft + $(window).width();

  // Get the top, bottom, and height of the slide;
  var elemTop = $(elem).offset().top;
  var elemHeight = $(elem).height();
  var elemBottom = elemTop + elemHeight;
  var elemLeft = $(elem).offset().left;
  var elemWidth = $(elem).width();
  var elemRight = elemLeft + elemWidth;
  var elemArea = elemHeight * elemWidth;
  
  // Calculate what's hiding in the slide.
  var missingLeft = 0;
  var missingRight = 0;
  var missingTop = 0;
  var missingBottom = 0;
  
  // Find out how much of the slide is missing from the left.
  if (elemLeft < docViewLeft) {
    missingLeft = docViewLeft - elemLeft;
  }

  // Find out how much of the slide is missing from the right.
  if (elemRight > docViewRight) {
    missingRight = elemRight - docViewRight;
  }
  
  // Find out how much of the slide is missing from the top.
  if (elemTop < docViewTop) {
    missingTop = docViewTop - elemTop;
  }

  // Find out how much of the slide is missing from the bottom.
  if (elemBottom > docViewBottom) {
    missingBottom = elemBottom - docViewBottom;
  }
  
  // If there is no amountVisible defined then check to see if the whole slide
  // is visible.
  if (type == 'full') {
    return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom)
    && (elemBottom <= docViewBottom) &&  (elemTop >= docViewTop)
    && (elemLeft >= docViewLeft) && (elemRight <= docViewRight)
    && (elemLeft <= docViewRight) && (elemRight >= docViewLeft));
  }
  else if(type == 'vertical') {
    var verticalShowing = elemHeight - missingTop - missingBottom;
    
    // If user specified a percentage then find out if the current shown percent
    // is larger than the allowed percent.
    // Otherwise check to see if the amount of px shown is larger than the
    // allotted amount.
    if (amountVisible.indexOf('%')) {
      return (((verticalShowing/elemHeight)*100) >= parseInt(amountVisible));
    }
    else {
      return (verticalShowing >= parseInt(amountVisible));
    }
  }
  else if(type == 'horizontal') {
    var horizontalShowing = elemWidth - missingLeft - missingRight;
    
    // If user specified a percentage then find out if the current shown percent
    // is larger than the allowed percent.
    // Otherwise check to see if the amount of px shown is larger than the
    // allotted amount.
    if (amountVisible.indexOf('%')) {
      return (((horizontalShowing/elemWidth)*100) >= parseInt(amountVisible));
    }
    else {
      return (horizontalShowing >= parseInt(amountVisible));
    }
  }
  else if(type == 'area') {
    var areaShowing = (elemWidth - missingLeft - missingRight) * (elemHeight - missingTop - missingBottom);
    
    // If user specified a percentage then find out if the current shown percent
    // is larger than the allowed percent.
    // Otherwise check to see if the amount of px shown is larger than the
    // allotted amount.
    if (amountVisible.indexOf('%')) {
      return (((areaShowing/elemArea)*100) >= parseInt(amountVisible));
    }
    else {
      return (areaShowing >= parseInt(amountVisible));
    }
  }
}

;
// $Id: views_slideshow.js,v 1.1.2.2.2.35 2010/07/01 03:29:08 redndahead Exp $

/**
 * @file
 * A simple jQuery ThumbnailHover Div Slideshow Rotator.
 */

/**
 * This will set our initial behavior, by starting up each individual slideshow.
 */
Drupal.behaviors.viewsSlideshowThumbnailHover = function (context) {
  $('.views_slideshow_thumbnailhover_main:not(.viewsSlideshowThumbnailHover-processed)', context).addClass('viewsSlideshowThumbnailHover-processed').each(function() {
    var fullId = '#' + $(this).attr('id');
    var settings = Drupal.settings.viewsSlideshowThumbnailHover[fullId];
    settings.targetId = '#' + $(fullId + " :first").attr('id');
		settings.paused = false;
		
    settings.opts = {
      speed:settings.speed,
      timeout:parseInt(settings.timeout),
      delay:parseInt(settings.delay),
      sync:settings.sync==1,
      random:settings.random==1,
      pause:false,
      allowPagerClickBubble:(settings.pager_event=='click')? false : true,
      pager:(settings.pager_event == 'hoverIntent') ? null : '#views_slideshow_breakout_teasers_' + settings.vss_id,
      nowrap:parseInt(settings.nowrap),
      pagerAnchorBuilder:(settings.pager_event == 'hoverIntent') ? null : function(idx, slide) { 
        return '#views_slideshow_thumbnailhover_div_breakout_teaser_' + settings.vss_id + '_' + idx; 
      },
      after:function(curr, next, opts) {
        // Used for Image Counter.
        if (settings.image_count) {
          $('#views_slideshow_thumbnailhover_image_count_' + settings.vss_id + ' span.num').html(opts.currSlide + 1);
          $('#views_slideshow_thumbnailhover_image_count_' + settings.vss_id + ' span.total').html(opts.slideCount);
        }
      },
      before:function(current, next, opts) {
        // Remember last slide.
        if (settings.remember_slide) {
          createCookie(settings.view_id, opts.currSlide + 1, settings.remember_slide_days);
        }

        // Make variable height.
        if (settings.fixed_height == 0) {
          //get the height of the current slide
          var $ht = $(this).height();
          //set the container's height to that of the current slide
          $(this).parent().animate({height: $ht});
        }
        
        var currId = (currId=$(current).attr('id')).substring(currId.lastIndexOf('_')+1)
        var nextId = (nextId=$(next).attr('id')).substring(nextId.lastIndexOf('_')+1)
        $('#views_slideshow_thumbnailhover_div_breakout_teaser_' + settings.vss_id + '_' + currId).removeClass('activeSlide');
        $('#views_slideshow_thumbnailhover_div_breakout_teaser_' + settings.vss_id + '_' + nextId).addClass('activeSlide');
      },
      pagerEvent: (settings.pager_event == 'hoverIntent') ? null : settings.pager_event,
      prev:(settings.controls > 0)?'#views_slideshow_thumbnailhover_prev_' + settings.vss_id:null,
      next:(settings.controls > 0)?'#views_slideshow_thumbnailhover_next_' + settings.vss_id:null,
      cleartype:(settings.ie.cleartype == 'true')? true : false,
      cleartypeNoBg:(settings.ie.cleartypenobg == 'true')? true : false
    };

    // Set the starting slide if we are supposed to remember the slide
    if (settings.remember_slide) {
      var startSlide = readCookie(settings.view_id);
      if (startSlide == null) {
        startSlide = 0;
      }
      settings.opts.startingSlide =  startSlide;
    }

    if (settings.effect == 'none') {
      settings.opts.speed = 1;
    }
    else {
      settings.opts.fx = settings.effect;
    }

    // Pause on hover.
    if (settings.pause == 1) {
      $('#views_slideshow_thumbnailhover_teaser_section_' + settings.vss_id).hover(function() {
        $(settings.targetId).cycle('pause');
      }, function() {
        if (settings.paused == false) {
          $(settings.targetId).cycle('resume');
        }
      });
    }

    // Pause on clicking of the slide.
    if (settings.pause_on_click == 1) {
      $('#views_slideshow_thumbnailhover_teaser_section_' + settings.vss_id).click(function() { 
        viewsSlideshowThumbnailHoverPause(settings);
      });
    }
    
    // Add additional settings.
		if (settings.advanced != "\n") {
      var advanced = settings.advanced.split("\n");
      for (i=0; i<advanced.length; i++) {
        var prop = '';
        var value = '';
        var property = advanced[i].split(":");
        for (j=0; j<property.length; j++) {
          if (j == 0) {
            prop = property[j];
          }
          else if (j == 1) {
            value = property[j];
          }
          else {
            value += ":" + property[j];
          }
        }

        // Need to evaluate so true, false and numerics aren't a string.
        if (value == 'true' || value == 'false' || IsNumeric(value)) {
          value = eval(value);
        }
        else {
          // Parse strings into functions.
          var func = value.match(/function\s*\((.*?)\)\s*\{(.*)\}/i);
          if (func) {
            value = new Function(func[1].match(/(\w+)/g), func[2]);
          }
        }
	
        // Call both functions if prop was set previously.
        if (typeof(value) == "function" && prop in settings.opts) {
          var callboth = function(before_func, new_func) {
            return function() {
              before_func.apply(null, arguments);
              new_func.apply(null, arguments);
            };
          };
          settings.opts[prop] = callboth(settings.opts[prop], value);
        }
        else {
          settings.opts[prop] = value;
        }
      }
    }

    $(settings.targetId).cycle(settings.opts);

    // Start Paused
    if (settings.start_paused) {
      viewsSlideshowThumbnailHoverPause(settings);
    }
    
    // Pause if hidden.
    if (settings.pause_when_hidden) {
      var checkPause = function(settings) {
        // If the slideshow is visible and it is paused then resume.
        // otherwise if the slideshow is not visible and it is not paused then
        // pause it.
        var visible = viewsSlideshowThumbnailHoverIsVisible(settings.targetId, settings.pause_when_hidden_type, settings.amount_allowed_visible);
        if (visible && settings.paused) {
          viewsSlideshowThumbnailHoverResume(settings);
        }
        else if (!visible && !settings.paused) {
          viewsSlideshowThumbnailHoverPause(settings);
        }
      }
     
      // Check when scrolled.
      $(window).scroll(function() {
       checkPause(settings);
      });
      
      // Check when window is resized.
      $(window).resize(function() {
        checkPause(settings);
      });
    }

    // Show image count for people who have js enabled.
    $('#views_slideshow_thumbnailhover_image_count_' + settings.vss_id).show();
    
    if (settings.pager_event == 'hoverIntent') {
      $('#views_slideshow_thumbnailhover_breakout_teasers_' + settings.vss_id + ' .views_slideshow_thumbnailhover_div_breakout_teaser').each(function(i,obj) {
        $(obj).hoverIntent(
          function() {
            $('.views_slideshow_thumbnailhover_div_breakout_teaser').removeClass('activeSlide');
            var id = $(this).attr('id');
            id = parseInt(id.substring(id.lastIndexOf('_')+1));
            $(settings.targetId).cycle(id);
            $('#views_slideshow_thumbnailhover_div_breakout_teaser_' + settings.vss_id + '_' + id).addClass('activeSlide');
            $(settings.targetId).cycle('stop');
          },
          function() {
            var id = $(this).attr('id');
            settings.opts.startingSlide = parseInt(id.substring(id.lastIndexOf('_')+1));
            $(settings.targetId).cycle(settings.opts);
          }
        );
      });
    }

    if (settings.controls > 0) {
      // Show controls for people who have js enabled browsers.
      $('#views_slideshow_thumbnailhover_controls_' + settings.vss_id).show();
      
      $('#views_slideshow_thumbnailhover_playpause_' + settings.vss_id).click(function(e) {
        if (settings.paused) {
          viewsSlideshowThumbnailHoverResume(settings);
        }
        else {
          viewsSlideshowThumbnailHoverPause(settings);
        }
        e.preventDefault();
      });
    }
  });
}

// Pause the slideshow 
viewsSlideshowThumbnailHoverPause = function (settings) {
  //make Resume translatable
  var resume = Drupal.t('Resume');

  $(settings.targetId).cycle('pause');
  if (settings.controls > 0) {
    $('#views_slideshow_thumbnailhover_playpause_' + settings.vss_id)
      .addClass('views_slideshow_thumbnailhover_play')
      .addClass('views_slideshow_play')
      .removeClass('views_slideshow_thumbnailhover_pause')
      .removeClass('views_slideshow_pause')
      .text(resume);
  }
  settings.paused = true;
}

// Resume the slideshow
viewsSlideshowThumbnailHoverResume = function (settings) {
  $(settings.targetId).cycle('resume');
  if (settings.controls > 0) {
    $('#views_slideshow_thumbnailhover_playpause_' + settings.vss_id)
      .addClass('views_slideshow_thumbnailhover_pause')
      .addClass('views_slideshow_pause')
      .removeClass('views_slideshow_thumbnailhover_play')
      .removeClass('views_slideshow_play')
      .text('Pause');
  }
  settings.paused = false;
}

// Verify that the value is a number.
function IsNumeric(sText) {
  var ValidChars = "0123456789";
  var IsNumber=true;
  var Char;

  for (var i=0; i < sText.length && IsNumber == true; i++) { 
    Char = sText.charAt(i); 
    if (ValidChars.indexOf(Char) == -1) {
      IsNumber = false;
    }
  }
  return IsNumber;
}

/**
 * Cookie Handling Functions
 */
function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else {
    var expires = "";
  }
  document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length,c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  createCookie(name,"",-1);
}

/**
 * Checks to see if the slide is visible enough.
 * elem = element to check.
 * amountVisible = amount that should be visible. Either in percent or px. If
 *                it's not defined then all of the slide must be visible.
 *
 * Returns true or false
 */
function viewsSlideshowThumbnailHoverIsVisible(elem, type, amountVisible) {
  // Get the top and bottom of the window;
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var docViewLeft = $(window).scrollLeft();
  var docViewRight = docViewLeft + $(window).width();

  // Get the top, bottom, and height of the slide;
  var elemTop = $(elem).offset().top;
  var elemHeight = $(elem).height();
  var elemBottom = elemTop + elemHeight;
  var elemLeft = $(elem).offset().left;
  var elemWidth = $(elem).width();
  var elemRight = elemLeft + elemWidth;
  var elemArea = elemHeight * elemWidth;
  
  // Calculate what's hiding in the slide.
  var missingLeft = 0;
  var missingRight = 0;
  var missingTop = 0;
  var missingBottom = 0;
  
  // Find out how much of the slide is missing from the left.
  if (elemLeft < docViewLeft) {
    missingLeft = docViewLeft - elemLeft;
  }

  // Find out how much of the slide is missing from the right.
  if (elemRight > docViewRight) {
    missingRight = elemRight - docViewRight;
  }
  
  // Find out how much of the slide is missing from the top.
  if (elemTop < docViewTop) {
    missingTop = docViewTop - elemTop;
  }

  // Find out how much of the slide is missing from the bottom.
  if (elemBottom > docViewBottom) {
    missingBottom = elemBottom - docViewBottom;
  }
  
  // If there is no amountVisible defined then check to see if the whole slide
  // is visible.
  if (type == 'full') {
    return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom)
    && (elemBottom <= docViewBottom) &&  (elemTop >= docViewTop)
    && (elemLeft >= docViewLeft) && (elemRight <= docViewRight)
    && (elemLeft <= docViewRight) && (elemRight >= docViewLeft));
  }
  else if(type == 'vertical') {
    var verticalShowing = elemHeight - missingTop - missingBottom;
    
    // If user specified a percentage then find out if the current shown percent
    // is larger than the allowed percent.
    // Otherwise check to see if the amount of px shown is larger than the
    // allotted amount.
    if (amountVisible.indexOf('%')) {
      return (((verticalShowing/elemHeight)*100) >= parseInt(amountVisible));
    }
    else {
      return (verticalShowing >= parseInt(amountVisible));
    }
  }
  else if(type == 'horizontal') {
    var horizontalShowing = elemWidth - missingLeft - missingRight;
    
    // If user specified a percentage then find out if the current shown percent
    // is larger than the allowed percent.
    // Otherwise check to see if the amount of px shown is larger than the
    // allotted amount.
    if (amountVisible.indexOf('%')) {
      return (((horizontalShowing/elemWidth)*100) >= parseInt(amountVisible));
    }
    else {
      return (horizontalShowing >= parseInt(amountVisible));
    }
  }
  else if(type == 'area') {
    var areaShowing = (elemWidth - missingLeft - missingRight) * (elemHeight - missingTop - missingBottom);
    
    // If user specified a percentage then find out if the current shown percent
    // is larger than the allowed percent.
    // Otherwise check to see if the amount of px shown is larger than the
    // allotted amount.
    if (amountVisible.indexOf('%')) {
      return (((areaShowing/elemArea)*100) >= parseInt(amountVisible));
    }
    else {
      return (areaShowing >= parseInt(amountVisible));
    }
  }
}
;
/*
 * Copyright (c) 2009 Simo Kinnunen.
 * Licensed under the MIT license.
 *
 * @version 1.09i
 */
var Cufon=(function(){var m=function(){return m.replace.apply(null,arguments)};var x=m.DOM={ready:(function(){var C=false,E={loaded:1,complete:1};var B=[],D=function(){if(C){return}C=true;for(var F;F=B.shift();F()){}};if(document.addEventListener){document.addEventListener("DOMContentLoaded",D,false);window.addEventListener("pageshow",D,false)}if(!window.opera&&document.readyState){(function(){E[document.readyState]?D():setTimeout(arguments.callee,10)})()}if(document.readyState&&document.createStyleSheet){(function(){try{document.body.doScroll("left");D()}catch(F){setTimeout(arguments.callee,1)}})()}q(window,"load",D);return function(F){if(!arguments.length){D()}else{C?F():B.push(F)}}})(),root:function(){return document.documentElement||document.body}};var n=m.CSS={Size:function(C,B){this.value=parseFloat(C);this.unit=String(C).match(/[a-z%]*$/)[0]||"px";this.convert=function(D){return D/B*this.value};this.convertFrom=function(D){return D/this.value*B};this.toString=function(){return this.value+this.unit}},addClass:function(C,B){var D=C.className;C.className=D+(D&&" ")+B;return C},color:j(function(C){var B={};B.color=C.replace(/^rgba\((.*?),\s*([\d.]+)\)/,function(E,D,F){B.opacity=parseFloat(F);return"rgb("+D+")"});return B}),fontStretch:j(function(B){if(typeof B=="number"){return B}if(/%$/.test(B)){return parseFloat(B)/100}return{"ultra-condensed":0.5,"extra-condensed":0.625,condensed:0.75,"semi-condensed":0.875,"semi-expanded":1.125,expanded:1.25,"extra-expanded":1.5,"ultra-expanded":2}[B]||1}),getStyle:function(C){var B=document.defaultView;if(B&&B.getComputedStyle){return new a(B.getComputedStyle(C,null))}if(C.currentStyle){return new a(C.currentStyle)}return new a(C.style)},gradient:j(function(F){var G={id:F,type:F.match(/^-([a-z]+)-gradient\(/)[1],stops:[]},C=F.substr(F.indexOf("(")).match(/([\d.]+=)?(#[a-f0-9]+|[a-z]+\(.*?\)|[a-z]+)/ig);for(var E=0,B=C.length,D;E<B;++E){D=C[E].split("=",2).reverse();G.stops.push([D[1]||E/(B-1),D[0]])}return G}),quotedList:j(function(E){var D=[],C=/\s*((["'])([\s\S]*?[^\\])\2|[^,]+)\s*/g,B;while(B=C.exec(E)){D.push(B[3]||B[1])}return D}),recognizesMedia:j(function(G){var E=document.createElement("style"),D,C,B;E.type="text/css";E.media=G;try{E.appendChild(document.createTextNode("/**/"))}catch(F){}C=g("head")[0];C.insertBefore(E,C.firstChild);D=(E.sheet||E.styleSheet);B=D&&!D.disabled;C.removeChild(E);return B}),removeClass:function(D,C){var B=RegExp("(?:^|\\s+)"+C+"(?=\\s|$)","g");D.className=D.className.replace(B,"");return D},supports:function(D,C){var B=document.createElement("span").style;if(B[D]===undefined){return false}B[D]=C;return B[D]===C},textAlign:function(E,D,B,C){if(D.get("textAlign")=="right"){if(B>0){E=" "+E}}else{if(B<C-1){E+=" "}}return E},textShadow:j(function(F){if(F=="none"){return null}var E=[],G={},B,C=0;var D=/(#[a-f0-9]+|[a-z]+\(.*?\)|[a-z]+)|(-?[\d.]+[a-z%]*)|,/ig;while(B=D.exec(F)){if(B[0]==","){E.push(G);G={};C=0}else{if(B[1]){G.color=B[1]}else{G[["offX","offY","blur"][C++]]=B[2]}}}E.push(G);return E}),textTransform:(function(){var B={uppercase:function(C){return C.toUpperCase()},lowercase:function(C){return C.toLowerCase()},capitalize:function(C){return C.replace(/\b./g,function(D){return D.toUpperCase()})}};return function(E,D){var C=B[D.get("textTransform")];return C?C(E):E}})(),whiteSpace:(function(){var D={inline:1,"inline-block":1,"run-in":1};var C=/^\s+/,B=/\s+$/;return function(H,F,G,E){if(E){if(E.nodeName.toLowerCase()=="br"){H=H.replace(C,"")}}if(D[F.get("display")]){return H}if(!G.previousSibling){H=H.replace(C,"")}if(!G.nextSibling){H=H.replace(B,"")}return H}})()};n.ready=(function(){var B=!n.recognizesMedia("all"),E=false;var D=[],H=function(){B=true;for(var K;K=D.shift();K()){}};var I=g("link"),J=g("style");function C(K){return K.disabled||G(K.sheet,K.media||"screen")}function G(M,P){if(!n.recognizesMedia(P||"all")){return true}if(!M||M.disabled){return false}try{var Q=M.cssRules,O;if(Q){search:for(var L=0,K=Q.length;O=Q[L],L<K;++L){switch(O.type){case 2:break;case 3:if(!G(O.styleSheet,O.media.mediaText)){return false}break;default:break search}}}}catch(N){}return true}function F(){if(document.createStyleSheet){return true}var L,K;for(K=0;L=I[K];++K){if(L.rel.toLowerCase()=="stylesheet"&&!C(L)){return false}}for(K=0;L=J[K];++K){if(!C(L)){return false}}return true}x.ready(function(){if(!E){E=n.getStyle(document.body).isUsable()}if(B||(E&&F())){H()}else{setTimeout(arguments.callee,10)}});return function(K){if(B){K()}else{D.push(K)}}})();function s(D){var C=this.face=D.face,B={"\u0020":1,"\u00a0":1,"\u3000":1};this.glyphs=D.glyphs;this.w=D.w;this.baseSize=parseInt(C["units-per-em"],10);this.family=C["font-family"].toLowerCase();this.weight=C["font-weight"];this.style=C["font-style"]||"normal";this.viewBox=(function(){var F=C.bbox.split(/\s+/);var E={minX:parseInt(F[0],10),minY:parseInt(F[1],10),maxX:parseInt(F[2],10),maxY:parseInt(F[3],10)};E.width=E.maxX-E.minX;E.height=E.maxY-E.minY;E.toString=function(){return[this.minX,this.minY,this.width,this.height].join(" ")};return E})();this.ascent=-parseInt(C.ascent,10);this.descent=-parseInt(C.descent,10);this.height=-this.ascent+this.descent;this.spacing=function(L,N,E){var O=this.glyphs,M,K,G,P=[],F=0,J=-1,I=-1,H;while(H=L[++J]){M=O[H]||this.missingGlyph;if(!M){continue}if(K){F-=G=K[H]||0;P[I]-=G}F+=P[++I]=~~(M.w||this.w)+N+(B[H]?E:0);K=M.k}P.total=F;return P}}function f(){var C={},B={oblique:"italic",italic:"oblique"};this.add=function(D){(C[D.style]||(C[D.style]={}))[D.weight]=D};this.get=function(H,I){var G=C[H]||C[B[H]]||C.normal||C.italic||C.oblique;if(!G){return null}I={normal:400,bold:700}[I]||parseInt(I,10);if(G[I]){return G[I]}var E={1:1,99:0}[I%100],K=[],F,D;if(E===undefined){E=I>400}if(I==500){I=400}for(var J in G){if(!k(G,J)){continue}J=parseInt(J,10);if(!F||J<F){F=J}if(!D||J>D){D=J}K.push(J)}if(I<F){I=F}if(I>D){I=D}K.sort(function(M,L){return(E?(M>=I&&L>=I)?M<L:M>L:(M<=I&&L<=I)?M>L:M<L)?-1:1});return G[K[0]]}}function r(){function D(F,G){if(F.contains){return F.contains(G)}return F.compareDocumentPosition(G)&16}function B(G){var F=G.relatedTarget;if(!F||D(this,F)){return}C(this,G.type=="mouseover")}function E(F){C(this,F.type=="mouseenter")}function C(F,G){setTimeout(function(){var H=d.get(F).options;m.replace(F,G?h(H,H.hover):H,true)},10)}this.attach=function(F){if(F.onmouseenter===undefined){q(F,"mouseover",B);q(F,"mouseout",B)}else{q(F,"mouseenter",E);q(F,"mouseleave",E)}}}function u(){var C=[],D={};function B(H){var E=[],G;for(var F=0;G=H[F];++F){E[F]=C[D[G]]}return E}this.add=function(F,E){D[F]=C.push(E)-1};this.repeat=function(){var E=arguments.length?B(arguments):C,F;for(var G=0;F=E[G++];){m.replace(F[0],F[1],true)}}}function A(){var D={},B=0;function C(E){return E.cufid||(E.cufid=++B)}this.get=function(E){var F=C(E);return D[F]||(D[F]={})}}function a(B){var D={},C={};this.extend=function(E){for(var F in E){if(k(E,F)){D[F]=E[F]}}return this};this.get=function(E){return D[E]!=undefined?D[E]:B[E]};this.getSize=function(F,E){return C[F]||(C[F]=new n.Size(this.get(F),E))};this.isUsable=function(){return !!B}}function q(C,B,D){if(C.addEventListener){C.addEventListener(B,D,false)}else{if(C.attachEvent){C.attachEvent("on"+B,function(){return D.call(C,window.event)})}}}function v(C,B){var D=d.get(C);if(D.options){return C}if(B.hover&&B.hoverables[C.nodeName.toLowerCase()]){b.attach(C)}D.options=B;return C}function j(B){var C={};return function(D){if(!k(C,D)){C[D]=B.apply(null,arguments)}return C[D]}}function c(F,E){var B=n.quotedList(E.get("fontFamily").toLowerCase()),D;for(var C=0;D=B[C];++C){if(i[D]){return i[D].get(E.get("fontStyle"),E.get("fontWeight"))}}return null}function g(B){return document.getElementsByTagName(B)}function k(C,B){return C.hasOwnProperty(B)}function h(){var C={},B,F;for(var E=0,D=arguments.length;B=arguments[E],E<D;++E){for(F in B){if(k(B,F)){C[F]=B[F]}}}return C}function o(E,M,C,N,F,D){var K=document.createDocumentFragment(),H;if(M===""){return K}var L=N.separate;var I=M.split(p[L]),B=(L=="words");if(B&&t){if(/^\s/.test(M)){I.unshift("")}if(/\s$/.test(M)){I.push("")}}for(var J=0,G=I.length;J<G;++J){H=z[N.engine](E,B?n.textAlign(I[J],C,J,G):I[J],C,N,F,D,J<G-1);if(H){K.appendChild(H)}}return K}function l(D,M){var C=D.nodeName.toLowerCase();if(M.ignore[C]){return}var E=!M.textless[C];var B=n.getStyle(v(D,M)).extend(M);var F=c(D,B),G,K,I,H,L,J;if(!F){return}for(G=D.firstChild;G;G=I){K=G.nodeType;I=G.nextSibling;if(E&&K==3){if(H){H.appendData(G.data);D.removeChild(G)}else{H=G}if(I){continue}}if(H){D.replaceChild(o(F,n.whiteSpace(H.data,B,H,J),B,M,G,D),H);H=null}if(K==1){if(G.firstChild){if(G.nodeName.toLowerCase()=="cufon"){z[M.engine](F,null,B,M,G,D)}else{arguments.callee(G,M)}}J=G}}}var t=" ".split(/\s+/).length==0;var d=new A();var b=new r();var y=new u();var e=false;var z={},i={},w={autoDetect:false,engine:null,forceHitArea:false,hover:false,hoverables:{a:true},ignore:{applet:1,canvas:1,col:1,colgroup:1,head:1,iframe:1,map:1,optgroup:1,option:1,script:1,select:1,style:1,textarea:1,title:1,pre:1},printable:true,selector:(window.Sizzle||(window.jQuery&&function(B){return jQuery(B)})||(window.dojo&&dojo.query)||(window.Ext&&Ext.query)||(window.YAHOO&&YAHOO.util&&YAHOO.util.Selector&&YAHOO.util.Selector.query)||(window.$$&&function(B){return $$(B)})||(window.$&&function(B){return $(B)})||(document.querySelectorAll&&function(B){return document.querySelectorAll(B)})||g),separate:"words",textless:{dl:1,html:1,ol:1,table:1,tbody:1,thead:1,tfoot:1,tr:1,ul:1},textShadow:"none"};var p={words:/\s/.test("\u00a0")?/[^\S\u00a0]+/:/\s+/,characters:"",none:/^/};m.now=function(){x.ready();return m};m.refresh=function(){y.repeat.apply(y,arguments);return m};m.registerEngine=function(C,B){if(!B){return m}z[C]=B;return m.set("engine",C)};m.registerFont=function(D){if(!D){return m}var B=new s(D),C=B.family;if(!i[C]){i[C]=new f()}i[C].add(B);return m.set("fontFamily",'"'+C+'"')};m.replace=function(D,C,B){C=h(w,C);if(!C.engine){return m}if(!e){n.addClass(x.root(),"cufon-active cufon-loading");n.ready(function(){n.addClass(n.removeClass(x.root(),"cufon-loading"),"cufon-ready")});e=true}if(C.hover){C.forceHitArea=true}if(C.autoDetect){delete C.fontFamily}if(typeof C.textShadow=="string"){C.textShadow=n.textShadow(C.textShadow)}if(typeof C.color=="string"&&/^-/.test(C.color)){C.textGradient=n.gradient(C.color)}else{delete C.textGradient}if(!B){y.add(D,arguments)}if(D.nodeType||typeof D=="string"){D=[D]}n.ready(function(){for(var F=0,E=D.length;F<E;++F){var G=D[F];if(typeof G=="string"){m.replace(C.selector(G),C,true)}else{l(G,C)}}});return m};m.set=function(B,C){w[B]=C;return m};return m})();Cufon.registerEngine("vml",(function(){var e=document.namespaces;if(!e){return}e.add("cvml","urn:schemas-microsoft-com:vml");e=null;var b=document.createElement("cvml:shape");b.style.behavior="url(#default#VML)";if(!b.coordsize){return}b=null;var h=(document.documentMode||0)<8;document.write(('<style type="text/css">cufoncanvas{text-indent:0;}@media screen{cvml\\:shape,cvml\\:rect,cvml\\:fill,cvml\\:shadow{behavior:url(#default#VML);display:block;antialias:true;position:absolute;}cufoncanvas{position:absolute;text-align:left;}cufon{display:inline-block;position:relative;vertical-align:'+(h?"middle":"text-bottom")+";}cufon cufontext{position:absolute;left:-10000in;font-size:1px;}a cufon{cursor:pointer}}@media print{cufon cufoncanvas{display:none;}}</style>").replace(/;/g,"!important;"));function c(i,j){return a(i,/(?:em|ex|%)$|^[a-z-]+$/i.test(j)?"1em":j)}function a(l,m){if(m==="0"){return 0}if(/px$/i.test(m)){return parseFloat(m)}var k=l.style.left,j=l.runtimeStyle.left;l.runtimeStyle.left=l.currentStyle.left;l.style.left=m.replace("%","em");var i=l.style.pixelLeft;l.style.left=k;l.runtimeStyle.left=j;return i}function f(l,k,j,n){var i="computed"+n,m=k[i];if(isNaN(m)){m=k.get(n);k[i]=m=(m=="normal")?0:~~j.convertFrom(a(l,m))}return m}var g={};function d(p){var q=p.id;if(!g[q]){var n=p.stops,o=document.createElement("cvml:fill"),i=[];o.type="gradient";o.angle=180;o.focus="0";o.method="sigma";o.color=n[0][1];for(var m=1,l=n.length-1;m<l;++m){i.push(n[m][0]*100+"% "+n[m][1])}o.colors=i.join(",");o.color2=n[l][1];g[q]=o}return g[q]}return function(ac,G,Y,C,K,ad,W){var n=(G===null);if(n){G=K.alt}var I=ac.viewBox;var p=Y.computedFontSize||(Y.computedFontSize=new Cufon.CSS.Size(c(ad,Y.get("fontSize"))+"px",ac.baseSize));var y,q;if(n){y=K;q=K.firstChild}else{y=document.createElement("cufon");y.className="cufon cufon-vml";y.alt=G;q=document.createElement("cufoncanvas");y.appendChild(q);if(C.printable){var Z=document.createElement("cufontext");Z.appendChild(document.createTextNode(G));y.appendChild(Z)}if(!W){y.appendChild(document.createElement("cvml:shape"))}}var ai=y.style;var R=q.style;var l=p.convert(I.height),af=Math.ceil(l);var V=af/l;var P=V*Cufon.CSS.fontStretch(Y.get("fontStretch"));var U=I.minX,T=I.minY;R.height=af;R.top=Math.round(p.convert(T-ac.ascent));R.left=Math.round(p.convert(U));ai.height=p.convert(ac.height)+"px";var F=Y.get("color");var ag=Cufon.CSS.textTransform(G,Y).split("");var L=ac.spacing(ag,f(ad,Y,p,"letterSpacing"),f(ad,Y,p,"wordSpacing"));if(!L.length){return null}var k=L.total;var x=-U+k+(I.width-L[L.length-1]);var ah=p.convert(x*P),X=Math.round(ah);var O=x+","+I.height,m;var J="r"+O+"ns";var u=C.textGradient&&d(C.textGradient);var o=ac.glyphs,S=0;var H=C.textShadow;var ab=-1,aa=0,w;while(w=ag[++ab]){var D=o[ag[ab]]||ac.missingGlyph,v;if(!D){continue}if(n){v=q.childNodes[aa];while(v.firstChild){v.removeChild(v.firstChild)}}else{v=document.createElement("cvml:shape");q.appendChild(v)}v.stroked="f";v.coordsize=O;v.coordorigin=m=(U-S)+","+T;v.path=(D.d?"m"+D.d+"xe":"")+"m"+m+J;v.fillcolor=F;if(u){v.appendChild(u.cloneNode(false))}var ae=v.style;ae.width=X;ae.height=af;if(H){var s=H[0],r=H[1];var B=Cufon.CSS.color(s.color),z;var N=document.createElement("cvml:shadow");N.on="t";N.color=B.color;N.offset=s.offX+","+s.offY;if(r){z=Cufon.CSS.color(r.color);N.type="double";N.color2=z.color;N.offset2=r.offX+","+r.offY}N.opacity=B.opacity||(z&&z.opacity)||1;v.appendChild(N)}S+=L[aa++]}var M=v.nextSibling,t,A;if(C.forceHitArea){if(!M){M=document.createElement("cvml:rect");M.stroked="f";M.className="cufon-vml-cover";t=document.createElement("cvml:fill");t.opacity=0;M.appendChild(t);q.appendChild(M)}A=M.style;A.width=X;A.height=af}else{if(M){q.removeChild(M)}}ai.width=Math.max(Math.ceil(p.convert(k*P)),0);if(h){var Q=Y.computedYAdjust;if(Q===undefined){var E=Y.get("lineHeight");if(E=="normal"){E="1em"}else{if(!isNaN(E)){E+="em"}}Y.computedYAdjust=Q=0.5*(a(ad,E)-parseFloat(ai.height))}if(Q){ai.marginTop=Math.ceil(Q)+"px";ai.marginBottom=Q+"px"}}return y}})());Cufon.registerEngine("canvas",(function(){var b=document.createElement("canvas");if(!b||!b.getContext||!b.getContext.apply){return}b=null;var a=Cufon.CSS.supports("display","inline-block");var e=!a&&(document.compatMode=="BackCompat"||/frameset|transitional/i.test(document.doctype.publicId));var f=document.createElement("style");f.type="text/css";f.appendChild(document.createTextNode(("cufon{text-indent:0;}@media screen,projection{cufon{display:inline;display:inline-block;position:relative;vertical-align:middle;"+(e?"":"font-size:1px;line-height:1px;")+"}cufon cufontext{display:-moz-inline-box;display:inline-block;width:0;height:0;overflow:hidden;text-indent:-10000in;}"+(a?"cufon canvas{position:relative;}":"cufon canvas{position:absolute;}")+"}@media print{cufon{padding:0;}cufon canvas{display:none;}}").replace(/;/g,"!important;")));document.getElementsByTagName("head")[0].appendChild(f);function d(p,h){var n=0,m=0;var g=[],o=/([mrvxe])([^a-z]*)/g,k;generate:for(var j=0;k=o.exec(p);++j){var l=k[2].split(",");switch(k[1]){case"v":g[j]={m:"bezierCurveTo",a:[n+~~l[0],m+~~l[1],n+~~l[2],m+~~l[3],n+=~~l[4],m+=~~l[5]]};break;case"r":g[j]={m:"lineTo",a:[n+=~~l[0],m+=~~l[1]]};break;case"m":g[j]={m:"moveTo",a:[n=~~l[0],m=~~l[1]]};break;case"x":g[j]={m:"closePath"};break;case"e":break generate}h[g[j].m].apply(h,g[j].a)}return g}function c(m,k){for(var j=0,h=m.length;j<h;++j){var g=m[j];k[g.m].apply(k,g.a)}}return function(V,w,P,t,C,W){var k=(w===null);if(k){w=C.getAttribute("alt")}var A=V.viewBox;var m=P.getSize("fontSize",V.baseSize);var B=0,O=0,N=0,u=0;var z=t.textShadow,L=[];if(z){for(var U=z.length;U--;){var F=z[U];var K=m.convertFrom(parseFloat(F.offX));var I=m.convertFrom(parseFloat(F.offY));L[U]=[K,I];if(I<B){B=I}if(K>O){O=K}if(I>N){N=I}if(K<u){u=K}}}var Z=Cufon.CSS.textTransform(w,P).split("");var E=V.spacing(Z,~~m.convertFrom(parseFloat(P.get("letterSpacing"))||0),~~m.convertFrom(parseFloat(P.get("wordSpacing"))||0));if(!E.length){return null}var h=E.total;O+=A.width-E[E.length-1];u+=A.minX;var s,n;if(k){s=C;n=C.firstChild}else{s=document.createElement("cufon");s.className="cufon cufon-canvas";s.setAttribute("alt",w);n=document.createElement("canvas");s.appendChild(n);if(t.printable){var S=document.createElement("cufontext");S.appendChild(document.createTextNode(w));s.appendChild(S)}}var aa=s.style;var H=n.style;var j=m.convert(A.height);var Y=Math.ceil(j);var M=Y/j;var G=M*Cufon.CSS.fontStretch(P.get("fontStretch"));var J=h*G;var Q=Math.ceil(m.convert(J+O-u));var o=Math.ceil(m.convert(A.height-B+N));n.width=Q;n.height=o;H.width=Q+"px";H.height=o+"px";B+=A.minY;H.top=Math.round(m.convert(B-V.ascent))+"px";H.left=Math.round(m.convert(u))+"px";var r=Math.max(Math.ceil(m.convert(J)),0)+"px";if(a){aa.width=r;aa.height=m.convert(V.height)+"px"}else{aa.paddingLeft=r;aa.paddingBottom=(m.convert(V.height)-1)+"px"}var X=n.getContext("2d"),D=j/A.height;X.scale(D,D*M);X.translate(-u,-B);X.save();function T(){var x=V.glyphs,ab,l=-1,g=-1,y;X.scale(G,1);while(y=Z[++l]){var ab=x[Z[l]]||V.missingGlyph;if(!ab){continue}if(ab.d){X.beginPath();if(ab.code){c(ab.code,X)}else{ab.code=d("m"+ab.d,X)}X.fill()}X.translate(E[++g],0)}X.restore()}if(z){for(var U=z.length;U--;){var F=z[U];X.save();X.fillStyle=F.color;X.translate.apply(X,L[U]);T()}}var q=t.textGradient;if(q){var v=q.stops,p=X.createLinearGradient(0,A.minY,0,A.maxY);for(var U=0,R=v.length;U<R;++U){p.addColorStop.apply(p,v[U])}X.fillStyle=p}else{X.fillStyle=P.get("color")}T();return s}})());
/*!
 * The following copyright notice may not be removed under any circumstances.
 * 
 * Copyright:
 * Copyright � 1989, 1995, 2002 Adobe Systems Incorporated.  All Rights Reserved.
 * � 1981, 1995, 2002 Heidelberger Druckmaschinen AG. All rights reserved.
 * 
 * Trademark:
 * Avenir is a trademark of Heidelberger Druckmaschinen AG, exclusively licensed
 * through Linotype Library GmbH, and may be registered in certain jurisdictions.
 * 
 * Full name:
 * AvenirLTStd-Roman
 * 
 * Designer:
 * Adrian Frutiger
 * 
 * Vendor URL:
 * http://www.adobe.com/type
 * 
 * License information:
 * http://www.adobe.com/type/legal.html
 */
Cufon.registerFont({"w":200,"face":{"font-family":"Avenir LT Std","font-weight":400,"font-stretch":"normal","units-per-em":"360","panose-1":"2 11 5 3 2 2 3 2 2 4","ascent":"272","descent":"-88","x-height":"4","bbox":"-12 -283 345 90","underline-thickness":"18","underline-position":"-18","stemh":"24","stemv":"28","unicode-range":"U+0020-U+007E"},"glyphs":{" ":{"w":100},"!":{"d":"65,-255r0,184r-30,0r0,-184r30,0xm50,-41v12,0,22,10,22,22v0,12,-11,21,-22,21v-11,0,-22,-9,-22,-21v0,-12,10,-22,22,-22","w":100},"\"":{"d":"54,-168r0,-87r26,0r0,87r-26,0xm107,-168r0,-87r26,0r0,87r-26,0","w":186},"#":{"d":"35,0r11,-77r-35,0r0,-23r39,0r7,-55r-35,0r0,-24r38,0r11,-76r24,0r-10,76r45,0r11,-76r24,0r-11,76r35,0r0,24r-38,0r-8,55r36,0r0,23r-39,0r-11,77r-24,0r11,-77r-46,0r-11,77r-24,0xm119,-100r8,-55r-46,0r-7,55r45,0"},"$":{"d":"92,30r0,-26v-30,0,-59,-9,-77,-33r25,-22v10,16,31,26,52,27r0,-91v-38,-10,-71,-29,-71,-73v0,-40,33,-68,71,-71r0,-24r17,0r0,24v26,0,53,10,70,29r-23,22v-11,-14,-28,-23,-47,-23r0,90v39,12,76,26,76,73v0,43,-35,69,-76,72r0,26r-17,0xm92,-146r0,-85v-23,4,-39,20,-39,43v0,25,17,33,39,42xm109,-111r0,87v24,-3,44,-20,44,-43v0,-26,-22,-37,-44,-44"},"%":{"d":"14,-196v0,-35,28,-63,63,-63v35,0,63,28,63,63v0,35,-28,63,-63,63v-35,0,-63,-28,-63,-63xm40,-196v0,20,17,37,37,37v20,0,37,-17,37,-37v0,-20,-17,-37,-37,-37v-20,0,-37,17,-37,37xm160,-59v0,-35,28,-63,63,-63v35,0,63,28,63,63v0,35,-28,63,-63,63v-35,0,-63,-28,-63,-63xm186,-59v0,20,17,37,37,37v20,0,37,-17,37,-37v0,-20,-17,-37,-37,-37v-20,0,-37,17,-37,37xm61,0r160,-266r20,11r-160,267","w":299},"&":{"d":"213,0r-34,-37v-36,63,-156,53,-157,-31v0,-36,25,-60,56,-74v-17,-17,-30,-35,-30,-59v0,-38,30,-59,65,-59v35,0,64,20,64,57v0,32,-28,52,-54,65r54,57r34,-57r35,0r-49,78r58,60r-42,0xm78,-199v0,19,15,31,26,42v19,-10,43,-22,43,-47v0,-19,-15,-30,-33,-30v-20,0,-36,14,-36,35xm161,-57r-64,-67v-22,12,-45,28,-45,55v0,27,23,47,50,47v26,0,43,-16,59,-35","w":259},"(":{"d":"92,42r-20,14v-69,-94,-68,-229,0,-320r20,15v-62,84,-61,206,0,291","w":100},")":{"d":"8,-250r20,-14v69,94,68,229,0,320r-20,-14v62,-85,61,-207,0,-292","w":100},"*":{"d":"91,-255r0,54r51,-18r7,21r-51,18r32,42r-18,14r-32,-44r-31,44r-18,-14r31,-42r-51,-18r7,-21r51,18r0,-54r22,0","w":159},"+":{"d":"26,-97r0,-24r82,0r0,-82r24,0r0,82r82,0r0,24r-82,0r0,82r-24,0r0,-82r-82,0","w":239},",":{"d":"76,-37r-30,87r-27,0r24,-87r33,0","w":100},"-":{"d":"16,-73r0,-26r88,0r0,26r-88,0","w":119},".":{"d":"50,-41v12,0,22,10,22,22v0,12,-11,21,-22,21v-11,0,-22,-9,-22,-21v0,-12,10,-22,22,-22","w":100},"\/":{"d":"132,-261r-108,276r-23,-9r108,-276","w":133},"0":{"d":"14,-127v0,-58,16,-132,86,-132v70,0,86,74,86,132v0,58,-16,131,-86,131v-70,0,-86,-73,-86,-131xm44,-127v0,37,6,105,56,105v50,0,56,-68,56,-105v0,-37,-6,-106,-56,-106v-50,0,-56,69,-56,106"},"1":{"d":"130,-255r0,255r-31,0r0,-216r-46,39r-18,-22r68,-56r27,0"},"2":{"d":"18,0r0,-35r96,-93v24,-23,36,-37,36,-62v0,-27,-20,-43,-46,-43v-26,0,-44,16,-49,40r-32,-2v7,-85,158,-86,157,5v0,41,-23,63,-49,87r-79,75r130,0r0,28r-164,0"},"3":{"d":"73,-121r0,-26v33,0,66,-1,68,-44v2,-51,-76,-55,-91,-14r-29,-11v24,-64,152,-57,150,23v0,27,-18,51,-44,59v32,5,53,33,53,65v1,89,-146,99,-167,20r31,-10v11,57,105,45,103,-13v-1,-40,-36,-51,-74,-49"},"4":{"d":"151,-255r0,169r37,0r0,28r-37,0r0,58r-30,0r0,-58r-108,0r0,-34r101,-163r37,0xm121,-86r-1,-134r-81,134r82,0"},"5":{"d":"171,-255r0,28r-104,0r-1,69v59,-22,114,19,114,78v0,90,-136,113,-163,38r30,-12v20,51,102,37,102,-26v0,-59,-73,-72,-114,-42r2,-133r134,0"},"6":{"d":"142,-255r-68,107v54,-25,111,11,111,72v0,50,-37,80,-85,80v-48,0,-85,-30,-85,-80v0,-30,10,-49,24,-71r68,-108r35,0xm100,-130v-31,0,-55,21,-55,54v0,33,24,54,55,54v31,0,55,-21,55,-54v0,-33,-24,-54,-55,-54"},"7":{"d":"173,-255r0,28r-102,227r-34,0r103,-227r-123,0r0,-28r156,0"},"8":{"d":"27,-193v0,-40,32,-66,73,-66v75,0,101,102,31,123v31,6,50,34,50,66v0,46,-36,74,-81,74v-45,0,-81,-28,-81,-74v0,-32,19,-60,50,-66v-26,-7,-42,-33,-42,-57xm58,-191v0,24,17,42,42,42v25,0,43,-18,43,-42v0,-24,-18,-42,-43,-42v-25,0,-42,18,-42,42xm49,-72v0,30,23,50,51,50v28,0,51,-20,51,-50v0,-30,-23,-51,-51,-51v-28,0,-51,21,-51,51"},"9":{"d":"58,0r68,-107v-54,25,-111,-11,-111,-72v0,-50,37,-80,85,-80v48,0,85,30,85,80v0,30,-10,49,-24,71r-67,108r-36,0xm100,-125v31,0,55,-21,55,-54v0,-33,-24,-54,-55,-54v-31,0,-55,21,-55,54v0,33,24,54,55,54"},":":{"d":"50,-173v12,0,22,10,22,22v0,12,-11,21,-22,21v-11,0,-22,-9,-22,-21v0,-12,10,-22,22,-22xm50,-41v12,0,22,10,22,22v0,12,-11,21,-22,21v-11,0,-22,-9,-22,-21v0,-12,10,-22,22,-22","w":100},";":{"d":"76,-37r-30,87r-27,0r24,-87r33,0xm50,-173v12,0,22,10,22,22v0,12,-11,21,-22,21v-11,0,-22,-9,-22,-21v0,-12,10,-22,22,-22","w":100},"<":{"d":"214,-203r0,24r-159,70r159,70r0,24r-188,-84r0,-21","w":239},"=":{"d":"26,-125r0,-24r188,0r0,24r-188,0xm26,-69r0,-24r188,0r0,24r-188,0","w":239},">":{"d":"26,-15r0,-24r159,-70r-159,-70r0,-24r188,83r0,21","w":239},"?":{"d":"157,-197v0,56,-70,59,-59,126r-30,0r0,-23v-6,-48,57,-58,56,-102v0,-23,-14,-39,-38,-39v-24,0,-40,16,-44,39r-32,-4v5,-79,147,-83,147,3xm83,-41v12,0,22,10,22,22v0,29,-43,26,-43,0v0,-12,9,-22,21,-22","w":173},"@":{"d":"94,-105v0,20,14,34,32,34v31,0,55,-46,55,-76v0,-18,-14,-34,-29,-34v-34,0,-58,45,-58,76xm223,-198r-34,120v0,5,3,7,9,7v23,0,50,-38,50,-78v0,-57,-48,-93,-98,-93v-62,0,-110,53,-110,115v0,64,51,114,110,114v36,0,72,-18,91,-44r23,0v-23,39,-67,63,-114,63v-76,0,-134,-58,-134,-133v0,-75,60,-134,134,-134v66,0,122,46,122,110v0,62,-50,104,-84,104v-13,0,-20,-10,-24,-24v-24,41,-96,24,-96,-32v0,-50,33,-102,84,-102v18,0,31,10,41,30r6,-23r24,0","w":288},"A":{"d":"2,0r112,-255r29,0r108,255r-35,0r-26,-63r-127,0r-27,63r-34,0xm179,-91r-52,-125r-52,125r104,0","w":253},"B":{"d":"32,0r0,-255v74,0,159,-11,162,63v1,31,-20,48,-45,58v33,3,58,30,58,63v0,83,-94,71,-175,71xm62,-227r0,80v46,-1,100,9,100,-41v0,-47,-55,-38,-100,-39xm62,-119r0,91v51,1,113,6,113,-44v0,-55,-60,-47,-113,-47","w":226},"C":{"d":"237,-221r-27,19v-54,-70,-161,-9,-161,75v0,94,118,142,171,70r23,19v-68,90,-226,32,-226,-89v0,-115,148,-181,220,-94","w":253},"D":{"d":"27,0r0,-255r98,0v50,0,125,34,125,128v0,136,-102,130,-223,127xm57,-227r0,199v90,5,160,-5,160,-99v0,-93,-69,-106,-160,-100","w":266},"E":{"d":"32,0r0,-255r160,0r0,28r-130,0r0,80r121,0r0,28r-121,0r0,91r136,0r0,28r-166,0","w":213},"F":{"d":"32,0r0,-255r158,0r0,28r-128,0r0,84r119,0r0,29r-119,0r0,114r-30,0","k":{"A":20,",":57,".":57}},"G":{"d":"248,-143r0,126v-97,55,-231,9,-231,-110v0,-117,144,-176,227,-101r-23,24v-59,-63,-172,-16,-172,77v0,85,92,130,169,91r0,-78r-59,0r0,-29r89,0","w":280},"H":{"d":"32,0r0,-255r30,0r0,108r136,0r0,-108r30,0r0,255r-30,0r0,-119r-136,0r0,119r-30,0","w":259},"I":{"d":"32,0r0,-255r30,0r0,255r-30,0","w":93},"J":{"d":"142,-255r0,189v0,34,-19,72,-73,72v-36,0,-62,-18,-68,-54r30,-6v4,20,17,32,38,32v35,0,43,-26,43,-54r0,-179r30,0","w":173},"K":{"d":"32,0r0,-255r30,0r0,110r4,0r116,-110r44,0r-126,116r132,139r-44,0r-122,-130r-4,0r0,130r-30,0","w":226},"L":{"d":"32,0r0,-255r30,0r0,227r117,0r0,28r-147,0","w":180,"k":{"T":33,"V":33,"W":20,"y":13,"Y":40}},"M":{"d":"32,0r0,-255r45,0r87,200r87,-200r44,0r0,255r-30,0r-1,-214r-92,214r-18,0r-92,-214r0,214r-30,0","w":326},"N":{"d":"32,0r0,-255r38,0r148,212r0,-212r30,0r0,255r-38,0r-148,-212r0,212r-30,0","w":280},"O":{"d":"150,6v-77,0,-133,-56,-133,-133v0,-77,56,-134,133,-134v77,0,133,57,133,134v0,77,-56,133,-133,133xm150,-22v60,0,101,-46,101,-105v0,-59,-41,-106,-101,-106v-60,0,-101,47,-101,106v0,59,41,105,101,105","w":300},"P":{"d":"32,0r0,-255v78,0,165,-11,165,70v0,76,-66,72,-135,71r0,114r-30,0xm62,-227r0,84v48,1,102,5,102,-42v0,-47,-54,-43,-102,-42","w":213,"k":{"A":27,",":64,".":64}},"Q":{"d":"296,-26r0,26r-148,0v-75,0,-131,-57,-131,-131v0,-74,56,-130,131,-130v75,0,131,56,131,130v1,44,-26,87,-62,105r79,0xm49,-131v0,58,43,103,99,103v56,0,99,-45,99,-103v0,-57,-42,-102,-99,-102v-57,0,-99,45,-99,102","w":300},"R":{"d":"32,0r0,-255v78,2,166,-17,166,70v0,37,-23,62,-63,68r72,117r-37,0r-69,-114r-39,0r0,114r-30,0xm62,-227r0,84v46,-1,104,9,104,-42v0,-52,-56,-40,-104,-42","w":219,"k":{"T":6,"V":6,"W":6,"Y":13}},"S":{"d":"177,-231r-24,21v-23,-37,-100,-30,-101,20v0,23,12,33,53,46v40,13,77,26,77,76v0,84,-127,97,-168,39r26,-21v22,42,109,38,109,-16v0,-31,-21,-37,-66,-52v-36,-12,-64,-28,-64,-70v0,-79,115,-96,158,-43"},"T":{"d":"203,-255r0,28r-85,0r0,227r-30,0r0,-227r-84,0r0,-28r199,0","w":206,"k":{"w":40,"y":40,"A":33,",":40,".":40,"c":40,"e":40,"o":40,"-":46,"a":40,"r":33,"s":40,"u":33,":":40,";":40}},"U":{"d":"223,-255v-6,117,34,261,-96,261v-130,0,-90,-144,-96,-261r30,0r0,153v0,41,15,80,66,80v103,0,55,-140,66,-233r30,0","w":253},"V":{"d":"94,0r-97,-255r34,0r79,210r81,-210r32,0r-100,255r-29,0","w":219,"k":{"y":6,"A":17,",":46,".":46,"e":20,"o":20,"-":20,"a":20,"r":13,"u":13,":":17,";":17,"i":6}},"W":{"d":"77,0r-75,-255r34,0r58,212r63,-212r34,0r63,212r59,-212r32,0r-75,255r-33,0r-64,-216r-63,216r-33,0","w":346,"k":{"A":9,",":27,".":27,"e":17,"o":21,"a":13,"r":6,"u":6,":":6,";":6}},"X":{"d":"-1,0r93,-134r-86,-121r39,0r69,100r68,-100r38,0r-86,121r94,134r-40,0r-76,-113r-76,113r-37,0","w":226},"Y":{"d":"92,0r0,-110r-95,-145r38,0r72,115r74,-115r36,0r-95,145r0,110r-30,0","w":213,"k":{"v":20,"A":27,",":50,".":50,"e":40,"o":40,"q":33,"-":40,"a":33,"u":27,":":33,";":33,"i":13,"p":27}},"Z":{"d":"10,0r0,-27r148,-200r-145,0r0,-28r181,0r0,26r-147,201r149,0r0,28r-186,0","w":206},"[":{"d":"69,-264r0,22r-33,0r0,277r33,0r0,21r-59,0r0,-320r59,0","w":93},"\\":{"d":"109,15r-108,-276r23,-9r108,276","w":133},"]":{"d":"24,-242r0,-22r59,0r0,320r-59,0r0,-21r33,0r0,-277r-33,0","w":93},"^":{"d":"29,-99r80,-156r22,0r80,156r-27,0r-64,-126r-65,126r-26,0","w":239},"_":{"d":"0,45r0,-18r180,0r0,18r-180,0","w":180},"a":{"d":"42,-129r-18,-18v42,-44,138,-35,138,45v0,34,-2,72,3,102r-27,0v-3,-8,0,-20,-3,-25v-20,43,-120,38,-118,-21v2,-60,69,-59,117,-59v6,-51,-65,-51,-92,-24xm134,-81v-40,-1,-87,1,-87,32v0,21,16,30,37,30v36,-1,52,-27,50,-62","w":186},"b":{"d":"25,0r0,-272r28,0r1,128v46,-60,149,-19,149,60v0,84,-112,116,-150,54r0,30r-28,0xm53,-84v0,35,24,62,60,62v36,0,60,-27,60,-62v0,-35,-24,-63,-60,-63v-36,0,-60,28,-60,63","w":219},"c":{"d":"169,-146r-22,20v-33,-43,-107,-14,-100,42v-5,55,69,84,100,42r21,19v-49,56,-151,20,-151,-61v0,-80,101,-118,152,-62","w":173},"d":{"d":"167,0v-1,-9,2,-23,-1,-30v-10,19,-35,34,-65,34v-47,0,-84,-38,-84,-88v0,-80,102,-118,150,-60r0,-128r28,0r0,272r-28,0xm167,-84v0,-35,-24,-63,-60,-63v-36,0,-60,28,-60,63v0,35,24,62,60,62v36,0,60,-27,60,-62","w":219},"e":{"d":"156,-47r21,16v-46,67,-160,32,-160,-53v0,-52,36,-89,86,-89v51,0,83,36,80,97r-136,0v-1,55,81,73,109,29xm47,-99r106,0v-1,-28,-20,-48,-51,-48v-31,0,-51,20,-55,48"},"f":{"d":"41,0r0,-145r-37,0r0,-23r37,0v-8,-66,13,-124,81,-104r-4,25v-47,-19,-54,27,-49,79r41,0r0,23r-41,0r0,145r-28,0","w":113,"k":{"f":6}},"g":{"d":"195,-168r0,160v9,100,-119,120,-175,62r20,-23v18,19,37,29,63,29v58,-1,68,-41,63,-89v-42,64,-149,21,-149,-55v0,-84,108,-120,150,-56r0,-28r28,0xm47,-84v0,34,27,60,60,60v39,0,60,-29,60,-61v0,-36,-24,-62,-60,-62v-35,0,-60,28,-60,63","w":219},"h":{"d":"27,0r0,-272r28,0r0,127v8,-13,28,-28,54,-28v84,-2,63,96,65,173r-29,0v-5,-55,21,-146,-38,-147v-65,-2,-51,83,-52,147r-28,0"},"i":{"d":"29,0r0,-168r28,0r0,168r-28,0xm23,-234v0,-11,8,-21,20,-21v12,0,21,10,21,21v0,12,-9,20,-21,20v-12,0,-20,-8,-20,-20","w":86},"j":{"d":"57,-168r0,206v7,22,-29,60,-69,45r3,-25v25,8,38,-8,38,-33r0,-193r28,0xm23,-234v0,-11,8,-21,20,-21v12,0,21,10,21,21v0,12,-9,20,-21,20v-12,0,-20,-8,-20,-20","w":86},"k":{"d":"27,0r0,-272r28,0r0,178r76,-74r40,0r-81,76r90,92r-42,0r-83,-89r0,89r-28,0","w":180},"l":{"d":"29,0r0,-272r28,0r0,272r-28,0","w":86},"m":{"d":"27,0r-2,-168r27,0v0,9,1,18,1,27v8,-17,28,-32,56,-32v36,0,49,20,54,32v12,-20,27,-32,53,-32v87,0,60,97,64,173r-28,0v-8,-59,26,-144,-41,-147v-58,-2,-43,88,-44,147r-28,0v-5,-50,17,-147,-32,-147v-65,0,-51,83,-52,147r-28,0","w":306},"n":{"d":"27,0r-2,-168r27,0v0,9,1,18,1,27v8,-17,28,-32,56,-32v84,-2,63,96,65,173r-29,0v-5,-55,21,-146,-38,-147v-65,-2,-51,83,-52,147r-28,0"},"o":{"d":"17,-84v0,-53,38,-89,90,-89v52,0,90,36,90,89v0,53,-38,88,-90,88v-52,0,-90,-35,-90,-88xm47,-84v0,35,24,62,60,62v36,0,60,-27,60,-62v0,-35,-24,-63,-60,-63v-36,0,-60,28,-60,63","w":213},"p":{"d":"25,82r0,-250r28,0v1,7,-2,18,1,24v46,-60,149,-19,149,60v0,84,-112,116,-150,54r0,112r-28,0xm53,-84v0,35,24,62,60,62v36,0,60,-27,60,-62v0,-35,-24,-63,-60,-63v-36,0,-60,28,-60,63","w":219},"q":{"d":"195,-168r0,250r-28,0r-1,-106v-46,58,-149,19,-149,-60v0,-50,37,-89,84,-89v31,-1,54,17,66,34r0,-29r28,0xm167,-84v0,-35,-24,-63,-60,-63v-36,0,-60,28,-60,63v0,35,24,62,60,62v36,0,60,-27,60,-62","w":219},"r":{"d":"27,0r-2,-168r27,0v0,9,1,18,1,27v8,-19,37,-38,68,-30r-2,28v-41,-9,-64,16,-64,61r0,82r-28,0","w":126,"k":{",":33,".":33,"c":6,"d":6,"e":6,"g":6,"o":6,"q":6,"-":20}},"s":{"d":"43,-123v11,36,104,15,98,74v-6,65,-104,70,-133,22r22,-17v11,14,23,22,43,22v18,0,38,-8,38,-26v0,-18,-18,-22,-36,-26v-32,-7,-60,-14,-60,-49v0,-58,97,-68,120,-19r-23,15v-10,-25,-65,-29,-69,4","w":153},"t":{"d":"119,-168r0,23r-50,0r0,85v-7,34,22,46,50,33r1,25v-42,16,-79,1,-79,-50r0,-93r-37,0r0,-23r37,0r0,-48r28,0r0,48r50,0","w":126},"u":{"d":"174,-168r1,168r-27,0v-1,-9,2,-21,-1,-28v-8,17,-28,32,-56,32v-84,3,-61,-95,-64,-172r28,0v6,55,-21,145,38,146v65,2,51,-83,52,-146r29,0"},"v":{"d":"170,-168r-67,168r-31,0r-68,-168r32,0r54,132r50,-132r30,0","w":173,"k":{",":27,".":27}},"w":{"d":"262,-168r-55,168r-27,0r-49,-130r-42,130r-29,0r-55,-168r31,0r39,129r44,-129r29,0r45,129r38,-129r31,0","w":266,"k":{",":20,".":20}},"x":{"d":"1,0r69,-91r-60,-77r36,0r45,60r42,-60r34,0r-58,77r70,91r-37,0r-53,-73r-53,73r-35,0","w":180},"y":{"d":"170,-168r-81,207v-11,36,-39,57,-82,43r4,-24v25,6,43,-2,51,-25r11,-32r-69,-169r32,0r53,132r50,-132r31,0","w":173,"k":{",":27,".":27}},"z":{"d":"11,0r0,-22r99,-123r-95,0r0,-23r132,0r0,21r-99,123r101,0r0,24r-138,0","w":159},"{":{"d":"106,-264r0,22v-52,-12,-37,48,-37,92v0,33,-24,42,-31,47v8,1,31,10,31,46v0,38,-21,101,37,92r0,21v-38,3,-63,-5,-63,-45v0,-41,5,-106,-31,-104r0,-21v36,1,31,-64,31,-105v0,-40,26,-48,63,-45","w":119},"|":{"d":"28,-270r24,0r0,360r-24,0r0,-360","w":79},"}":{"d":"14,-242r0,-22v38,-3,63,5,63,45v0,42,-5,106,31,105r0,21v-36,-2,-31,63,-31,104v0,40,-25,48,-63,45r0,-21v52,11,37,-48,37,-92v0,-38,24,-43,31,-47v-8,-2,-31,-14,-31,-46v0,-38,21,-101,-37,-92","w":119},"~":{"d":"82,-133v38,0,90,49,109,-1r12,17v-11,15,-24,32,-46,32v-37,0,-90,-50,-108,1r-12,-18v11,-15,23,-31,45,-31","w":239},"'":{"d":"37,-168r0,-87r26,0r0,87r-26,0","w":100},"`":{"d":"31,-255r37,52r-26,0r-51,-52r40,0","w":86},"\u00a0":{"w":100}}});
;
/*!
 * The following copyright notice may not be removed under any circumstances.
 * 
 * Copyright:
 * Copyright � 1989, 1995, 2002 Adobe Systems Incorporated.  All Rights Reserved.
 * � 1981, 1995, 2002 Heidelberger Druckmaschinen AG. All rights reserved.
 * 
 * Trademark:
 * Avenir is a trademark of Heidelberger Druckmaschinen AG, exclusively licensed
 * through Linotype Library GmbH, and may be registered in certain jurisdictions.
 * 
 * Full name:
 * AvenirLTStd-Light
 * 
 * Designer:
 * Adrian Frutiger
 * 
 * Vendor URL:
 * http://www.adobe.com/type
 * 
 * License information:
 * http://www.adobe.com/type/legal.html
 */
Cufon.registerFont({"w":187,"face":{"font-family":"Avenir LT Std","font-weight":300,"font-stretch":"normal","units-per-em":"360","panose-1":"2 11 4 2 2 2 3 2 2 4","ascent":"272","descent":"-88","x-height":"4","bbox":"-11 -283 338 90","underline-thickness":"18","underline-position":"-18","stemh":"19","stemv":"22","unicode-range":"U+0020-U+007E"},"glyphs":{" ":{"w":93},"!":{"d":"77,-255r0,188r-21,0r0,-188r21,0xm49,-15v0,-10,8,-17,18,-17v10,0,17,7,17,17v0,10,-7,17,-17,17v-10,0,-18,-7,-18,-17","w":133},"\"":{"d":"58,-173r0,-82r22,0r0,82r-22,0xm108,-173r0,-82r21,0r0,82r-21,0"},"#":{"d":"33,0r11,-82r-39,0r0,-18r41,0r8,-56r-39,0r0,-17r42,0r10,-82r19,0r-11,82r50,0r11,-82r19,0r-11,82r39,0r0,17r-42,0r-7,56r38,0r0,18r-41,0r-11,82r-18,0r10,-82r-50,0r-11,82r-18,0xm115,-100r7,-56r-49,0r-8,56r50,0"},"$":{"d":"101,-113r0,98v46,-1,73,-74,16,-92xm84,-144r0,-96v-23,4,-43,22,-43,49v0,27,19,40,43,47xm84,28r0,-24v-26,0,-55,-12,-72,-37r20,-15v12,19,31,32,52,33r0,-105r-26,-9v-78,-33,-33,-126,26,-130r0,-24r17,0r0,24v24,0,48,6,67,32r-20,14v-11,-17,-30,-27,-47,-27r0,102v37,10,71,22,71,73v0,41,-34,67,-71,69r0,24r-17,0"},"%":{"d":"14,-197v0,-34,28,-62,62,-62v34,0,62,28,62,62v0,34,-28,62,-62,62v-34,0,-62,-28,-62,-62xm33,-197v0,24,20,42,43,42v23,0,42,-18,42,-42v0,-24,-19,-43,-42,-43v-23,0,-43,19,-43,43xm156,-58v0,-34,28,-62,62,-62v34,0,62,28,62,62v0,34,-28,62,-62,62v-34,0,-62,-28,-62,-62xm175,-58v0,24,20,43,43,43v23,0,42,-19,42,-43v0,-24,-19,-42,-42,-42v-23,0,-43,18,-43,42xm57,2r164,-269r17,10r-165,269","w":293},"&":{"d":"113,-140r62,66r37,-66r25,0r-47,81r58,61r-32,0r-38,-41v-21,28,-43,45,-79,45v-43,0,-78,-29,-78,-74v0,-40,29,-57,59,-75v-14,-17,-31,-37,-31,-60v0,-37,30,-58,64,-58v30,0,57,22,57,54v0,37,-29,52,-57,67xm111,-240v-21,0,-39,15,-39,36v0,18,17,37,28,50v20,-10,47,-25,47,-52v0,-20,-15,-34,-36,-34xm164,-54r-71,-74v-23,14,-48,30,-48,60v0,32,26,53,58,53v27,0,47,-18,61,-39","w":253},"(":{"d":"71,-264r18,12v-63,86,-61,211,0,297r-17,11v-67,-93,-67,-229,-1,-320","w":93},")":{"d":"23,56r-19,-11v63,-86,62,-211,0,-298r17,-11v68,93,69,228,2,320","w":93},"*":{"d":"89,-255r0,55r52,-17r5,17r-52,17r33,44r-13,10r-34,-45r-32,45r-15,-10r33,-44r-52,-18r5,-16r53,17r0,-55r17,0","w":159},"+":{"d":"28,-100r0,-18r83,0r0,-83r18,0r0,83r83,0r0,18r-83,0r0,83r-18,0r0,-83r-83,0","w":239},",":{"d":"18,50r24,-82r25,0r-28,82r-21,0","w":93},"-":{"d":"15,-76r0,-19r89,0r0,19r-89,0","w":119},".":{"d":"30,-15v0,-10,7,-17,17,-17v10,0,17,7,17,17v0,10,-7,17,-17,17v-10,0,-17,-7,-17,-17","w":93},"\/":{"d":"3,6r110,-274r18,7r-109,274","w":133},"0":{"d":"9,-127v0,-55,13,-132,85,-132v72,0,84,77,84,132v0,55,-12,131,-84,131v-72,0,-85,-76,-85,-131xm33,-127v0,40,7,112,61,112v54,0,60,-72,60,-112v0,-40,-6,-113,-60,-113v-54,0,-61,73,-61,113"},"1":{"d":"94,0r0,-225r-46,38r-14,-16r63,-52r19,0r0,255r-22,0"},"2":{"d":"15,0r0,-25r96,-101v17,-18,36,-39,36,-66v0,-30,-22,-48,-51,-48v-26,0,-46,17,-51,42r-25,-3v10,-80,151,-76,151,7v0,40,-27,66,-53,93r-79,82r133,0r0,19r-157,0"},"3":{"d":"15,-46r23,-7v9,24,25,38,54,38v38,0,57,-25,57,-56v0,-41,-39,-54,-77,-52r0,-22v35,2,66,-9,68,-48v3,-54,-78,-61,-96,-17r-20,-11v28,-57,139,-51,139,27v0,29,-17,50,-45,60v32,6,54,33,54,65v0,40,-25,73,-78,73v-38,0,-66,-15,-79,-50"},"4":{"d":"118,0r0,-63r-110,0r0,-25r102,-167r30,0r0,173r39,0r0,19r-39,0r0,63r-22,0xm118,-82r0,-149r-90,149r90,0"},"5":{"d":"162,-255r0,20r-105,0r-1,81v59,-27,117,18,116,77v-2,95,-126,104,-157,38r22,-11v23,50,108,48,112,-26v2,-60,-72,-85,-115,-49r2,-130r126,0"},"6":{"d":"130,-255r-69,112v52,-30,116,10,116,68v0,46,-35,79,-83,79v-48,0,-84,-33,-84,-79v0,-27,13,-48,23,-65r70,-115r27,0xm94,-135v-33,0,-60,27,-60,60v0,35,25,60,60,60v35,0,59,-25,59,-60v0,-33,-26,-60,-59,-60"},"7":{"d":"159,-255r0,20r-101,235r-24,0r103,-235r-122,0r0,-20r144,0"},"8":{"d":"94,-259v72,0,95,102,29,122v30,10,49,34,49,66v0,46,-33,75,-78,75v-83,0,-110,-122,-30,-140v-64,-21,-45,-123,30,-123xm48,-193v0,26,19,47,46,47v27,0,46,-21,46,-47v0,-28,-20,-47,-46,-47v-26,0,-46,19,-46,47xm39,-71v0,31,22,56,55,56v33,0,55,-25,55,-56v0,-33,-24,-56,-55,-56v-31,0,-55,23,-55,56"},"9":{"d":"57,0r69,-112v-52,30,-116,-10,-116,-68v0,-46,36,-79,84,-79v48,0,83,33,83,79v0,27,-13,48,-23,65r-69,115r-28,0xm94,-120v33,0,59,-27,59,-60v0,-35,-24,-60,-59,-60v-35,0,-60,25,-60,60v0,33,27,60,60,60"},":":{"d":"30,-151v0,-10,7,-17,17,-17v10,0,17,7,17,17v0,10,-7,17,-17,17v-10,0,-17,-7,-17,-17xm30,-15v0,-10,7,-17,17,-17v10,0,17,7,17,17v0,10,-7,17,-17,17v-10,0,-17,-7,-17,-17","w":93},";":{"d":"18,50r24,-82r25,0r-28,82r-21,0xm30,-151v0,-10,7,-17,17,-17v10,0,17,7,17,17v0,10,-7,17,-17,17v-10,0,-17,-7,-17,-17","w":93},"<":{"d":"210,-202r0,18r-161,75r161,75r0,18r-180,-84r0,-18","w":239},"=":{"d":"28,-126r0,-18r184,0r0,18r-184,0xm28,-75r0,-17r184,0r0,17r-184,0","w":239},">":{"d":"30,-16r0,-18r161,-75r-161,-75r0,-18r180,84r0,18","w":239},"?":{"d":"73,-67v-14,-73,57,-77,57,-131v0,-26,-17,-44,-43,-44v-25,0,-42,17,-47,42r-23,-3v5,-76,139,-79,136,4v-2,68,-71,58,-58,132r-22,0xm67,-15v0,-10,8,-17,18,-17v10,0,17,7,17,17v0,10,-7,17,-17,17v-10,0,-18,-7,-18,-17","w":166},"@":{"d":"91,-101v0,20,12,34,31,34v34,0,61,-50,61,-81v0,-23,-10,-37,-30,-37v-36,0,-62,50,-62,84xm224,-198r-39,122v0,4,1,9,9,9v25,0,59,-46,59,-86v0,-54,-49,-91,-103,-91v-66,0,-115,55,-115,116v0,64,51,117,115,117v36,0,73,-19,92,-47r21,0v-22,40,-68,64,-113,64v-78,0,-134,-59,-134,-134v0,-75,59,-133,133,-133v70,0,123,46,123,108v0,60,-43,106,-84,106v-13,0,-20,-10,-23,-24v-24,38,-95,27,-95,-29v0,-50,32,-104,85,-104v17,0,31,9,39,29r8,-23r22,0","w":288},"A":{"d":"2,0r111,-255r25,0r107,255r-27,0r-28,-68r-135,0r-28,68r-25,0xm182,-89r-58,-140r-59,140r117,0","w":246},"B":{"d":"35,0r0,-255v75,3,151,-19,155,67v1,27,-17,46,-45,54v33,3,57,27,57,63v0,91,-87,68,-167,71xm59,-233r0,88r49,0v49,0,59,-27,59,-43v-1,-56,-57,-44,-108,-45xm59,-123r0,101r54,0v56,0,65,-31,65,-49v-2,-60,-61,-52,-119,-52","w":226},"C":{"d":"225,-55r20,15v-69,94,-227,34,-227,-87v0,-117,148,-181,220,-94r-19,16v-56,-77,-188,-16,-177,78v-9,98,126,154,183,72","w":253},"D":{"d":"32,0r0,-255r79,0v132,0,137,109,137,128v0,19,-5,127,-137,127r-79,0xm55,-233r0,211v94,6,170,-13,170,-105v0,-91,-75,-112,-170,-106","w":266},"E":{"d":"35,0r0,-255r156,0r0,22r-132,0r0,90r124,0r0,22r-124,0r0,99r138,0r0,22r-162,0","w":213},"F":{"d":"35,0r0,-255r153,0r0,22r-129,0r0,90r120,0r0,22r-120,0r0,121r-24,0","w":200,"k":{"A":20,",":46,".":46}},"G":{"d":"245,-138r0,121v-97,56,-227,7,-227,-110v0,-118,146,-176,224,-99r-17,18v-15,-19,-47,-32,-75,-32v-67,0,-108,50,-108,113v0,63,41,112,108,112v27,0,52,-3,71,-15r0,-86r-61,0r0,-22r85,0","w":280},"H":{"d":"35,0r0,-255r24,0r0,112r142,0r0,-112r24,0r0,255r-24,0r0,-121r-142,0r0,121r-24,0","w":259},"I":{"d":"35,0r0,-255r24,0r0,255r-24,0","w":93},"J":{"d":"139,-255r0,180v0,31,-4,81,-70,81v-37,0,-59,-21,-66,-52r23,-4v6,24,21,35,43,35v34,0,46,-23,46,-54r0,-186r24,0","w":173},"K":{"d":"35,0r0,-255r24,0v2,36,-4,80,2,112r123,-112r33,0r-129,117r136,138r-34,0r-129,-132r-2,0r0,132r-24,0","w":219},"L":{"d":"35,0r0,-255r24,0r0,233r118,0r0,22r-142,0","w":180,"k":{"T":33,"V":33,"W":20,"y":13,"Y":40}},"M":{"d":"32,0r0,-255r36,0r93,214r93,-214r34,0r0,255r-24,0r-1,-225r-97,225r-10,0r-100,-225r0,225r-24,0","w":320},"N":{"d":"35,0r0,-255r31,0r155,223r0,-223r24,0r0,255r-30,0r-156,-223r0,223r-24,0","w":280},"O":{"d":"18,-127v0,-78,54,-134,132,-134v78,0,132,56,132,134v0,78,-54,133,-132,133v-78,0,-132,-55,-132,-133xm42,-127v0,63,41,112,108,112v67,0,108,-49,108,-112v0,-63,-41,-113,-108,-113v-67,0,-108,50,-108,113","w":300},"P":{"d":"35,0r0,-255v75,1,155,-14,155,67v0,67,-63,70,-131,67r0,121r-24,0xm59,-233r0,90v51,0,108,8,108,-45v0,-52,-57,-45,-108,-45","w":206,"k":{"A":27,",":55,".":55}},"Q":{"d":"293,-22r0,22r-141,0v-71,0,-134,-53,-134,-131v0,-74,51,-130,130,-130v136,0,173,190,65,239r80,0xm42,-131v0,60,46,109,110,109v66,0,101,-52,101,-109v0,-62,-39,-109,-105,-109v-66,0,-106,47,-106,109","w":300},"R":{"d":"59,-233r0,90r50,0v50,0,60,-27,60,-45v0,-18,-10,-45,-60,-45r-50,0xm35,0r0,-255r67,0v20,0,91,0,91,67v0,49,-38,62,-66,66r75,122r-27,0r-72,-121r-44,0r0,121r-24,0","w":213,"k":{"T":-2,"V":-2,"W":-2,"y":-9,"Y":5}},"S":{"d":"19,-32r23,-15v22,47,116,42,115,-18v0,-71,-131,-30,-131,-127v-1,-75,114,-92,151,-37r-20,14v-23,-40,-107,-32,-107,23v0,77,131,31,131,127v0,84,-123,92,-162,33","w":200},"T":{"d":"91,0r0,-233r-87,0r0,-22r198,0r0,22r-87,0r0,233r-24,0","w":206,"k":{"w":40,"y":40,"A":24,",":40,".":40,"c":40,"e":40,"o":40,"-":46,"a":40,"i":-9,"r":33,"s":40,"u":33,":":40,";":40}},"U":{"d":"216,-255v-7,115,35,261,-93,261v-128,0,-86,-146,-93,-261r24,0r0,151v0,36,10,89,69,89v59,0,69,-53,69,-89r0,-151r24,0","w":246},"V":{"d":"94,0r-95,-255r25,0r83,224r82,-224r26,0r-97,255r-24,0","w":213,"k":{"y":6,"A":20,",":46,".":46,"e":20,"o":20,"-":20,"a":20,"i":-2,"r":13,"u":13,":":19,";":19}},"W":{"d":"76,0r-74,-255r24,0r64,226r65,-226r30,0r65,226r64,-226r24,0r-74,255r-29,0r-65,-227r-65,227r-29,0","w":339,"k":{"A":6,",":27,".":27,"e":6,"o":6,"a":13,"i":-9,"r":6,"u":6,":":6,";":6}},"X":{"d":"0,0r96,-134r-87,-121r28,0r74,104r72,-104r28,0r-86,121r95,134r-30,0r-81,-115r-80,115r-29,0","w":219},"Y":{"d":"91,0r0,-110r-93,-145r28,0r77,121r79,-121r27,0r-94,145r0,110r-24,0","w":206,"k":{"v":20,"A":27,",":44,".":36,"e":33,"o":33,"q":33,"-":40,"a":33,"i":3,"u":27,":":33,";":33,"p":27}},"Z":{"d":"8,0r0,-19r149,-214r-147,0r0,-22r178,0r0,20r-150,213r154,0r0,22r-184,0","w":200},"[":{"d":"81,-264r0,18r-33,0r0,285r33,0r0,17r-54,0r0,-320r54,0","w":93},"\\":{"d":"21,-268r109,274r-19,7r-109,-274","w":133},"]":{"d":"13,56r0,-17r32,0r0,-285r-32,0r0,-18r54,0r0,320r-54,0","w":93},"^":{"d":"30,-92r82,-163r16,0r82,163r-20,0r-70,-143r-70,143r-20,0","w":239},"_":{"d":"0,45r0,-18r180,0r0,18r-180,0","w":180},"a":{"d":"92,-171v87,0,61,101,69,171r-20,0v-2,-7,0,-20,-3,-27v-20,45,-117,44,-117,-18v0,-60,68,-56,118,-56v8,-60,-66,-61,-98,-31r-13,-15v16,-15,42,-24,64,-24xm139,-84v-44,-2,-96,3,-96,38v0,21,20,31,38,31v51,-2,60,-31,58,-69","w":186},"b":{"d":"24,0r0,-272r22,0r0,135v42,-66,155,-29,155,54v0,82,-112,118,-155,54r0,29r-22,0xm44,-83v0,37,30,68,69,68v41,0,66,-31,66,-68v0,-37,-25,-68,-66,-68v-39,0,-69,31,-69,68","w":219},"c":{"d":"152,-38r16,14v-47,58,-149,19,-149,-59v0,-80,103,-118,150,-59r-17,13v-36,-46,-117,-12,-111,46v-6,58,77,93,111,45","w":173},"d":{"d":"174,0r0,-29v-43,64,-155,28,-155,-54v0,-83,112,-120,155,-54r0,-135r22,0r0,272r-22,0xm176,-83v0,-37,-30,-68,-69,-68v-41,0,-66,31,-66,68v0,37,25,68,66,68v39,0,69,-31,69,-68","w":219},"e":{"d":"160,-45r16,14v-43,66,-157,33,-157,-53v0,-50,35,-87,84,-87v56,0,82,41,78,93r-140,0v0,18,13,63,63,63v22,0,45,-12,56,-30xm41,-97r118,0v0,-30,-25,-54,-57,-54v-47,0,-61,45,-61,54","w":200},"f":{"d":"41,0r0,-147r-35,0r0,-19r35,0r0,-55v3,-54,39,-61,75,-51r-5,19v-27,-11,-48,1,-48,40r0,47r39,0r0,19r-39,0r0,147r-22,0","w":106,"k":{"f":6}},"g":{"d":"196,-166r0,155v0,83,-55,97,-89,97v-33,0,-65,-13,-82,-35r17,-15v15,21,39,31,65,31v77,0,66,-55,67,-99v-39,63,-152,23,-152,-53v0,-76,113,-118,152,-53r0,-28r22,0xm175,-85v0,-36,-30,-66,-66,-66v-36,0,-66,30,-66,66v0,36,30,66,66,66v36,0,66,-30,66,-66","w":219},"h":{"d":"29,0r0,-272r21,0r1,135v9,-18,30,-34,57,-34v90,3,57,94,63,171r-21,0v-7,-60,24,-151,-42,-151v-32,0,-58,22,-58,68r0,83r-21,0","w":200},"i":{"d":"32,0r0,-166r22,0r0,166r-22,0xm27,-232v0,-9,7,-16,16,-16v9,0,16,7,16,16v0,9,-7,16,-16,16v-9,0,-16,-7,-16,-16","w":86},"j":{"d":"54,-166r0,195v4,40,-23,66,-65,54r3,-19v25,11,40,-8,40,-33r0,-197r22,0xm27,-232v0,-9,7,-16,16,-16v9,0,16,7,16,16v0,9,-7,16,-16,16v-9,0,-16,-7,-16,-16","w":86},"k":{"d":"29,0r0,-272r21,0r0,181r82,-75r32,0r-86,76r96,90r-33,0r-91,-88r0,88r-21,0","w":173},"l":{"d":"32,0r0,-272r22,0r0,272r-22,0","w":86},"m":{"d":"29,0r-2,-166r22,0v1,12,-2,27,2,29v16,-38,93,-51,107,2v10,-25,34,-36,57,-36v90,3,57,94,63,171r-22,0v-7,-59,25,-151,-41,-151v-75,0,-45,85,-51,151r-21,0v-4,-55,18,-151,-35,-151v-32,0,-58,22,-58,68r0,83r-21,0","w":306},"n":{"d":"29,0r-2,-166r22,0v1,12,-2,27,2,29v9,-18,30,-34,57,-34v90,3,57,94,63,171r-21,0v-7,-60,24,-151,-42,-151v-32,0,-58,22,-58,68r0,83r-21,0","w":200},"o":{"d":"19,-83v0,-51,37,-88,88,-88v51,0,87,37,87,88v0,51,-36,87,-87,87v-51,0,-88,-36,-88,-87xm41,-83v0,37,25,68,66,68v41,0,66,-31,66,-68v0,-37,-25,-68,-66,-68v-41,0,-66,31,-66,68","w":213},"p":{"d":"24,82r0,-248r22,0r0,29v42,-66,155,-29,155,54v0,82,-112,118,-155,54r0,111r-22,0xm44,-83v0,37,30,68,69,68v41,0,66,-31,66,-68v0,-37,-25,-68,-66,-68v-39,0,-69,31,-69,68","w":219},"q":{"d":"174,82r0,-111v-43,64,-155,28,-155,-54v0,-83,112,-120,155,-54r0,-29r22,0r0,248r-22,0xm176,-83v0,-37,-30,-68,-69,-68v-41,0,-66,31,-66,68v0,37,25,68,66,68v39,0,69,-31,69,-68","w":219},"r":{"d":"29,0r-2,-166r22,0v0,12,0,26,1,29v13,-22,35,-40,66,-32r-3,21v-76,-10,-63,76,-63,148r-21,0","w":119,"k":{",":33,".":33,"c":6,"d":6,"e":6,"g":6,"o":6,"q":6,"-":20}},"s":{"d":"10,-30r19,-12v14,35,87,38,87,-4v0,-46,-99,-14,-99,-77v0,-60,96,-62,115,-16r-19,11v-10,-30,-74,-33,-74,3v0,45,98,11,98,78v-1,66,-104,64,-127,17","w":153},"t":{"d":"110,-166r0,19r-47,0r0,108v-1,26,28,28,47,18r2,20v-34,14,-71,-2,-71,-38r0,-108r-36,0r0,-19r36,0r0,-48r22,0r0,48r47,0","w":119},"u":{"d":"170,-166r1,166r-21,0v-1,-12,1,-27,-3,-29v-9,18,-30,33,-57,33v-90,-2,-56,-93,-63,-170r22,0v7,59,-25,151,41,151v32,0,58,-22,58,-68r0,-83r22,0","w":200},"v":{"d":"73,0r-68,-166r25,0r55,143r54,-143r22,0r-65,166r-23,0","w":166,"k":{",":27,".":27}},"w":{"d":"58,0r-53,-166r24,0r41,139r47,-139r20,0r50,139r39,-139r22,0r-49,166r-22,0r-51,-136r-46,136r-22,0","w":253,"k":{",":20,".":20}},"x":{"d":"2,0r69,-90r-59,-76r28,0r46,63r45,-63r27,0r-58,76r71,90r-28,0r-58,-75r-57,75r-26,0","w":173},"y":{"d":"161,-166r-85,217v-9,28,-34,42,-67,32r2,-20v43,18,49,-33,62,-62r-68,-167r25,0r55,140r53,-140r23,0","w":166,"k":{",":33,".":33}},"z":{"d":"10,0r0,-13r99,-134r-94,0r0,-19r124,0r0,13r-99,134r104,0r0,19r-134,0","w":153},"{":{"d":"99,-264r0,18v-40,-6,-28,50,-28,86v0,39,-22,52,-26,57v5,3,26,19,26,56v0,34,-14,90,28,86r0,17v-55,11,-51,-45,-50,-96v0,-28,-5,-55,-25,-55r0,-17v59,-7,-21,-167,75,-152","w":119},"|":{"d":"31,-270r18,0r0,360r-18,0r0,-360","w":79},"}":{"d":"21,56r0,-17v40,6,28,-50,28,-86v0,-39,22,-52,26,-57v-5,-3,-26,-19,-26,-56v0,-34,14,-90,-28,-86r0,-18v55,-11,51,45,50,97v0,28,5,55,25,55r0,17v-60,6,21,166,-75,151","w":119},"~":{"d":"82,-131v23,0,56,24,77,24v14,0,23,-11,31,-24r13,11v-8,17,-21,33,-45,33v-24,0,-56,-24,-77,-25v-14,0,-24,12,-32,25r-12,-12v8,-15,21,-32,45,-32","w":239},"'":{"d":"36,-173r0,-82r22,0r0,82r-22,0","w":93},"`":{"d":"27,-255r37,52r-18,0r-48,-52r29,0","w":86},"\u00a0":{"w":93}}});
;
/*!
 * The following copyright notice may not be removed under any circumstances.
 * 
 * Copyright:
 * 4.1 (c)1986-92 by Richard A. Ware. All Rights Reserved.
 */
Cufon.registerFont({"w":194,"face":{"font-family":"AlexandriaFLF","font-weight":500,"font-stretch":"normal","units-per-em":"360","panose-1":"2 0 6 3 7 0 0 2 0 4","ascent":"288","descent":"-72","x-height":"4","bbox":"0 -277 345 73","underline-thickness":"6.84","underline-position":"-44.64","unicode-range":"U+0020-U+007E"},"glyphs":{" ":{"w":126},"!":{"d":"40,-257r-14,0r0,191r14,0r0,-191xm33,-24v-8,0,-13,4,-13,12v0,8,5,12,13,13v6,-1,13,-5,13,-13v0,-8,-6,-12,-13,-12","w":63},"\"":{"d":"77,-262r-15,0r4,58r7,0xm37,-262r-15,0r4,58r7,0","w":97,"k":{"A":36}},"#":{"d":"75,-186r11,-45r-9,0r-11,45r-41,0r-3,9r42,0r-11,41r-36,0r-3,9r37,0r-11,45r9,0r11,-45r49,0r-11,47r9,0r11,-47r40,0r3,-9r-40,0r10,-41r37,0r3,-9r-38,0r11,-45r-9,0r-11,45r-49,0xm62,-136r10,-41r50,0r-10,41r-50,0","w":185},"$":{"d":"101,-132v43,4,65,65,36,100v-9,10,-21,17,-36,20r0,-120xm101,-244v20,2,33,12,44,23r0,19r13,0r0,-52r-13,0r0,14v-12,-11,-27,-17,-44,-18r0,-19r-15,0r0,19v-33,4,-57,24,-58,60v-1,39,28,52,58,60r0,128v-34,-5,-62,-18,-59,-61r-13,0r0,71r13,0r0,-25v12,16,31,28,59,28r0,19r15,0r0,-20v36,-5,64,-33,64,-74v-1,-44,-28,-64,-64,-75r0,-97xm86,-152v-46,-2,-59,-69,-17,-87v5,-3,11,-4,17,-5r0,92","w":174},"%":{"d":"238,-257r-15,0r-158,257r14,0xm77,-115v42,0,71,-29,71,-71v0,-41,-27,-70,-71,-70v-42,0,-70,28,-70,70v0,43,29,71,70,71xm77,-129v-34,0,-56,-23,-56,-57v0,-35,23,-57,56,-57v34,0,57,22,57,57v0,35,-22,57,-57,57xm226,6v44,0,71,-29,71,-70v0,-42,-28,-71,-71,-71v-41,0,-70,28,-70,71v0,42,28,70,70,70xm226,-7v-33,0,-56,-22,-56,-57v0,-35,23,-57,56,-57v34,0,57,22,57,57v0,35,-23,57,-57,57","w":302},"&":{"d":"147,-35v-42,41,-150,20,-119,-59v8,-23,28,-37,45,-51xm79,-164v-27,-29,-13,-78,35,-78v23,0,41,4,54,12r0,25r14,0r0,-35v-39,-26,-134,-25,-133,38v0,20,8,34,17,46v-26,21,-55,40,-57,85v-4,80,101,87,146,49r16,22r19,0r-23,-32v26,-24,38,-62,41,-106r24,0r0,-11r-62,0r0,11r24,0v-2,37,-15,73,-35,94","w":242},"'":{"d":"37,-257r-15,0r4,57r7,0","w":59,"k":{"v":18,"s":14,"e":22,"d":18,"A":47}},"(":{"d":"58,22v-28,-86,-27,-205,0,-293r-15,0v-28,88,-28,208,0,293r15,0","w":73},")":{"d":"21,-271v27,89,28,206,0,293r15,0v28,-93,27,-202,0,-293r-15,0","w":73},"*":{"d":"40,-251v-12,0,-5,24,-1,30v-7,-6,-13,-15,-24,-17v-11,10,5,15,15,18v3,1,5,2,7,2v-9,2,-37,11,-22,19v11,-2,17,-11,24,-17v-10,9,-2,46,6,22v0,-7,-2,-17,-4,-22v6,6,20,25,28,12v-3,-10,-17,-11,-27,-14v10,-2,37,-10,22,-20v-11,2,-18,12,-23,17v2,-7,9,-28,-1,-30","w":77},"+":{"d":"54,-141r-37,0r0,15r37,0r0,36r14,0r0,-36r37,0r0,-15r-37,0r0,-37r-14,0r0,37","w":119},",":{"d":"49,-19r-13,-6r-27,59r5,3","w":58,"k":{"1":32,"\"":29}},"-":{"d":"93,-141r-74,0r0,15r74,0r0,-15","w":111},".":{"d":"30,1v8,1,13,-6,13,-13v0,-7,-6,-13,-13,-12v-7,-1,-13,6,-13,12v0,7,5,14,13,13","w":58,"k":{"1":36,"\"":29}},"\/":{"d":"111,-257r-15,0r-88,257r15,0","w":113,"k":{"1":36}},"0":{"d":"98,4v122,0,123,-264,0,-264v-67,0,-91,62,-91,132v0,70,25,132,91,132xm98,-11v-104,-5,-103,-229,0,-234v103,7,102,227,0,234","k":{"6":-5,"1":48,"0":-7}},"1":{"d":"107,-257r-37,0r0,13r22,0r0,231r-22,0r0,13r59,0r0,-13r-22,0r0,-244","k":{"9":49,"8":48,"7":42,"6":53,"5":55,"4":59,"3":53,"2":46,"1":97,"0":51,"\/":32,".":43,",":40}},"2":{"d":"159,-151v47,-55,-13,-132,-89,-102v-23,10,-39,28,-44,55r16,0v4,-58,120,-64,120,2v0,62,-70,73,-102,111v-19,22,-31,51,-37,85r153,0r0,-15r-132,0v12,-72,78,-92,115,-136","k":{"4":33,"1":40}},"3":{"d":"160,-69v5,73,-119,73,-127,15r-15,0v7,37,35,57,80,57v46,0,77,-26,77,-72v0,-37,-18,-59,-45,-69v71,-19,47,-122,-32,-122v-44,0,-69,21,-77,57r15,0v6,-28,27,-43,62,-43v35,0,62,18,61,51v-1,40,-33,52,-77,49r0,15v50,-5,75,18,78,62","k":{"2":11,"1":53}},"4":{"d":"156,-77r0,-184r-11,0r-130,188r0,10r126,0r0,48r-15,0r0,15r45,0r0,-15r-15,0r0,-48r23,0r0,-14r-23,0xm141,-77r-106,0r106,-152r0,152","k":{"1":40}},"5":{"d":"185,-87v3,-70,-69,-111,-134,-82r16,-72r81,0r0,19r14,0r0,-35r-107,0r-21,102r13,4v50,-32,128,-1,123,64v-3,45,-30,76,-76,76v-37,0,-58,-20,-70,-46r-14,6v15,30,40,55,84,55v54,0,89,-37,91,-91","k":{"1":46}},"6":{"d":"28,-133v5,-71,50,-127,132,-106r5,-14v-101,-23,-157,48,-153,150v2,64,27,108,89,108v55,0,86,-35,86,-90v0,-54,-32,-86,-86,-86v-35,0,-59,16,-73,38xm101,-7v-51,0,-78,-46,-73,-102v12,-26,34,-47,73,-47v46,0,72,29,72,71v0,46,-27,78,-72,78","k":{"7":13,"1":35}},"7":{"d":"85,-15v2,-110,45,-173,102,-229r0,-13r-173,0r0,39r14,0r0,-25r139,0v-54,54,-97,119,-96,228r-21,0r0,15r55,0r0,-15r-20,0","k":{"9":13,"8":14,"6":20,"5":26,"4":55,"3":16,"2":16,"1":39,"0":15,"\/":32,".":36,",":32}},"8":{"d":"123,-138v26,-8,51,-25,51,-59v0,-42,-33,-63,-77,-63v-44,0,-76,21,-76,64v0,33,22,50,48,58v-28,9,-52,30,-52,67v0,46,32,74,80,74v49,0,80,-28,80,-74v0,-38,-25,-59,-54,-67xm97,-246v35,0,62,17,62,49v0,33,-28,50,-62,50v-34,0,-61,-17,-61,-50v0,-32,26,-49,61,-49xm97,-128v39,0,64,21,64,58v0,37,-25,58,-64,58v-39,0,-63,-22,-63,-58v0,-36,24,-58,63,-58","k":{"5":14,"2":11,"1":46}},"9":{"d":"172,-123v-3,73,-51,126,-132,106r-5,15v103,22,157,-50,154,-151v-2,-63,-27,-108,-90,-108v-55,0,-86,36,-86,90v0,54,32,84,86,86v36,0,59,-16,73,-38xm99,-248v53,0,79,48,73,102v-13,26,-35,47,-73,47v-45,0,-72,-29,-72,-72v0,-46,26,-77,72,-77","k":{"1":40}},":":{"d":"30,1v8,1,13,-6,13,-13v0,-7,-6,-13,-13,-12v-7,-1,-13,6,-13,12v0,7,5,14,13,13xm30,-145v8,1,13,-6,13,-13v0,-7,-6,-13,-13,-13v-7,0,-13,6,-13,13v0,7,5,14,13,13","w":58},";":{"d":"54,-19r-13,-6r-26,59r4,3xm37,-145v8,1,14,-5,13,-13v1,-8,-6,-13,-13,-13v-7,0,-13,6,-13,13v0,7,5,14,13,13","w":73},"<":{"d":"83,-190r-72,56r71,56r8,-9r-60,-47r61,-47","w":101},"=":{"d":"95,-119r-82,0r0,15r82,0r0,-15xm95,-163r-82,0r0,15r82,0r0,-15","w":106},">":{"d":"21,-78r71,-56r-72,-56r-8,9r61,47r-60,47","w":101},"?":{"d":"135,-189v-1,43,-24,63,-69,62r0,61r15,0r0,-48v42,-5,66,-28,69,-75v5,-74,-98,-95,-131,-41v-3,6,-6,12,-8,17r13,6v10,-20,23,-39,54,-39v36,0,57,22,57,57xm73,1v8,1,13,-6,13,-13v0,-7,-6,-13,-13,-12v-7,-1,-13,6,-13,12v0,7,5,14,13,13","w":157},"@":{"d":"98,-100v-23,1,-34,-18,-34,-39v0,-24,16,-40,41,-40v22,0,34,14,34,36v0,26,-15,42,-41,43xm108,-59v-55,-1,-87,-31,-87,-85v0,-51,34,-84,87,-84v51,0,87,31,87,80v0,23,-8,46,-31,48v-31,-7,-11,-56,-12,-84r-7,0r-2,23v-11,-42,-93,-27,-86,22v-9,54,73,59,84,19v0,15,6,28,22,27v30,-1,39,-27,39,-55v0,-53,-37,-87,-94,-87v-58,0,-90,37,-95,91v-6,82,99,120,158,71r-5,-5v-13,12,-33,19,-58,19","w":214},"A":{"d":"133,-241r53,147r-105,0xm78,-15r-24,0r22,-64r116,0r22,64r-24,0r0,15r62,0r0,-15r-20,0r-82,-227r21,0r0,-15r-75,0r0,15r20,0r-81,227r-23,0r0,15r66,0r0,-15","w":262,"k":{"y":57,"w":39,"v":42,"u":35,"t":23,"r":11,"q":27,"p":24,"o":26,"n":11,"j":15,"i":11,"g":24,"e":26,"d":24,"c":22,"a":18,"Y":41,"W":46,"V":41,"U":22,"T":32,"Q":30,"O":30,"J":20,"G":27,"C":29,"'":36,"\"":29}},"B":{"d":"144,-142v21,-8,35,-25,35,-53v0,-39,-26,-61,-67,-62r-105,0r0,15r28,0r0,227r-28,0r0,15v86,0,193,16,190,-73v-1,-38,-23,-60,-53,-69xm161,-195v0,53,-56,48,-109,47r0,-94v53,-2,109,-6,109,47xm179,-73v0,62,-64,60,-127,58r0,-118v64,-3,128,-2,127,60","w":203,"k":{"y":13,"u":11,"Y":26,"X":20,"W":20,"V":20,"T":15,"J":11,"A":18}},"C":{"d":"145,-14v-71,-2,-114,-45,-114,-114v0,-70,45,-112,114,-115v36,-1,59,14,79,30r0,23r17,0r0,-64r-17,0r0,20v-19,-14,-44,-27,-79,-26v-79,4,-131,52,-131,132v0,80,52,129,131,132v44,2,72,-17,93,-39r0,-22v-21,24,-48,43,-93,43","w":253,"k":{"y":11,"a":14,"A":22}},"D":{"d":"118,0v75,-4,120,-52,120,-129v0,-77,-45,-128,-120,-128r-108,0r0,15r27,0r0,227r-27,0r0,15r108,0xm221,-129v0,88,-65,125,-166,114r0,-227v101,-10,166,25,166,113","w":242,"k":{"Y":26,"W":18,"V":18,"T":13,"J":15,"I":11,"E":11,"D":13,"A":24}},"E":{"d":"161,-215r0,-42r-151,0r0,15r27,0r0,227r-27,0r0,15r151,0r0,-43r-16,0r0,28r-91,0r0,-120r68,0r0,18r16,0r0,-52r-16,0r0,19r-68,0r0,-92r91,0r0,27r16,0","w":174,"k":{"y":18,"w":13,"v":15,"u":17,"q":17,"o":13,"e":11,"d":14}},"F":{"d":"163,-215r0,-42r-151,0r0,15r27,0r0,227r-27,0r0,15r72,0r0,-15r-27,0r0,-122r66,0r0,18r16,0r0,-52r-16,0r0,19r-66,0r0,-90r91,0r0,27r15,0","w":174,"k":{"y":19,"x":15,"u":18,"r":23,"o":26,"j":17,"i":13,"e":27,"a":32,"S":15,"Q":18,"O":22,"J":33,"G":22,"C":18,"A":35,";":30,":":28,".":63,"-":17,",":70}},"G":{"d":"226,-231v-18,-18,-48,-29,-84,-29v-77,0,-129,53,-129,132v0,80,52,132,129,132v48,0,82,-20,96,-53r0,49r18,0r0,-109r27,0r0,-16r-136,0r0,16r92,0v-5,58,-33,97,-97,95v-69,-2,-112,-45,-112,-114v0,-70,44,-112,112,-115v38,-2,65,17,84,34r0,23r18,0r0,-67r-18,0r0,22","w":288,"k":{"y":16,"Y":22,"W":23,"V":23,"T":17,"J":18,"E":15,"A":18}},"H":{"d":"84,-242r0,-15r-72,0r0,15r27,0r0,227r-27,0r0,15r72,0r0,-15r-27,0r0,-121r136,0r0,121r-28,0r0,15r72,0r0,-15r-27,0r0,-227r27,0r0,-15r-72,0r0,15r28,0r0,91r-136,0r0,-91r27,0","w":246,"k":{"y":13,"u":13,"e":13}},"I":{"d":"90,-242r0,-15r-70,0r0,15r26,0r0,227r-26,0r0,15r70,0r0,-15r-26,0r0,-227r26,0","w":108,"k":{"O":18,"G":14,"C":14}},"J":{"d":"76,-33v6,38,-28,51,-48,31r-12,12v26,30,77,14,77,-43r0,-209r21,0r0,-15r-63,0r0,15r25,0r0,209","w":121,"k":{"A":15}},"K":{"d":"66,-242r0,-15r-54,0r0,15r19,0r0,227r-19,0r0,15r54,0r0,-15r-18,0r0,-110r5,-5r95,115r-19,0r0,15r60,0r0,-15r-19,0r-104,-127r100,-100r18,0r0,-15r-58,0r0,15r18,0r-96,96r0,-96r18,0","w":192,"k":{"y":46,"w":36,"v":38,"u":26,"o":23,"g":23,"e":25,"a":13,"Q":30,"O":31,"G":35,"C":31}},"L":{"d":"72,-242r0,-15r-59,0r0,15r21,0r0,227r-22,0r0,15r135,0r0,-37r-17,0r0,22r-79,0r0,-227r21,0","w":157,"k":{"y":38,"w":33,"v":31,"u":18,"o":18,"e":13,"a":13,"Y":42,"W":42,"V":39,"U":14,"T":35,"Q":35,"O":23,"G":25,"C":24}},"M":{"d":"272,-225r0,210r-24,0r0,15r65,0r0,-15r-24,0r0,-227r24,0r0,-15r-43,0r-108,224r-108,-224r-43,0r0,15r24,0r0,227r-24,0r0,15r65,0r0,-15r-24,0r0,-210r110,230","w":321,"k":{"y":18,"u":14,"o":11,"e":11}},"N":{"d":"226,-242r0,205r-185,-220r-32,0r0,15r22,0r0,227r-22,0r0,15r61,0r0,-15r-22,0r0,-205r186,221r9,0r0,-243r22,0r0,-15r-62,0r0,15r23,0","w":270,"k":{"y":11,"e":13,"a":11,"A":18}},"O":{"d":"14,-128v0,79,53,132,131,132v79,0,133,-53,133,-132v0,-79,-54,-132,-133,-132v-78,0,-131,53,-131,132xm260,-128v0,69,-46,114,-115,114v-69,0,-114,-46,-114,-114v0,-69,45,-115,114,-115v69,0,115,46,115,115","w":289,"k":{"u":13,"l":11,"b":11,"Z":14,"Y":25,"X":29,"W":27,"V":24,"T":17,"N":11,"J":31,"I":13,"F":18,"D":13,"A":33}},"P":{"d":"48,-118v66,5,120,-6,118,-69v-2,-43,-26,-70,-70,-70r-89,0r0,15r24,0r0,227r-24,0r0,15r65,0r0,-15r-24,0r0,-103xm48,-242v55,-4,100,2,100,55v0,54,-45,57,-100,54r0,-109","w":174,"k":{"o":13,"e":13,"a":19,"Y":15,"J":29,"I":11,"A":38,";":15,".":76,",":79}},"Q":{"d":"88,32v56,-40,114,26,171,-2r0,-16v-34,19,-77,-1,-104,-11v72,-9,123,-56,123,-131v0,-79,-54,-132,-133,-132v-78,0,-132,54,-132,132v0,79,53,127,132,132v-22,1,-41,2,-57,13r0,15xm260,-128v0,69,-46,114,-115,114v-69,0,-114,-46,-114,-114v0,-69,45,-115,114,-115v69,0,115,46,115,115","w":291,"k":{"Y":33,"T":15,"A":26}},"R":{"d":"78,-116v57,5,91,-20,91,-70v0,-43,-25,-70,-70,-71r-91,0r0,15r25,0r0,227r-25,0r0,15r68,0r0,-15r-26,0r0,-101r7,0r93,116r37,0r0,-15r-28,0xm51,-242v56,-4,102,3,101,56v0,51,-45,60,-101,55r0,-111","w":197,"k":{"y":15,"u":13,"o":24,"g":20,"e":20,"d":18,"c":16,"a":22,"Y":21,"W":19,"V":15,"U":11,"T":22,"Q":18,"O":15,"J":16,"G":15,"C":14}},"S":{"d":"45,-198v-4,-52,82,-54,98,-19r0,18r15,0r0,-55r-15,0r0,14v-31,-35,-115,-16,-115,42v0,84,120,46,120,126v0,36,-23,58,-59,58v-35,0,-63,-18,-60,-60r-15,0r0,74r15,0r0,-25v34,48,136,30,136,-47v0,-67,-69,-66,-107,-95v-9,-7,-12,-17,-13,-31","w":172,"k":{"y":17,"w":16,"J":13}},"T":{"d":"174,-257r-161,0r0,42r15,0r0,-27r57,0r0,227r-27,0r0,15r72,0r0,-15r-27,0r0,-227r56,0r0,27r15,0r0,-42","w":187,"k":{"y":44,"w":41,"v":45,"u":47,"s":30,"r":36,"o":45,"j":24,"i":26,"h":11,"f":13,"e":45,"c":30,"a":42,"S":11,"O":18,"J":30,"G":18,"C":18,"A":30,";":39,":":35,".":42,"-":33,",":44}},"U":{"d":"104,-15v-34,-1,-57,-15,-57,-49r0,-178r22,0r0,-15r-61,0r0,15r22,0r0,176v0,46,32,69,74,69v43,0,75,-24,75,-69r0,-176r22,0r0,-15r-61,0r0,15r22,0r0,178v-2,33,-23,49,-58,49","w":208,"k":{"w":11,"v":11,"s":14,"a":15,"J":29,"A":26}},"V":{"d":"64,-257r-58,0r0,15r20,0r78,251r78,-251r19,0r0,-15r-58,0r0,15r22,0r-61,200r-61,-200r21,0r0,-15","w":206,"k":{"y":31,"u":26,"t":13,"s":30,"r":31,"o":39,"j":22,"i":22,"g":39,"f":11,"e":37,"d":33,"c":33,"a":40,"Q":17,"O":20,"J":35,"G":24,"C":26,"A":46,";":51,":":44,".":57,"-":28,",":70}},"W":{"d":"76,-257r-67,0r0,15r24,0r78,251r65,-218r65,218r77,-251r27,0r0,-15r-70,0r0,15r26,0r-60,200r-65,-219r-65,219r-60,-200r25,0r0,-15","w":352,"k":{"y":37,"u":40,"s":33,"r":28,"o":42,"m":31,"i":22,"e":44,"a":51,"S":18,"Q":26,"O":30,"J":36,"G":24,"C":30,"A":52,";":50,":":37,".":68,"-":26,",":72}},"X":{"d":"70,-257r-54,0r0,15r20,0r61,112r-65,115r-22,0r0,15r58,0r0,-15r-19,0r57,-100r57,100r-19,0r0,15r58,0r0,-15r-22,0r-66,-115r62,-112r20,0r0,-15r-54,0r0,15r17,0r-54,98r-52,-98r17,0r0,-15","w":211,"k":{"y":31,"o":17,"e":21,"a":19,"O":24,"C":25}},"Y":{"d":"65,-257r-54,0r0,15r20,0r63,112r0,115r-20,0r0,15r57,0r0,-15r-20,0r0,-115r64,-112r20,0r0,-15r-54,0r0,15r16,0r-54,98r-55,-98r17,0r0,-15","w":204,"k":{"v":33,"u":41,"t":20,"s":44,"r":36,"q":50,"p":41,"o":46,"n":39,"m":42,"i":18,"g":44,"e":34,"d":48,"c":42,"a":43,"S":26,"Q":28,"O":29,"J":29,"G":20,"C":38,"A":41,";":59,":":48,".":53,"-":26,",":53}},"Z":{"d":"183,-257r-165,0r0,46r15,0r0,-31r123,0r-147,242r174,0r0,-50r-15,0r0,35r-130,0","w":196,"k":{"y":22,"u":14,"o":11,"e":14,"O":14}},"[":{"d":"60,-259r0,-13r-39,0r0,289r40,0r0,-12r-25,-1r0,-263r24,0","w":77},"\\":{"d":"96,0r-73,-257r-15,0r74,257r14,0","w":101},"]":{"d":"62,17r0,-289r-39,0r0,13r25,0r0,263r-26,1r0,12r40,0","w":77},"^":{"d":"59,-265r-41,103r11,4r35,-88r36,89r10,-5r-40,-103r-11,0","w":126},"_":{"d":"132,13r-132,0r0,13r132,0r0,-13","w":128},"`":{"d":"66,-217r10,-11r-44,-45r-11,11","w":95},"a":{"d":"16,-64v0,74,108,87,140,39r0,25r41,0r0,-13r-27,0r0,-113v4,-67,-95,-81,-133,-41r9,11v37,-35,126,-19,110,54v-30,-48,-140,-38,-140,38xm90,-11v-35,0,-59,-18,-59,-53v0,-35,24,-54,59,-54v37,0,63,18,63,54v0,35,-27,53,-63,53","w":203,"k":{"y":24,"w":18,"v":20,"u":22,"t":16,"p":11,"o":11,"e":14}},"b":{"d":"118,-176v46,0,68,36,68,82v0,46,-22,83,-68,83v-47,0,-68,-37,-68,-83v0,-46,22,-82,68,-82xm118,-191v-35,0,-57,18,-68,42r0,-121r-37,0r0,13r22,0r0,244r-22,0r0,13r37,0r0,-36v12,23,33,40,68,40v57,0,83,-42,83,-98v0,-55,-26,-97,-83,-97","w":213,"k":{"y":22,"w":11,"u":13,"r":11,"k":18,"b":13}},"c":{"d":"25,-92v-3,-65,62,-100,120,-75r0,17r13,0r0,-43r-13,0r0,9v-73,-23,-135,21,-135,92v0,81,90,123,148,80r0,-19v-13,13,-27,22,-53,22v-51,-1,-77,-33,-80,-83","w":173,"k":{"q":11,"k":11}},"d":{"d":"98,4v35,0,56,-18,69,-40r0,36r36,0r0,-13r-22,0r0,-257r-41,0r0,13r27,0r0,108v-13,-23,-33,-41,-69,-42v-57,0,-83,44,-83,97v0,54,25,97,83,98xm98,-176v47,0,69,37,69,82v0,45,-22,83,-69,83v-46,0,-68,-37,-68,-83v0,-45,21,-82,68,-82","w":216,"k":{"y":17,"w":15,"v":18,"u":15,"o":13,"g":13,"e":11,"d":17,"a":13}},"e":{"d":"168,-46v-14,38,-90,49,-118,13v-11,-14,-20,-33,-19,-58r157,0v1,-60,-30,-99,-87,-99v-56,0,-82,40,-87,97v-9,96,123,132,166,55xm32,-104v-6,-78,110,-95,134,-28v3,9,6,18,6,28r-140,0","w":202,"k":{"y":16,"w":11,"k":14,"'":18}},"f":{"d":"46,-230v-2,-25,26,-25,40,-16r0,-15v-24,-11,-55,-3,-55,28r0,48r-20,0r0,12r20,0r0,160r-22,0r0,13r59,0r0,-13r-22,0r0,-160r35,0r0,-12r-35,0r0,-45","w":95,"k":{"u":11,"o":17,"g":13,"e":18,"d":15,"a":18}},"g":{"d":"15,-94v0,55,27,98,83,98v35,0,56,-18,69,-40v5,52,-11,92,-58,91v-19,1,-31,-6,-43,-11r-7,13v46,28,122,8,122,-60r0,-170r22,0r0,-12r-36,0r0,36v-13,-23,-33,-42,-69,-42v-56,0,-83,43,-83,97xm98,-176v47,0,69,37,69,82v0,45,-22,83,-69,83v-47,0,-68,-37,-68,-83v0,-45,21,-82,68,-82","w":213,"k":{"h":11,"g":13,"e":11,"a":15}},"h":{"d":"165,-130v6,-62,-88,-80,-115,-37r0,-103r-37,0r0,13r22,0r0,244r-22,0r0,13r59,0r0,-13r-22,0v5,-67,-22,-163,50,-163v73,0,45,98,50,163r-22,0r0,13r59,0r0,-13r-22,0r0,-117","w":196,"k":{"y":27,"w":20,"v":18,"u":17,"t":11,"o":11}},"i":{"d":"54,-185r-37,0r0,12r22,0r0,160r-22,0r0,13r59,0r0,-13r-22,0r0,-172xm46,-226v7,0,12,-4,12,-11v0,-7,-5,-13,-12,-12v-7,0,-11,5,-11,12v0,6,5,11,11,11","w":91,"k":{"y":16,"w":15,"v":11,"u":14,"q":11,"g":11,"d":11}},"j":{"d":"15,52v20,-9,39,-22,39,-52r0,-185r-37,0r0,12r23,0r0,173v0,22,-15,31,-32,39xm47,-226v6,0,12,-5,12,-11v0,-6,-5,-12,-12,-12v-7,0,-11,5,-11,12v0,6,5,11,11,11","w":79,"k":{"a":11}},"k":{"d":"53,-105r0,-165r-36,0r0,13r22,0r0,244r-22,0r0,13r58,0r0,-13r-22,0r0,-75r6,-5r80,80r-24,0r0,13r63,0r0,-13r-20,0r-88,-89r71,-71r25,0r0,-12r-74,0r0,12r29,0","w":189,"k":{"y":15,"u":11,"o":27,"g":22,"e":27,"d":18,"c":24,"a":22}},"l":{"d":"49,-13r0,-257r-37,0r0,13r22,0r0,244r-22,0r0,13r59,0r0,-13r-22,0","w":81,"k":{"y":13,"o":10,"e":13,"a":11}},"m":{"d":"231,-141v5,-56,-82,-62,-99,-23v-8,-30,-70,-35,-84,-7r0,-14r-37,0r0,12r22,0r0,160r-22,0r0,13r58,0r0,-13r-21,0r0,-127v0,-23,17,-35,40,-35v22,1,37,13,37,34r0,128r-22,0r0,13r58,0r0,-13r-22,0v7,-60,-26,-161,41,-162v21,0,37,13,36,34r0,128r-22,0r0,13r59,0r0,-13r-22,0r0,-128","w":261,"k":{"y":20,"w":13,"v":18,"u":13}},"n":{"d":"168,-130v7,-64,-87,-78,-115,-37r0,-18r-36,0r0,12r22,0r0,160r-22,0r0,13r58,0r0,-13r-22,0r0,-123v6,-22,23,-39,51,-40v72,-2,45,97,50,163r-22,0r0,13r59,0r0,-13r-23,0r0,-117","w":201,"k":{"y":24,"w":18,"v":20,"u":22,"t":13,"d":13,"a":11,"'":18}},"o":{"d":"101,4v57,0,88,-40,88,-97v0,-57,-31,-98,-88,-98v-58,0,-88,42,-88,98v0,55,30,97,88,97xm101,-176v49,0,74,36,74,83v0,47,-26,82,-74,82v-48,0,-73,-34,-73,-82v0,-48,25,-83,73,-83","w":201,"k":{"y":22,"x":11,"w":15,"v":13,"u":11,"n":11,"k":11,"j":11,"'":11}},"p":{"d":"114,-176v46,0,69,36,69,82v0,46,-21,83,-68,83v-46,0,-68,-36,-68,-83v0,-46,20,-82,67,-82xm198,-94v0,-55,-26,-97,-83,-97v-35,0,-57,17,-68,42r0,-36r-37,0r0,12r22,0r0,225r-22,0r0,13r59,0r0,-13r-22,0r0,-88v12,23,33,40,68,40v57,0,83,-42,83,-98","w":209,"k":{"y":18,"v":18,"u":13}},"q":{"d":"18,-94v0,56,26,98,82,98v36,0,56,-18,69,-40r0,101r40,0r0,-15r-25,0r0,-223r22,0r0,-12r-37,0r0,36v-12,-23,-35,-42,-70,-42v-56,0,-81,41,-81,97xm100,-176v47,0,69,36,69,82v0,47,-23,83,-69,83v-47,0,-68,-38,-68,-83v0,-44,22,-82,68,-82","w":216,"k":{"a":13}},"r":{"d":"114,-176v-15,-17,-58,-13,-65,8r0,-17r-36,0r0,12r22,0r0,160r-22,0r0,13r58,0r0,-13r-22,0r0,-125v-1,-30,36,-46,57,-26","w":120,"k":{"o":13,"k":16,"g":13,"e":16,"d":16,"c":11,"a":20,";":15,".":31,"-":17,",":35}},"s":{"d":"16,-143v0,67,96,34,101,92v3,46,-71,46,-93,20r0,-18r-13,0r0,49r13,0r0,-13v33,28,108,17,108,-38v0,-66,-97,-35,-101,-92v-3,-43,68,-40,85,-16r0,14r13,0r0,-43r-13,0r0,12v-29,-26,-100,-17,-100,33","w":138},"t":{"d":"85,-15v-13,9,-39,10,-39,-15r0,-143r35,0r0,-12r-35,0r0,-57r-15,0r0,57r-20,0r0,12r20,0r0,145v-3,30,32,40,54,27r0,-14","w":97,"k":{"t":13,"q":15,"o":14,"g":14,"e":22,"d":17,"a":13}},"u":{"d":"37,-58v-9,64,86,81,115,39r0,19r37,0r0,-13r-22,0r0,-172r-37,0r0,12r22,0r0,125v-6,21,-24,37,-50,38v-76,3,-43,-107,-50,-175r-37,0r0,12r22,0r0,115","w":201,"k":{"y":15,"w":17,"v":13,"u":18,"q":15,"o":15,"e":16,"d":13}},"v":{"d":"56,-173r0,-12r-46,0r0,12r14,0r59,173r7,0r54,-173r15,0r0,-12r-46,0r0,12r17,0r-44,137r-47,-137r17,0","w":168,"k":{"q":11,"o":14,"g":14,"e":13,"c":15,"a":22,".":36,",":36}},"w":{"d":"56,-173r0,-12r-46,0r0,12r14,0r59,173r4,0r45,-148r48,148r4,0r55,-173r15,0r0,-12r-46,0r0,12r16,0r-42,137r-47,-149r-4,0r-45,149r-47,-137r17,0","w":263,"k":{"u":11,"o":16,"g":14,"e":14,"d":14,"c":13,"a":20,".":32,",":32}},"x":{"d":"60,-185r-51,0r0,12r18,0r47,83r-45,77r-19,0r0,13r53,0r0,-13r-19,0r38,-66r40,66r-20,0r0,13r53,0r0,-13r-17,0r-49,-78r50,-82r17,0r0,-12r-52,0r0,12r19,0r-41,70r-40,-70r18,0r0,-12","w":163,"k":{"o":14,"e":16,"a":11}},"y":{"d":"75,-173r0,-12r-63,0r0,12r23,0r61,152r-31,81r-24,0r0,13r62,0r0,-13r-23,0r92,-233r20,0r0,-12r-56,0r0,12r21,0r-52,132r-54,-132r24,0","w":203,"k":{"u":11,"s":13,"q":22,"o":22,"g":20,"e":26,"d":17,"c":20,"b":13,"a":28,"W":30,".":54,",":50}},"z":{"d":"144,-185r-130,0r0,44r15,0r0,-32r90,0r-111,173r136,0r0,-43r-15,0r0,30r-94,0","w":156},"{":{"d":"18,-128v37,-20,-19,-132,46,-130r0,-12v-59,-8,-45,58,-45,110v0,15,-9,25,-16,32v44,23,-26,150,61,146r0,-11v-64,6,-7,-100,-46,-135","w":66},"|":{"d":"32,17r0,-289r-13,0r0,289r13,0","w":47},"}":{"d":"66,-128v-33,-19,0,-100,-26,-133v-7,-8,-20,-9,-35,-9r0,12v63,-6,8,99,46,130v-21,18,-14,66,-14,104v0,22,-8,33,-32,31r0,11v81,11,18,-108,61,-146","w":66},"~":{"d":"31,-125v30,-37,90,34,126,-11r-9,-10v-35,36,-94,-35,-127,12","w":174},"\u00a0":{"w":126}}});
/*!
 * The following copyright notice may not be removed under any circumstances.
 * 
 * Copyright:
 * 4.1 (c)1986-92 by Richard A. Ware. All Rights Reserved.
 */
Cufon.registerFont({"w":213,"face":{"font-family":"AlexandriaFLF","font-weight":700,"font-stretch":"normal","units-per-em":"360","panose-1":"2 0 8 3 6 0 0 2 0 4","ascent":"288","descent":"-72","x-height":"5","bbox":"0 -285.021 389 73","underline-thickness":"9","underline-position":"-43.56","unicode-range":"U+0020-U+007E"},"glyphs":{" ":{"w":99},"!":{"d":"60,-257r-34,0r0,193r34,0r0,-193xm43,4v11,0,21,-10,21,-22v0,-12,-10,-22,-21,-22v-27,0,-28,44,0,44","w":84},"\"":{"d":"91,-269r-22,0r4,69r13,0xm43,-269r-22,0r5,69r13,0","w":108,"k":{"A":25}},"#":{"d":"118,-245r-26,0r-11,43r-48,0r-7,25r49,0r-11,41r-44,0r-7,25r45,0r-11,43r26,0r11,-43r46,0r-10,43r25,0r11,-43r48,0r8,-25r-50,0r11,-41r46,0r8,-25r-48,0r11,-43r-26,0r-10,43r-47,0xm101,-177r46,0r-10,41r-47,0","w":236},"$":{"d":"43,-16v11,9,22,15,41,17r0,21r29,0r0,-21v41,-6,71,-27,71,-74v0,-49,-33,-68,-71,-80r0,-75v13,2,24,9,34,14r0,19r30,0r0,-62r-30,0r0,11v-9,-5,-19,-10,-34,-11r0,-20r-29,0r0,19v-37,6,-69,24,-69,64v0,45,31,61,69,69r0,97v-21,-7,-46,-13,-42,-45r-28,0r0,73r29,0r0,-16xm113,-116v44,7,52,81,0,87r0,-87xm84,-161v-28,-1,-49,-36,-25,-56v7,-6,15,-9,25,-11r0,67","w":192},"%":{"d":"229,-257r-27,0r-139,257r27,0xm76,-133v38,0,63,-26,63,-64v0,-38,-25,-63,-63,-63v-38,0,-64,25,-64,63v0,38,26,64,64,64xm76,-157v-24,0,-40,-16,-40,-40v0,-24,16,-40,40,-40v25,0,37,17,40,40v-2,23,-16,40,-40,40xm220,5v38,0,64,-26,64,-64v0,-38,-26,-64,-64,-64v-38,0,-63,26,-63,64v0,38,25,64,63,64xm260,-59v2,43,-62,52,-76,15v-10,-27,8,-56,36,-55v24,1,39,16,40,40","w":293},"&":{"d":"135,-53v-25,33,-107,22,-90,-34v6,-20,20,-32,37,-45xm96,-179v-19,-21,-5,-54,29,-44v11,0,20,3,27,6r0,24r30,0r0,-45v-44,-30,-160,-21,-128,58v4,10,7,17,12,24v-28,23,-57,39,-57,88v0,79,106,83,145,44r17,24r44,0r-34,-51v15,-20,24,-49,27,-87r23,0r0,-29r-75,0r0,29r20,0v-3,21,-5,44,-15,58","w":239},"'":{"d":"44,-267r-22,0r4,69r14,0","w":64,"k":{"s":14,"e":18,"A":22}},"(":{"d":"92,20v-51,-73,-52,-212,0,-284r-20,-11v-63,70,-62,236,0,306","w":115},")":{"d":"44,31v63,-79,64,-227,0,-306r-20,11v53,72,51,211,0,284","w":115},"*":{"d":"58,-267v-17,4,-6,36,-2,48v-10,-10,-19,-23,-36,-26v-4,0,-8,4,-8,8v3,16,28,17,42,22v-14,4,-38,7,-42,22v10,21,35,-10,44,-18v-3,14,-16,43,2,48v17,-5,6,-35,2,-48v11,9,20,23,37,26v4,0,7,-5,7,-8v-3,-15,-26,-19,-41,-22v15,-3,58,-16,34,-30v-17,3,-26,17,-37,26v3,-12,16,-43,-2,-48","w":116},"+":{"d":"67,-113r-46,0r0,25r46,0r0,46r25,0r0,-46r46,0r0,-25r-46,0r0,-46r-25,0r0,46","w":156},",":{"d":"71,-25r-31,0r-25,67r17,0","w":82,"k":{"1":18,"'":22,"\"":29}},"-":{"d":"110,-116r-84,0r0,26r84,0r0,-26","w":132},".":{"d":"43,4v11,0,21,-10,21,-22v0,-12,-10,-22,-21,-22v-27,0,-28,44,0,44","w":84,"k":{"1":22,"'":25,"\"":32}},"\/":{"d":"109,-268r-22,0r-82,285r22,0","w":110,"k":{"2":18,"1":32}},"0":{"d":"108,-260v-129,2,-129,262,0,264v130,-1,129,-263,0,-264xm108,-227v49,0,62,48,62,98v0,50,-12,99,-62,99v-48,0,-62,-51,-62,-99v0,-49,14,-98,62,-98","k":{"9":-8,"7":10,"1":41,"0":-10}},"1":{"d":"165,-30r-35,0r0,-227r-71,0r0,29r35,0r0,198r-35,0r0,30r106,0r0,-30","k":{"9":38,"8":38,"7":42,"6":34,"5":39,"4":46,"3":44,"2":32,"1":70,"0":31,"\/":25,".":32,",":29}},"2":{"d":"163,-127v46,-44,20,-135,-52,-131v-47,3,-74,24,-79,67r33,0v1,-23,20,-37,46,-37v45,0,60,61,25,83v-52,34,-115,56,-113,145r164,0r0,-73r-29,0r0,43r-97,0v6,-61,68,-65,102,-97","k":{"7":13,"4":17,"3":15,"1":42}},"3":{"d":"155,-138v18,-7,33,-27,33,-55v0,-79,-127,-82,-149,-23v-4,8,-6,16,-6,25r33,0v1,-23,20,-38,45,-38v24,0,44,15,45,36v0,33,-23,44,-59,41r0,29v40,-2,64,11,66,49v3,58,-96,64,-101,10r-33,0v6,43,35,68,83,68v49,0,84,-29,84,-78v0,-33,-17,-55,-41,-64","k":{"3":14,"2":14,"1":48}},"4":{"d":"168,-257r-27,0r-128,178r0,19r121,0r0,30r-22,0r0,30r78,0r0,-30r-22,0r0,-30r33,0r0,-29r-33,0r0,-168xm134,-89r-80,0r80,-110r0,110","k":{"1":30,"0":-5}},"5":{"d":"195,-88v2,-68,-64,-100,-128,-81r8,-59r79,0r0,27r29,0r0,-56r-135,0r-17,121r15,9v32,-34,120,-19,115,39v8,69,-104,82,-121,26r-28,13v12,32,39,53,84,53v59,0,97,-31,99,-92","k":{"2":19,"1":46}},"6":{"d":"175,-83v0,58,-85,73,-113,32v-9,-14,-16,-35,-16,-59v25,-42,129,-40,129,27xm206,-86v5,-83,-115,-107,-160,-59v2,-46,21,-83,69,-84v30,0,47,14,56,33r29,-11v-14,-31,-40,-53,-85,-53v-75,0,-103,59,-103,135v0,72,26,129,98,129v57,0,93,-31,96,-90","k":{"1":43}},"7":{"d":"111,-30v5,-88,39,-148,84,-198r0,-29r-175,0r0,53r30,0r0,-24r103,0v-40,52,-72,115,-75,198r-21,0r0,30r77,0r0,-30r-23,0","k":{"8":19,"7":11,"6":19,"4":48,"3":23,"2":27,"1":36,"0":19,"\/":32,".":47,",":50}},"8":{"d":"146,-137v22,-9,41,-25,40,-57v-2,-43,-34,-66,-78,-66v-44,0,-79,23,-79,66v0,32,17,48,40,57v-28,9,-49,32,-49,67v0,48,37,74,87,74v50,0,87,-25,88,-74v1,-37,-22,-58,-49,-67xm108,-232v28,0,49,12,49,38v0,27,-20,39,-50,39v-28,0,-49,-12,-49,-39v0,-26,22,-38,50,-38xm164,-73v3,56,-92,63,-109,18v-13,-36,14,-66,52,-65v33,1,55,17,57,47","k":{"7":12,"3":10,"2":15,"1":46}},"9":{"d":"42,-173v0,-58,83,-73,111,-32v10,14,17,35,18,59v-25,41,-129,42,-129,-27xm106,-260v-59,0,-92,32,-96,90v-5,83,113,106,160,59v-1,47,-23,84,-69,84v-29,0,-47,-13,-56,-33r-29,10v14,31,39,54,85,54v75,0,103,-59,103,-135v0,-72,-26,-129,-98,-129","k":{"3":10,"1":36,"0":-8}},":":{"d":"43,4v11,0,21,-10,21,-22v0,-12,-10,-22,-21,-22v-27,0,-28,44,0,44xm43,-129v12,0,21,-9,21,-21v0,-12,-10,-22,-21,-22v-12,0,-21,11,-22,22v0,11,10,21,22,21","w":84},";":{"d":"71,-25r-31,0r-25,67r17,0xm47,-129v12,0,22,-10,22,-21v0,-12,-11,-21,-22,-22v-11,0,-21,10,-21,22v0,12,9,21,21,21","w":82},"<":{"d":"113,-176r-96,69r94,70r0,-27r-56,-43r58,-42r0,-27","w":126},"=":{"d":"118,-80r-93,0r0,25r93,0r0,-25xm118,-136r-93,0r0,26r93,0r0,-26","w":141},">":{"d":"22,-37r94,-70r-97,-69r0,27r59,42r-56,43r0,27","w":126},"?":{"d":"126,-186v0,30,-25,47,-59,46r0,64r35,0r0,-44v34,-5,56,-28,59,-66v7,-82,-119,-100,-142,-30v-3,8,-5,15,-6,22r38,0v2,-19,15,-31,36,-32v25,-1,39,18,39,40xm85,3v12,0,21,-9,21,-21v0,-12,-9,-22,-21,-22v-12,0,-23,9,-22,22v0,12,10,21,22,21","w":170},"@":{"d":"141,-141v3,38,-57,51,-66,16v-7,-26,8,-50,36,-50v20,0,29,17,30,34xm163,-76v-53,32,-137,2,-133,-68v3,-50,33,-80,83,-80v51,0,84,30,84,80v0,20,-7,40,-27,40v-33,0,-9,-54,-11,-80r-11,0r-2,17v-19,-35,-91,-17,-85,31v-5,46,65,56,82,23v2,11,10,21,25,20v29,0,40,-25,40,-51v0,-57,-38,-91,-95,-91v-56,0,-91,35,-94,91v-5,81,90,115,152,76","w":222},"A":{"d":"122,-215r35,112r-71,0xm233,-30r-19,0r-63,-198r24,0r0,-29r-107,0r0,29r24,0r-63,198r-19,0r0,30r76,0r0,-30r-23,0r13,-39r92,0r12,39r-22,0r0,30r75,0r0,-30","w":244,"k":{"y":33,"w":36,"v":38,"u":23,"t":21,"q":17,"p":17,"o":17,"j":28,"g":17,"e":17,"d":15,"c":15,"Y":29,"W":24,"V":29,"U":19,"T":23,"Q":26,"O":22,"G":21,"C":19,"'":25,"\"":29}},"B":{"d":"164,-139v16,-8,29,-25,29,-53v0,-81,-99,-64,-179,-65r0,29r25,0r0,198r-25,0r0,30v86,0,186,14,186,-75v0,-34,-15,-52,-36,-64xm157,-190v1,40,-41,40,-82,38r0,-76v42,-2,81,-1,82,38xm163,-76v0,44,-41,49,-88,46r0,-93v47,-3,88,2,88,47","w":212,"k":{"y":15,"u":15,"l":12,"j":18,"i":10,"Y":21,"W":18,"V":16,"T":10,"J":14,"E":15,"A":13}},"C":{"d":"141,-225v28,0,49,8,62,20r0,19r33,0r0,-70r-33,0r0,13v-16,-11,-35,-17,-65,-17v-78,0,-120,52,-125,132v-7,118,143,169,223,96r-19,-29v-18,17,-41,29,-77,29v-57,0,-91,-39,-91,-96v0,-60,33,-97,92,-97","w":250,"k":{"u":14,"o":13}},"D":{"d":"207,-131v0,77,-45,110,-130,101r0,-197v82,-7,130,21,130,96xm244,-131v0,-77,-40,-126,-118,-126r-110,0r0,30r24,0r0,197r-24,0r0,30r110,0v78,-3,118,-52,118,-131","w":255,"k":{"Y":22,"W":26,"V":15,"T":12,"R":15,"J":35,"I":12,"E":21,"D":12,"A":19}},"E":{"d":"18,-30r0,30r183,0r0,-59r-29,0r0,29r-93,0r0,-90r60,0r0,-30r-60,0r0,-78r93,0r0,26r29,0r0,-55r-183,0r0,29r24,0r0,198r-24,0","w":216,"k":{"y":39,"w":17,"v":30,"u":16,"q":14,"p":14,"o":12,"g":15,"d":12,"Q":18,"O":11,"C":15}},"F":{"d":"17,-30r0,30r85,0r0,-30r-25,0r0,-90r67,0r0,-30r-67,0r0,-78r89,0r0,26r29,0r0,-55r-178,0r0,29r24,0r0,198r-24,0","w":209,"k":{"y":49,"u":31,"s":25,"r":44,"o":37,"j":41,"i":24,"e":38,"a":48,"Q":19,"O":21,"J":42,"G":18,"C":24,"A":36,";":53,":":54,".":77,"-":27,",":90}},"G":{"d":"13,-126v-8,114,139,169,214,94r0,32r33,0r0,-139r-119,0r0,33r83,0v-2,48,-32,76,-82,76v-59,0,-93,-39,-93,-97v0,-60,32,-99,91,-99v34,0,59,10,72,29r0,17r34,0r0,-79r-34,0r0,21v-16,-14,-41,-22,-74,-22v-80,0,-119,54,-125,134","w":270,"k":{"S":-8}},"H":{"d":"249,-30r-24,0r0,-198r24,0r0,-29r-85,0r0,29r24,0r0,78r-114,0r0,-78r24,0r0,-29r-85,0r0,29r24,0r0,198r-24,0r0,30r85,0r0,-30r-24,0r0,-88r114,0r0,88r-24,0r0,30r85,0r0,-30","w":262,"k":{"y":12}},"I":{"d":"108,-30r-25,0r0,-198r25,0r0,-29r-86,0r0,29r24,0r0,198r-24,0r0,30r86,0r0,-30","w":125,"k":{"o":14}},"J":{"d":"19,1v10,11,26,22,47,22v42,0,63,-30,63,-71r0,-180r25,0r0,-29r-90,0r0,29r29,0r0,177v8,44,-43,51,-60,25","w":159,"k":{"A":15}},"K":{"d":"231,-30r-25,0r-97,-107r90,-91r20,0r0,-29r-85,0r0,29r20,0r-80,80r0,-80r24,0r0,-29r-85,0r0,29r24,0r0,198r-24,0r0,30r85,0r0,-30r-24,0r0,-91r83,91r-24,0r0,30r98,0r0,-30","w":239,"k":{"y":37,"u":20,"o":18,"g":15,"e":16,"Y":12,"V":13,"Q":29,"O":33,"G":33,"C":35}},"L":{"d":"151,-51r0,21r-77,0r0,-198r24,0r0,-29r-85,0r0,29r24,0r0,198r-24,0r0,30r168,0r0,-51r-30,0","w":193,"k":{"y":38,"u":25,"o":16,"Y":46,"W":46,"V":46,"U":18,"T":38,"Q":23,"O":20,"G":20,"C":22}},"M":{"d":"311,-30r-24,0r0,-198r24,0r0,-29r-72,0r-77,194r-77,-194r-71,0r0,29r23,0r0,198r-23,0r0,30r84,0r0,-30r-24,0r0,-167r77,197r22,0r77,-197r0,167r-23,0r0,30r84,0r0,-30","w":323,"k":{"y":13,"o":14}},"N":{"d":"98,0r0,-30r-24,0r0,-162r134,192r36,0r0,-228r23,0r0,-29r-84,0r0,29r24,0r0,160r-135,-189r-58,0r0,29r23,0r0,198r-23,0r0,30r84,0","w":281,"k":{"y":19,"u":17,"o":25,"e":14,"O":14,"J":18,"A":13}},"O":{"d":"145,-260v-84,0,-131,53,-131,132v0,79,49,132,131,132v82,0,131,-51,131,-132v0,-81,-48,-132,-131,-132xm145,-225v60,0,95,38,95,97v0,59,-35,96,-95,96v-59,0,-94,-37,-94,-96v0,-58,35,-97,94,-97","w":289,"k":{"y":15,"r":13,"l":13,"j":18,"b":15,"a":15,"Z":11,"Y":27,"X":22,"W":18,"V":24,"T":18,"N":14,"K":11,"J":44,"H":14,"F":11,"E":16,"D":19,"B":14,"A":24}},"P":{"d":"123,-223v29,15,25,78,-15,77r-35,0r0,-81v17,1,38,-2,50,4xm73,-117v62,5,104,-12,104,-69v0,-43,-25,-71,-69,-71r-96,0r0,29r24,0r0,198r-24,0r0,30r86,0r0,-30r-25,0r0,-87","w":187,"k":{"u":13,"s":13,"o":23,"j":23,"e":20,"c":23,"a":23,"J":41,"A":26,".":54,",":50}},"Q":{"d":"145,-260v-84,0,-131,53,-131,132v0,73,42,120,109,130v-18,1,-34,7,-48,13r0,33v16,-7,30,-11,48,-12v36,-1,45,25,81,24v14,0,28,-2,40,-6r0,-39v-30,16,-71,3,-92,-11v77,-6,124,-53,124,-132v0,-81,-48,-132,-131,-132xm145,-225v60,0,95,38,95,97v0,59,-35,96,-95,96v-59,0,-94,-37,-94,-96v0,-58,35,-97,94,-97","w":289,"k":{"u":13,"a":15,"V":21,"U":9,"T":13,"E":19,"D":15,"A":15}},"R":{"d":"75,-227v42,-2,81,-2,81,41v0,41,-38,43,-81,40r0,-81xm192,-186v-1,-43,-26,-71,-70,-71r-108,0r0,29r24,0r0,198r-24,0r0,30r86,0r0,-30r-25,0r0,-87r19,0r76,117r47,0r0,-30r-24,0r-60,-87v36,-4,60,-27,59,-69","w":227,"k":{"y":20,"u":25,"t":13,"o":23,"e":23,"d":20,"a":13,"Y":27,"W":26,"V":28,"U":21,"T":25,"Q":20,"O":17,"G":21,"C":23}},"S":{"d":"15,-192v0,87,131,45,136,120v4,55,-86,54,-109,21r0,-22r-28,0r0,73r29,0r0,-16v46,36,141,16,141,-57v0,-71,-78,-70,-124,-99v-30,-19,1,-63,38,-59v20,2,38,7,49,16r0,18r30,0r0,-61r-30,0r0,11v-45,-27,-132,-11,-132,55","w":194,"k":{"y":25,"w":23,"v":20,"u":15,"t":13,"r":13,"j":20,"i":10,"A":13}},"T":{"d":"145,0r0,-30r-25,0r0,-198r37,0r0,35r29,0r0,-64r-169,0r0,64r30,0r0,-35r37,0r0,198r-25,0r0,30r86,0","w":200,"k":{"y":31,"w":45,"u":34,"s":24,"r":22,"o":40,"j":28,"i":18,"h":9,"f":13,"e":31,"c":40,"a":32,"Q":19,"O":15,"J":30,"G":22,"C":18,"A":17,";":45,":":30,".":38,"-":24,",":51}},"U":{"d":"117,4v53,0,86,-27,86,-81r0,-151r25,0r0,-29r-85,0r0,29r24,0r0,153v-1,30,-18,46,-50,45v-30,0,-49,-15,-49,-45r0,-153r25,0r0,-29r-86,0r0,29r24,0r0,151v1,54,33,81,86,81","w":235,"k":{"y":19,"t":14,"s":19,"r":22,"q":16,"p":19,"o":19,"n":16,"m":13,"g":19,"e":22,"d":22,"c":18,"a":18,"Q":11,"J":36,"A":27}},"V":{"d":"171,-228r18,0r-59,186r-59,-186r18,0r0,-29r-76,0r0,29r19,0r73,228r49,0r74,-228r19,0r0,-29r-76,0r0,29","w":259,"k":{"y":33,"u":38,"t":25,"s":40,"r":34,"o":44,"j":32,"i":28,"g":41,"f":21,"e":45,"d":51,"c":48,"a":39,"S":13,"Q":21,"O":22,"J":41,"G":22,"C":25,"A":30,";":53,":":43,".":70,",":85}},"W":{"d":"152,-228r23,0r-52,192r-51,-192r16,0r0,-29r-76,0r0,29r21,0r65,228r49,0r54,-202r54,202r49,0r64,-227r21,-1r0,-29r-76,0r0,29r18,0r-50,192r-52,-191r22,0r0,-30r-99,0r0,29","w":401,"k":{"y":37,"u":40,"t":18,"s":37,"r":38,"o":44,"m":25,"j":45,"i":32,"e":41,"c":48,"a":37,"S":21,"Q":25,"O":23,"J":49,"G":22,"C":24,"A":18,";":49,":":43,".":64,"-":46,",":69}},"X":{"d":"87,-30r-20,0r45,-73r47,73r-23,0r0,30r81,0r0,-30r-18,0r-68,-106r58,-92r24,0r0,-29r-77,0r0,29r13,0r-37,62r-37,-62r16,0r0,-29r-78,0r0,29r22,0r57,93r-65,105r-22,0r0,30r82,0r0,-30","w":223,"k":{"y":30,"O":18,"C":14}},"Y":{"d":"98,-124r0,94r-20,0r0,30r76,0r0,-30r-20,0r0,-94r73,-104r16,0r0,-29r-79,0r0,29r24,0r-52,72r-50,-72r23,0r0,-29r-80,0r0,29r16,0","w":231,"k":{"w":46,"v":44,"u":48,"t":21,"s":33,"r":40,"q":49,"p":54,"o":50,"n":38,"m":50,"j":36,"i":23,"g":51,"e":44,"d":49,"c":45,"a":45,"Q":18,"O":18,"J":36,"G":26,"C":32,"A":26,";":58,":":45,".":62,"-":40,",":64}},"Z":{"d":"154,-33r-101,0r130,-199r0,-25r-165,0r0,64r30,0r0,-31r91,0r-130,199r0,25r174,0r0,-64r-29,0r0,31","w":196,"k":{"y":25,"u":18}},"[":{"d":"71,-270r0,-12r-50,0r0,306r51,0r0,-12r-25,0r0,-282r24,0","w":86},"\\":{"d":"111,13r-77,-283r-29,0r77,283r29,0","w":111},"]":{"d":"68,24r0,-307r-50,0r0,13r24,0r0,282r-25,0r0,12r51,0","w":86},"^":{"d":"59,-242r-41,92r12,0r34,-61r35,61r11,0r-40,-92r-11,0","w":126},"_":{"d":"134,9r-134,0r0,14r134,0r0,-14","w":128},"`":{"d":"73,-213r12,-13r-48,-48r-13,13","w":104},"a":{"d":"42,-145v28,-30,112,-22,97,40v-35,-41,-129,-18,-129,44v0,63,90,83,129,47r0,14r62,0r0,-22r-26,0r0,-98v4,-75,-106,-90,-151,-47xm87,-26v-28,0,-44,-13,-46,-35v2,-22,18,-35,46,-35v29,0,50,12,50,35v0,23,-21,35,-50,35","w":205,"k":{"y":21,"w":23,"v":19,"u":15,"j":17}},"b":{"d":"227,-93v0,-56,-37,-96,-96,-96v-30,0,-51,9,-64,25r0,-106r-57,0r0,26r22,0r0,218r-22,0r0,26r57,0r0,-20v14,16,32,25,64,24v60,-2,96,-39,96,-97xm130,-28v-39,0,-65,-26,-65,-65v0,-39,26,-64,65,-64v38,0,64,26,64,64v0,39,-25,65,-64,65","w":232,"k":{"y":11,"j":18,"e":-3}},"c":{"d":"159,-47v-34,38,-116,17,-116,-46v0,-55,70,-82,109,-50r0,11r26,0r0,-56r-26,0r0,9v-67,-30,-141,15,-141,87v0,89,112,125,169,68","w":187},"d":{"d":"106,4v32,0,52,-7,65,-21r0,17r57,0r0,-26r-22,0r0,-244r-65,0r0,25r30,0r0,79v-12,-13,-36,-24,-65,-23v-58,3,-94,41,-94,97v0,55,36,94,94,96xm110,-28v-39,0,-65,-25,-65,-64v0,-39,26,-64,65,-64v39,0,64,25,64,64v0,38,-26,64,-64,64","w":236,"k":{"y":13,"j":18,"e":13}},"e":{"d":"160,-44v-18,22,-78,22,-96,-2v-7,-9,-17,-20,-18,-34r157,0v1,-65,-29,-110,-92,-110v-60,0,-94,38,-98,97v-6,88,111,128,169,70xm46,-107v2,-56,99,-69,118,-17v3,6,3,11,4,17r-122,0","w":209,"k":{"w":14,"j":14,"W":35}},"f":{"d":"100,-267v-54,-11,-77,23,-69,82r-20,0r0,23r20,0r0,140r-22,0r0,22r79,0r0,-22r-22,0r0,-140r36,0r0,-23r-36,0v-3,-30,1,-62,34,-50r0,-32","w":113,"k":{"q":18,"o":16,"g":17,"f":11,"e":19,"d":13,"c":15,"a":13}},"g":{"d":"13,-92v-6,86,107,126,161,72v12,60,-62,74,-100,50r-24,23v14,13,30,20,62,20v62,0,97,-31,97,-91r0,-141r22,0r0,-26r-57,0r0,22v-16,-14,-36,-27,-65,-26v-58,2,-92,40,-96,97xm111,-28v-39,0,-65,-25,-65,-64v0,-39,26,-64,65,-64v38,0,64,26,64,64v0,39,-25,64,-64,64","w":240,"k":{"o":12}},"h":{"d":"193,-125v7,-66,-92,-85,-123,-43r0,-102r-57,0r0,22r22,0r0,226r-22,0r0,22r79,0r0,-22r-22,0r0,-106v8,-37,88,-47,88,5r0,101r-22,0r0,22r79,0r0,-22r-22,0r0,-103","w":221,"k":{"y":19,"w":18,"v":14,"u":19,"t":14,"j":21,"e":13}},"i":{"d":"18,-163r22,0r0,141r-22,0r0,22r79,0r0,-22r-22,0r0,-163r-57,0r0,22xm57,-214v12,0,21,-9,21,-21v0,-12,-9,-22,-21,-22v-12,0,-22,10,-22,22v0,11,10,21,22,21","w":113,"k":{"y":17,"w":16,"v":13,"u":19,"q":13,"o":13,"j":22,"g":17,"e":14,"c":16,"a":14}},"j":{"d":"23,68v42,0,66,-16,66,-58r0,-195r-57,0r0,22r22,0r0,175v-1,18,-9,28,-31,27r0,29xm71,-214v12,0,21,-9,21,-21v0,-12,-9,-22,-21,-22v-12,0,-23,9,-22,22v0,12,10,21,22,21","w":113},"k":{"d":"97,-164r24,0r-59,60r0,-166r-56,0r0,22r23,0r0,226r-23,0r0,22r80,0r0,-22r-24,0r0,-70r92,92r37,0r0,-22r-16,0r-79,-80r62,-62r22,0r0,-22r-83,0r0,22","w":199,"k":{"y":11,"o":23,"j":14,"e":22,"d":15,"c":16,"a":14}},"l":{"d":"18,-248r22,0r0,226r-22,0r0,22r79,0r0,-22r-22,0r0,-248r-57,0r0,22","w":113,"k":{"y":21,"w":21,"v":18,"u":21,"t":13,"q":18,"p":13,"o":14,"j":22,"g":13,"e":11,"c":13,"a":11}},"m":{"d":"286,-130v7,-65,-87,-77,-118,-39v-15,-30,-84,-27,-99,0r0,-16r-57,0r0,22r22,0r0,141r-22,0r0,22r79,0r0,-22r-22,0r0,-105v5,-19,20,-34,44,-34v53,0,22,90,30,139r-22,0r0,22r78,0r0,-22r-22,0r0,-105v4,-21,22,-33,46,-34v20,-1,28,13,28,31r0,108r-22,0r0,22r79,0r0,-22r-22,0r0,-108","w":318,"k":{"y":26,"w":21,"v":14,"u":11,"o":12,"j":22,"e":13,"c":11}},"n":{"d":"192,-125v8,-66,-91,-86,-123,-43r0,-17r-57,0r0,22r22,0r0,141r-22,0r0,22r79,0r0,-22r-22,0r0,-106v9,-36,88,-48,88,5r0,101r-22,0r0,22r79,0r0,-22r-22,0r0,-103","w":222,"k":{"y":21,"w":24,"v":15,"u":11,"o":14,"j":14}},"o":{"d":"111,5v59,0,97,-40,97,-97v0,-58,-39,-97,-97,-97v-58,0,-97,39,-97,97v0,57,38,97,97,97xm111,-28v-39,0,-65,-25,-65,-64v0,-39,26,-64,65,-64v39,0,64,25,64,64v0,38,-26,64,-64,64","w":217,"k":{"p":17,"n":13,"j":19}},"p":{"d":"231,-92v0,-84,-103,-125,-159,-73r0,-20r-57,0r0,25r21,0r0,204r-22,0r0,26r79,0r0,-26r-22,0r0,-63v56,51,160,12,160,-73xm134,-28v-39,0,-65,-26,-65,-64v0,-38,26,-64,65,-64v38,0,64,26,64,64v0,39,-25,64,-64,64","w":239,"k":{"u":11,"j":18}},"q":{"d":"13,-92v0,86,106,125,161,71r0,91r57,0r0,-26r-22,0r0,-204r21,0r0,-25r-56,0r0,22v-54,-53,-161,-15,-161,71xm110,-28v-39,0,-64,-25,-64,-64v0,-38,26,-64,64,-64v39,0,65,25,65,64v0,39,-26,64,-65,64","w":244,"k":{"u":4,"e":19,"a":14}},"r":{"d":"144,-191v-35,-1,-59,9,-72,29r0,-23r-57,0r0,22r22,0r0,141r-22,0r0,22r79,0r0,-22r-22,0r0,-94v3,-33,37,-39,72,-42r0,-33","w":153,"k":{"o":19,"h":16,"g":13,"e":19,"d":14,"c":13,"a":13,";":18,":":11,".":51,"-":13,",":62}},"s":{"d":"47,-139v0,-32,68,-22,79,-4r0,12r26,0r0,-54r-26,0r0,9v-31,-27,-111,-14,-111,37v0,65,94,37,106,88v-2,32,-68,27,-82,8r0,-13r-26,0r0,56r25,0r0,-10v35,27,117,17,117,-41v0,-53,-64,-56,-100,-74v-5,-4,-8,-9,-8,-14","w":163},"t":{"d":"109,-31v-17,12,-43,8,-43,-19r0,-110r42,0r0,-24r-42,0r0,-65r-35,18r0,47r-20,0r0,24r20,0v5,65,-22,165,48,165v10,0,20,-3,30,-7r0,-29","w":116,"k":{"e":10}},"u":{"d":"36,-163v-2,87,-11,188,90,166v15,-4,24,-11,33,-20r0,17r57,0r0,-22r-22,0r0,-163r-57,0r0,22r22,0r0,103v-6,20,-25,35,-50,35v-63,0,-30,-102,-38,-160r-57,0r0,22r22,0","w":229,"k":{"y":21,"w":13,"v":11,"u":14,"t":13,"p":13,"j":18,"e":14,"c":15}},"v":{"d":"97,0r25,0r77,-163r15,0r0,-22r-69,0r0,22r19,0r-55,118r-54,-118r18,0r0,-22r-69,0r0,22r16,0","w":218,"k":{"o":16,"e":13,"c":13,".":40,",":50}},"w":{"d":"205,-163r0,-22r-73,0r0,22r25,0r-48,118r-48,-118r18,0r0,-22r-68,0r0,22r16,0r69,163r25,0r47,-118r48,118r25,0r69,-163r16,0r0,-22r-68,0r0,22r18,0r-47,118r-48,-118r24,0","w":335,"k":{"s":11,"q":18,"o":15,"e":19,"d":11,"c":17,"a":18,".":47,",":58}},"x":{"d":"186,0r0,-22r-20,0r-50,-67r55,-74r15,0r0,-22r-68,0r0,22r19,0r-38,51r-39,-51r20,0r0,-22r-68,0r0,22r15,0r53,71r-52,70r-16,0r0,22r68,0r0,-22r-18,0r35,-47r35,47r-14,0r0,22r68,0","w":194},"y":{"d":"26,-163r72,154r-26,55r-19,0r0,22r71,0r0,-22r-19,0r98,-209r17,0r0,-22r-68,0r0,22r16,0r-54,118r-54,-118r18,0r0,-22r-68,0r0,22r16,0","w":227,"k":{"q":14,"o":22,"g":16,"e":22,"d":13,"c":16,"b":15,"a":13,"W":22,".":43,",":58}},"z":{"d":"140,-25r-86,0r110,-136r0,-24r-146,0r0,60r25,0r0,-35r82,0r-110,137r0,23r150,0r0,-64r-25,0r0,39","w":175},"{":{"d":"29,-128v39,-5,25,-65,27,-107v0,-22,8,-30,31,-30r0,-20v-35,1,-55,16,-57,50v-2,40,12,97,-20,107v52,23,-26,162,77,158r0,-22v-67,5,4,-116,-58,-136","w":97},"|":{"d":"45,17r0,-289r-26,0r0,289r26,0","w":64},"}":{"d":"90,-128v-54,-20,28,-159,-77,-157r0,20v71,-7,-6,124,59,137v-12,7,-27,12,-27,33v0,41,16,107,-32,103r0,22v35,-1,57,-14,57,-49v0,-41,-11,-96,20,-109","w":97},"~":{"d":"148,-147v-37,37,-94,-37,-132,13r13,15v35,-37,92,35,132,-12","w":175},"\u00a0":{"w":99}}});
;
/*!
 * The following copyright notice may not be removed under any circumstances.
 * 
 * Copyright:
 * Copyright � 1989, 1995, 2002 Adobe Systems Incorporated.  All Rights Reserved.
 * � 1981, 1995, 2002 Heidelberger Druckmaschinen AG. All rights reserved.
 * 
 * Trademark:
 * Avenir is a trademark of Heidelberger Druckmaschinen AG, exclusively licensed
 * through Linotype Library GmbH, and may be registered in certain jurisdictions.
 * 
 * Full name:
 * AvenirLTStd-Heavy
 * 
 * Designer:
 * Adrian Frutiger
 * 
 * Vendor URL:
 * http://www.adobe.com/type
 * 
 * License information:
 * http://www.adobe.com/type/legal.html
 */
Cufon.registerFont({"w":213,"face":{"font-family":"Avenir LT Std","font-weight":700,"font-stretch":"normal","units-per-em":"360","panose-1":"2 11 7 3 2 2 3 2 2 4","ascent":"272","descent":"-88","x-height":"4","bbox":"-21 -283 353 90","underline-thickness":"18","underline-position":"-18","stemh":"37","stemv":"43","unicode-range":"U+0020-U+007E"},"glyphs":{" ":{"w":106},"!":{"d":"75,-255r0,175r-43,0r0,-175r43,0xm24,-25v0,-15,13,-28,29,-28v15,0,29,12,29,27v0,15,-13,28,-29,28v-15,0,-29,-12,-29,-27","w":106},"\"":{"d":"43,-160r0,-95r37,0r0,95r-37,0xm107,-160r0,-95r37,0r0,95r-37,0","w":186},"#":{"d":"39,0r10,-75r-32,0r0,-30r36,0r7,-44r-33,0r0,-31r37,0r11,-75r31,0r-11,75r37,0r11,-75r32,0r-11,75r32,0r0,31r-37,0r-6,44r33,0r0,30r-36,0r-11,75r-32,0r11,-75r-37,0r-11,75r-31,0xm128,-149r-37,0r-6,44r37,0"},"$":{"d":"114,-283r0,24v28,-1,54,8,74,25r-30,33v-11,-12,-27,-19,-44,-19v2,23,-4,53,2,72v40,11,81,24,81,73v0,48,-37,74,-83,79r0,26r-19,0r0,-26v-31,0,-62,-9,-84,-32r33,-33v12,16,31,24,51,26v-2,-24,4,-56,-2,-76v-40,-11,-76,-26,-76,-74v0,-43,38,-70,78,-74r0,-24r19,0xm95,-154r0,-66v-17,3,-32,15,-32,33v0,22,15,25,32,33xm114,-105r0,70v20,-4,38,-16,38,-37v0,-21,-21,-27,-38,-33"},"%":{"d":"182,-63v0,-37,30,-67,67,-67v37,0,67,30,67,67v0,37,-30,67,-67,67v-37,0,-67,-30,-67,-67xm281,-63v0,-18,-14,-32,-32,-32v-18,0,-33,14,-33,32v0,18,15,33,33,33v18,0,32,-15,32,-33xm11,-192v0,-37,30,-67,67,-67v37,0,67,30,67,67v0,37,-30,67,-67,67v-37,0,-67,-30,-67,-67xm110,-192v0,-18,-14,-33,-32,-33v-18,0,-33,15,-33,33v0,18,15,32,33,32v18,0,32,-14,32,-32xm74,-2r150,-264r29,13r-149,265","w":326},"&":{"d":"258,-134r-51,74r55,60r-56,0r-27,-29v-41,58,-161,43,-162,-39v0,-34,24,-60,55,-72v-16,-18,-29,-34,-29,-59v0,-42,34,-62,73,-62v38,0,72,18,72,60v0,31,-25,53,-50,66r41,44r28,-43r51,0xm117,-222v-43,1,-33,46,-6,62v29,-7,53,-59,6,-62xm152,-58r-52,-56v-17,10,-35,21,-35,43v0,22,18,39,40,39v20,0,34,-13,47,-26","w":266},"(":{"d":"70,-264v9,8,23,11,30,21v-61,80,-60,199,0,279r-30,20v-69,-89,-70,-231,0,-320","w":106},")":{"d":"37,56v-10,-7,-22,-12,-31,-20v61,-81,62,-199,0,-279r31,-21v68,89,69,231,0,320","w":106},"*":{"d":"99,-255r0,51r48,-16r10,29r-49,16r31,42r-25,18r-31,-42r-30,41r-25,-18r31,-41r-49,-17r9,-29r49,17r0,-51r31,0","w":166},"+":{"d":"22,-127r80,0r0,-80r36,0r0,80r79,0r0,36r-79,0r0,79r-36,0r0,-79r-80,0r0,-36","w":239},",":{"d":"14,45r25,-95r44,0r-31,95r-38,0","w":106},"-":{"d":"101,-107r0,37r-89,0r0,-37r89,0","w":113},".":{"d":"24,-25v0,-15,13,-28,29,-28v15,0,29,12,29,27v0,15,-13,28,-29,28v-15,0,-29,-12,-29,-27","w":106},"\/":{"d":"139,-261r-112,278r-30,-11r112,-279","w":140},"0":{"d":"14,-127v0,-121,65,-132,93,-132v28,0,92,11,92,132v0,121,-64,131,-92,131v-28,0,-93,-10,-93,-131xm58,-127v0,32,6,90,49,90v43,0,49,-58,49,-90v0,-32,-6,-91,-49,-91v-43,0,-49,59,-49,91"},"1":{"d":"96,0r0,-205r-47,44r-26,-30r77,-64r40,0r0,255r-44,0"},"2":{"d":"21,0r0,-46r99,-95v12,-12,27,-26,27,-44v-2,-45,-75,-42,-77,1r-45,-3v4,-47,38,-72,84,-72v46,0,83,23,83,72v0,32,-16,54,-39,75r-79,73r118,0r0,39r-171,0"},"3":{"d":"80,-113r0,-39v31,0,60,0,61,-35v0,-40,-67,-44,-75,-5r-45,-12v18,-79,163,-72,163,13v1,28,-19,49,-44,58v32,5,50,31,50,62v1,93,-156,101,-173,16r46,-13v5,47,86,41,84,-7v-1,-37,-35,-39,-67,-38"},"4":{"d":"121,0r0,-52r-109,0r0,-43r103,-160r49,0r0,164r37,0r0,39r-37,0r0,52r-43,0xm121,-91r-1,-103r-64,103r65,0"},"5":{"d":"182,-255r0,39r-103,0r-1,51v61,-14,112,21,112,81v0,100,-152,121,-174,30r45,-12v9,43,86,35,86,-14v0,-58,-74,-59,-113,-37r3,-138r145,0"},"6":{"d":"105,-255r52,0v-19,32,-45,64,-60,95v54,-18,100,25,100,77v0,53,-38,87,-90,87v-86,0,-108,-92,-67,-156xm61,-83v0,26,21,46,47,46v26,0,46,-20,46,-46v0,-26,-20,-47,-46,-47v-26,0,-47,21,-47,47"},"7":{"d":"15,-214r0,-41r173,0r0,40r-102,215r-50,0r104,-214r-125,0"},"8":{"d":"107,-259v43,0,78,24,78,67v1,28,-15,48,-39,58v24,5,47,31,47,62v0,50,-38,76,-86,76v-48,0,-86,-26,-86,-76v-1,-32,24,-56,46,-63v-26,-8,-39,-30,-39,-57v0,-43,36,-67,79,-67xm107,-222v-22,0,-35,15,-35,34v0,20,14,36,35,36v19,0,34,-16,34,-36v0,-19,-14,-34,-34,-34xm107,-118v-23,0,-43,17,-43,40v0,24,18,41,43,41v25,0,42,-17,42,-41v0,-23,-19,-40,-42,-40"},"9":{"d":"108,0r-52,0v19,-32,45,-64,60,-95v-53,17,-100,-26,-100,-77v0,-53,39,-87,91,-87v84,0,108,93,67,156xm152,-172v0,-26,-21,-46,-47,-46v-26,0,-46,20,-46,46v0,26,20,47,46,47v26,0,47,-21,47,-47"},":":{"d":"24,-25v0,-15,13,-28,29,-28v15,0,29,12,29,27v0,15,-13,28,-29,28v-15,0,-29,-12,-29,-27xm24,-147v0,-15,13,-28,29,-28v15,0,29,12,29,27v0,15,-13,28,-29,28v-15,0,-29,-12,-29,-27","w":106},";":{"d":"14,45r25,-95r44,0r-31,95r-38,0xm24,-147v0,-15,13,-28,29,-28v15,0,29,12,29,27v0,15,-13,28,-29,28v-15,0,-29,-12,-29,-27","w":106},"<":{"d":"211,-15r-182,-81r0,-26r182,-81r0,40r-122,54r122,54r0,40","w":239},"=":{"d":"22,-160r195,0r0,37r-195,0r0,-37xm22,-95r195,0r0,37r-195,0r0,-37","w":239},">":{"d":"29,-203r182,81r0,26r-182,81r0,-40r121,-54r-121,-54r0,-40","w":239},"?":{"d":"177,-191v0,53,-65,57,-58,116r-43,0v-12,-64,56,-64,56,-114v0,-19,-14,-31,-33,-31v-20,0,-34,16,-36,35r-46,-4v0,-91,160,-99,160,-2xm68,-25v0,-15,13,-28,29,-28v15,0,29,12,29,27v0,15,-13,28,-29,28v-15,0,-29,-12,-29,-27","w":186},"@":{"d":"144,-164v-24,0,-38,19,-38,43v0,8,0,34,30,34v44,1,48,-76,8,-77xm222,-48r40,0v-68,99,-250,52,-250,-79v0,-77,61,-134,137,-134v63,0,127,42,127,118v0,77,-57,88,-79,88v-21,1,-27,-13,-30,-20v-27,36,-105,20,-98,-41v-8,-59,75,-111,112,-60r3,-16r34,0r-17,91v0,9,2,14,10,14v15,0,31,-17,31,-54v0,-58,-39,-88,-94,-88v-60,0,-100,43,-100,102v0,91,110,128,174,79","w":288},"A":{"d":"0,0r111,-255r39,0r110,255r-52,0r-24,-58r-110,0r-23,58r-51,0xm89,-97r79,0r-39,-104","w":259},"B":{"d":"28,0r0,-255v79,2,175,-18,178,64v1,30,-20,47,-44,57v32,4,55,28,55,61v0,88,-103,72,-189,73xm74,-216r0,65v38,2,90,1,87,-32v4,-34,-48,-35,-87,-33xm74,-112r0,73v44,-2,92,11,97,-37v5,-41,-54,-36,-97,-36","w":233},"C":{"d":"239,-221r-36,26v-53,-59,-141,-6,-141,66v0,80,97,130,145,63r37,27v-69,91,-229,36,-229,-87v0,-121,149,-181,224,-95","w":246},"D":{"d":"28,0r0,-255r101,0v67,0,129,42,129,128v0,125,-108,134,-230,127xm74,-41v76,4,137,-11,137,-86v0,-78,-59,-92,-137,-87r0,173","w":273},"E":{"d":"28,0r0,-255r169,0r0,41r-123,0r0,63r117,0r0,41r-117,0r0,69r130,0r0,41r-176,0","w":219},"F":{"d":"28,0r0,-255r165,0r0,41r-119,0r0,67r112,0r0,41r-112,0r0,106r-46,0","w":206,"k":{"A":20,",":46,".":46}},"G":{"d":"255,-149r0,130v-31,17,-67,25,-106,25v-78,0,-134,-52,-134,-132v0,-125,149,-173,235,-103r-34,34v-52,-53,-154,-15,-154,66v0,79,82,116,148,81r0,-60r-53,0r0,-41r98,0","w":280},"H":{"d":"28,0r0,-255r46,0r0,102r119,0r0,-102r45,0r0,255r-45,0r0,-112r-119,0r0,112r-46,0","w":266},"I":{"d":"27,0r0,-255r46,0r0,255r-46,0","w":100},"J":{"d":"153,-255r0,184v0,41,-25,77,-76,77v-40,0,-67,-18,-75,-58r42,-10v3,17,15,27,31,27v26,0,33,-18,33,-49r0,-171r45,0","w":180},"K":{"d":"28,0r0,-255r46,0v2,35,-4,77,2,108r105,-108r62,0r-120,118r128,137r-64,0r-111,-125r-2,0r0,125r-46,0","w":246},"L":{"d":"28,0r0,-255r46,0r0,214r108,0r0,41r-154,0","w":186,"k":{"T":33,"V":33,"W":20,"y":13,"Y":40}},"M":{"d":"30,0r0,-255r69,0r68,179r68,-179r69,0r0,255r-43,0r-1,-212r-77,212r-32,0r-78,-212r0,212r-43,0","w":333},"N":{"d":"28,0r0,-255r61,0r124,189r0,-189r45,0r0,255r-57,0r-127,-195r0,195r-46,0","w":286},"O":{"d":"15,-126v0,-82,56,-135,134,-135v80,0,136,51,136,133v0,80,-56,134,-136,134v-78,0,-134,-52,-134,-132xm62,-129v0,54,35,94,88,94v53,0,88,-40,88,-94v0,-51,-35,-91,-88,-91v-53,0,-88,40,-88,91","w":299},"P":{"d":"28,0r0,-255v84,1,180,-15,180,74v0,76,-64,77,-134,76r0,105r-46,0xm74,-144v39,-1,87,9,87,-36v0,-42,-47,-35,-87,-36r0,72","w":219,"k":{"A":27,",":61,".":61}},"Q":{"d":"302,-37r0,37r-148,0v-79,0,-139,-50,-139,-133v0,-75,61,-128,134,-128v73,0,134,53,134,128v0,53,-30,82,-54,96r73,0xm148,-220v-49,0,-86,37,-86,88v0,52,36,91,86,91v50,0,87,-39,87,-91v0,-51,-36,-88,-87,-88","w":306},"R":{"d":"28,0r0,-255v85,2,182,-17,184,73v0,37,-21,64,-59,69r68,113r-55,0r-59,-108r-33,0r0,108r-46,0xm74,-147v41,-2,91,10,91,-35v0,-44,-51,-32,-91,-34r0,69","w":226,"k":{"T":6,"Y":13}},"S":{"d":"186,-235r-33,35v-18,-28,-86,-30,-86,12v0,51,121,22,121,113v0,91,-128,104,-177,47r34,-33v19,34,95,38,95,-8v0,-56,-121,-24,-121,-115v1,-82,116,-99,167,-51","w":206},"T":{"d":"81,0r0,-214r-78,0r0,-41r201,0r0,41r-78,0r0,214r-45,0","w":206,"k":{"w":40,"y":40,"A":33,",":40,".":40,"c":40,"e":40,"o":40,"-":46,"a":36,"r":33,"s":40,"u":33,":":30,";":30}},"U":{"d":"233,-255r0,161v0,62,-45,100,-103,100v-58,0,-103,-38,-103,-100r0,-161r46,0r0,160v0,25,13,58,57,58v44,0,57,-33,57,-58r0,-160r46,0","w":259},"V":{"d":"99,0r-100,-255r52,0r70,193r71,-193r49,0r-103,255r-39,0","w":240,"k":{"y":6,"A":20,",":46,".":46,"e":20,"o":20,"-":20,"a":20,"r":13,"u":13,":":17,";":17,"i":-4}},"W":{"d":"75,0r-75,-255r50,0r48,184r58,-184r44,0r57,184r50,-184r46,0r-74,255r-43,0r-60,-194r-59,194r-42,0","w":353,"k":{"A":9,",":27,".":27,"e":6,"o":6,"-":10,"a":13,"r":6,"u":6,":":6,";":6}},"X":{"d":"0,0r91,-133r-85,-122r57,0r60,94r60,-94r55,0r-84,122r93,133r-58,0r-67,-108r-67,108r-55,0","w":246},"Y":{"d":"91,0r0,-109r-96,-146r57,0r62,102r64,-102r54,0r-96,146r0,109r-45,0","w":226,"k":{"v":20,"A":27,",":40,".":40,"e":33,"o":33,"q":33,"-":40,"a":33,"u":31,":":33,";":33,"i":3,"p":31}},"Z":{"d":"13,0r0,-41r136,-173r-134,0r0,-41r190,0r0,41r-137,173r139,0r0,41r-194,0","w":219},"[":{"d":"93,-264r0,29r-33,0r0,263r33,0r0,28r-69,0r0,-320r69,0","w":100},"\\":{"d":"1,-261r30,-12r112,279r-30,11","w":140},"]":{"d":"7,56r0,-28r33,0r0,-263r-33,0r0,-29r69,0r0,320r-69,0","w":100},"^":{"d":"67,-123r-42,0r80,-132r30,0r80,132r-43,0r-52,-87","w":239},"_":{"d":"180,45r-180,0r0,-18r180,0r0,18","w":180},"a":{"d":"132,0v-1,-7,2,-18,-1,-24v-25,45,-118,35,-118,-24v0,-59,70,-60,119,-60v4,-46,-63,-44,-85,-19r-23,-22v19,-19,47,-28,74,-28v74,0,73,54,73,78r0,99r-39,0xm86,-28v33,-1,46,-18,44,-49v-28,0,-74,-2,-74,26v0,16,16,23,30,23","w":193},"b":{"d":"24,0r0,-272r44,0r1,121v9,-11,26,-26,58,-26v49,0,84,40,84,91v0,79,-101,123,-145,60r0,26r-42,0xm168,-86v0,-27,-19,-52,-51,-52v-32,0,-51,25,-51,52v0,27,19,51,51,51v32,0,51,-24,51,-51","w":226},"c":{"d":"172,-151r-29,30v-28,-36,-88,-7,-84,35v-5,41,57,70,84,35r29,31v-60,53,-156,13,-156,-66v0,-80,102,-121,156,-65","w":173},"d":{"d":"161,0r0,-26v-43,63,-145,19,-145,-60v0,-51,34,-91,83,-91v33,0,49,16,60,26r0,-121r43,0r0,272r-41,0xm59,-86v0,27,19,51,51,51v32,0,51,-24,51,-51v0,-27,-19,-52,-51,-52v-32,0,-51,25,-51,52","w":226},"e":{"d":"191,-71r-132,0v3,50,73,51,93,17r31,24v-51,66,-167,30,-167,-56v0,-54,42,-91,94,-91v52,-1,85,40,81,106xm59,-104r89,0v0,-25,-17,-41,-44,-41v-26,0,-42,16,-45,41","w":206},"f":{"d":"40,0r0,-136r-36,0r0,-37r36,0v-1,-58,0,-104,64,-103v9,0,19,0,28,2r-3,37v-41,-13,-52,19,-46,64r40,0r0,37r-40,0r0,136r-43,0","w":126,"k":{"f":6}},"g":{"d":"161,-173r41,0v-5,115,31,259,-97,259v-32,0,-62,-6,-86,-28r26,-35v36,45,134,27,113,-46v-47,56,-150,10,-142,-63v-9,-79,101,-124,145,-61r0,-26xm110,-138v-30,0,-51,20,-51,51v0,27,22,50,51,50v32,0,51,-21,51,-50v0,-30,-20,-51,-51,-51","w":226},"h":{"d":"68,-272r0,123v7,-14,24,-28,51,-28v87,2,58,99,63,177r-43,0r0,-87v0,-19,-2,-51,-32,-51v-60,0,-33,83,-39,138r-44,0r0,-272r44,0","w":206},"i":{"d":"25,0r0,-173r43,0r0,173r-43,0xm19,-231v0,-14,11,-26,27,-26v16,0,29,11,29,26v0,15,-13,25,-29,25v-16,0,-27,-11,-27,-25","w":93},"j":{"d":"25,-173r43,0r0,183v5,52,-25,87,-81,73r3,-37v26,10,35,-12,35,-35r0,-184xm19,-231v0,-14,11,-26,27,-26v16,0,29,11,29,26v0,15,-13,25,-29,25v-16,0,-27,-11,-27,-25","w":93},"k":{"d":"24,0r0,-272r44,0r0,171r66,-72r56,0r-74,79r78,94r-57,0r-69,-88r0,88r-44,0","w":193},"l":{"d":"25,0r0,-272r43,0r0,272r-43,0","w":93},"m":{"d":"23,0r0,-173r41,0r0,27v11,-37,95,-44,107,1v13,-22,31,-32,57,-32v84,0,59,99,63,177r-44,0r0,-98v0,-22,-6,-40,-32,-40v-58,0,-31,84,-37,138r-43,0v-7,-49,22,-138,-30,-138v-60,0,-33,83,-39,138r-43,0","w":313},"n":{"d":"24,0r0,-173r42,0r0,28v8,-17,24,-32,53,-32v87,2,58,99,63,177r-43,0r0,-87v0,-19,-2,-51,-32,-51v-60,0,-33,83,-39,138r-44,0","w":206},"o":{"d":"16,-86v0,-54,42,-91,94,-91v52,0,94,37,94,91v0,54,-42,90,-94,90v-52,0,-94,-36,-94,-90xm59,-86v0,27,19,51,51,51v32,0,51,-24,51,-51v0,-27,-19,-52,-51,-52v-32,0,-51,25,-51,52","w":219},"p":{"d":"24,82r0,-255r42,0r0,26v43,-63,145,-18,145,61v0,51,-35,90,-84,90v-33,0,-48,-16,-59,-26r0,104r-44,0xm168,-86v0,-27,-19,-52,-51,-52v-32,0,-51,25,-51,52v0,27,19,51,51,51v32,0,51,-24,51,-51","w":226},"q":{"d":"202,-173r0,255r-43,0r-1,-104v-9,11,-27,26,-59,26v-49,0,-83,-39,-83,-90v0,-79,101,-124,145,-61r0,-26r41,0xm59,-86v0,27,19,51,51,51v32,0,51,-24,51,-51v0,-27,-19,-52,-51,-52v-32,0,-51,25,-51,52","w":226},"r":{"d":"24,0r0,-173r44,0r0,28v11,-24,37,-37,67,-30r0,42v-7,-2,-14,-3,-21,-3v-41,0,-46,34,-46,43r0,93r-44,0","w":140,"k":{",":33,".":33,"c":6,"d":6,"e":6,"g":6,"m":-6,"n":-6,"o":6,"q":6,"-":20}},"s":{"d":"145,-151r-28,26v-10,-21,-59,-28,-59,1v0,31,93,6,93,73v0,67,-106,70,-142,29r29,-27v11,12,23,21,41,21v13,0,29,-6,29,-20v0,-36,-94,-7,-94,-73v1,-64,98,-73,131,-30","w":159},"t":{"d":"40,-136r-36,0r0,-37r36,0r0,-50r43,0r0,50r48,0r0,37r-48,0v2,45,-17,127,48,98r0,37v-48,14,-91,0,-91,-56r0,-79","w":140},"u":{"d":"182,-173r0,173r-41,0v-1,-9,2,-21,-1,-28v-8,17,-24,32,-53,32v-87,-2,-58,-99,-63,-177r44,0r0,88v0,19,1,50,31,50v61,-2,34,-83,40,-138r43,0","w":206},"v":{"d":"72,0r-71,-173r47,0r47,121r46,-121r45,0r-68,173r-46,0","w":186,"k":{",":33,".":33}},"w":{"d":"61,0r-60,-173r47,0r38,121r35,-121r47,0r38,121r36,-121r43,0r-58,173r-43,0r-42,-118r-36,118r-45,0","w":286,"k":{",":27,".":27}},"x":{"d":"0,0r69,-93r-60,-80r53,0r34,52r38,-52r49,0r-59,80r70,93r-53,0r-45,-62r-45,62r-51,0","w":193},"y":{"d":"75,1r-74,-174r48,0r49,120r43,-120r45,0r-82,210v-13,43,-49,57,-99,45r5,-39v24,10,51,5,57,-21","w":186,"k":{",":33,".":33}},"z":{"d":"12,0r0,-40r93,-98r-88,0r0,-35r140,0r0,39r-94,99r98,0r0,35r-149,0","w":173},"{":{"d":"0,-88r0,-32v10,0,34,-4,34,-25r0,-71v7,-46,36,-51,79,-48r0,31v-24,-4,-48,7,-42,25r0,68v1,29,-26,33,-36,37v12,1,36,5,36,38v0,41,-23,99,42,91r0,30v-43,3,-79,-1,-79,-47r0,-69v0,-23,-24,-28,-34,-28","w":100},"|":{"d":"22,-270r36,0r0,360r-36,0r0,-360","w":79},"}":{"d":"100,-120r0,32v-10,0,-34,4,-34,25r0,72v-7,45,-37,50,-79,47r0,-30v24,4,48,-7,43,-25r0,-68v-1,-29,25,-33,35,-37v-12,-1,-35,-6,-35,-39v0,-42,22,-98,-43,-90r0,-31v43,-3,79,2,79,48r0,69v0,23,24,27,34,27","w":100},"~":{"d":"80,-139v25,-1,57,25,78,25v15,0,24,-14,32,-25r13,31v-11,15,-23,31,-45,31v-35,0,-91,-50,-109,0r-13,-31v8,-15,21,-31,44,-31","w":239},"'":{"d":"35,-160r0,-95r37,0r0,95r-37,0","w":106},"`":{"d":"28,-255r36,52r-34,0r-51,-52r49,0","w":93},"\u00a0":{"w":106}}});
;
/*!
 * The following copyright notice may not be removed under any circumstances.
 * 
 * Copyright:
 * Copyright � 2000 Adobe Systems Incorporated. All Rights Reserved. U.S. Patent
 * Des. 318,290 and 327,903.
 * 
 * Trademark:
 * Adobe Garamond is a trademark of Adobe Systems Incorporated.
 * 
 * Full name:
 * AGaramondPro-Regular
 * 
 * Designer:
 * Robert Slimbach
 * 
 * Vendor URL:
 * http://www.adobe.com/type
 * 
 * License information:
 * http://www.adobe.com/type/legal.html
 */
Cufon.registerFont({"w":500,"face":{"font-family":"Adobe Garamond Pro","font-weight":400,"font-stretch":"normal","units-per-em":"1000","panose-1":"2 2 5 2 6 5 6 2 4 3","ascent":"725","descent":"-275","x-height":"14","cap-height":"2","bbox":"-38 -750 993.681 269","underline-thickness":"50","underline-position":"-50","stemh":"40","stemv":"74","unicode-range":"U+0020-U+007E"},"glyphs":{" ":{"w":250,"k":{"W":15,"V":15}},"\u00a0":{"w":250,"k":{"T":45,"Y":15}},"!":{"d":"110,14v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55xm110,-646v32,0,51,4,49,29r-36,412v-6,6,-21,6,-26,0r-36,-412v0,-25,17,-29,49,-29","w":220},"\"":{"d":"122,-700v50,-1,45,18,38,63r-26,161v-8,8,-16,8,-24,0r-31,-200v0,-22,15,-24,43,-24xm282,-700v50,-1,45,18,38,63r-26,161v-8,8,-16,8,-24,0r-31,-200v0,-22,15,-24,43,-24","w":404},"#":{"d":"334,-403r-117,0r-44,162r118,0xm478,-403r-102,0r-44,162r99,0r-15,50r-98,0r-51,188r-39,0r50,-188r-119,0r-51,188r-38,0r50,-188r-111,0r15,-50r109,0r43,-162r-112,0r16,-50r109,0r45,-171r43,0r-46,171r116,0r45,-171r44,0r-46,171r101,0"},"$":{"d":"251,-383r27,-228v-102,1,-120,64,-120,108v0,54,42,92,93,120xm272,-285r-31,254v67,1,130,-28,130,-108v0,-45,-10,-98,-99,-146xm325,-717r-10,78v31,1,64,7,100,15v9,22,20,76,21,113v-3,7,-16,9,-22,3v-12,-30,-33,-84,-102,-100r-30,242v79,37,162,92,162,193v0,100,-79,173,-207,173r-11,94v-8,6,-26,6,-33,0r12,-96v-67,-4,-107,-23,-122,-32v-16,-19,-28,-73,-28,-131v4,-6,15,-10,22,-3v13,43,47,116,131,134r33,-267v-73,-31,-149,-87,-150,-179v0,-88,69,-157,191,-160r9,-77v5,-7,28,-7,34,0"},"%":{"d":"799,-180v0,79,-45,194,-153,194v-108,0,-153,-115,-153,-194v0,-80,45,-194,153,-194v108,0,153,114,153,194xm727,-180v0,-52,-8,-166,-80,-166v-71,0,-82,112,-82,166v0,54,11,166,82,166v72,0,80,-114,80,-166xm351,-446v0,79,-45,194,-153,194v-108,0,-153,-115,-153,-194v0,-80,45,-194,153,-194v108,0,153,114,153,194xm279,-446v0,-52,-8,-166,-80,-166v-71,0,-82,112,-82,166v0,54,11,166,82,166v72,0,80,-114,80,-166xm636,-635r-407,675v-16,0,-30,-9,-30,-22r404,-670v10,-1,24,5,33,17","w":844},"&":{"d":"507,-205v29,-40,107,-163,126,-230v-1,-15,-41,-16,-60,-19v-6,-6,-6,-19,2,-24r82,0v75,0,114,-2,141,-4v7,5,7,18,2,24v-87,4,-120,51,-159,116v-32,54,-74,109,-116,167r33,50v53,81,104,90,145,90v31,0,67,-12,90,-36v8,1,14,9,11,18v-54,77,-195,87,-266,38v-16,-11,-50,-73,-61,-92v-57,64,-129,121,-218,121v-141,0,-196,-101,-196,-180v0,-104,73,-165,162,-218v-36,-25,-77,-64,-77,-121v0,-79,68,-135,140,-135v76,0,129,50,129,119v0,66,-52,92,-101,120v55,34,124,98,191,196xm251,-363v-50,25,-113,86,-113,167v0,89,61,151,149,151v67,0,127,-39,172,-92v-48,-84,-128,-173,-208,-226xm284,-611v-44,0,-72,41,-72,82v0,42,28,78,82,109v34,-23,60,-56,60,-104v0,-44,-22,-87,-70,-87","w":818},"(":{"d":"146,-270v0,180,55,314,154,418v0,10,-4,14,-14,15v-123,-81,-206,-246,-206,-433v0,-187,83,-352,206,-433v10,1,14,5,14,15v-99,104,-154,238,-154,418","w":320,"k":{"W":-12,"V":-12,"J":-32,"T":-12,"Y":-12}},")":{"d":"174,-270v0,-180,-55,-314,-154,-418v0,-10,4,-14,14,-15v123,81,206,246,206,433v0,187,-83,352,-206,433v-10,-1,-14,-5,-14,-15v99,-104,154,-238,154,-418","w":320},"*":{"d":"210,-676r10,130r125,-63v9,1,15,12,12,21r-115,80r114,79v3,9,-2,21,-12,22r-124,-65r-10,130v-6,5,-18,5,-24,0r-11,-130r-124,65v-9,-1,-15,-13,-12,-22r114,-79r-115,-80v-3,-9,3,-20,12,-21r125,63r11,-130v6,-5,17,-5,24,0","w":394},"+":{"d":"280,0r-59,0r0,-181r-163,0r0,-49r163,0r0,-164r59,0r0,164r163,0r0,49r-163,0r0,181"},",":{"d":"138,10v0,-64,-73,-39,-74,-87v0,-23,17,-45,49,-45v35,0,79,32,79,98v0,79,-60,124,-130,145v-9,-3,-12,-19,-6,-24v31,-10,82,-35,82,-87","w":250},"-":{"d":"40,-225r233,-22v18,11,19,41,7,57r-233,22v-18,-15,-19,-40,-7,-57","w":320,"k":{"W":70}},"\u00ad":{"d":"40,-225r233,-22v18,11,19,41,7,57r-233,22v-18,-15,-19,-40,-7,-57","w":320},".":{"d":"125,14v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55","w":250},"\/":{"d":"297,-686r-209,702r-58,0r209,-702r58,0","w":327},"0":{"d":"250,-640v146,0,207,176,207,327v0,157,-61,327,-207,327v-146,0,-207,-170,-207,-327v0,-151,61,-327,207,-327xm250,-610v-110,0,-118,203,-118,297v0,94,8,297,118,297v110,0,118,-203,118,-297v0,-94,-8,-297,-118,-297"},"1":{"d":"220,-122r0,-340v0,-68,-3,-90,-48,-94r-48,-4v-8,-4,-9,-18,-1,-22v84,-15,139,-41,165,-58v4,0,8,2,10,5v-2,18,-4,68,-4,125r0,388v-5,103,16,96,85,100v6,6,5,21,-2,24v-39,-2,-84,-2,-119,-2v-36,0,-81,0,-120,2v-7,-3,-8,-18,-2,-24v67,-4,84,3,84,-100"},"2":{"d":"192,-62v73,0,182,11,218,-22v19,-17,26,-30,32,-41v7,-3,18,0,22,6v-13,50,-31,102,-46,122r-274,-3v-56,0,-89,1,-111,3v-8,-4,-10,-20,4,-24v46,-30,79,-64,153,-144v66,-71,143,-168,143,-275v0,-91,-44,-146,-125,-146v-60,0,-103,41,-130,100v-9,5,-24,0,-24,-13v32,-88,111,-141,201,-141v100,0,165,67,165,163v0,62,-29,130,-107,220r-61,70v-82,94,-91,110,-91,116v0,7,5,9,31,9"},"3":{"d":"80,-45v49,0,45,66,101,66v65,0,179,-85,179,-196v0,-88,-42,-163,-129,-163v-49,0,-85,24,-108,49v-16,-2,-28,-16,-22,-35v38,-17,120,-54,160,-87v22,-18,53,-48,53,-96v0,-53,-37,-86,-82,-86v-59,0,-104,37,-135,88v-10,2,-21,-4,-21,-14v31,-70,104,-121,181,-121v82,0,139,44,139,115v0,41,-16,80,-107,128v-5,10,0,14,12,15v76,6,145,59,145,154v0,72,-35,144,-97,198v-74,65,-160,86,-209,86v-45,0,-99,-15,-99,-62v0,-20,15,-39,39,-39"},"4":{"d":"273,-189r-239,0v-9,0,-14,-7,-14,-17v0,-12,4,-18,18,-38r321,-448v10,-14,19,-15,40,-14v18,0,18,9,10,19r-318,440v-12,16,2,16,28,16r155,0v24,0,28,-3,28,-26r0,-184v-1,-27,38,-34,60,-37v14,-2,14,11,14,31r0,193v-4,42,54,16,84,23v9,7,9,33,0,42v-30,5,-84,-16,-84,22r0,45v-2,96,9,94,69,100v6,6,5,21,-2,24v-83,-4,-144,-4,-229,0v-7,-3,-8,-18,-2,-24v70,-4,98,4,90,-100v-2,-31,16,-67,-29,-67"},"5":{"d":"78,-415r53,-178v5,-16,10,-25,26,-27r241,-34v6,-1,9,5,9,9v-7,22,-22,47,-36,62r-189,27v-46,2,-36,55,-52,87v-10,20,18,18,35,20v125,12,265,67,265,219v0,173,-199,293,-358,298v-11,-4,-12,-24,-3,-31v155,-9,287,-104,287,-241v0,-142,-151,-178,-269,-180v-15,0,-16,-8,-9,-31"},"6":{"d":"40,-236v0,-85,28,-209,134,-313v104,-102,226,-134,287,-142v7,3,9,16,4,21v-64,22,-150,51,-222,128v-85,90,-114,200,-114,303v0,116,40,223,139,223v75,0,113,-68,113,-153v0,-102,-63,-196,-169,-159v-6,-3,-7,-10,-3,-16v29,-18,59,-30,98,-30v96,0,159,77,159,179v0,117,-86,209,-211,209v-146,0,-215,-114,-215,-250"},"7":{"d":"357,-564r-200,0v-56,0,-78,3,-107,51v-7,3,-17,1,-21,-8v10,-27,25,-70,34,-108v93,5,317,5,409,0v4,1,7,5,7,9v-119,201,-237,467,-349,684v-16,2,-31,-6,-34,-19r283,-572v15,-30,10,-37,-22,-37"},"8":{"d":"263,-640v89,0,166,50,166,146v0,82,-68,119,-118,152v69,39,132,91,132,186v0,106,-83,170,-200,170v-89,0,-185,-45,-185,-167v0,-75,69,-126,135,-168v-58,-32,-113,-80,-114,-157v-1,-102,82,-162,184,-162xm253,-16v71,0,116,-48,116,-121v0,-79,-75,-134,-148,-167v-66,45,-91,99,-91,142v0,84,49,146,123,146xm256,-610v-69,0,-105,53,-105,111v0,69,68,111,131,142v46,-33,80,-78,80,-135v0,-61,-42,-118,-106,-118"},"9":{"d":"239,-610v-64,0,-112,49,-112,138v-1,94,54,193,159,164v4,2,5,8,3,12v-97,71,-242,3,-242,-140v0,-120,91,-204,205,-204v125,0,199,108,199,255v0,109,-45,239,-147,330v-74,66,-171,103,-238,115v-8,-3,-11,-18,-6,-23v60,-18,110,-42,148,-70v92,-68,154,-171,154,-360v0,-91,-26,-217,-123,-217"},":":{"d":"125,-280v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55xm125,14v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55","w":250},";":{"d":"138,10v0,-64,-73,-39,-74,-87v0,-23,17,-45,49,-45v35,0,79,32,79,98v0,79,-60,124,-130,145v-9,-3,-12,-19,-6,-24v31,-10,82,-35,82,-87xm125,-280v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55","w":250},"<":{"d":"56,-228r389,-185r0,58r-317,148r0,2r317,149r0,57r-389,-184r0,-45"},"=":{"d":"443,-305r0,49r-385,0r0,-49r385,0xm443,-155r0,49r-385,0r0,-49r385,0"},">":{"d":"55,-413r389,185r0,45r-389,184r0,-57r317,-149r0,-2r-317,-148r0,-58"},"?":{"d":"235,-473v1,-68,-62,-99,-138,-93v-9,0,-33,-16,-33,-42v0,-20,14,-38,40,-38v80,0,172,98,172,204v0,107,-59,98,-117,132v-43,26,-37,54,-25,102v-4,8,-15,10,-21,5v-14,-23,-36,-69,-36,-99v0,-63,62,-74,103,-98v39,-23,55,-47,55,-73xm126,14v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55","w":321},"@":{"d":"452,-325v-91,0,-167,161,-168,231v0,9,5,13,12,13v6,0,32,-9,80,-52v49,-43,107,-132,119,-163v-5,-15,-21,-29,-43,-29xm518,-338r25,-51v16,-6,39,2,48,12v-13,30,-101,234,-117,277v-9,23,-3,33,23,32v106,-4,195,-78,195,-204v0,-141,-114,-230,-261,-230v-198,0,-317,164,-317,348v0,118,69,275,298,275v72,0,122,-21,155,-35v7,3,11,11,8,19v-60,38,-137,51,-193,51v-165,0,-337,-98,-337,-309v0,-235,189,-383,395,-383v191,0,295,126,295,260v0,128,-97,241,-259,241v-66,0,-72,-15,-72,-22v3,-26,24,-68,33,-94r-2,-1v-60,62,-148,117,-189,117v-28,0,-41,-22,-41,-45v0,-18,17,-82,76,-154v48,-59,120,-126,200,-126v13,0,32,8,37,22","w":765},"A":{"d":"464,-43v-9,-60,-40,-131,-57,-189v-7,-22,-11,-25,-41,-25r-151,0v-25,0,-31,3,-38,24r-35,105v-15,45,-22,72,-22,83v-5,22,40,20,62,23v7,5,7,20,-2,24v-46,-4,-146,-4,-200,0v-6,-4,-8,-19,-2,-24v48,-4,73,-3,93,-52v19,-46,46,-111,88,-223r102,-270v14,-36,20,-55,17,-71v24,-7,41,-25,52,-38v6,0,13,2,15,9r181,536v36,100,41,102,112,109v7,5,5,20,0,24v-69,-4,-175,-3,-236,0v-8,-4,-9,-19,-2,-24v22,-2,66,-4,64,-21xm296,-554v-27,60,-61,164,-87,233v-8,23,-7,24,21,24r123,0v29,0,31,-4,22,-31r-77,-226r-2,0","w":623,"k":{"c":12,"C":12,"d":12,"e":6,"G":12,"o":6,"O":12,"t":6,"T":70,"u":18,"U":20,"Y":62,"z":-6,"Q":12,"V":80,"W":100,"b":12,"p":12,"q":6,"v":42,"w":42}},"B":{"d":"263,-663v128,-1,244,26,244,153v0,72,-42,109,-102,137v0,10,8,13,19,15v54,10,134,60,134,167v0,116,-86,193,-266,193v-29,0,-78,-2,-120,-2v-44,0,-78,1,-115,2v-6,-3,-8,-18,-2,-24v66,-10,73,-11,73,-128r0,-380v4,-93,-16,-96,-81,-100v-8,-6,-7,-21,1,-25v54,-5,122,-8,215,-8xm210,-313v0,89,-17,198,21,257v16,25,49,28,79,28v89,0,157,-40,157,-140v0,-79,-40,-171,-199,-171v-54,0,-58,6,-58,26xm270,-632v-37,0,-60,-5,-60,32r0,203v0,28,1,30,52,28v102,-4,157,-34,157,-128v0,-98,-73,-135,-149,-135","w":605,"k":{"h":12,"b":6,"W":32,"V":20,"e":6,"T":10,"u":12,"U":10,"r":12,"y":18,"a":12,"i":12,"k":12,"l":12}},"C":{"d":"435,16v-214,2,-390,-119,-391,-342v0,-215,178,-350,396,-350v66,0,147,19,205,28v1,23,6,86,14,147v-5,7,-20,8,-27,2v-15,-68,-60,-146,-205,-146v-153,0,-283,97,-283,304v0,210,133,326,297,326v129,0,186,-84,209,-143v7,-5,22,-3,26,4v-7,52,-33,118,-48,137v-52,15,-100,32,-193,33","w":696,"k":{"u":18,"z":18,"r":6,"y":24}},"D":{"d":"128,-150r0,-380v2,-83,-11,-95,-69,-100v-8,-5,-9,-21,1,-25v161,-6,342,-25,471,22v135,49,203,163,203,295v0,127,-62,234,-175,298v-139,78,-333,29,-512,43v-6,-3,-8,-19,-2,-25v73,-9,83,-8,83,-128xm210,-173v-3,116,30,150,143,150v194,0,281,-127,281,-313v0,-110,-53,-296,-315,-296v-59,0,-91,8,-99,14v-8,6,-10,31,-10,76r0,369","w":780,"k":{"e":6,"o":12,"u":12,"Y":10,"V":20,"W":20,"r":12,"a":6,"i":12,"h":18}},"E":{"d":"128,-150r0,-360v2,-114,-6,-121,-77,-128v-6,-4,-4,-22,2,-25r306,3v64,0,123,0,135,-3v6,17,12,89,16,133v-4,6,-20,8,-25,2v-15,-47,-24,-82,-76,-95v-34,-8,-117,-5,-169,-6v-30,0,-30,2,-30,40r0,200v-8,49,57,21,91,28v72,-6,95,14,109,-41r8,-41v5,-6,22,-6,26,1v-4,47,-4,150,0,197v-4,7,-21,7,-26,1v-9,-31,-7,-68,-40,-77v-21,-6,-96,-5,-135,-5v-30,0,-33,1,-33,27v0,77,-14,206,19,245v11,13,33,23,121,23v77,0,106,-4,128,-15v18,-10,45,-45,71,-97v7,-5,21,-2,25,7v-7,35,-32,112,-45,139r-358,-3v-43,0,-77,1,-136,3v-6,-3,-8,-19,-2,-25v79,-6,95,-2,95,-128","w":584,"k":{"c":6,"d":6,"e":6,"o":6,"t":18,"u":18,"Y":12,"z":6,"V":12,"W":12,"b":12,"p":18,"q":6,"v":30,"w":30,"r":6,"y":42,"i":6,"h":12,"g":6,"k":12,"l":12,"n":6,"f":12,"j":12,"m":6,"x":6}},"F":{"d":"210,-589r0,211v-7,49,66,19,101,27v72,-7,94,14,110,-41r9,-40v5,-6,22,-6,26,0v-4,48,-4,150,0,198v-4,6,-19,6,-26,0v-9,-31,-10,-68,-42,-77v-25,-7,-103,-4,-145,-5v-30,0,-33,1,-33,27r0,139v-4,126,13,122,95,128v6,4,4,21,-2,24v-59,-2,-93,-2,-133,-2v-44,0,-78,0,-125,2v-6,-3,-8,-18,-2,-24v74,-8,85,-7,85,-128r0,-360v2,-115,-6,-121,-76,-128v-6,-4,-4,-22,2,-25r300,3v64,0,122,0,135,-3v0,46,1,94,3,132v-3,6,-17,9,-24,3v-8,-45,-20,-82,-70,-95v-33,-9,-108,-5,-158,-6v-30,0,-30,2,-30,40","w":538,"k":{".":140,",":140,"e":36,"o":36,"u":24,"r":36,"y":18,"a":48,"i":34,"l":18,"A":36}},"G":{"d":"453,-676v95,1,126,21,189,24v0,33,6,85,15,145v-3,8,-21,9,-27,3v-28,-108,-103,-142,-205,-142v-194,0,-279,138,-279,288v0,192,99,343,300,343v62,0,99,-13,110,-30v10,-8,10,-66,10,-97v0,-84,-3,-90,-65,-97r-44,-5v-8,-4,-7,-22,0,-25v53,3,183,5,249,0v7,4,8,19,1,25v-73,-5,-58,71,-59,142v0,40,6,64,24,71v3,3,3,9,-1,12v-66,10,-157,35,-228,35v-208,0,-402,-129,-397,-338v5,-213,156,-358,407,-354","w":747,"k":{"e":6,"o":6,"u":12,"r":24,"y":12,"a":6,"i":12,"h":12,"l":6,"n":18}},"H":{"d":"546,-323r-286,0v-48,0,-50,2,-50,32r0,141v-3,123,13,121,90,128v6,4,4,21,-2,24v-54,-2,-88,-2,-127,-2v-43,0,-77,1,-116,2v-6,-3,-8,-18,-2,-24v67,-11,75,-12,75,-128r0,-360v1,-121,-11,-124,-85,-128v-6,-4,-4,-22,2,-25v49,1,83,3,126,3v39,0,73,-1,116,-3v6,3,8,21,2,25v-72,6,-81,11,-79,128r0,115v0,31,2,32,50,32r286,0v48,0,50,-1,50,-32r0,-115v2,-117,-7,-122,-80,-128v-6,-4,-4,-22,2,-25v46,2,80,3,121,3v39,0,73,-1,118,-3v6,3,8,21,2,25v-74,6,-81,10,-81,128r0,360v-3,120,10,120,84,128v6,4,4,21,-2,24v-48,-2,-82,-2,-121,-2v-41,0,-77,0,-121,2v-6,-3,-8,-18,-2,-24v72,-11,80,-11,80,-128r0,-141v0,-30,-2,-32,-50,-32","w":806,"k":{"e":18,"o":18,"u":30,"y":24,"a":6,"i":18}},"I":{"d":"128,-152r0,-356v2,-121,-7,-126,-86,-130v-6,-4,-4,-22,2,-25v50,2,84,3,126,3v40,0,74,-1,124,-3v6,3,8,21,2,25v-78,4,-86,9,-86,130r0,356v-3,122,10,122,86,130v6,4,4,21,-2,24v-50,-2,-84,-2,-124,-2v-42,0,-76,0,-126,2v-6,-3,-8,-18,-2,-24v75,-8,86,-8,86,-130","w":338,"k":{"c":18,"d":18,"e":18,"o":18,"t":24,"u":32,"z":6,"b":6,"p":32,"v":24,"w":24,"r":18,"y":12,"a":18,"i":12,"h":6,"g":6,"k":6,"l":6,"n":12,"f":6,"j":18,"m":12,"s":18}},"J":{"d":"-2,134v40,0,100,31,119,-13v21,-48,26,-197,26,-251r0,-378v1,-126,-16,-124,-94,-130v-6,-4,-4,-22,2,-25v58,2,92,3,134,3v40,0,74,-1,114,-3v6,3,8,21,2,25v-70,8,-76,15,-76,130r0,371v10,199,-61,326,-220,338v-12,0,-43,-1,-43,-21v0,-17,15,-46,36,-46","w":345,"k":{"e":30,"o":30,"u":36,"y":12,"a":30,"i":24}},"K":{"d":"128,-150r0,-360v2,-120,-8,-123,-85,-128v-6,-4,-4,-22,2,-25v49,2,81,3,126,3v39,0,73,-1,116,-3v6,3,8,21,2,25v-72,6,-81,11,-79,128r0,115v0,19,2,38,13,38v35,0,61,-41,79,-59r108,-107v24,-27,76,-69,85,-99v0,-9,-32,-13,-44,-16v-7,-7,-6,-20,2,-25v67,4,144,4,209,0v7,5,8,19,2,25v-109,15,-143,59,-233,144r-90,85v-28,29,-31,30,-4,62v95,109,172,199,250,281v29,31,58,41,107,44v7,5,5,21,-2,24v-61,-4,-160,-4,-234,0v-7,-2,-10,-18,-4,-24v13,-3,46,-5,47,-14v-23,-44,-49,-64,-97,-117v-40,-44,-140,-174,-177,-174v-15,0,-17,13,-17,48r0,129v-3,122,10,121,85,128v6,4,4,21,-2,24v-49,-2,-83,-2,-122,-2v-41,0,-75,0,-116,2v-6,-3,-8,-17,-2,-24v68,-9,75,-10,75,-128","w":675,"k":{"C":36,"e":6,"G":36,"o":18,"O":36,"u":24,"Q":36,"v":30,"w":48,"y":24}},"L":{"d":"128,-150r0,-360v3,-120,-9,-123,-90,-128v-6,-4,-4,-22,2,-25v56,2,90,3,130,3v39,0,73,-1,123,-3v6,3,8,21,2,25v-77,5,-85,8,-85,128r0,348v0,63,3,89,21,108v11,11,30,23,109,23v85,0,106,-4,122,-13v20,-12,46,-48,66,-99v6,-5,25,-1,25,6v0,11,-28,113,-42,140r-341,-3v-42,0,-74,1,-130,3v-6,-3,-8,-19,-2,-25v78,-7,90,-4,90,-128","w":553,"k":{"C":12,"G":12,"O":12,"T":100,"u":6,"U":18,"Y":70,"Q":12,"V":90,"W":110,"w":36,"y":46,"j":12,"A":-12}},"M":{"d":"275,-498r183,368r2,0v27,-47,54,-98,77,-150v47,-105,157,-287,170,-383v69,5,98,3,164,0v6,4,6,21,1,25v-70,4,-95,17,-91,97v5,106,3,291,11,435v3,50,2,77,49,81r32,3v6,6,5,20,-2,24v-41,-2,-82,-2,-118,-2v-35,0,-80,0,-121,2v-7,-5,-8,-18,-2,-24r29,-3v49,-5,49,-21,49,-85r-3,-417v-13,9,-47,90,-67,130r-89,175v-45,88,-90,182,-108,228v-4,5,-14,6,-19,0v-50,-132,-120,-267,-178,-393v-21,-46,-39,-102,-66,-144v-4,62,-7,120,-10,178v-3,61,-7,150,-7,235v0,97,18,92,83,96v7,6,6,21,-1,24v-68,-4,-142,-4,-213,0v-6,-5,-8,-19,-2,-24v55,-8,78,-3,84,-85v5,-66,11,-121,17,-220v5,-83,7,-168,11,-223v5,-70,-18,-82,-83,-88v-5,-6,-4,-21,4,-25v62,4,97,5,162,0v-4,39,21,101,52,165","w":912,"k":{"j":18,"c":24,"d":24,"e":24,"o":24,"u":36,"y":30,"a":12,"i":12,"n":6}},"N":{"d":"681,-615v-29,124,-15,299,-15,445v0,30,0,149,3,175v-2,6,-8,11,-19,11v-62,-79,-270,-311,-360,-415v-27,-31,-95,-113,-116,-135r-2,0v-11,77,-5,195,-5,283v0,47,2,177,18,207v9,17,53,19,82,22v6,8,5,19,-2,24v-45,-2,-80,-2,-117,-2v-42,0,-69,0,-104,2v-7,-5,-8,-18,-2,-24v27,-2,68,-6,71,-23v28,-139,8,-334,13,-495v0,-28,0,-49,-22,-72v-17,-18,-48,-23,-79,-26v-6,-6,-6,-21,2,-25v70,3,109,6,159,0v62,112,188,237,268,329v68,78,118,133,165,181v9,-2,6,-21,6,-42r0,-214v0,-47,-1,-177,-20,-207v-1,-11,-46,-19,-79,-22v-7,-6,-6,-22,2,-25v46,2,80,3,118,3v43,0,69,-1,103,-3v8,5,8,19,2,25v-29,3,-69,14,-70,23","w":783,"k":{"e":30,"o":30,"u":30,"y":30,"a":30,"i":24}},"O":{"d":"402,16v-222,0,-356,-159,-356,-339v0,-200,150,-353,356,-353v231,0,347,167,347,343v0,202,-154,349,-347,349xm415,-15v120,0,234,-90,234,-287v0,-165,-73,-343,-264,-343v-104,0,-239,71,-239,290v0,148,72,340,269,340","w":795,"k":{"c":6,"d":6,"e":6,"o":6,"t":6,"T":30,"u":6,"Y":30,"z":6,"V":24,"W":52,"b":6,"p":6,"q":6,"r":6,"a":6,"i":6,"h":12,"g":6,"k":12,"l":12,"n":6,"f":6,"j":12,"m":6,"s":6,"A":30,"X":30}},"P":{"d":"245,-663v160,-2,272,46,272,190v0,92,-57,138,-100,159v-44,21,-97,30,-138,30v-7,-3,-7,-18,-1,-20v108,-20,147,-81,147,-168v0,-97,-57,-160,-161,-160v-53,0,-54,4,-54,36r0,446v-5,121,13,120,93,128v6,5,4,21,-2,24v-57,-2,-90,-2,-130,-2v-42,0,-78,0,-122,2v-6,-3,-8,-18,-2,-24v72,-9,81,-9,81,-128r0,-369v4,-108,-13,-105,-83,-111v-7,-6,-6,-21,2,-25v53,-7,115,-8,198,-8","w":549,"k":{"h":6,"H":12,".":180,",":180,"e":54,"o":54,"t":18,"u":30,"r":30,"y":6,"a":42,"i":12,"l":6,"n":36,"s":36,"A":40,"E":12,"I":12}},"Q":{"d":"402,-676v231,0,347,167,347,343v0,165,-102,292,-242,334v-2,5,2,9,8,15v79,70,204,204,364,162v6,1,8,10,5,15v-168,90,-390,-68,-459,-143v-51,-57,-90,-31,-165,-61v-132,-53,-214,-169,-214,-312v0,-200,150,-353,356,-353xm415,-15v120,0,234,-90,234,-287v0,-165,-73,-343,-264,-343v-104,0,-239,71,-239,290v0,148,72,340,269,340","w":795,"k":{"X":12,"W":30,"V":18,"T":24,"u":6,"U":12,"Y":12,"a":6,"A":24}},"R":{"d":"276,-663v133,0,249,38,250,170v0,95,-75,147,-125,167v-5,6,0,16,5,24v80,129,133,209,201,266v17,15,42,26,69,28v5,2,6,10,1,14v-116,16,-170,-8,-251,-121v-26,-37,-67,-106,-98,-151v-15,-22,-31,-32,-71,-32v-45,0,-47,1,-47,22r0,126v-2,119,8,119,81,128v6,5,4,21,-2,24v-45,-2,-79,-2,-119,-2v-42,0,-78,0,-125,2v-6,-3,-8,-17,-2,-24v74,-8,85,-7,85,-128r0,-359v1,-117,-8,-114,-76,-121v-7,-5,-7,-21,1,-25v56,-5,125,-8,223,-8xm282,-328v94,0,154,-37,153,-150v0,-79,-49,-154,-160,-154v-62,0,-65,4,-65,32r0,234v2,55,-4,38,72,38","w":645,"k":{"C":36,"e":12,"G":36,"o":12,"O":36,"T":45,"u":12,"U":25,"Y":45,"Q":36,"V":70,"W":75,"y":30}},"S":{"d":"281,-676v44,0,90,15,128,19v9,24,18,80,18,122v-4,6,-20,9,-25,3v-13,-48,-40,-113,-136,-113v-98,0,-119,65,-119,111v0,100,100,130,165,173v62,41,125,97,125,192v0,110,-83,185,-213,185v-84,0,-137,-26,-156,-37v-12,-22,-25,-93,-27,-140v5,-7,20,-9,24,-3v15,51,56,149,172,149v84,0,125,-55,125,-115v0,-44,-10,-91,-82,-140v-84,-57,-203,-100,-203,-234v0,-95,74,-172,204,-172","w":489,"k":{"e":12,"o":12,"t":24,"u":24,"p":24,"q":12,"v":30,"w":30,"r":12,"y":12,"a":12,"i":12,"h":12,"k":12,"l":12,"n":24,"j":18,"m":24}},"T":{"d":"368,-595r0,445v-4,125,12,122,92,128v6,4,4,21,-2,24v-57,-2,-91,-2,-131,-2v-40,0,-75,0,-139,2v-6,-3,-8,-19,0,-24v81,-6,98,-1,98,-128r0,-447v11,-53,-50,-25,-85,-32v-43,0,-98,2,-123,25v-24,22,-34,44,-45,69v-8,6,-22,1,-26,-7v16,-45,31,-109,38,-149v3,-2,16,-3,19,0v6,32,39,31,85,31r405,0v54,0,63,-2,78,-28v5,-2,16,-1,18,3v-11,41,-18,122,-15,152v-4,8,-21,8,-26,2v-3,-25,-11,-61,-25,-73v-31,-26,-119,-26,-187,-25v-30,0,-29,1,-29,34","w":660,"k":{"C":24,"e":120,"G":24,"o":120,"O":32,"T":-12,"u":130,"Y":-12,"z":70,"Q":32,"W":-6,"w":130,"r":130,"y":110,"a":110,"i":40,"m":130,"s":100,"A":65,".":120,"X":-18,",":120,":":80,";":80,"S":12,"}":-12,"]":-12,"\u00ad":100,")":-12}},"U":{"d":"385,16v-174,-1,-265,-86,-265,-291r0,-236v2,-119,-8,-124,-85,-128v-6,-4,-4,-21,2,-24v50,2,84,3,124,3v41,0,74,-1,123,-3v6,3,8,20,2,24v-76,5,-84,9,-84,128r0,216v0,161,50,267,203,267v185,0,200,-181,200,-381v0,-62,-3,-177,-19,-207v-9,-17,-45,-22,-75,-23v-6,-7,-4,-20,2,-24v71,4,151,4,219,0v8,5,7,18,2,24v-30,1,-69,7,-74,26v-43,170,33,437,-84,556v-45,46,-117,73,-191,73","w":746,"k":{"c":24,"d":30,"t":30,"z":18,"b":6,"p":30,"v":12,"r":36,"y":12,"a":12,"i":18,"g":18,"k":6,"l":6,"n":36,"f":12,"m":36,"x":12,"s":30,"A":12}},"V":{"d":"322,13v-62,-197,-149,-379,-222,-567v-25,-64,-40,-81,-99,-84v-7,-6,-6,-20,2,-25v63,4,157,4,225,0v7,4,9,19,2,25v-27,3,-69,11,-62,21v39,123,135,353,192,485r189,-446v25,-53,4,-51,-54,-60v-7,-6,-5,-21,2,-25v58,4,138,4,201,0v7,4,7,20,2,25v-41,1,-65,7,-82,37v-77,135,-214,455,-270,614v-7,5,-17,3,-26,0","w":676,"k":{"}":-12,"]":-24,"Q":24,";":45,":":18,".":120,"-":60,",":120,")":-12,"C":24,"e":100,"G":24,"o":100,"O":24,"u":88,"r":78,"y":70,"a":88,"i":10,"A":75}},"W":{"d":"94,-565v-21,-50,-37,-75,-97,-74v-6,-6,-5,-20,3,-24v69,4,142,4,217,0v9,3,8,17,2,24v-20,1,-60,5,-56,19v45,144,160,360,225,507r77,-162v15,-36,23,-35,8,-67r-110,-240v-17,-39,-39,-57,-88,-57v-7,-8,-6,-20,2,-24v52,4,173,4,217,0v7,3,8,16,2,24v-43,4,-74,-4,-47,57r77,174v19,-39,37,-77,59,-131v44,-106,28,-96,-31,-100v-7,-10,-5,-21,2,-24v50,4,140,4,190,0v7,3,8,16,2,24v-68,-5,-84,39,-111,91v-26,49,-70,134,-87,183v29,88,75,168,109,252r90,-211r80,-199v13,-36,30,-79,30,-91v0,-24,-37,-24,-62,-25v-8,-9,-7,-20,1,-24v66,4,133,4,189,0v8,3,9,16,2,24v-81,6,-72,24,-138,176r-73,169v-44,102,-84,200,-125,307v-6,4,-19,4,-26,0v-32,-89,-90,-214,-121,-284v-36,73,-96,201,-125,284v-7,4,-19,4,-27,0v-17,-42,-56,-132,-69,-161","w":960,"k":{"}":-12,"m":94,"]":-24,"Q":18,";":60,":":60,".":120,"-":40,",":120,")":-12,"C":18,"d":78,"e":78,"G":18,"o":78,"O":18,"t":76,"T":-12,"u":84,"r":82,"y":70,"a":88,"i":10,"A":75}},"X":{"d":"334,-385v35,-50,129,-182,148,-237v-1,-10,-41,-15,-59,-16v-6,-7,-6,-21,2,-25v62,4,141,4,202,0v7,5,8,19,2,25v-61,1,-79,25,-131,87v-41,49,-84,109,-130,177v-16,23,-17,17,3,45r169,238v46,60,61,66,126,69v7,6,6,19,-1,24v-46,-2,-85,-2,-126,-2v-45,0,-93,0,-129,2v-6,-6,-7,-18,-2,-24v18,-2,63,-3,63,-16v0,-6,-2,-16,-22,-44v-43,-61,-99,-144,-143,-201v-27,34,-102,142,-141,213v-15,27,-26,40,16,45r27,3v7,6,5,19,-1,24v-71,-4,-142,-4,-211,0v-9,-4,-10,-18,-3,-24v72,-1,109,-45,144,-92v12,-16,82,-103,136,-184v15,-23,13,-20,-3,-43r-150,-215v-42,-60,-58,-72,-119,-82v-5,-6,-4,-22,2,-25v47,2,80,3,116,3v39,0,84,-1,113,-3v7,3,8,18,2,25v-19,1,-58,3,-58,15v0,9,10,26,30,55v33,48,92,138,128,183","w":643,"k":{"Q":24,"C":24,"e":6,"G":24,"O":24,"u":24,"y":12}},"Y":{"d":"383,-366v-55,86,-45,108,-47,214v-2,131,16,124,98,130v6,6,4,21,-2,24v-62,-2,-96,-2,-136,-2v-42,0,-78,0,-131,2v-6,-3,-8,-18,-2,-24v79,-7,94,-4,91,-130v-2,-81,3,-108,-32,-175r-118,-230v-44,-80,-48,-69,-107,-81v-6,-7,-5,-21,2,-25v52,4,173,4,223,0v13,6,7,31,-10,27v-35,5,-42,10,-42,18v37,105,96,206,144,299v31,-53,64,-111,94,-169v28,-52,65,-120,65,-132v2,-8,-39,-17,-60,-18v-7,-7,-6,-21,2,-25v69,4,121,4,189,0v8,5,9,19,2,25v-87,6,-102,62,-160,163","w":574,"k":{"C":18,"d":90,"e":90,"G":18,"o":94,"O":18,"t":52,"T":-12,"u":74,"Y":-12,"Q":18,"V":-12,"W":-12,"q":88,"v":50,"a":68,"l":-6,".":80,"X":-12,",":80,":":60,";":37,"}":-12,"]":-24,"\u00ad":74,")":-12}},"Z":{"d":"27,-9v109,-121,225,-303,325,-443v64,-91,85,-109,109,-169v0,-5,-10,-8,-36,-8v-98,3,-260,-13,-298,33v-20,24,-36,51,-44,68v-9,4,-21,1,-25,-7v12,-35,29,-95,41,-150v4,-3,12,-3,17,0v3,24,18,25,99,25r264,0v72,0,104,-1,115,-3v4,1,6,5,6,9v-89,94,-238,324,-329,452v-11,16,-99,140,-99,155v0,10,10,13,28,14v74,2,273,13,333,-21v29,-16,53,-53,70,-92v9,-3,22,-2,25,7v-9,46,-29,117,-45,142r-439,-3v-53,0,-89,2,-112,3v-4,-2,-5,-7,-5,-12","w":641,"k":{"C":30,"e":6,"G":30,"o":12,"O":30,"u":6,"Q":30,"w":36,"y":36,"a":6,"i":12,"A":-12}},"[":{"d":"117,150r0,-836r179,3v7,4,7,15,1,22v-37,7,-89,1,-114,20v-12,10,-13,82,-13,156r0,434v0,74,1,146,13,156v24,20,78,12,114,20v6,7,6,18,-1,22","w":320,"k":{"W":-24,"V":-12,"J":-32,"T":-12,"Y":-12}},"\\":{"d":"88,-686r191,702r-58,0r-191,-702r58,0","w":309},"]":{"d":"203,-686r0,836r-179,-3v-7,-4,-7,-15,-1,-22v37,-7,89,-1,114,-20v12,-10,13,-82,13,-156r0,-434v0,-74,-1,-146,-13,-156v-24,-20,-78,-12,-114,-20v-6,-7,-6,-18,1,-22","w":320},"^":{"d":"450,-244r-62,0r-138,-311r-138,311r-63,0r177,-382r48,0"},"_":{"d":"500,75r0,50r-500,0r0,-50r500,0"},"a":{"d":"136,14v-60,0,-100,-38,-101,-93v0,-39,19,-62,83,-85r119,-43v28,-8,16,-46,18,-75v3,-37,-11,-87,-73,-87v-28,0,-64,14,-64,45v0,43,-41,50,-67,53v-9,0,-14,-5,-14,-14v0,-52,113,-123,186,-123v72,0,107,42,104,117r-6,174v-2,46,7,87,46,87v21,5,41,-30,45,0v0,5,-35,44,-84,44v-24,0,-48,-9,-65,-31v-4,-6,-10,-14,-16,-14v-24,6,-72,45,-111,45xm112,-91v0,17,12,60,63,60v40,0,72,-22,74,-72r2,-59v-5,-27,-25,-10,-51,-3v-60,17,-88,35,-88,74","w":404},"b":{"d":"108,-12v-12,-2,-23,27,-35,10r2,-562v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v23,-9,71,-22,109,-42v4,0,8,2,10,6v-6,94,-4,211,-4,313v0,17,4,21,10,21v26,-15,66,-33,115,-33v114,0,191,86,191,196v0,133,-96,226,-230,226v-55,0,-94,-19,-127,-26xm229,-365v-58,0,-80,-3,-80,73r0,156v-1,70,34,120,101,120v94,0,131,-80,131,-166v0,-103,-53,-183,-152,-183","k":{"w":6,"v":6,".":16,",":16,"y":6}},"c":{"d":"398,-88v-88,179,-362,109,-362,-97v0,-108,76,-223,242,-223v23,0,63,4,89,17v27,14,9,106,-21,70v-78,-93,-238,-54,-238,108v0,78,54,171,161,171v58,0,88,-26,111,-60v8,-2,17,5,18,14","w":400},"d":{"d":"424,-585r0,439v-1,110,4,132,72,106v7,2,8,27,-1,32v-42,6,-104,21,-132,32v-22,3,-6,-46,-13,-56r-4,0v-44,30,-78,46,-126,46v-111,0,-185,-87,-185,-192v0,-162,148,-260,309,-221v5,0,6,-6,6,-14r0,-151v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v25,-8,72,-23,108,-42v4,0,8,2,10,6v-1,30,-3,80,-3,124xm266,-34v39,0,84,-10,84,-65r0,-197v0,-20,-4,-29,-11,-39v-15,-21,-45,-42,-84,-42v-91,0,-138,79,-138,166v0,91,51,177,149,177","w":509},"e":{"d":"378,-289v0,43,-1,33,-63,33r-185,0v-26,0,-27,2,-27,27v0,101,59,187,166,187v33,0,61,-10,92,-49v10,-3,17,3,18,12v-33,68,-105,93,-157,93v-117,0,-187,-91,-187,-196v0,-123,81,-226,201,-226v86,0,142,59,142,119xm121,-286v46,0,177,1,175,-10v17,-36,-5,-82,-71,-82v-59,0,-110,54,-111,87v0,2,0,5,7,5","w":396,"k":{"z":-4,"w":-6}},"f":{"d":"262,-675v-93,0,-94,156,-93,253v0,27,0,28,17,28r114,0v11,10,4,38,-7,42r-107,0v-17,0,-17,2,-17,29r0,211v-4,95,14,85,74,90v7,5,5,21,-2,24v-45,-2,-74,-2,-109,-2v-36,0,-66,0,-96,2v-7,-3,-9,-19,-2,-24v53,-7,61,-3,61,-90r0,-211v7,-44,-26,-25,-55,-29v-6,-4,-7,-13,-3,-19v19,-13,57,-14,58,-41v7,-152,46,-303,211,-303v53,0,98,24,98,60v1,30,-43,49,-62,24v-18,-23,-47,-44,-80,-44","w":290,"k":{"'":-78,"}":-110,"]":-116,".":16,",":16,")":-110,"\"":-78}},"g":{"d":"208,-408v63,0,110,36,157,36r81,0v11,5,9,41,-6,41r-72,0v2,17,2,34,2,51v0,69,-41,159,-168,159v-18,0,-32,-2,-44,-3v-12,7,-44,26,-44,55v0,58,118,39,189,39v56,0,140,10,140,110v0,109,-119,189,-249,189v-123,0,-166,-66,-166,-116v0,-16,4,-29,11,-37v17,-18,45,-44,68,-67v9,-9,17,-18,8,-24v-39,-10,-73,-44,-73,-79v16,-27,67,-44,78,-82v-34,-18,-75,-57,-75,-121v0,-86,74,-151,163,-151xm271,32v-98,0,-168,11,-168,95v0,54,43,99,134,99v89,0,148,-50,148,-107v0,-61,-42,-87,-114,-87xm217,-150v54,0,82,-43,82,-104v0,-68,-28,-125,-89,-125v-48,0,-84,44,-84,112v0,71,43,117,91,117","w":446,"k":{"f":-6}},"h":{"d":"88,-112r0,-452v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v25,-8,72,-23,108,-42v4,0,8,2,10,6r-3,334v0,9,1,17,4,19v36,-28,84,-52,135,-52v163,0,129,152,129,296v0,90,10,84,65,90v7,5,5,21,-2,24v-63,-4,-135,-4,-193,0v-7,-3,-9,-19,-2,-24v51,-7,58,-5,58,-90r0,-143v0,-64,-31,-107,-95,-107v-34,0,-60,12,-78,27v-18,15,-21,20,-21,55r0,168v-2,86,7,83,59,90v7,4,5,21,-2,24v-59,-4,-133,-4,-192,0v-7,-3,-9,-19,-2,-24v54,-6,63,-1,63,-90","w":515},"i":{"d":"98,-112r0,-175v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,80,-34,103,-49v5,0,9,2,10,6v-6,93,-3,204,-4,304v-1,86,7,83,59,90v7,5,5,21,-2,24v-56,-4,-134,-4,-188,0v-7,-3,-9,-19,-2,-24v51,-7,59,-4,59,-90xm131,-624v30,0,50,24,50,52v0,36,-25,52,-53,52v-31,0,-51,-23,-51,-50v0,-33,24,-54,54,-54","w":257},"j":{"d":"46,201v71,-70,49,-326,54,-488v1,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,80,-34,103,-49v5,0,9,2,10,6v-2,35,-4,83,-4,124v0,127,14,311,-21,398v-29,72,-83,120,-152,163v-12,-2,-18,-13,-14,-23v26,-13,44,-30,59,-45xm133,-624v30,0,50,24,50,52v0,35,-25,52,-53,52v-31,0,-51,-23,-51,-51v0,-33,24,-53,54,-53","w":253},"k":{"d":"88,-112r0,-452v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v25,-8,72,-23,108,-42v4,0,8,2,10,6v-1,30,-3,80,-3,124r0,343v0,13,1,15,18,15v38,0,83,-56,109,-83v28,-30,35,-41,35,-46v2,-10,-34,-10,-46,-13v-9,-2,-9,-22,0,-25r23,0v44,0,103,-2,159,-8v7,3,8,20,2,25v-78,1,-150,95,-186,125v-12,11,-13,16,-7,24v31,42,129,152,161,182v18,17,40,21,66,24v7,5,6,21,-2,24v-59,-4,-144,-4,-199,0v-8,-4,-9,-20,-2,-24v13,-3,44,-4,43,-13v0,-6,-4,-13,-17,-28r-107,-124v-18,-16,-50,-32,-50,20r0,55v-3,87,9,84,62,90v7,5,5,21,-2,24v-61,-4,-134,-4,-193,0v-7,-3,-9,-19,-1,-24v52,-7,60,-3,60,-90","w":482},"l":{"d":"162,-585r0,473v-3,87,9,84,62,90v7,5,5,21,-2,24v-61,-4,-134,-4,-193,0v-7,-3,-9,-19,-2,-24v53,-7,61,-3,61,-90r0,-452v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v25,-8,72,-23,108,-42v4,0,8,2,10,6v-1,30,-3,80,-3,124","w":247},"m":{"d":"535,-360v-64,0,-98,22,-98,95r0,153v-2,86,7,83,59,90v7,5,5,21,-2,24v-55,-4,-132,-4,-185,0v-7,-3,-9,-19,-2,-24v49,-8,56,-6,56,-90r0,-145v0,-67,-30,-103,-92,-103v-41,0,-70,17,-84,36v-30,40,-12,145,-15,212v-4,84,7,82,56,90v7,5,5,21,-2,24v-54,-4,-133,-4,-188,0v-7,-3,-9,-19,-2,-24v53,-6,62,-2,62,-90r0,-175v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,80,-34,100,-49v27,6,-9,63,20,65v69,-54,190,-82,239,8v9,2,16,-2,25,-8v40,-28,89,-51,136,-51v80,0,119,55,119,124r0,172v-3,87,9,84,62,90v7,5,5,21,-2,24v-59,-4,-132,-4,-188,0v-7,-3,-9,-19,-2,-24v49,-8,56,-6,56,-90r0,-145v0,-68,-30,-103,-93,-103","w":787},"n":{"d":"272,-360v-63,2,-100,27,-100,95r0,153v-2,84,7,82,56,90v7,5,5,21,-2,24v-52,-4,-133,-4,-185,0v-7,-3,-9,-19,-2,-24v51,-7,59,-4,59,-90r0,-175v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,80,-34,100,-49v27,6,-9,63,20,65v42,-26,89,-51,140,-51v150,1,117,159,117,296v0,86,8,83,60,90v7,5,5,21,-2,24v-55,-4,-134,-4,-187,0v-7,-3,-9,-19,-2,-24v50,-7,60,-6,57,-90v-5,-112,32,-252,-94,-248","w":525},"o":{"d":"248,-408v112,0,203,84,203,207v0,125,-86,215,-211,215v-115,0,-205,-81,-205,-206v0,-126,95,-216,213,-216xm365,-185v0,-85,-34,-193,-129,-193v-66,0,-115,63,-115,173v0,91,41,189,129,189v88,0,115,-92,115,-169","w":486,"k":{"v":16,"w":12,"y":12,"x":6,".":16}},"p":{"d":"85,143r0,-430v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v26,-11,73,-35,99,-54v30,3,-10,57,17,66v13,-6,76,-47,140,-47v101,0,166,82,166,183v-1,156,-127,262,-299,235v-9,2,-14,7,-14,28v0,62,-23,199,38,190r39,5v7,5,5,22,-2,25v-48,-2,-77,-3,-112,-3v-36,0,-66,1,-94,3v-7,-3,-9,-20,-2,-25v51,-7,59,-4,59,-90xm159,-101v-4,57,56,82,102,82v82,0,130,-76,130,-180v0,-80,-45,-161,-135,-161v-43,0,-74,16,-85,28v-25,48,-7,160,-12,231","w":507,"k":{"w":6,".":16,",":16}},"q":{"d":"275,-408v69,0,107,40,145,0v5,0,9,4,11,9v-18,146,-3,375,-8,542v-3,91,14,79,67,90v7,5,5,22,-2,25v-73,-4,-133,-4,-206,0v-7,-3,-9,-20,-2,-25v57,-6,69,2,69,-90r0,-130v0,-19,-1,-24,-9,-24v-26,15,-63,25,-102,25v-149,0,-203,-118,-203,-196v0,-145,111,-226,240,-226xm349,-231v3,-108,-14,-146,-99,-146v-76,0,-131,69,-131,163v0,110,64,188,158,188v44,0,60,-10,65,-16v15,-44,6,-131,7,-189","w":497},"r":{"d":"221,-345v-79,10,-42,150,-49,233v-7,94,15,85,76,90v7,5,5,21,-2,24v-47,-2,-76,-2,-111,-2v-36,0,-66,0,-92,2v-7,-3,-9,-19,-2,-24v50,-7,57,-5,57,-90r0,-175v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,76,-34,101,-49v5,1,8,3,8,7v1,22,-4,51,3,67v35,-29,73,-60,110,-60v25,0,45,16,45,40v0,33,-27,46,-43,46v-25,-2,-44,-26,-66,-23","w":332,"k":{"c":6,"d":14,"e":10,"o":6,"t":-8,"u":-8,"z":-6,"q":6,"v":-18,"w":-18,"y":-18,"k":6,"f":-8,"x":-6,".":60,",":60}},"s":{"d":"186,-408v42,0,79,10,88,18v9,22,16,54,16,81v-4,7,-19,9,-24,3v-17,-44,-47,-73,-88,-73v-39,0,-64,25,-64,61v0,41,42,63,77,83v53,30,102,64,102,135v0,117,-175,142,-251,84v-8,-19,-14,-68,-12,-101v5,-6,18,-7,23,-1v17,55,49,103,106,103v36,0,67,-21,67,-64v0,-46,-40,-71,-77,-92v-58,-33,-99,-68,-99,-123v0,-73,60,-114,136,-114","w":323},"t":{"d":"235,-30v21,0,43,-14,52,-19v8,0,12,11,10,17v-18,21,-54,46,-105,46v-86,0,-104,-48,-104,-108r0,-229v6,-41,-21,-26,-49,-29v-6,-4,-7,-15,-3,-21v63,-19,79,-42,106,-90v5,-2,17,-2,21,5v5,23,-14,64,18,64r111,0v12,9,10,36,1,42r-113,0v-17,0,-18,3,-18,24r0,187v0,58,7,111,73,111","w":307},"u":{"d":"252,-32v47,0,101,-22,101,-88r0,-164v2,-74,-13,-68,-50,-84v-7,-4,-7,-18,1,-22v40,-1,95,-8,117,-18v5,1,8,3,9,7v-5,68,-2,177,-3,255v0,105,3,119,70,104v6,5,7,25,-1,29v-39,8,-97,18,-131,37v-27,1,2,-54,-21,-62v-30,19,-79,52,-138,52v-88,0,-121,-44,-121,-125r0,-167v2,-72,-5,-75,-42,-90v-6,-5,-5,-19,2,-22v46,-1,85,-10,107,-18v6,1,9,3,10,7v-5,58,-2,181,-3,254v0,69,17,115,93,115","w":512},"v":{"d":"204,12v-32,-99,-83,-210,-122,-307v-17,-45,-29,-76,-80,-77v-6,-8,-5,-22,3,-25v53,4,115,4,175,0v11,8,5,31,-11,27v-31,5,-32,10,-22,40v28,82,59,161,91,223v32,-62,79,-177,94,-247v3,-16,-26,-15,-41,-18v-6,-7,-5,-21,2,-25v61,4,89,4,145,0v7,4,8,19,2,25v-53,4,-54,26,-88,89v-14,27,-76,172,-92,213v-10,25,-20,48,-33,82v-5,6,-18,5,-23,0","w":432,"k":{"q":6,".":75,",":75,"c":6,"d":6,"e":6,"o":6,"a":6}},"w":{"d":"476,-108v26,-53,79,-174,92,-248v2,-13,-29,-14,-43,-17v-8,-5,-6,-21,1,-24v53,4,92,4,144,0v8,4,9,19,3,24v-44,7,-48,13,-74,67v-27,56,-75,176,-94,227v-14,36,-26,65,-34,91v-7,5,-17,6,-24,0v-24,-73,-78,-203,-112,-276v-22,44,-52,124,-74,174v-18,43,-32,79,-40,102v-6,6,-18,5,-24,0v-36,-106,-80,-207,-122,-309v-27,-66,-34,-66,-76,-76v-5,-5,-6,-20,3,-24v58,4,120,4,176,0v8,4,9,19,2,24v-15,3,-46,6,-45,18v19,74,70,180,96,248v21,-35,51,-123,72,-169v30,-66,-4,-87,-53,-97v-7,-6,-5,-21,3,-24v59,4,117,4,178,0v9,3,9,19,3,24r-21,4v-40,8,-40,12,-26,46","w":660,"k":{".":75,",":75,"c":6,"d":6,"e":6}},"x":{"d":"177,-199r-90,-127v-17,-27,-39,-42,-76,-46v-6,-5,-5,-22,2,-25v77,4,95,4,172,0v7,4,7,20,1,25v-25,5,-48,4,-24,38r61,87v4,-3,36,-54,58,-85v23,-32,3,-34,-20,-40v-6,-6,-4,-22,4,-25v46,4,102,4,146,0v8,4,8,20,2,25v-24,5,-53,10,-65,20v-17,14,-54,60,-94,114v-12,16,-12,19,1,37r93,131v28,37,40,47,82,48v6,6,5,21,-2,24v-70,-4,-130,-4,-194,0v-8,-3,-10,-19,-3,-24v25,-2,69,0,46,-33r-74,-104v-19,22,-41,63,-60,90v-29,42,-10,43,20,47v6,5,5,21,-2,24v-50,-4,-97,-4,-147,0v-8,-4,-9,-20,-2,-24v86,-4,113,-100,162,-151v10,-10,11,-15,3,-26","w":432,"k":{"v":6,"q":6,"c":12,"d":12,"e":12,"o":6}},"y":{"d":"68,249v-29,0,-44,-18,-44,-43v1,-35,25,-40,59,-40v16,0,27,-7,33,-18v47,-88,89,-118,51,-223r-78,-214v-19,-59,-30,-77,-84,-83v-6,-5,-5,-22,3,-25v62,4,117,4,182,0v8,3,9,20,2,25v-14,4,-47,5,-47,18v21,79,59,188,90,257v22,-35,99,-209,106,-253v2,-15,-37,-19,-55,-22v-7,-5,-6,-22,2,-25v61,4,105,4,164,0v8,4,9,19,2,25v-56,6,-63,27,-102,109r-220,465v-17,38,-41,47,-64,47","w":438,"k":{"c":12,"d":6,"e":6,"o":6,"q":6,".":85,",":85}},"z":{"d":"338,3v-75,-4,-240,-5,-307,0v-4,-2,-6,-6,-6,-10v61,-79,143,-229,202,-327v16,-28,9,-30,-9,-30v-49,2,-111,-7,-138,15v-17,13,-27,36,-36,58v-5,6,-20,4,-23,-4v2,-26,19,-93,33,-117v3,-2,9,-2,12,0v3,17,14,18,71,18r142,0v27,0,46,0,61,-3v4,2,6,5,5,10v-62,88,-135,218,-195,317v-21,36,-19,40,19,40v59,-1,114,7,152,-30v12,-11,21,-33,32,-58v7,-4,19,-2,22,6v-9,49,-24,96,-37,115","w":377,"k":{"c":6,"d":6,"e":6,"o":6}},"{":{"d":"197,-578v1,109,68,282,-56,308r0,4v123,26,57,198,56,308v0,39,10,84,84,96v5,4,5,15,0,20v-49,0,-142,-9,-142,-126v0,-66,15,-142,15,-206v0,-34,-4,-77,-87,-83v-5,-6,-5,-16,0,-22v83,-6,87,-49,87,-83v-1,-64,-15,-141,-15,-206v0,-117,93,-126,142,-126v5,5,5,15,0,20v-74,12,-84,57,-84,96","w":320,"k":{"W":-12,"V":-12,"J":-32,"T":-12,"Y":-12}},"|":{"d":"149,-750r0,1000r-59,0r0,-1000r59,0","w":239},"}":{"d":"123,42v-1,-111,-67,-282,56,-308r0,-4v-123,-26,-57,-199,-56,-308v0,-39,-10,-84,-84,-96v-5,-5,-5,-15,0,-20v49,0,142,9,142,126v-1,65,-14,142,-15,206v0,34,4,77,87,83v5,6,5,16,0,22v-83,6,-87,49,-87,83v0,64,15,141,15,206v0,117,-93,126,-142,126v-5,-5,-5,-16,0,-20v74,-12,84,-57,84,-96","w":320},"~":{"d":"424,-281v-11,29,-42,101,-102,101r-124,0v-52,0,-64,15,-89,65v-13,5,-29,1,-33,-14v12,-30,45,-101,102,-101r124,0v52,0,64,-15,89,-65v13,-5,29,-1,33,14"},"'":{"d":"118,-700v50,-1,45,18,38,63r-26,161v-8,8,-16,8,-24,0r-31,-200v0,-22,15,-24,43,-24","w":235},"`":{"d":"63,-624v0,-14,41,-42,53,-39v10,0,17,10,23,23r79,161v0,9,-15,13,-21,12r-117,-127v-12,-13,-17,-22,-17,-30","w":360}}});
;
/*!
 * The following copyright notice may not be removed under any circumstances.
 * 
 * Copyright:
 * Copyright � 2000 Adobe Systems Incorporated. All Rights Reserved. U.S. Patent
 * Des. 318,290 and 327,903.
 * 
 * Trademark:
 * Adobe Garamond is a trademark of Adobe Systems Incorporated.
 * 
 * Full name:
 * AGaramondPro-Regular
 * 
 * Designer:
 * Robert Slimbach
 * 
 * Vendor URL:
 * http://www.adobe.com/type
 * 
 * License information:
 * http://www.adobe.com/type/legal.html
 */
Cufon.registerFont({"w":500,"face":{"font-family":"Adobe Garamond Pro","font-weight":400,"font-stretch":"normal","units-per-em":"1000","panose-1":"2 2 5 2 6 5 6 2 4 3","ascent":"725","descent":"-275","x-height":"14","cap-height":"2","bbox":"-38 -750 993.681 269","underline-thickness":"50","underline-position":"-50","stemh":"40","stemv":"74","unicode-range":"U+0020-U+007E"},"glyphs":{" ":{"w":250,"k":{"W":15,"V":15}},"\u00a0":{"w":250,"k":{"T":45,"Y":15}},"!":{"d":"110,14v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55xm110,-646v32,0,51,4,49,29r-36,412v-6,6,-21,6,-26,0r-36,-412v0,-25,17,-29,49,-29","w":220},"\"":{"d":"122,-700v50,-1,45,18,38,63r-26,161v-8,8,-16,8,-24,0r-31,-200v0,-22,15,-24,43,-24xm282,-700v50,-1,45,18,38,63r-26,161v-8,8,-16,8,-24,0r-31,-200v0,-22,15,-24,43,-24","w":404},"#":{"d":"334,-403r-117,0r-44,162r118,0xm478,-403r-102,0r-44,162r99,0r-15,50r-98,0r-51,188r-39,0r50,-188r-119,0r-51,188r-38,0r50,-188r-111,0r15,-50r109,0r43,-162r-112,0r16,-50r109,0r45,-171r43,0r-46,171r116,0r45,-171r44,0r-46,171r101,0"},"$":{"d":"251,-383r27,-228v-102,1,-120,64,-120,108v0,54,42,92,93,120xm272,-285r-31,254v67,1,130,-28,130,-108v0,-45,-10,-98,-99,-146xm325,-717r-10,78v31,1,64,7,100,15v9,22,20,76,21,113v-3,7,-16,9,-22,3v-12,-30,-33,-84,-102,-100r-30,242v79,37,162,92,162,193v0,100,-79,173,-207,173r-11,94v-8,6,-26,6,-33,0r12,-96v-67,-4,-107,-23,-122,-32v-16,-19,-28,-73,-28,-131v4,-6,15,-10,22,-3v13,43,47,116,131,134r33,-267v-73,-31,-149,-87,-150,-179v0,-88,69,-157,191,-160r9,-77v5,-7,28,-7,34,0"},"%":{"d":"799,-180v0,79,-45,194,-153,194v-108,0,-153,-115,-153,-194v0,-80,45,-194,153,-194v108,0,153,114,153,194xm727,-180v0,-52,-8,-166,-80,-166v-71,0,-82,112,-82,166v0,54,11,166,82,166v72,0,80,-114,80,-166xm351,-446v0,79,-45,194,-153,194v-108,0,-153,-115,-153,-194v0,-80,45,-194,153,-194v108,0,153,114,153,194xm279,-446v0,-52,-8,-166,-80,-166v-71,0,-82,112,-82,166v0,54,11,166,82,166v72,0,80,-114,80,-166xm636,-635r-407,675v-16,0,-30,-9,-30,-22r404,-670v10,-1,24,5,33,17","w":844},"&":{"d":"507,-205v29,-40,107,-163,126,-230v-1,-15,-41,-16,-60,-19v-6,-6,-6,-19,2,-24r82,0v75,0,114,-2,141,-4v7,5,7,18,2,24v-87,4,-120,51,-159,116v-32,54,-74,109,-116,167r33,50v53,81,104,90,145,90v31,0,67,-12,90,-36v8,1,14,9,11,18v-54,77,-195,87,-266,38v-16,-11,-50,-73,-61,-92v-57,64,-129,121,-218,121v-141,0,-196,-101,-196,-180v0,-104,73,-165,162,-218v-36,-25,-77,-64,-77,-121v0,-79,68,-135,140,-135v76,0,129,50,129,119v0,66,-52,92,-101,120v55,34,124,98,191,196xm251,-363v-50,25,-113,86,-113,167v0,89,61,151,149,151v67,0,127,-39,172,-92v-48,-84,-128,-173,-208,-226xm284,-611v-44,0,-72,41,-72,82v0,42,28,78,82,109v34,-23,60,-56,60,-104v0,-44,-22,-87,-70,-87","w":818},"(":{"d":"146,-270v0,180,55,314,154,418v0,10,-4,14,-14,15v-123,-81,-206,-246,-206,-433v0,-187,83,-352,206,-433v10,1,14,5,14,15v-99,104,-154,238,-154,418","w":320,"k":{"W":-12,"V":-12,"J":-32,"T":-12,"Y":-12}},")":{"d":"174,-270v0,-180,-55,-314,-154,-418v0,-10,4,-14,14,-15v123,81,206,246,206,433v0,187,-83,352,-206,433v-10,-1,-14,-5,-14,-15v99,-104,154,-238,154,-418","w":320},"*":{"d":"210,-676r10,130r125,-63v9,1,15,12,12,21r-115,80r114,79v3,9,-2,21,-12,22r-124,-65r-10,130v-6,5,-18,5,-24,0r-11,-130r-124,65v-9,-1,-15,-13,-12,-22r114,-79r-115,-80v-3,-9,3,-20,12,-21r125,63r11,-130v6,-5,17,-5,24,0","w":394},"+":{"d":"280,0r-59,0r0,-181r-163,0r0,-49r163,0r0,-164r59,0r0,164r163,0r0,49r-163,0r0,181"},",":{"d":"138,10v0,-64,-73,-39,-74,-87v0,-23,17,-45,49,-45v35,0,79,32,79,98v0,79,-60,124,-130,145v-9,-3,-12,-19,-6,-24v31,-10,82,-35,82,-87","w":250},"-":{"d":"40,-225r233,-22v18,11,19,41,7,57r-233,22v-18,-15,-19,-40,-7,-57","w":320,"k":{"W":70}},"\u00ad":{"d":"40,-225r233,-22v18,11,19,41,7,57r-233,22v-18,-15,-19,-40,-7,-57","w":320},".":{"d":"125,14v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55","w":250},"\/":{"d":"297,-686r-209,702r-58,0r209,-702r58,0","w":327},"0":{"d":"250,-640v146,0,207,176,207,327v0,157,-61,327,-207,327v-146,0,-207,-170,-207,-327v0,-151,61,-327,207,-327xm250,-610v-110,0,-118,203,-118,297v0,94,8,297,118,297v110,0,118,-203,118,-297v0,-94,-8,-297,-118,-297"},"1":{"d":"220,-122r0,-340v0,-68,-3,-90,-48,-94r-48,-4v-8,-4,-9,-18,-1,-22v84,-15,139,-41,165,-58v4,0,8,2,10,5v-2,18,-4,68,-4,125r0,388v-5,103,16,96,85,100v6,6,5,21,-2,24v-39,-2,-84,-2,-119,-2v-36,0,-81,0,-120,2v-7,-3,-8,-18,-2,-24v67,-4,84,3,84,-100"},"2":{"d":"192,-62v73,0,182,11,218,-22v19,-17,26,-30,32,-41v7,-3,18,0,22,6v-13,50,-31,102,-46,122r-274,-3v-56,0,-89,1,-111,3v-8,-4,-10,-20,4,-24v46,-30,79,-64,153,-144v66,-71,143,-168,143,-275v0,-91,-44,-146,-125,-146v-60,0,-103,41,-130,100v-9,5,-24,0,-24,-13v32,-88,111,-141,201,-141v100,0,165,67,165,163v0,62,-29,130,-107,220r-61,70v-82,94,-91,110,-91,116v0,7,5,9,31,9"},"3":{"d":"80,-45v49,0,45,66,101,66v65,0,179,-85,179,-196v0,-88,-42,-163,-129,-163v-49,0,-85,24,-108,49v-16,-2,-28,-16,-22,-35v38,-17,120,-54,160,-87v22,-18,53,-48,53,-96v0,-53,-37,-86,-82,-86v-59,0,-104,37,-135,88v-10,2,-21,-4,-21,-14v31,-70,104,-121,181,-121v82,0,139,44,139,115v0,41,-16,80,-107,128v-5,10,0,14,12,15v76,6,145,59,145,154v0,72,-35,144,-97,198v-74,65,-160,86,-209,86v-45,0,-99,-15,-99,-62v0,-20,15,-39,39,-39"},"4":{"d":"273,-189r-239,0v-9,0,-14,-7,-14,-17v0,-12,4,-18,18,-38r321,-448v10,-14,19,-15,40,-14v18,0,18,9,10,19r-318,440v-12,16,2,16,28,16r155,0v24,0,28,-3,28,-26r0,-184v-1,-27,38,-34,60,-37v14,-2,14,11,14,31r0,193v-4,42,54,16,84,23v9,7,9,33,0,42v-30,5,-84,-16,-84,22r0,45v-2,96,9,94,69,100v6,6,5,21,-2,24v-83,-4,-144,-4,-229,0v-7,-3,-8,-18,-2,-24v70,-4,98,4,90,-100v-2,-31,16,-67,-29,-67"},"5":{"d":"78,-415r53,-178v5,-16,10,-25,26,-27r241,-34v6,-1,9,5,9,9v-7,22,-22,47,-36,62r-189,27v-46,2,-36,55,-52,87v-10,20,18,18,35,20v125,12,265,67,265,219v0,173,-199,293,-358,298v-11,-4,-12,-24,-3,-31v155,-9,287,-104,287,-241v0,-142,-151,-178,-269,-180v-15,0,-16,-8,-9,-31"},"6":{"d":"40,-236v0,-85,28,-209,134,-313v104,-102,226,-134,287,-142v7,3,9,16,4,21v-64,22,-150,51,-222,128v-85,90,-114,200,-114,303v0,116,40,223,139,223v75,0,113,-68,113,-153v0,-102,-63,-196,-169,-159v-6,-3,-7,-10,-3,-16v29,-18,59,-30,98,-30v96,0,159,77,159,179v0,117,-86,209,-211,209v-146,0,-215,-114,-215,-250"},"7":{"d":"357,-564r-200,0v-56,0,-78,3,-107,51v-7,3,-17,1,-21,-8v10,-27,25,-70,34,-108v93,5,317,5,409,0v4,1,7,5,7,9v-119,201,-237,467,-349,684v-16,2,-31,-6,-34,-19r283,-572v15,-30,10,-37,-22,-37"},"8":{"d":"263,-640v89,0,166,50,166,146v0,82,-68,119,-118,152v69,39,132,91,132,186v0,106,-83,170,-200,170v-89,0,-185,-45,-185,-167v0,-75,69,-126,135,-168v-58,-32,-113,-80,-114,-157v-1,-102,82,-162,184,-162xm253,-16v71,0,116,-48,116,-121v0,-79,-75,-134,-148,-167v-66,45,-91,99,-91,142v0,84,49,146,123,146xm256,-610v-69,0,-105,53,-105,111v0,69,68,111,131,142v46,-33,80,-78,80,-135v0,-61,-42,-118,-106,-118"},"9":{"d":"239,-610v-64,0,-112,49,-112,138v-1,94,54,193,159,164v4,2,5,8,3,12v-97,71,-242,3,-242,-140v0,-120,91,-204,205,-204v125,0,199,108,199,255v0,109,-45,239,-147,330v-74,66,-171,103,-238,115v-8,-3,-11,-18,-6,-23v60,-18,110,-42,148,-70v92,-68,154,-171,154,-360v0,-91,-26,-217,-123,-217"},":":{"d":"125,-280v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55xm125,14v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55","w":250},";":{"d":"138,10v0,-64,-73,-39,-74,-87v0,-23,17,-45,49,-45v35,0,79,32,79,98v0,79,-60,124,-130,145v-9,-3,-12,-19,-6,-24v31,-10,82,-35,82,-87xm125,-280v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55","w":250},"<":{"d":"56,-228r389,-185r0,58r-317,148r0,2r317,149r0,57r-389,-184r0,-45"},"=":{"d":"443,-305r0,49r-385,0r0,-49r385,0xm443,-155r0,49r-385,0r0,-49r385,0"},">":{"d":"55,-413r389,185r0,45r-389,184r0,-57r317,-149r0,-2r-317,-148r0,-58"},"?":{"d":"235,-473v1,-68,-62,-99,-138,-93v-9,0,-33,-16,-33,-42v0,-20,14,-38,40,-38v80,0,172,98,172,204v0,107,-59,98,-117,132v-43,26,-37,54,-25,102v-4,8,-15,10,-21,5v-14,-23,-36,-69,-36,-99v0,-63,62,-74,103,-98v39,-23,55,-47,55,-73xm126,14v-29,0,-56,-21,-56,-55v0,-34,24,-59,56,-59v34,0,56,27,56,59v0,32,-22,55,-56,55","w":321},"@":{"d":"452,-325v-91,0,-167,161,-168,231v0,9,5,13,12,13v6,0,32,-9,80,-52v49,-43,107,-132,119,-163v-5,-15,-21,-29,-43,-29xm518,-338r25,-51v16,-6,39,2,48,12v-13,30,-101,234,-117,277v-9,23,-3,33,23,32v106,-4,195,-78,195,-204v0,-141,-114,-230,-261,-230v-198,0,-317,164,-317,348v0,118,69,275,298,275v72,0,122,-21,155,-35v7,3,11,11,8,19v-60,38,-137,51,-193,51v-165,0,-337,-98,-337,-309v0,-235,189,-383,395,-383v191,0,295,126,295,260v0,128,-97,241,-259,241v-66,0,-72,-15,-72,-22v3,-26,24,-68,33,-94r-2,-1v-60,62,-148,117,-189,117v-28,0,-41,-22,-41,-45v0,-18,17,-82,76,-154v48,-59,120,-126,200,-126v13,0,32,8,37,22","w":765},"A":{"d":"464,-43v-9,-60,-40,-131,-57,-189v-7,-22,-11,-25,-41,-25r-151,0v-25,0,-31,3,-38,24r-35,105v-15,45,-22,72,-22,83v-5,22,40,20,62,23v7,5,7,20,-2,24v-46,-4,-146,-4,-200,0v-6,-4,-8,-19,-2,-24v48,-4,73,-3,93,-52v19,-46,46,-111,88,-223r102,-270v14,-36,20,-55,17,-71v24,-7,41,-25,52,-38v6,0,13,2,15,9r181,536v36,100,41,102,112,109v7,5,5,20,0,24v-69,-4,-175,-3,-236,0v-8,-4,-9,-19,-2,-24v22,-2,66,-4,64,-21xm296,-554v-27,60,-61,164,-87,233v-8,23,-7,24,21,24r123,0v29,0,31,-4,22,-31r-77,-226r-2,0","w":623,"k":{"c":12,"C":12,"d":12,"e":6,"G":12,"o":6,"O":12,"t":6,"T":70,"u":18,"U":20,"Y":62,"z":-6,"Q":12,"V":80,"W":100,"b":12,"p":12,"q":6,"v":42,"w":42}},"B":{"d":"263,-663v128,-1,244,26,244,153v0,72,-42,109,-102,137v0,10,8,13,19,15v54,10,134,60,134,167v0,116,-86,193,-266,193v-29,0,-78,-2,-120,-2v-44,0,-78,1,-115,2v-6,-3,-8,-18,-2,-24v66,-10,73,-11,73,-128r0,-380v4,-93,-16,-96,-81,-100v-8,-6,-7,-21,1,-25v54,-5,122,-8,215,-8xm210,-313v0,89,-17,198,21,257v16,25,49,28,79,28v89,0,157,-40,157,-140v0,-79,-40,-171,-199,-171v-54,0,-58,6,-58,26xm270,-632v-37,0,-60,-5,-60,32r0,203v0,28,1,30,52,28v102,-4,157,-34,157,-128v0,-98,-73,-135,-149,-135","w":605,"k":{"h":12,"b":6,"W":32,"V":20,"e":6,"T":10,"u":12,"U":10,"r":12,"y":18,"a":12,"i":12,"k":12,"l":12}},"C":{"d":"435,16v-214,2,-390,-119,-391,-342v0,-215,178,-350,396,-350v66,0,147,19,205,28v1,23,6,86,14,147v-5,7,-20,8,-27,2v-15,-68,-60,-146,-205,-146v-153,0,-283,97,-283,304v0,210,133,326,297,326v129,0,186,-84,209,-143v7,-5,22,-3,26,4v-7,52,-33,118,-48,137v-52,15,-100,32,-193,33","w":696,"k":{"u":18,"z":18,"r":6,"y":24}},"D":{"d":"128,-150r0,-380v2,-83,-11,-95,-69,-100v-8,-5,-9,-21,1,-25v161,-6,342,-25,471,22v135,49,203,163,203,295v0,127,-62,234,-175,298v-139,78,-333,29,-512,43v-6,-3,-8,-19,-2,-25v73,-9,83,-8,83,-128xm210,-173v-3,116,30,150,143,150v194,0,281,-127,281,-313v0,-110,-53,-296,-315,-296v-59,0,-91,8,-99,14v-8,6,-10,31,-10,76r0,369","w":780,"k":{"e":6,"o":12,"u":12,"Y":10,"V":20,"W":20,"r":12,"a":6,"i":12,"h":18}},"E":{"d":"128,-150r0,-360v2,-114,-6,-121,-77,-128v-6,-4,-4,-22,2,-25r306,3v64,0,123,0,135,-3v6,17,12,89,16,133v-4,6,-20,8,-25,2v-15,-47,-24,-82,-76,-95v-34,-8,-117,-5,-169,-6v-30,0,-30,2,-30,40r0,200v-8,49,57,21,91,28v72,-6,95,14,109,-41r8,-41v5,-6,22,-6,26,1v-4,47,-4,150,0,197v-4,7,-21,7,-26,1v-9,-31,-7,-68,-40,-77v-21,-6,-96,-5,-135,-5v-30,0,-33,1,-33,27v0,77,-14,206,19,245v11,13,33,23,121,23v77,0,106,-4,128,-15v18,-10,45,-45,71,-97v7,-5,21,-2,25,7v-7,35,-32,112,-45,139r-358,-3v-43,0,-77,1,-136,3v-6,-3,-8,-19,-2,-25v79,-6,95,-2,95,-128","w":584,"k":{"c":6,"d":6,"e":6,"o":6,"t":18,"u":18,"Y":12,"z":6,"V":12,"W":12,"b":12,"p":18,"q":6,"v":30,"w":30,"r":6,"y":42,"i":6,"h":12,"g":6,"k":12,"l":12,"n":6,"f":12,"j":12,"m":6,"x":6}},"F":{"d":"210,-589r0,211v-7,49,66,19,101,27v72,-7,94,14,110,-41r9,-40v5,-6,22,-6,26,0v-4,48,-4,150,0,198v-4,6,-19,6,-26,0v-9,-31,-10,-68,-42,-77v-25,-7,-103,-4,-145,-5v-30,0,-33,1,-33,27r0,139v-4,126,13,122,95,128v6,4,4,21,-2,24v-59,-2,-93,-2,-133,-2v-44,0,-78,0,-125,2v-6,-3,-8,-18,-2,-24v74,-8,85,-7,85,-128r0,-360v2,-115,-6,-121,-76,-128v-6,-4,-4,-22,2,-25r300,3v64,0,122,0,135,-3v0,46,1,94,3,132v-3,6,-17,9,-24,3v-8,-45,-20,-82,-70,-95v-33,-9,-108,-5,-158,-6v-30,0,-30,2,-30,40","w":538,"k":{".":140,",":140,"e":36,"o":36,"u":24,"r":36,"y":18,"a":48,"i":34,"l":18,"A":36}},"G":{"d":"453,-676v95,1,126,21,189,24v0,33,6,85,15,145v-3,8,-21,9,-27,3v-28,-108,-103,-142,-205,-142v-194,0,-279,138,-279,288v0,192,99,343,300,343v62,0,99,-13,110,-30v10,-8,10,-66,10,-97v0,-84,-3,-90,-65,-97r-44,-5v-8,-4,-7,-22,0,-25v53,3,183,5,249,0v7,4,8,19,1,25v-73,-5,-58,71,-59,142v0,40,6,64,24,71v3,3,3,9,-1,12v-66,10,-157,35,-228,35v-208,0,-402,-129,-397,-338v5,-213,156,-358,407,-354","w":747,"k":{"e":6,"o":6,"u":12,"r":24,"y":12,"a":6,"i":12,"h":12,"l":6,"n":18}},"H":{"d":"546,-323r-286,0v-48,0,-50,2,-50,32r0,141v-3,123,13,121,90,128v6,4,4,21,-2,24v-54,-2,-88,-2,-127,-2v-43,0,-77,1,-116,2v-6,-3,-8,-18,-2,-24v67,-11,75,-12,75,-128r0,-360v1,-121,-11,-124,-85,-128v-6,-4,-4,-22,2,-25v49,1,83,3,126,3v39,0,73,-1,116,-3v6,3,8,21,2,25v-72,6,-81,11,-79,128r0,115v0,31,2,32,50,32r286,0v48,0,50,-1,50,-32r0,-115v2,-117,-7,-122,-80,-128v-6,-4,-4,-22,2,-25v46,2,80,3,121,3v39,0,73,-1,118,-3v6,3,8,21,2,25v-74,6,-81,10,-81,128r0,360v-3,120,10,120,84,128v6,4,4,21,-2,24v-48,-2,-82,-2,-121,-2v-41,0,-77,0,-121,2v-6,-3,-8,-18,-2,-24v72,-11,80,-11,80,-128r0,-141v0,-30,-2,-32,-50,-32","w":806,"k":{"e":18,"o":18,"u":30,"y":24,"a":6,"i":18}},"I":{"d":"128,-152r0,-356v2,-121,-7,-126,-86,-130v-6,-4,-4,-22,2,-25v50,2,84,3,126,3v40,0,74,-1,124,-3v6,3,8,21,2,25v-78,4,-86,9,-86,130r0,356v-3,122,10,122,86,130v6,4,4,21,-2,24v-50,-2,-84,-2,-124,-2v-42,0,-76,0,-126,2v-6,-3,-8,-18,-2,-24v75,-8,86,-8,86,-130","w":338,"k":{"c":18,"d":18,"e":18,"o":18,"t":24,"u":32,"z":6,"b":6,"p":32,"v":24,"w":24,"r":18,"y":12,"a":18,"i":12,"h":6,"g":6,"k":6,"l":6,"n":12,"f":6,"j":18,"m":12,"s":18}},"J":{"d":"-2,134v40,0,100,31,119,-13v21,-48,26,-197,26,-251r0,-378v1,-126,-16,-124,-94,-130v-6,-4,-4,-22,2,-25v58,2,92,3,134,3v40,0,74,-1,114,-3v6,3,8,21,2,25v-70,8,-76,15,-76,130r0,371v10,199,-61,326,-220,338v-12,0,-43,-1,-43,-21v0,-17,15,-46,36,-46","w":345,"k":{"e":30,"o":30,"u":36,"y":12,"a":30,"i":24}},"K":{"d":"128,-150r0,-360v2,-120,-8,-123,-85,-128v-6,-4,-4,-22,2,-25v49,2,81,3,126,3v39,0,73,-1,116,-3v6,3,8,21,2,25v-72,6,-81,11,-79,128r0,115v0,19,2,38,13,38v35,0,61,-41,79,-59r108,-107v24,-27,76,-69,85,-99v0,-9,-32,-13,-44,-16v-7,-7,-6,-20,2,-25v67,4,144,4,209,0v7,5,8,19,2,25v-109,15,-143,59,-233,144r-90,85v-28,29,-31,30,-4,62v95,109,172,199,250,281v29,31,58,41,107,44v7,5,5,21,-2,24v-61,-4,-160,-4,-234,0v-7,-2,-10,-18,-4,-24v13,-3,46,-5,47,-14v-23,-44,-49,-64,-97,-117v-40,-44,-140,-174,-177,-174v-15,0,-17,13,-17,48r0,129v-3,122,10,121,85,128v6,4,4,21,-2,24v-49,-2,-83,-2,-122,-2v-41,0,-75,0,-116,2v-6,-3,-8,-17,-2,-24v68,-9,75,-10,75,-128","w":675,"k":{"C":36,"e":6,"G":36,"o":18,"O":36,"u":24,"Q":36,"v":30,"w":48,"y":24}},"L":{"d":"128,-150r0,-360v3,-120,-9,-123,-90,-128v-6,-4,-4,-22,2,-25v56,2,90,3,130,3v39,0,73,-1,123,-3v6,3,8,21,2,25v-77,5,-85,8,-85,128r0,348v0,63,3,89,21,108v11,11,30,23,109,23v85,0,106,-4,122,-13v20,-12,46,-48,66,-99v6,-5,25,-1,25,6v0,11,-28,113,-42,140r-341,-3v-42,0,-74,1,-130,3v-6,-3,-8,-19,-2,-25v78,-7,90,-4,90,-128","w":553,"k":{"C":12,"G":12,"O":12,"T":100,"u":6,"U":18,"Y":70,"Q":12,"V":90,"W":110,"w":36,"y":46,"j":12,"A":-12}},"M":{"d":"275,-498r183,368r2,0v27,-47,54,-98,77,-150v47,-105,157,-287,170,-383v69,5,98,3,164,0v6,4,6,21,1,25v-70,4,-95,17,-91,97v5,106,3,291,11,435v3,50,2,77,49,81r32,3v6,6,5,20,-2,24v-41,-2,-82,-2,-118,-2v-35,0,-80,0,-121,2v-7,-5,-8,-18,-2,-24r29,-3v49,-5,49,-21,49,-85r-3,-417v-13,9,-47,90,-67,130r-89,175v-45,88,-90,182,-108,228v-4,5,-14,6,-19,0v-50,-132,-120,-267,-178,-393v-21,-46,-39,-102,-66,-144v-4,62,-7,120,-10,178v-3,61,-7,150,-7,235v0,97,18,92,83,96v7,6,6,21,-1,24v-68,-4,-142,-4,-213,0v-6,-5,-8,-19,-2,-24v55,-8,78,-3,84,-85v5,-66,11,-121,17,-220v5,-83,7,-168,11,-223v5,-70,-18,-82,-83,-88v-5,-6,-4,-21,4,-25v62,4,97,5,162,0v-4,39,21,101,52,165","w":912,"k":{"j":18,"c":24,"d":24,"e":24,"o":24,"u":36,"y":30,"a":12,"i":12,"n":6}},"N":{"d":"681,-615v-29,124,-15,299,-15,445v0,30,0,149,3,175v-2,6,-8,11,-19,11v-62,-79,-270,-311,-360,-415v-27,-31,-95,-113,-116,-135r-2,0v-11,77,-5,195,-5,283v0,47,2,177,18,207v9,17,53,19,82,22v6,8,5,19,-2,24v-45,-2,-80,-2,-117,-2v-42,0,-69,0,-104,2v-7,-5,-8,-18,-2,-24v27,-2,68,-6,71,-23v28,-139,8,-334,13,-495v0,-28,0,-49,-22,-72v-17,-18,-48,-23,-79,-26v-6,-6,-6,-21,2,-25v70,3,109,6,159,0v62,112,188,237,268,329v68,78,118,133,165,181v9,-2,6,-21,6,-42r0,-214v0,-47,-1,-177,-20,-207v-1,-11,-46,-19,-79,-22v-7,-6,-6,-22,2,-25v46,2,80,3,118,3v43,0,69,-1,103,-3v8,5,8,19,2,25v-29,3,-69,14,-70,23","w":783,"k":{"e":30,"o":30,"u":30,"y":30,"a":30,"i":24}},"O":{"d":"402,16v-222,0,-356,-159,-356,-339v0,-200,150,-353,356,-353v231,0,347,167,347,343v0,202,-154,349,-347,349xm415,-15v120,0,234,-90,234,-287v0,-165,-73,-343,-264,-343v-104,0,-239,71,-239,290v0,148,72,340,269,340","w":795,"k":{"c":6,"d":6,"e":6,"o":6,"t":6,"T":30,"u":6,"Y":30,"z":6,"V":24,"W":52,"b":6,"p":6,"q":6,"r":6,"a":6,"i":6,"h":12,"g":6,"k":12,"l":12,"n":6,"f":6,"j":12,"m":6,"s":6,"A":30,"X":30}},"P":{"d":"245,-663v160,-2,272,46,272,190v0,92,-57,138,-100,159v-44,21,-97,30,-138,30v-7,-3,-7,-18,-1,-20v108,-20,147,-81,147,-168v0,-97,-57,-160,-161,-160v-53,0,-54,4,-54,36r0,446v-5,121,13,120,93,128v6,5,4,21,-2,24v-57,-2,-90,-2,-130,-2v-42,0,-78,0,-122,2v-6,-3,-8,-18,-2,-24v72,-9,81,-9,81,-128r0,-369v4,-108,-13,-105,-83,-111v-7,-6,-6,-21,2,-25v53,-7,115,-8,198,-8","w":549,"k":{"h":6,"H":12,".":180,",":180,"e":54,"o":54,"t":18,"u":30,"r":30,"y":6,"a":42,"i":12,"l":6,"n":36,"s":36,"A":40,"E":12,"I":12}},"Q":{"d":"402,-676v231,0,347,167,347,343v0,165,-102,292,-242,334v-2,5,2,9,8,15v79,70,204,204,364,162v6,1,8,10,5,15v-168,90,-390,-68,-459,-143v-51,-57,-90,-31,-165,-61v-132,-53,-214,-169,-214,-312v0,-200,150,-353,356,-353xm415,-15v120,0,234,-90,234,-287v0,-165,-73,-343,-264,-343v-104,0,-239,71,-239,290v0,148,72,340,269,340","w":795,"k":{"X":12,"W":30,"V":18,"T":24,"u":6,"U":12,"Y":12,"a":6,"A":24}},"R":{"d":"276,-663v133,0,249,38,250,170v0,95,-75,147,-125,167v-5,6,0,16,5,24v80,129,133,209,201,266v17,15,42,26,69,28v5,2,6,10,1,14v-116,16,-170,-8,-251,-121v-26,-37,-67,-106,-98,-151v-15,-22,-31,-32,-71,-32v-45,0,-47,1,-47,22r0,126v-2,119,8,119,81,128v6,5,4,21,-2,24v-45,-2,-79,-2,-119,-2v-42,0,-78,0,-125,2v-6,-3,-8,-17,-2,-24v74,-8,85,-7,85,-128r0,-359v1,-117,-8,-114,-76,-121v-7,-5,-7,-21,1,-25v56,-5,125,-8,223,-8xm282,-328v94,0,154,-37,153,-150v0,-79,-49,-154,-160,-154v-62,0,-65,4,-65,32r0,234v2,55,-4,38,72,38","w":645,"k":{"C":36,"e":12,"G":36,"o":12,"O":36,"T":45,"u":12,"U":25,"Y":45,"Q":36,"V":70,"W":75,"y":30}},"S":{"d":"281,-676v44,0,90,15,128,19v9,24,18,80,18,122v-4,6,-20,9,-25,3v-13,-48,-40,-113,-136,-113v-98,0,-119,65,-119,111v0,100,100,130,165,173v62,41,125,97,125,192v0,110,-83,185,-213,185v-84,0,-137,-26,-156,-37v-12,-22,-25,-93,-27,-140v5,-7,20,-9,24,-3v15,51,56,149,172,149v84,0,125,-55,125,-115v0,-44,-10,-91,-82,-140v-84,-57,-203,-100,-203,-234v0,-95,74,-172,204,-172","w":489,"k":{"e":12,"o":12,"t":24,"u":24,"p":24,"q":12,"v":30,"w":30,"r":12,"y":12,"a":12,"i":12,"h":12,"k":12,"l":12,"n":24,"j":18,"m":24}},"T":{"d":"368,-595r0,445v-4,125,12,122,92,128v6,4,4,21,-2,24v-57,-2,-91,-2,-131,-2v-40,0,-75,0,-139,2v-6,-3,-8,-19,0,-24v81,-6,98,-1,98,-128r0,-447v11,-53,-50,-25,-85,-32v-43,0,-98,2,-123,25v-24,22,-34,44,-45,69v-8,6,-22,1,-26,-7v16,-45,31,-109,38,-149v3,-2,16,-3,19,0v6,32,39,31,85,31r405,0v54,0,63,-2,78,-28v5,-2,16,-1,18,3v-11,41,-18,122,-15,152v-4,8,-21,8,-26,2v-3,-25,-11,-61,-25,-73v-31,-26,-119,-26,-187,-25v-30,0,-29,1,-29,34","w":660,"k":{"C":24,"e":120,"G":24,"o":120,"O":32,"T":-12,"u":130,"Y":-12,"z":70,"Q":32,"W":-6,"w":130,"r":130,"y":110,"a":110,"i":40,"m":130,"s":100,"A":65,".":120,"X":-18,",":120,":":80,";":80,"S":12,"}":-12,"]":-12,"\u00ad":100,")":-12}},"U":{"d":"385,16v-174,-1,-265,-86,-265,-291r0,-236v2,-119,-8,-124,-85,-128v-6,-4,-4,-21,2,-24v50,2,84,3,124,3v41,0,74,-1,123,-3v6,3,8,20,2,24v-76,5,-84,9,-84,128r0,216v0,161,50,267,203,267v185,0,200,-181,200,-381v0,-62,-3,-177,-19,-207v-9,-17,-45,-22,-75,-23v-6,-7,-4,-20,2,-24v71,4,151,4,219,0v8,5,7,18,2,24v-30,1,-69,7,-74,26v-43,170,33,437,-84,556v-45,46,-117,73,-191,73","w":746,"k":{"c":24,"d":30,"t":30,"z":18,"b":6,"p":30,"v":12,"r":36,"y":12,"a":12,"i":18,"g":18,"k":6,"l":6,"n":36,"f":12,"m":36,"x":12,"s":30,"A":12}},"V":{"d":"322,13v-62,-197,-149,-379,-222,-567v-25,-64,-40,-81,-99,-84v-7,-6,-6,-20,2,-25v63,4,157,4,225,0v7,4,9,19,2,25v-27,3,-69,11,-62,21v39,123,135,353,192,485r189,-446v25,-53,4,-51,-54,-60v-7,-6,-5,-21,2,-25v58,4,138,4,201,0v7,4,7,20,2,25v-41,1,-65,7,-82,37v-77,135,-214,455,-270,614v-7,5,-17,3,-26,0","w":676,"k":{"}":-12,"]":-24,"Q":24,";":45,":":18,".":120,"-":60,",":120,")":-12,"C":24,"e":100,"G":24,"o":100,"O":24,"u":88,"r":78,"y":70,"a":88,"i":10,"A":75}},"W":{"d":"94,-565v-21,-50,-37,-75,-97,-74v-6,-6,-5,-20,3,-24v69,4,142,4,217,0v9,3,8,17,2,24v-20,1,-60,5,-56,19v45,144,160,360,225,507r77,-162v15,-36,23,-35,8,-67r-110,-240v-17,-39,-39,-57,-88,-57v-7,-8,-6,-20,2,-24v52,4,173,4,217,0v7,3,8,16,2,24v-43,4,-74,-4,-47,57r77,174v19,-39,37,-77,59,-131v44,-106,28,-96,-31,-100v-7,-10,-5,-21,2,-24v50,4,140,4,190,0v7,3,8,16,2,24v-68,-5,-84,39,-111,91v-26,49,-70,134,-87,183v29,88,75,168,109,252r90,-211r80,-199v13,-36,30,-79,30,-91v0,-24,-37,-24,-62,-25v-8,-9,-7,-20,1,-24v66,4,133,4,189,0v8,3,9,16,2,24v-81,6,-72,24,-138,176r-73,169v-44,102,-84,200,-125,307v-6,4,-19,4,-26,0v-32,-89,-90,-214,-121,-284v-36,73,-96,201,-125,284v-7,4,-19,4,-27,0v-17,-42,-56,-132,-69,-161","w":960,"k":{"}":-12,"m":94,"]":-24,"Q":18,";":60,":":60,".":120,"-":40,",":120,")":-12,"C":18,"d":78,"e":78,"G":18,"o":78,"O":18,"t":76,"T":-12,"u":84,"r":82,"y":70,"a":88,"i":10,"A":75}},"X":{"d":"334,-385v35,-50,129,-182,148,-237v-1,-10,-41,-15,-59,-16v-6,-7,-6,-21,2,-25v62,4,141,4,202,0v7,5,8,19,2,25v-61,1,-79,25,-131,87v-41,49,-84,109,-130,177v-16,23,-17,17,3,45r169,238v46,60,61,66,126,69v7,6,6,19,-1,24v-46,-2,-85,-2,-126,-2v-45,0,-93,0,-129,2v-6,-6,-7,-18,-2,-24v18,-2,63,-3,63,-16v0,-6,-2,-16,-22,-44v-43,-61,-99,-144,-143,-201v-27,34,-102,142,-141,213v-15,27,-26,40,16,45r27,3v7,6,5,19,-1,24v-71,-4,-142,-4,-211,0v-9,-4,-10,-18,-3,-24v72,-1,109,-45,144,-92v12,-16,82,-103,136,-184v15,-23,13,-20,-3,-43r-150,-215v-42,-60,-58,-72,-119,-82v-5,-6,-4,-22,2,-25v47,2,80,3,116,3v39,0,84,-1,113,-3v7,3,8,18,2,25v-19,1,-58,3,-58,15v0,9,10,26,30,55v33,48,92,138,128,183","w":643,"k":{"Q":24,"C":24,"e":6,"G":24,"O":24,"u":24,"y":12}},"Y":{"d":"383,-366v-55,86,-45,108,-47,214v-2,131,16,124,98,130v6,6,4,21,-2,24v-62,-2,-96,-2,-136,-2v-42,0,-78,0,-131,2v-6,-3,-8,-18,-2,-24v79,-7,94,-4,91,-130v-2,-81,3,-108,-32,-175r-118,-230v-44,-80,-48,-69,-107,-81v-6,-7,-5,-21,2,-25v52,4,173,4,223,0v13,6,7,31,-10,27v-35,5,-42,10,-42,18v37,105,96,206,144,299v31,-53,64,-111,94,-169v28,-52,65,-120,65,-132v2,-8,-39,-17,-60,-18v-7,-7,-6,-21,2,-25v69,4,121,4,189,0v8,5,9,19,2,25v-87,6,-102,62,-160,163","w":574,"k":{"C":18,"d":90,"e":90,"G":18,"o":94,"O":18,"t":52,"T":-12,"u":74,"Y":-12,"Q":18,"V":-12,"W":-12,"q":88,"v":50,"a":68,"l":-6,".":80,"X":-12,",":80,":":60,";":37,"}":-12,"]":-24,"\u00ad":74,")":-12}},"Z":{"d":"27,-9v109,-121,225,-303,325,-443v64,-91,85,-109,109,-169v0,-5,-10,-8,-36,-8v-98,3,-260,-13,-298,33v-20,24,-36,51,-44,68v-9,4,-21,1,-25,-7v12,-35,29,-95,41,-150v4,-3,12,-3,17,0v3,24,18,25,99,25r264,0v72,0,104,-1,115,-3v4,1,6,5,6,9v-89,94,-238,324,-329,452v-11,16,-99,140,-99,155v0,10,10,13,28,14v74,2,273,13,333,-21v29,-16,53,-53,70,-92v9,-3,22,-2,25,7v-9,46,-29,117,-45,142r-439,-3v-53,0,-89,2,-112,3v-4,-2,-5,-7,-5,-12","w":641,"k":{"C":30,"e":6,"G":30,"o":12,"O":30,"u":6,"Q":30,"w":36,"y":36,"a":6,"i":12,"A":-12}},"[":{"d":"117,150r0,-836r179,3v7,4,7,15,1,22v-37,7,-89,1,-114,20v-12,10,-13,82,-13,156r0,434v0,74,1,146,13,156v24,20,78,12,114,20v6,7,6,18,-1,22","w":320,"k":{"W":-24,"V":-12,"J":-32,"T":-12,"Y":-12}},"\\":{"d":"88,-686r191,702r-58,0r-191,-702r58,0","w":309},"]":{"d":"203,-686r0,836r-179,-3v-7,-4,-7,-15,-1,-22v37,-7,89,-1,114,-20v12,-10,13,-82,13,-156r0,-434v0,-74,-1,-146,-13,-156v-24,-20,-78,-12,-114,-20v-6,-7,-6,-18,1,-22","w":320},"^":{"d":"450,-244r-62,0r-138,-311r-138,311r-63,0r177,-382r48,0"},"_":{"d":"500,75r0,50r-500,0r0,-50r500,0"},"a":{"d":"136,14v-60,0,-100,-38,-101,-93v0,-39,19,-62,83,-85r119,-43v28,-8,16,-46,18,-75v3,-37,-11,-87,-73,-87v-28,0,-64,14,-64,45v0,43,-41,50,-67,53v-9,0,-14,-5,-14,-14v0,-52,113,-123,186,-123v72,0,107,42,104,117r-6,174v-2,46,7,87,46,87v21,5,41,-30,45,0v0,5,-35,44,-84,44v-24,0,-48,-9,-65,-31v-4,-6,-10,-14,-16,-14v-24,6,-72,45,-111,45xm112,-91v0,17,12,60,63,60v40,0,72,-22,74,-72r2,-59v-5,-27,-25,-10,-51,-3v-60,17,-88,35,-88,74","w":404},"b":{"d":"108,-12v-12,-2,-23,27,-35,10r2,-562v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v23,-9,71,-22,109,-42v4,0,8,2,10,6v-6,94,-4,211,-4,313v0,17,4,21,10,21v26,-15,66,-33,115,-33v114,0,191,86,191,196v0,133,-96,226,-230,226v-55,0,-94,-19,-127,-26xm229,-365v-58,0,-80,-3,-80,73r0,156v-1,70,34,120,101,120v94,0,131,-80,131,-166v0,-103,-53,-183,-152,-183","k":{"w":6,"v":6,".":16,",":16,"y":6}},"c":{"d":"398,-88v-88,179,-362,109,-362,-97v0,-108,76,-223,242,-223v23,0,63,4,89,17v27,14,9,106,-21,70v-78,-93,-238,-54,-238,108v0,78,54,171,161,171v58,0,88,-26,111,-60v8,-2,17,5,18,14","w":400},"d":{"d":"424,-585r0,439v-1,110,4,132,72,106v7,2,8,27,-1,32v-42,6,-104,21,-132,32v-22,3,-6,-46,-13,-56r-4,0v-44,30,-78,46,-126,46v-111,0,-185,-87,-185,-192v0,-162,148,-260,309,-221v5,0,6,-6,6,-14r0,-151v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v25,-8,72,-23,108,-42v4,0,8,2,10,6v-1,30,-3,80,-3,124xm266,-34v39,0,84,-10,84,-65r0,-197v0,-20,-4,-29,-11,-39v-15,-21,-45,-42,-84,-42v-91,0,-138,79,-138,166v0,91,51,177,149,177","w":509},"e":{"d":"378,-289v0,43,-1,33,-63,33r-185,0v-26,0,-27,2,-27,27v0,101,59,187,166,187v33,0,61,-10,92,-49v10,-3,17,3,18,12v-33,68,-105,93,-157,93v-117,0,-187,-91,-187,-196v0,-123,81,-226,201,-226v86,0,142,59,142,119xm121,-286v46,0,177,1,175,-10v17,-36,-5,-82,-71,-82v-59,0,-110,54,-111,87v0,2,0,5,7,5","w":396,"k":{"z":-4,"w":-6}},"f":{"d":"262,-675v-93,0,-94,156,-93,253v0,27,0,28,17,28r114,0v11,10,4,38,-7,42r-107,0v-17,0,-17,2,-17,29r0,211v-4,95,14,85,74,90v7,5,5,21,-2,24v-45,-2,-74,-2,-109,-2v-36,0,-66,0,-96,2v-7,-3,-9,-19,-2,-24v53,-7,61,-3,61,-90r0,-211v7,-44,-26,-25,-55,-29v-6,-4,-7,-13,-3,-19v19,-13,57,-14,58,-41v7,-152,46,-303,211,-303v53,0,98,24,98,60v1,30,-43,49,-62,24v-18,-23,-47,-44,-80,-44","w":290,"k":{"'":-78,"}":-110,"]":-116,".":16,",":16,")":-110,"\"":-78}},"g":{"d":"208,-408v63,0,110,36,157,36r81,0v11,5,9,41,-6,41r-72,0v2,17,2,34,2,51v0,69,-41,159,-168,159v-18,0,-32,-2,-44,-3v-12,7,-44,26,-44,55v0,58,118,39,189,39v56,0,140,10,140,110v0,109,-119,189,-249,189v-123,0,-166,-66,-166,-116v0,-16,4,-29,11,-37v17,-18,45,-44,68,-67v9,-9,17,-18,8,-24v-39,-10,-73,-44,-73,-79v16,-27,67,-44,78,-82v-34,-18,-75,-57,-75,-121v0,-86,74,-151,163,-151xm271,32v-98,0,-168,11,-168,95v0,54,43,99,134,99v89,0,148,-50,148,-107v0,-61,-42,-87,-114,-87xm217,-150v54,0,82,-43,82,-104v0,-68,-28,-125,-89,-125v-48,0,-84,44,-84,112v0,71,43,117,91,117","w":446,"k":{"f":-6}},"h":{"d":"88,-112r0,-452v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v25,-8,72,-23,108,-42v4,0,8,2,10,6r-3,334v0,9,1,17,4,19v36,-28,84,-52,135,-52v163,0,129,152,129,296v0,90,10,84,65,90v7,5,5,21,-2,24v-63,-4,-135,-4,-193,0v-7,-3,-9,-19,-2,-24v51,-7,58,-5,58,-90r0,-143v0,-64,-31,-107,-95,-107v-34,0,-60,12,-78,27v-18,15,-21,20,-21,55r0,168v-2,86,7,83,59,90v7,4,5,21,-2,24v-59,-4,-133,-4,-192,0v-7,-3,-9,-19,-2,-24v54,-6,63,-1,63,-90","w":515},"i":{"d":"98,-112r0,-175v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,80,-34,103,-49v5,0,9,2,10,6v-6,93,-3,204,-4,304v-1,86,7,83,59,90v7,5,5,21,-2,24v-56,-4,-134,-4,-188,0v-7,-3,-9,-19,-2,-24v51,-7,59,-4,59,-90xm131,-624v30,0,50,24,50,52v0,36,-25,52,-53,52v-31,0,-51,-23,-51,-50v0,-33,24,-54,54,-54","w":257},"j":{"d":"46,201v71,-70,49,-326,54,-488v1,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,80,-34,103,-49v5,0,9,2,10,6v-2,35,-4,83,-4,124v0,127,14,311,-21,398v-29,72,-83,120,-152,163v-12,-2,-18,-13,-14,-23v26,-13,44,-30,59,-45xm133,-624v30,0,50,24,50,52v0,35,-25,52,-53,52v-31,0,-51,-23,-51,-51v0,-33,24,-53,54,-53","w":253},"k":{"d":"88,-112r0,-452v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v25,-8,72,-23,108,-42v4,0,8,2,10,6v-1,30,-3,80,-3,124r0,343v0,13,1,15,18,15v38,0,83,-56,109,-83v28,-30,35,-41,35,-46v2,-10,-34,-10,-46,-13v-9,-2,-9,-22,0,-25r23,0v44,0,103,-2,159,-8v7,3,8,20,2,25v-78,1,-150,95,-186,125v-12,11,-13,16,-7,24v31,42,129,152,161,182v18,17,40,21,66,24v7,5,6,21,-2,24v-59,-4,-144,-4,-199,0v-8,-4,-9,-20,-2,-24v13,-3,44,-4,43,-13v0,-6,-4,-13,-17,-28r-107,-124v-18,-16,-50,-32,-50,20r0,55v-3,87,9,84,62,90v7,5,5,21,-2,24v-61,-4,-134,-4,-193,0v-7,-3,-9,-19,-1,-24v52,-7,60,-3,60,-90","w":482},"l":{"d":"162,-585r0,473v-3,87,9,84,62,90v7,5,5,21,-2,24v-61,-4,-134,-4,-193,0v-7,-3,-9,-19,-2,-24v53,-7,61,-3,61,-90r0,-452v2,-66,-7,-69,-41,-89v-5,-3,-5,-17,0,-20v25,-8,72,-23,108,-42v4,0,8,2,10,6v-1,30,-3,80,-3,124","w":247},"m":{"d":"535,-360v-64,0,-98,22,-98,95r0,153v-2,86,7,83,59,90v7,5,5,21,-2,24v-55,-4,-132,-4,-185,0v-7,-3,-9,-19,-2,-24v49,-8,56,-6,56,-90r0,-145v0,-67,-30,-103,-92,-103v-41,0,-70,17,-84,36v-30,40,-12,145,-15,212v-4,84,7,82,56,90v7,5,5,21,-2,24v-54,-4,-133,-4,-188,0v-7,-3,-9,-19,-2,-24v53,-6,62,-2,62,-90r0,-175v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,80,-34,100,-49v27,6,-9,63,20,65v69,-54,190,-82,239,8v9,2,16,-2,25,-8v40,-28,89,-51,136,-51v80,0,119,55,119,124r0,172v-3,87,9,84,62,90v7,5,5,21,-2,24v-59,-4,-132,-4,-188,0v-7,-3,-9,-19,-2,-24v49,-8,56,-6,56,-90r0,-145v0,-68,-30,-103,-93,-103","w":787},"n":{"d":"272,-360v-63,2,-100,27,-100,95r0,153v-2,84,7,82,56,90v7,5,5,21,-2,24v-52,-4,-133,-4,-185,0v-7,-3,-9,-19,-2,-24v51,-7,59,-4,59,-90r0,-175v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,80,-34,100,-49v27,6,-9,63,20,65v42,-26,89,-51,140,-51v150,1,117,159,117,296v0,86,8,83,60,90v7,5,5,21,-2,24v-55,-4,-134,-4,-187,0v-7,-3,-9,-19,-2,-24v50,-7,60,-6,57,-90v-5,-112,32,-252,-94,-248","w":525},"o":{"d":"248,-408v112,0,203,84,203,207v0,125,-86,215,-211,215v-115,0,-205,-81,-205,-206v0,-126,95,-216,213,-216xm365,-185v0,-85,-34,-193,-129,-193v-66,0,-115,63,-115,173v0,91,41,189,129,189v88,0,115,-92,115,-169","w":486,"k":{"v":16,"w":12,"y":12,"x":6,".":16}},"p":{"d":"85,143r0,-430v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v26,-11,73,-35,99,-54v30,3,-10,57,17,66v13,-6,76,-47,140,-47v101,0,166,82,166,183v-1,156,-127,262,-299,235v-9,2,-14,7,-14,28v0,62,-23,199,38,190r39,5v7,5,5,22,-2,25v-48,-2,-77,-3,-112,-3v-36,0,-66,1,-94,3v-7,-3,-9,-20,-2,-25v51,-7,59,-4,59,-90xm159,-101v-4,57,56,82,102,82v82,0,130,-76,130,-180v0,-80,-45,-161,-135,-161v-43,0,-74,16,-85,28v-25,48,-7,160,-12,231","w":507,"k":{"w":6,".":16,",":16}},"q":{"d":"275,-408v69,0,107,40,145,0v5,0,9,4,11,9v-18,146,-3,375,-8,542v-3,91,14,79,67,90v7,5,5,22,-2,25v-73,-4,-133,-4,-206,0v-7,-3,-9,-20,-2,-25v57,-6,69,2,69,-90r0,-130v0,-19,-1,-24,-9,-24v-26,15,-63,25,-102,25v-149,0,-203,-118,-203,-196v0,-145,111,-226,240,-226xm349,-231v3,-108,-14,-146,-99,-146v-76,0,-131,69,-131,163v0,110,64,188,158,188v44,0,60,-10,65,-16v15,-44,6,-131,7,-189","w":497},"r":{"d":"221,-345v-79,10,-42,150,-49,233v-7,94,15,85,76,90v7,5,5,21,-2,24v-47,-2,-76,-2,-111,-2v-36,0,-66,0,-92,2v-7,-3,-9,-19,-2,-24v50,-7,57,-5,57,-90r0,-175v4,-43,-9,-50,-36,-68v-4,-4,-4,-15,1,-18v23,-8,76,-34,101,-49v5,1,8,3,8,7v1,22,-4,51,3,67v35,-29,73,-60,110,-60v25,0,45,16,45,40v0,33,-27,46,-43,46v-25,-2,-44,-26,-66,-23","w":332,"k":{"c":6,"d":14,"e":10,"o":6,"t":-8,"u":-8,"z":-6,"q":6,"v":-18,"w":-18,"y":-18,"k":6,"f":-8,"x":-6,".":60,",":60}},"s":{"d":"186,-408v42,0,79,10,88,18v9,22,16,54,16,81v-4,7,-19,9,-24,3v-17,-44,-47,-73,-88,-73v-39,0,-64,25,-64,61v0,41,42,63,77,83v53,30,102,64,102,135v0,117,-175,142,-251,84v-8,-19,-14,-68,-12,-101v5,-6,18,-7,23,-1v17,55,49,103,106,103v36,0,67,-21,67,-64v0,-46,-40,-71,-77,-92v-58,-33,-99,-68,-99,-123v0,-73,60,-114,136,-114","w":323},"t":{"d":"235,-30v21,0,43,-14,52,-19v8,0,12,11,10,17v-18,21,-54,46,-105,46v-86,0,-104,-48,-104,-108r0,-229v6,-41,-21,-26,-49,-29v-6,-4,-7,-15,-3,-21v63,-19,79,-42,106,-90v5,-2,17,-2,21,5v5,23,-14,64,18,64r111,0v12,9,10,36,1,42r-113,0v-17,0,-18,3,-18,24r0,187v0,58,7,111,73,111","w":307},"u":{"d":"252,-32v47,0,101,-22,101,-88r0,-164v2,-74,-13,-68,-50,-84v-7,-4,-7,-18,1,-22v40,-1,95,-8,117,-18v5,1,8,3,9,7v-5,68,-2,177,-3,255v0,105,3,119,70,104v6,5,7,25,-1,29v-39,8,-97,18,-131,37v-27,1,2,-54,-21,-62v-30,19,-79,52,-138,52v-88,0,-121,-44,-121,-125r0,-167v2,-72,-5,-75,-42,-90v-6,-5,-5,-19,2,-22v46,-1,85,-10,107,-18v6,1,9,3,10,7v-5,58,-2,181,-3,254v0,69,17,115,93,115","w":512},"v":{"d":"204,12v-32,-99,-83,-210,-122,-307v-17,-45,-29,-76,-80,-77v-6,-8,-5,-22,3,-25v53,4,115,4,175,0v11,8,5,31,-11,27v-31,5,-32,10,-22,40v28,82,59,161,91,223v32,-62,79,-177,94,-247v3,-16,-26,-15,-41,-18v-6,-7,-5,-21,2,-25v61,4,89,4,145,0v7,4,8,19,2,25v-53,4,-54,26,-88,89v-14,27,-76,172,-92,213v-10,25,-20,48,-33,82v-5,6,-18,5,-23,0","w":432,"k":{"q":6,".":75,",":75,"c":6,"d":6,"e":6,"o":6,"a":6}},"w":{"d":"476,-108v26,-53,79,-174,92,-248v2,-13,-29,-14,-43,-17v-8,-5,-6,-21,1,-24v53,4,92,4,144,0v8,4,9,19,3,24v-44,7,-48,13,-74,67v-27,56,-75,176,-94,227v-14,36,-26,65,-34,91v-7,5,-17,6,-24,0v-24,-73,-78,-203,-112,-276v-22,44,-52,124,-74,174v-18,43,-32,79,-40,102v-6,6,-18,5,-24,0v-36,-106,-80,-207,-122,-309v-27,-66,-34,-66,-76,-76v-5,-5,-6,-20,3,-24v58,4,120,4,176,0v8,4,9,19,2,24v-15,3,-46,6,-45,18v19,74,70,180,96,248v21,-35,51,-123,72,-169v30,-66,-4,-87,-53,-97v-7,-6,-5,-21,3,-24v59,4,117,4,178,0v9,3,9,19,3,24r-21,4v-40,8,-40,12,-26,46","w":660,"k":{".":75,",":75,"c":6,"d":6,"e":6}},"x":{"d":"177,-199r-90,-127v-17,-27,-39,-42,-76,-46v-6,-5,-5,-22,2,-25v77,4,95,4,172,0v7,4,7,20,1,25v-25,5,-48,4,-24,38r61,87v4,-3,36,-54,58,-85v23,-32,3,-34,-20,-40v-6,-6,-4,-22,4,-25v46,4,102,4,146,0v8,4,8,20,2,25v-24,5,-53,10,-65,20v-17,14,-54,60,-94,114v-12,16,-12,19,1,37r93,131v28,37,40,47,82,48v6,6,5,21,-2,24v-70,-4,-130,-4,-194,0v-8,-3,-10,-19,-3,-24v25,-2,69,0,46,-33r-74,-104v-19,22,-41,63,-60,90v-29,42,-10,43,20,47v6,5,5,21,-2,24v-50,-4,-97,-4,-147,0v-8,-4,-9,-20,-2,-24v86,-4,113,-100,162,-151v10,-10,11,-15,3,-26","w":432,"k":{"v":6,"q":6,"c":12,"d":12,"e":12,"o":6}},"y":{"d":"68,249v-29,0,-44,-18,-44,-43v1,-35,25,-40,59,-40v16,0,27,-7,33,-18v47,-88,89,-118,51,-223r-78,-214v-19,-59,-30,-77,-84,-83v-6,-5,-5,-22,3,-25v62,4,117,4,182,0v8,3,9,20,2,25v-14,4,-47,5,-47,18v21,79,59,188,90,257v22,-35,99,-209,106,-253v2,-15,-37,-19,-55,-22v-7,-5,-6,-22,2,-25v61,4,105,4,164,0v8,4,9,19,2,25v-56,6,-63,27,-102,109r-220,465v-17,38,-41,47,-64,47","w":438,"k":{"c":12,"d":6,"e":6,"o":6,"q":6,".":85,",":85}},"z":{"d":"338,3v-75,-4,-240,-5,-307,0v-4,-2,-6,-6,-6,-10v61,-79,143,-229,202,-327v16,-28,9,-30,-9,-30v-49,2,-111,-7,-138,15v-17,13,-27,36,-36,58v-5,6,-20,4,-23,-4v2,-26,19,-93,33,-117v3,-2,9,-2,12,0v3,17,14,18,71,18r142,0v27,0,46,0,61,-3v4,2,6,5,5,10v-62,88,-135,218,-195,317v-21,36,-19,40,19,40v59,-1,114,7,152,-30v12,-11,21,-33,32,-58v7,-4,19,-2,22,6v-9,49,-24,96,-37,115","w":377,"k":{"c":6,"d":6,"e":6,"o":6}},"{":{"d":"197,-578v1,109,68,282,-56,308r0,4v123,26,57,198,56,308v0,39,10,84,84,96v5,4,5,15,0,20v-49,0,-142,-9,-142,-126v0,-66,15,-142,15,-206v0,-34,-4,-77,-87,-83v-5,-6,-5,-16,0,-22v83,-6,87,-49,87,-83v-1,-64,-15,-141,-15,-206v0,-117,93,-126,142,-126v5,5,5,15,0,20v-74,12,-84,57,-84,96","w":320,"k":{"W":-12,"V":-12,"J":-32,"T":-12,"Y":-12}},"|":{"d":"149,-750r0,1000r-59,0r0,-1000r59,0","w":239},"}":{"d":"123,42v-1,-111,-67,-282,56,-308r0,-4v-123,-26,-57,-199,-56,-308v0,-39,-10,-84,-84,-96v-5,-5,-5,-15,0,-20v49,0,142,9,142,126v-1,65,-14,142,-15,206v0,34,4,77,87,83v5,6,5,16,0,22v-83,6,-87,49,-87,83v0,64,15,141,15,206v0,117,-93,126,-142,126v-5,-5,-5,-16,0,-20v74,-12,84,-57,84,-96","w":320},"~":{"d":"424,-281v-11,29,-42,101,-102,101r-124,0v-52,0,-64,15,-89,65v-13,5,-29,1,-33,-14v12,-30,45,-101,102,-101r124,0v52,0,64,-15,89,-65v13,-5,29,-1,33,14"},"'":{"d":"118,-700v50,-1,45,18,38,63r-26,161v-8,8,-16,8,-24,0r-31,-200v0,-22,15,-24,43,-24","w":235},"`":{"d":"63,-624v0,-14,41,-42,53,-39v10,0,17,10,23,23r79,161v0,9,-15,13,-21,12r-117,-127v-12,-13,-17,-22,-17,-30","w":360}}});
/*!
 * The following copyright notice may not be removed under any circumstances.
 * 
 * Copyright:
 * Copyright � 2000 Adobe Systems Incorporated. All Rights Reserved. U.S. Patent
 * Des. 327,902 and 327,903.
 * 
 * Trademark:
 * Adobe Garamond is a trademark of Adobe Systems Incorporated.
 * 
 * Full name:
 * AGaramondPro-Italic
 * 
 * Designer:
 * Robert Slimbach
 * 
 * Vendor URL:
 * http://www.adobe.com/type
 * 
 * License information:
 * http://www.adobe.com/type/legal.html
 */
Cufon.registerFont({"w":500,"face":{"font-family":"Adobe Garamond Pro","font-weight":400,"font-style":"italic","font-stretch":"normal","units-per-em":"1000","panose-1":"2 2 5 2 5 5 6 9 4 3","ascent":"725","descent":"-275","x-height":"14","cap-height":"2","bbox":"-156.335 -760 1052.88 285","underline-thickness":"50","underline-position":"-50","slope":"-18.5","stemh":"37","stemv":"74","unicode-range":"U+0020-U+007E"},"glyphs":{" ":{"w":250,"k":{"W":60,"V":48}},"\u00a0":{"w":250,"k":{"T":48,"Y":48}},"!":{"d":"251,-646v39,0,48,13,41,32r-157,409v-4,8,-20,8,-26,0r89,-410v8,-27,19,-31,53,-31xm85,-95v29,0,44,22,44,45v0,71,-101,90,-101,17v0,-36,28,-62,57,-62","w":206},"\"":{"d":"276,-686v41,0,39,14,19,63r-66,161v-3,8,-19,8,-23,0r18,-161v6,-55,8,-63,52,-63xm434,-686v41,0,39,14,19,63r-66,161v-3,8,-19,8,-23,0r18,-161v6,-55,8,-63,52,-63","w":380},"#":{"d":"403,-404r-113,0r-68,162r114,0xm549,-404r-100,0r-68,162r98,0r-21,48r-95,0r-79,191r-43,0r77,-191r-114,0r-79,191r-42,0r77,-191r-109,0r21,-48r106,0r66,-162r-109,0r23,-48r106,0r70,-172r47,0r-71,172r113,0r69,-172r48,0r-71,172r100,0"},"$":{"d":"301,-386r65,-223v-65,3,-128,48,-128,120v0,49,29,81,63,103xm311,-289r-75,259v89,-2,148,-54,148,-130v0,-67,-29,-101,-73,-129xm436,-717r-23,79v52,5,103,25,121,34v2,25,-4,69,-16,108v-2,6,-19,6,-23,3v3,-36,-13,-103,-92,-115r-70,242v66,41,124,85,124,171v0,129,-110,195,-231,195r-28,94v-8,8,-32,8,-38,0r28,-96v-79,-8,-127,-40,-144,-56v-9,-23,-7,-74,5,-128v4,-5,16,-5,23,-1v0,50,32,135,126,153r81,-276v-71,-40,-108,-92,-108,-149v0,-111,93,-176,204,-180r23,-78v9,-7,29,-7,38,0"},"%":{"d":"339,-640v74,0,113,54,113,135v0,119,-93,253,-198,253v-81,0,-120,-54,-120,-133v0,-122,87,-255,205,-255xm200,-357v0,50,19,75,51,75v89,0,135,-191,135,-257v0,-43,-18,-71,-49,-71v-87,0,-137,187,-137,253xm724,-376v74,0,113,54,113,135v0,119,-93,253,-198,253v-81,0,-120,-54,-120,-133v0,-122,87,-255,205,-255xm585,-93v0,50,19,75,51,75v89,0,135,-191,135,-257v0,-43,-18,-71,-49,-71v-87,0,-137,187,-137,253xm186,14r557,-668v12,0,29,9,29,25r-559,669v-18,0,-27,-11,-27,-26","w":863},"&":{"d":"778,-361v25,-1,21,-54,12,-71v0,-12,14,-25,32,-25v47,0,44,86,18,103v-26,16,-72,15,-101,35v-11,7,-19,2,-24,-6v-5,-8,-38,-46,-69,-71v0,18,-2,66,-4,85v-3,30,-25,129,-102,216v-70,79,-170,109,-240,109v-158,0,-221,-97,-221,-200v0,-50,16,-133,96,-215v65,-68,127,-78,149,-78v44,0,54,26,63,39v44,-36,113,-106,141,-143v14,-18,36,-58,36,-84v0,-26,-23,-30,-23,-52v0,-27,21,-41,40,-41v23,0,46,14,46,53v0,52,-33,114,-126,205v-83,82,-251,209,-355,280v-4,132,98,179,177,179v83,0,185,-56,243,-163v38,-70,51,-120,54,-202v-40,-36,-125,-8,-125,47v0,35,44,89,-18,89v-21,0,-43,-13,-43,-40v0,-26,16,-73,68,-112v105,-79,152,-37,232,37v23,21,26,26,44,26xm148,-260v71,-49,130,-93,163,-122v5,-34,-3,-53,-32,-55v-14,0,-47,15,-77,54v-31,41,-47,81,-54,123","w":770},"(":{"d":"461,-689v-226,146,-424,517,-253,841v0,8,-7,12,-16,11v-77,-80,-102,-182,-102,-312v0,-167,76,-316,196,-436v65,-65,128,-100,162,-118v8,0,13,7,13,14","w":320,"k":{"J":-36}},")":{"d":"-67,149v226,-146,424,-517,253,-841v0,-8,7,-12,16,-11v77,80,102,182,102,312v0,167,-76,316,-196,436v-65,65,-128,100,-162,118v-8,0,-13,-7,-13,-14","w":320},"*":{"d":"362,-670r-20,129r136,-33v9,3,12,15,7,23r-130,52r93,103v1,9,-6,20,-16,19r-106,-92r-39,125v-7,3,-19,2,-24,-6r19,-129r-136,36v-8,-3,-11,-16,-6,-24r128,-52r-94,-104v-1,-9,8,-18,17,-17r107,89r40,-124v7,-3,18,-1,24,5","w":400},"+":{"d":"340,0r-59,0r0,-181r-163,0r0,-49r163,0r0,-164r59,0r0,164r163,0r0,49r-163,0r0,181","w":560},",":{"d":"89,-117v47,0,71,42,71,79v0,72,-85,125,-151,144v-7,-3,-10,-13,-7,-20v33,-15,92,-49,92,-104v0,-19,-12,-34,-33,-39v-33,-17,-6,-60,28,-60","w":250},"-":{"d":"67,-230r243,-22v10,13,6,45,-8,57r-243,22v-10,-13,-5,-45,8,-57","w":320},"\u00ad":{"d":"67,-230r243,-22v10,13,6,45,-8,57r-243,22v-10,-13,-5,-45,8,-57","w":320},".":{"d":"103,-95v29,0,44,22,44,45v0,71,-101,90,-101,17v0,-36,28,-62,57,-62","w":250},"\/":{"d":"357,-676r-306,692r-58,0r306,-692r58,0","w":250},"0":{"d":"391,-640v123,0,143,110,143,192v0,116,-56,267,-134,362v-47,57,-107,100,-177,100v-125,0,-149,-111,-149,-193v0,-124,54,-266,132,-358v49,-58,110,-103,185,-103xm388,-610v-44,0,-91,44,-128,108v-60,104,-109,270,-109,380v0,63,20,106,76,106v85,0,140,-138,167,-210v30,-81,63,-187,63,-272v0,-60,-14,-112,-69,-112"},"1":{"d":"447,-635v-60,161,-106,345,-160,514v-31,95,-2,92,62,99v6,5,6,20,-1,24v-51,-2,-88,-2,-129,-2v-37,0,-77,0,-122,2v-8,-5,-8,-21,0,-24v78,-8,95,-17,121,-106r100,-337v36,-107,2,-92,-64,-101v-6,-5,-4,-17,2,-20v89,-15,142,-35,178,-54v7,-1,11,1,13,5"},"2":{"d":"525,-491v0,135,-157,229,-252,304v-116,92,-134,112,-134,118v0,6,8,9,34,9r117,0v86,0,95,-1,139,-60v8,-3,17,0,21,6v-13,39,-40,97,-57,117r-291,-3v-53,0,-87,1,-105,3v-5,-3,-8,-10,-5,-16v61,-41,107,-73,205,-155v89,-74,241,-189,241,-309v0,-76,-46,-116,-106,-116v-65,0,-114,43,-150,92v-10,5,-22,-1,-21,-15v45,-77,125,-124,202,-124v104,0,162,59,162,149"},"3":{"d":"37,-44v47,0,45,64,104,64v31,0,84,-13,130,-43v72,-47,112,-122,112,-205v0,-65,-39,-111,-111,-111v-46,0,-89,26,-118,51v-16,-2,-24,-24,-13,-39v83,-37,271,-93,271,-198v0,-55,-38,-75,-76,-75v-53,0,-105,37,-149,88v-11,1,-19,-5,-18,-17v38,-59,107,-111,197,-111v84,0,128,43,128,106v0,70,-86,113,-144,138v-16,7,-8,17,8,20v58,11,113,58,113,137v0,94,-76,183,-147,227v-89,55,-172,68,-229,68v-59,0,-100,-23,-100,-59v0,-23,17,-41,42,-41"},"4":{"d":"470,-191v-37,4,-79,-12,-90,24r-19,61v-28,76,-9,80,44,84v6,5,4,21,-3,24v-40,-2,-77,-2,-108,-2v-36,0,-77,0,-118,2v-8,-3,-7,-20,1,-24v72,-3,99,-7,114,-84r17,-58v8,-26,5,-27,-21,-27r-241,0v-9,0,-10,-5,-10,-15v0,-10,9,-18,29,-38r454,-449v11,-13,23,-14,44,-13v19,0,13,11,5,19r-450,439v-17,17,-4,19,24,19r157,0v24,0,29,-3,36,-26r55,-186v4,-24,44,-37,69,-37v13,0,10,11,4,31r-57,192v-6,19,-4,26,13,26r63,0v6,12,-1,32,-12,38"},"5":{"d":"288,-558v-62,1,-53,52,-76,89v-18,29,17,23,35,26v95,17,212,50,212,197v0,121,-100,219,-203,263v-79,34,-171,48,-234,51v-14,-4,-15,-25,-5,-34v226,-2,365,-121,365,-255v0,-137,-112,-166,-229,-167v-15,0,-13,-11,-4,-31r82,-174v8,-17,18,-23,44,-27r251,-34v6,1,9,5,11,9v-9,22,-27,47,-43,59"},"6":{"d":"406,-223v0,-92,-66,-132,-143,-104v-5,-7,-8,-15,-3,-18v102,-70,232,-21,232,135v0,109,-103,224,-239,224v-123,0,-185,-76,-185,-195v0,-142,97,-295,208,-377v140,-104,282,-128,352,-135v8,2,8,18,2,22v-71,18,-166,40,-263,111v-136,100,-215,266,-215,399v0,86,31,145,109,145v94,0,145,-119,145,-207"},"7":{"d":"472,-451r-381,515v-17,3,-32,-9,-31,-21r424,-570v24,-32,13,-39,-15,-39r-191,0v-75,0,-80,6,-113,46v-5,5,-19,0,-22,-7v14,-21,38,-64,52,-102r274,3v55,0,122,0,135,-3v6,2,8,7,7,12v-41,38,-84,91,-139,166"},"8":{"d":"242,14v-105,0,-187,-55,-187,-151v0,-114,108,-152,189,-201v-45,-30,-77,-77,-77,-123v0,-99,87,-179,191,-179v100,0,160,65,160,147v0,77,-78,117,-155,155v51,36,99,91,99,159v0,121,-108,193,-220,193xm246,-16v68,0,137,-46,137,-144v0,-77,-56,-123,-112,-156v-81,47,-140,97,-140,176v0,87,57,124,115,124xm447,-504v0,-61,-34,-106,-94,-106v-61,0,-116,48,-116,123v0,59,43,93,99,129v56,-34,111,-77,111,-146"},"9":{"d":"366,-640v125,0,174,83,174,189v0,70,-33,163,-86,243v-104,154,-264,241,-442,268v-8,-3,-10,-18,-5,-23v126,-32,247,-95,330,-201v76,-96,122,-229,122,-321v0,-65,-25,-125,-95,-125v-86,0,-151,95,-151,191v0,57,20,115,87,115v18,0,29,-2,38,-4v6,3,8,13,4,19v-94,60,-214,-1,-214,-120v0,-106,102,-231,238,-231"},":":{"d":"191,-394v29,0,44,22,44,45v0,71,-101,90,-101,17v0,-36,28,-62,57,-62xm103,-95v29,0,44,22,44,45v0,71,-101,90,-101,17v0,-36,28,-62,57,-62","w":250},";":{"d":"89,-117v47,0,71,42,71,79v0,72,-85,125,-151,144v-7,-3,-10,-13,-7,-20v33,-15,92,-49,92,-104v0,-19,-12,-34,-33,-39v-33,-17,-6,-60,28,-60xm191,-394v29,0,44,22,44,45v0,71,-101,90,-101,17v0,-36,28,-62,57,-62","w":250},"<":{"d":"118,-227r389,-185r0,58r-317,148r0,2r317,149r0,57r-389,-184r0,-45","w":560},"=":{"d":"503,-305r0,49r-385,0r0,-49r385,0xm503,-155r0,49r-385,0r0,-49r385,0","w":560},">":{"d":"118,-412r389,185r0,45r-389,184r0,-57r317,-149r0,-2r-317,-148r0,-58","w":560},"?":{"d":"260,-587v-33,0,-48,16,-67,22v-9,0,-19,-10,-19,-28v0,-27,30,-53,72,-53v58,0,117,69,117,142v0,94,-96,146,-170,183v-49,24,-54,64,-37,113v-3,6,-11,8,-16,5v-18,-23,-41,-65,-31,-106v21,-91,221,-110,221,-207v0,-44,-35,-71,-70,-71xm123,-95v29,0,44,22,44,45v0,71,-101,90,-101,17v0,-36,28,-62,57,-62","w":300},"@":{"d":"517,-325v-91,0,-167,161,-168,231v0,9,5,13,12,13v6,0,32,-9,80,-52v49,-43,107,-132,119,-163v-5,-15,-21,-29,-43,-29xm583,-338r25,-51v16,-6,39,2,48,12v-13,30,-101,234,-117,277v-9,23,-3,33,23,32v106,-4,195,-78,195,-204v0,-141,-114,-230,-261,-230v-198,0,-317,164,-317,348v0,118,69,275,298,275v72,0,122,-21,155,-35v7,3,11,11,8,19v-60,38,-137,51,-193,51v-165,0,-337,-98,-337,-309v0,-235,189,-383,395,-383v191,0,295,126,295,260v0,128,-97,241,-259,241v-66,0,-72,-15,-72,-22v3,-26,24,-68,33,-94r-2,-1v-60,62,-148,117,-189,117v-28,0,-41,-22,-41,-45v0,-18,17,-82,76,-154v48,-59,120,-126,200,-126v13,0,32,8,37,22","w":770},"A":{"d":"502,-143v-10,129,4,111,76,121v6,6,3,20,-4,24v-58,-4,-171,-4,-228,0v-23,-26,12,-23,30,-27v54,-12,45,-5,50,-78r9,-134v2,-23,0,-26,-21,-26r-144,0v-30,0,-32,1,-51,31r-73,114v-35,55,-45,70,-45,75v-2,15,43,18,60,21v8,6,5,20,-2,24v-62,-4,-148,-4,-213,0v-5,-6,-4,-18,2,-24v46,-2,74,-13,99,-45v29,-36,87,-116,165,-225r176,-248v43,-60,56,-86,60,-103v34,-5,71,-33,73,-33v5,0,12,4,12,8xm293,-303r126,0v17,0,19,-2,20,-20r16,-219v1,-14,0,-26,-2,-26v-3,0,-9,6,-38,49r-139,201v-8,12,-5,15,17,15","w":596,"k":{"c":18,"C":24,"d":6,"e":12,"g":12,"G":24,"l":18,"n":18,"o":18,"O":24,"r":12,"s":-6,"t":6,"T":60,"u":18,"U":54,"Y":72,"z":-6,"Q":24,"V":60,"W":70,"b":30,"m":18,"p":18,"q":12,"v":42,"w":36}},"B":{"d":"192,-651v142,-17,415,-44,415,138v0,101,-101,148,-156,152v-16,1,-9,8,0,11v104,35,147,184,50,277v-43,41,-116,75,-240,75v-54,0,-91,-2,-133,-2v-40,0,-74,0,-103,2v-8,-5,-8,-18,-1,-24v70,-10,75,-8,110,-122r121,-393v32,-96,2,-88,-63,-90v-7,-5,-5,-19,0,-24xm259,-307r-59,192v-20,66,8,87,80,87v84,0,189,-52,189,-187v0,-85,-73,-126,-148,-126v-48,0,-52,1,-62,34xm350,-600r-62,200v-7,24,-6,29,37,29v72,0,200,-35,200,-161v0,-58,-44,-100,-126,-100v-38,0,-39,-1,-49,32","w":581,"k":{"h":6,"W":18,"V":18,"e":6,"l":6,"r":6,"T":24,"u":12,"U":16,"a":6,"i":12,"y":18,"A":-8}},"C":{"d":"102,-253v0,-243,222,-425,479,-423v78,0,139,22,164,32v-16,39,-36,97,-45,134v-4,8,-24,7,-27,-1v19,-87,-6,-134,-109,-134v-71,0,-164,22,-256,112v-50,50,-118,167,-118,299v0,142,67,219,194,219v104,0,167,-61,215,-140v10,-4,21,-2,24,7v-9,27,-31,80,-65,141v-53,2,-115,23,-176,23v-130,0,-280,-59,-280,-269","w":646,"k":{"C":12,"e":6,"G":12,"O":12,"r":18,"u":18,"z":18,"Q":12,"a":6,"i":6,"y":24}},"D":{"d":"133,-144r125,-405v30,-87,-5,-71,-57,-80v-6,-4,-7,-19,0,-24v15,-2,102,-10,186,-10v288,0,360,146,360,273v0,130,-63,238,-158,303v-113,77,-243,89,-334,89v-30,0,-66,-2,-125,-2v-41,0,-75,0,-110,2v-8,-5,-8,-18,-1,-24v75,-8,78,-5,114,-122xm347,-593r-149,479v-18,59,3,82,91,82v289,0,369,-243,369,-351v0,-95,-15,-249,-264,-249v-31,0,-37,7,-47,39","w":739,"k":{"e":6,"o":6,"Y":30,"V":42,"W":42,"a":6,"y":-12,"h":6}},"E":{"d":"132,-144r116,-372v36,-107,24,-110,-42,-122v-6,-8,-5,-21,6,-25r300,3v64,0,111,0,141,-3v-7,17,-30,97,-41,133v-3,8,-22,7,-25,-1v6,-52,9,-76,-35,-89v-29,-9,-111,-9,-165,-9v-26,0,-30,4,-39,34r-64,205v-9,28,-10,29,18,29r50,0v117,4,123,-20,154,-79v4,-7,23,-6,25,2v-16,36,-51,149,-60,194v-6,8,-21,7,-25,-1v4,-72,-11,-87,-103,-81v-41,3,-81,-16,-89,29v-12,66,-71,189,-58,245v7,14,26,21,111,21v110,0,150,-6,213,-104v8,-5,19,-3,23,6v-9,29,-43,105,-69,132r-347,-3v-41,0,-75,1,-119,3v-8,-5,-8,-19,-1,-25v85,-7,87,0,125,-122","w":566,"k":{"c":12,"d":6,"e":6,"g":24,"l":18,"n":18,"o":6,"r":18,"s":12,"t":18,"u":24,"z":18,"b":24,"m":18,"p":18,"q":6,"v":30,"w":30,"a":12,"i":18,"y":30,"k":18,"f":18,"j":12,"x":6}},"F":{"d":"133,-144r117,-372v37,-106,24,-109,-41,-122v-6,-8,-5,-21,6,-25r294,3v64,0,112,0,141,-3v-7,17,-30,97,-41,133v-3,8,-22,7,-25,-1v6,-52,9,-76,-35,-89v-28,-8,-108,-9,-161,-9v-28,0,-28,3,-38,34r-69,219v-7,21,-5,25,16,25r69,0v118,4,122,-21,154,-80v5,-6,24,-5,26,2v-15,39,-53,146,-60,195v-6,7,-19,6,-26,0v5,-69,4,-82,-102,-82v-43,0,-92,-19,-106,28r-44,144v-28,93,-32,115,19,119r39,3v6,7,3,20,-4,24v-58,-2,-93,-2,-136,-2v-40,0,-74,0,-113,2v-8,-5,-8,-18,-1,-24v81,-7,83,-2,121,-122","w":533,"k":{".":140,",":140,"e":36,"l":18,"o":30,"r":24,"u":30,"a":36,"i":12,"y":12,"A":36}},"G":{"d":"518,-90r26,-82v23,-73,24,-87,-36,-95r-39,-5v-7,-6,-6,-21,2,-25v73,4,171,4,234,0v7,4,8,20,-1,25v-53,16,-56,9,-84,100r-31,99v-11,21,17,28,4,44v-69,24,-157,45,-210,45v-177,0,-282,-97,-282,-260v0,-97,41,-217,144,-307v96,-84,209,-125,348,-125v85,0,148,22,171,32v-17,36,-37,94,-47,133v-4,8,-22,7,-25,-1v18,-92,-14,-133,-111,-133v-118,0,-196,45,-262,107v-72,67,-130,201,-130,315v0,159,98,208,198,208v70,0,110,-10,131,-75","w":703,"k":{"e":24,"l":18,"n":18,"o":18,"r":18,"u":18,"a":24,"i":12,"y":12,"h":12}},"H":{"d":"313,-363r264,0v46,0,46,-1,53,-22r41,-131v38,-110,23,-112,-47,-122v-5,-7,-4,-21,6,-25v78,4,152,4,233,0v7,4,9,19,0,25v-77,12,-83,10,-117,122r-116,372v-37,110,-26,117,46,122v6,10,3,20,-4,24v-47,-2,-79,-2,-122,-2v-42,0,-75,0,-116,2v-8,-5,-8,-17,-1,-24v81,-7,84,-2,122,-122r48,-152v8,-26,7,-27,-36,-27r-272,0v-32,0,-31,3,-38,24r-49,155v-38,112,-25,118,50,122v6,8,3,20,-4,24v-49,-2,-83,-2,-126,-2v-41,0,-75,0,-107,2v-8,-5,-8,-17,-1,-24v73,-9,77,-6,113,-122r117,-372v39,-109,22,-112,-47,-122v-6,-9,-5,-21,6,-25v76,4,155,4,232,0v7,4,9,17,0,25v-77,13,-81,10,-116,122r-41,129v-7,23,-7,24,29,24","w":761,"k":{"e":24,"o":18,"u":24,"a":24,"i":12,"y":24}},"I":{"d":"133,-144r117,-372v39,-109,22,-112,-47,-122v-6,-8,-5,-21,6,-25v81,4,151,4,236,0v7,4,9,20,0,25v-80,12,-85,9,-120,122r-117,372v-38,112,-25,118,50,122v6,7,3,20,-4,24v-49,-2,-83,-2,-126,-2v-41,0,-75,0,-115,2v-8,-5,-8,-18,-1,-24v81,-7,83,-2,121,-122","w":341,"k":{"c":24,"d":6,"g":12,"l":18,"n":24,"o":24,"r":12,"s":18,"t":18,"u":30,"z":36,"b":18,"m":24,"p":24,"v":30,"w":30,"a":24,"y":6,"h":18,"k":18,"f":18}},"J":{"d":"-77,129v29,1,56,29,85,29v27,0,47,-39,65,-87v48,-129,135,-426,187,-587v36,-110,22,-113,-48,-123v-6,-8,-5,-20,6,-24v76,4,153,4,229,0v7,4,9,19,0,24v-76,10,-76,8,-112,123r-142,457v-33,118,-106,260,-246,260v-30,0,-63,-7,-63,-29v0,-16,18,-43,39,-43","w":340,"k":{"e":36,"o":36,"u":36,"a":36,"i":30,"y":12}},"K":{"d":"133,-144r117,-372v38,-109,25,-112,-40,-122v-6,-8,-5,-21,6,-25v68,4,153,4,227,0v7,4,8,20,-3,25v-75,15,-81,12,-115,122r-42,132v-7,22,-6,28,18,28v15,0,29,-2,43,-10v52,-30,150,-130,205,-182v54,-52,65,-66,65,-74v-1,-10,-30,-13,-43,-16v-7,-6,-5,-22,4,-25v57,4,166,3,218,0v9,5,8,21,-1,25v-132,27,-116,31,-265,156r-128,117v-17,15,-19,16,-8,38r110,220v31,65,50,81,117,85v7,6,5,21,-3,24v-73,-4,-146,-4,-219,0v-4,-8,-5,-20,3,-24r26,-3v34,-4,30,-14,10,-54r-105,-206v-21,-41,-31,-45,-40,-45v-19,0,-27,9,-33,28r-49,158v-37,111,-25,117,47,122v6,7,3,20,-4,24v-46,-2,-81,-2,-124,-2v-41,0,-74,0,-114,2v-8,-5,-8,-18,-1,-24v81,-7,83,-2,121,-122","w":623,"k":{"C":6,"e":6,"G":6,"o":6,"O":6,"u":18,"Q":6,"v":8,"w":30,"y":18,"h":-6}},"L":{"d":"324,-516r-110,354v-34,110,-41,131,81,131v115,0,158,-5,219,-104v10,-3,20,-1,22,6v-11,34,-46,105,-64,132r-347,-3v-41,0,-74,1,-106,3v-8,-5,-8,-19,-1,-25v74,-9,77,-6,114,-122r117,-372v39,-109,22,-112,-47,-122v-6,-8,-5,-21,6,-25v39,1,73,3,114,3v43,0,74,-2,126,-3v7,4,9,20,0,25v-83,10,-88,7,-124,122","w":546,"k":{"C":48,"G":48,"l":12,"O":48,"T":100,"u":18,"U":36,"Y":64,"Q":48,"V":90,"W":110,"w":30,"i":12,"y":45,"j":18,"A":-12}},"M":{"d":"71,-52v15,-8,130,-310,173,-422v33,-85,42,-118,42,-134v1,-29,-50,-23,-75,-30v-6,-6,-4,-21,5,-25v47,2,79,3,110,3v22,0,46,-1,67,-3v-22,75,11,231,21,324v9,86,17,139,22,183r2,0r274,-352v76,-98,92,-137,97,-155v44,5,119,3,163,0v8,5,5,19,0,25v-108,19,-94,26,-129,151r-87,310v-21,74,-27,109,-29,125v-4,27,45,28,74,30v7,7,4,20,-4,24v-73,-4,-174,-4,-249,0v-7,-6,-7,-20,0,-24v30,-3,63,-6,81,-22v62,-121,121,-371,169,-515r-3,-1r-245,319v-57,74,-116,150,-160,225v-6,4,-15,1,-19,-4v-5,-122,-33,-398,-52,-540r-121,321v-48,129,-60,168,-60,187v0,24,50,25,73,30v4,6,2,19,-3,24v-54,-4,-163,-4,-222,0v-6,-6,-6,-21,1,-24v35,-4,65,-5,84,-30","w":874,"k":{"j":18,"e":18,"n":12,"o":18,"u":30,"a":6,"i":12,"y":24}},"N":{"d":"79,-42v42,-44,119,-339,155,-447v19,-58,33,-101,33,-119v0,-27,-44,-22,-66,-30v-8,-7,-5,-22,5,-25r164,3v-1,41,3,46,49,159r101,249v20,48,37,91,49,113v7,0,12,-12,17,-28r79,-251v30,-95,48,-166,48,-185v7,-33,-39,-29,-64,-35v-6,-9,-2,-21,6,-25v64,4,148,4,208,0v5,4,7,19,1,25v-26,5,-63,10,-75,22v-68,118,-170,480,-217,631v-6,6,-16,7,-24,6v-31,-100,-185,-467,-239,-601v-5,0,-15,33,-32,88r-78,260v-21,69,-37,149,-42,166v-12,42,35,38,72,44v6,6,4,19,-3,24v-57,-4,-157,-4,-219,0v-6,-4,-10,-17,-2,-24v29,-4,50,-3,74,-20","w":739,"k":{"e":30,"o":30,"u":30,"a":30,"i":24,"y":24}},"O":{"d":"262,-585v187,-159,491,-122,481,183v-4,111,-56,234,-135,309v-61,58,-145,109,-262,109v-170,0,-248,-133,-244,-261v3,-123,54,-250,160,-340xm502,-645v-64,0,-115,30,-150,60v-72,61,-160,204,-166,378v-3,94,35,192,167,192v49,0,105,-14,167,-80v90,-97,135,-242,139,-351v5,-127,-57,-199,-157,-199","w":730,"k":{"c":6,"d":6,"e":6,"g":6,"l":12,"n":6,"o":6,"r":6,"s":6,"t":6,"T":48,"u":6,"Y":50,"z":6,"V":36,"W":60,"b":6,"m":6,"p":6,"q":6,"a":6,"i":6,"y":-8,"h":12,"k":12,"f":6,"j":6,"x":-6,"A":24,"X":30}},"P":{"d":"360,-663v164,-1,248,48,253,173v6,134,-144,205,-270,204v-8,-3,-9,-14,-3,-21v74,-9,189,-63,189,-199v0,-87,-56,-126,-135,-126v-32,0,-34,4,-44,35r-141,453v-29,93,-32,115,19,119r36,3v6,7,3,20,-4,24v-48,-2,-83,-2,-131,-2v-41,0,-75,0,-115,2v-8,-5,-8,-18,-1,-24v81,-7,84,-2,121,-122r129,-414v31,-73,-22,-64,-74,-69v-7,-6,-4,-18,3,-24v43,-7,102,-12,168,-12","w":552,"k":{"h":6,"H":12,".":180,",":180,"e":42,"l":6,"n":18,"o":42,"r":24,"s":30,"t":12,"u":24,"a":54,"i":6,"y":6,"A":60,"E":12,"I":12}},"Q":{"d":"262,-585v187,-159,492,-122,481,183v-7,182,-128,362,-299,404v-7,4,-6,10,-1,15v74,68,197,206,357,159v6,3,7,10,5,15v-28,20,-77,28,-112,28v-132,0,-258,-88,-333,-182v-12,-15,-24,-22,-46,-23v-71,-4,-216,-65,-212,-259v3,-123,54,-250,160,-340xm502,-645v-64,0,-115,30,-150,60v-72,61,-160,204,-166,378v-3,94,35,192,167,192v49,0,105,-14,167,-80v90,-97,135,-242,139,-351v5,-127,-57,-199,-157,-199","w":730,"k":{"X":12,"W":36,"V":24,"T":48,"u":6,"U":12,"Y":36,"a":6,"A":16}},"R":{"d":"377,-663v152,-1,235,47,235,166v0,101,-96,164,-172,177v-14,2,-16,7,-12,24v13,54,32,120,50,173v23,67,34,97,99,104v7,4,2,16,0,19v-64,4,-137,14,-156,-56v-20,-75,-43,-132,-59,-194v-12,-46,-18,-50,-50,-50v-52,0,-57,3,-66,32r-39,124v-36,108,-26,116,44,122v6,7,3,20,-4,24v-42,-2,-80,-2,-120,-2v-41,0,-75,0,-106,2v-8,-5,-8,-18,-1,-24v72,-10,76,-7,112,-122r117,-374v39,-122,19,-103,-47,-112v-5,-8,-4,-21,5,-25v57,-7,114,-8,170,-8xm349,-600r-70,229v-12,41,-11,42,40,42v104,0,208,-71,211,-181v2,-78,-45,-122,-131,-122v-39,0,-42,6,-50,32","w":574,"k":{"C":2,"e":12,"G":2,"o":6,"O":-8,"T":30,"u":12,"Y":36,"Q":2,"V":24,"W":36,"a":-6,"y":12,"X":-6}},"S":{"d":"211,16v-72,0,-141,-22,-175,-70v-3,-36,5,-86,18,-116v6,-5,16,-5,21,2v7,87,48,153,148,153v64,0,146,-41,146,-141v0,-74,-47,-115,-100,-157v-52,-42,-104,-102,-104,-174v0,-109,86,-189,219,-189v65,0,123,25,145,40v-14,25,-27,81,-37,116v-7,6,-21,5,-27,-1v14,-68,-5,-124,-94,-124v-91,0,-135,59,-135,128v0,67,61,116,97,144v61,47,108,100,108,182v0,119,-91,207,-230,207","w":471,"k":{"e":12,"l":18,"n":24,"t":24,"u":18,"m":24,"p":12,"w":24,"a":6,"i":12,"y":12,"h":12,"k":24,"j":6}},"T":{"d":"306,-660r370,0v51,0,65,-3,85,-23v5,-2,15,0,17,5v-20,34,-51,115,-57,142v-4,7,-18,7,-23,0v5,-79,2,-93,-95,-93r-61,0v-32,0,-33,2,-46,44r-138,441v-40,116,-22,118,55,122v8,7,7,20,-4,24v-54,-2,-89,-2,-132,-2v-41,0,-75,0,-122,2v-8,-5,-8,-18,-1,-24v86,-6,90,2,129,-122r138,-441v34,-74,-43,-34,-87,-44v-99,0,-120,24,-168,90v-10,4,-21,-1,-23,-10v25,-40,58,-102,72,-134v4,-5,15,-5,19,0v-3,18,11,23,72,23","w":634,"k":{"C":24,"e":130,"G":24,"o":120,"O":24,"r":110,"s":100,"T":-12,"u":110,"Y":-12,"z":90,"Q":24,"W":-6,"m":130,"w":110,"a":130,"i":40,"y":110,"A":65,",":120,".":120,"X":-18,"}":24,":":80,"\u00ad":100,";":80}},"U":{"d":"159,-266r78,-250v38,-110,22,-113,-48,-123v-6,-6,-5,-20,4,-24v42,2,74,3,117,3v43,0,74,-1,123,-3v7,4,8,19,-1,24v-81,11,-85,9,-120,123r-72,231v-46,149,-33,265,130,265v195,0,239,-228,293,-389v21,-65,36,-130,40,-155v7,-44,12,-64,-38,-71r-28,-4v-6,-7,-4,-20,4,-24v49,2,74,3,107,3v33,0,69,-1,108,-3v8,5,8,19,0,24v-60,9,-84,18,-102,78r-95,299v-22,69,-55,155,-117,209v-52,46,-126,69,-189,69v-182,0,-251,-98,-194,-282","w":734,"k":{"d":30,"g":24,"l":12,"n":30,"r":30,"s":30,"t":24,"z":12,"b":6,"m":30,"p":30,"a":6,"i":24,"h":6,"k":12,"x":12,"A":30}},"V":{"d":"249,-95r-41,-429v-10,-99,-12,-109,-73,-115v-7,-8,-5,-20,5,-24v60,4,151,4,206,0v9,4,9,19,1,24v-51,8,-77,1,-70,77r35,376v3,37,6,57,10,60v97,-124,225,-332,307,-453v40,-59,-4,-53,-40,-60v-6,-6,-5,-21,3,-24v67,3,139,5,201,0v8,4,6,18,2,24v-40,4,-63,9,-88,38v-97,114,-339,475,-426,614v-6,4,-17,4,-25,0v-1,-32,-5,-84,-7,-108","w":622,"k":{"Q":6,";":45,":":18,".":120,"-":60,",":120,"C":6,"e":94,"G":6,"o":94,"O":6,"r":74,"u":64,"a":88,"i":10,"y":70,"A":75}},"W":{"d":"254,-171r-57,-394v-4,-57,-18,-67,-68,-74v-7,-6,-6,-20,3,-24v63,4,136,4,200,0v7,3,8,20,1,24v-43,4,-74,1,-65,61r54,378v9,63,11,81,16,84v10,-4,104,-139,131,-175v15,-20,20,-29,16,-56r-34,-228v-5,-53,-26,-58,-69,-64v-6,-6,-6,-21,4,-24v51,4,149,4,196,0v6,3,7,19,0,24v-40,6,-69,-3,-61,57r25,189r52,-77v46,-67,85,-129,92,-142v10,-21,-26,-24,-46,-27v-7,-5,-5,-21,4,-24v53,4,124,4,173,0v7,4,6,19,0,24v-59,12,-64,13,-112,75v-41,52,-116,159,-136,189v-13,18,-19,21,-15,46r19,129v10,65,13,80,17,84v55,-67,218,-327,287,-438v23,-37,33,-56,35,-66v1,-9,-7,-11,-26,-15r-20,-4v-6,-6,-5,-20,3,-24v58,4,127,4,175,0v6,3,7,20,0,24v-70,2,-104,71,-135,117r-155,228r-200,307v-6,4,-18,4,-24,0v-8,-96,-24,-192,-39,-284r-134,191v-22,32,-41,60,-60,93v-6,4,-17,4,-25,0v-2,-31,-14,-132,-22,-184","w":878,"k":{"m":10,"Q":12,";":60,":":60,".":120,"-":40,",":120,"C":12,"d":8,"e":86,"G":12,"o":80,"r":58,"T":-12,"u":58,"a":100,"i":10,"y":56,"A":75}},"X":{"d":"-42,-22v81,-9,93,-20,177,-110r164,-176v10,-11,14,-20,8,-34r-87,-234v-15,-47,-34,-56,-80,-62v-7,-6,-3,-22,5,-25v75,4,149,4,209,0v7,5,6,20,0,25v-39,5,-82,6,-61,52v27,60,46,134,73,203v59,-60,130,-142,185,-207v26,-30,28,-40,-3,-44r-29,-4v-6,-7,-5,-20,4,-25v53,4,137,4,191,0v8,4,7,20,-1,25v-37,5,-55,11,-82,35v-77,66,-146,143,-233,235v-17,17,-19,20,-11,41v34,90,72,180,100,252v18,48,38,51,87,53v6,4,6,20,-2,24v-68,-4,-152,-4,-220,0v-8,-4,-9,-20,-2,-24v38,-6,76,-3,57,-51v-29,-71,-52,-145,-80,-215v-57,62,-149,169,-203,235v-10,12,-12,21,7,24r40,7v6,5,5,22,-3,24v-62,-4,-145,-4,-207,0v-8,-4,-9,-19,-3,-24","w":576,"k":{"Q":24,"C":24,"e":6,"G":24,"O":24,"u":24,"y":12}},"Y":{"d":"299,-579r51,235v5,25,10,25,30,1r123,-148v40,-49,96,-117,96,-126v0,-7,-5,-13,-29,-17r-25,-4v-7,-6,-6,-20,2,-25v71,4,125,4,194,0v7,4,7,19,0,25v-30,6,-60,10,-77,27v-63,58,-148,166,-212,237v-83,92,-86,122,-122,230v-42,124,-21,116,55,122v7,5,4,21,-3,24v-56,-2,-91,-2,-134,-2v-41,0,-74,0,-119,2v-8,-5,-8,-19,-1,-24v86,-6,87,-1,126,-122v19,-60,45,-107,27,-187r-50,-217v-16,-75,-25,-84,-82,-90v-6,-7,-3,-21,3,-25v63,4,153,4,213,0v6,5,7,19,0,25v-42,6,-78,4,-66,59","w":564,"k":{"d":6,"e":84,"o":84,"t":45,"T":-12,"u":80,"Y":-12,"V":-12,"W":-12,"q":94,"v":50,"a":90,"A":30,",":80,".":80,"X":-12,":":60,"\u00ad":74,";":37}},"Z":{"d":"-18,-13v189,-172,393,-402,577,-589v18,-18,12,-27,-28,-27r-153,0v-110,0,-137,13,-197,100v-10,5,-21,0,-23,-12v21,-35,44,-89,60,-144v3,-3,9,-3,12,0v5,24,18,25,96,25r267,0v68,0,98,-2,107,-3v7,1,9,6,8,12v-110,94,-320,326,-443,450v-13,14,-135,143,-135,153v0,9,11,17,90,17v94,-1,225,6,274,-22v26,-14,59,-50,82,-89v6,-4,18,1,20,8v-11,41,-47,112,-69,137r-431,-3v-52,0,-84,1,-110,3v-5,-2,-7,-11,-4,-16","w":601,"k":{"C":30,"e":6,"G":30,"o":6,"O":30,"u":6,"Q":30,"w":36,"a":6,"i":12,"y":36,"A":-12}},"[":{"d":"40,150r249,-836r65,0v55,0,95,-1,109,3v4,5,2,16,-3,20v-39,7,-93,4,-121,21v-13,15,-38,82,-60,156r-130,436v-22,74,-40,142,-35,156v21,18,76,13,109,21v2,6,-1,16,-5,20v-23,6,-128,2,-178,3","w":320,"k":{"W":-24,"V":-18,"J":-36}},"\\":{"d":"117,-676r191,692r-57,0r-191,-692r57,0","w":340},"]":{"d":"365,-686r-249,836r-65,0v-55,0,-95,1,-109,-3v-4,-5,-2,-16,3,-20v39,-7,93,-4,121,-21v13,-15,38,-82,60,-156r130,-436v22,-74,40,-142,35,-156v-21,-18,-76,-13,-109,-21v-2,-6,1,-16,5,-20v23,-6,128,-2,178,-3","w":320},"^":{"d":"518,-244r-62,0r-138,-311r-138,311r-63,0r177,-382r48,0","w":560},"_":{"d":"500,75r0,50r-500,0r0,-50r500,0"},"a":{"d":"240,-365v57,-30,102,-47,134,-2r28,-64v17,-8,43,2,49,15v-15,37,-102,258,-134,349v-6,17,-1,23,7,23v17,0,46,-17,103,-67v7,0,14,5,14,13v-42,56,-113,112,-168,112v-23,0,-29,-19,-29,-27v0,-37,31,-93,43,-129r-3,-1v-71,88,-173,157,-220,157v-28,0,-44,-28,-44,-56v0,-23,22,-103,88,-194v48,-66,91,-106,132,-129xm299,-356v-24,0,-40,11,-71,39v-64,57,-125,188,-125,259v0,12,6,19,15,19v10,0,40,-13,97,-65v76,-71,118,-157,134,-216v-7,-19,-24,-36,-50,-36","w":444},"b":{"d":"350,-709r-181,475r2,0v70,-106,137,-174,212,-174v59,0,86,39,86,90v0,69,-48,156,-129,235v-82,79,-156,97,-191,97v-54,0,-80,-35,-80,-92v0,-47,22,-119,51,-197r116,-308v30,-78,22,-76,-13,-99v-2,-4,-1,-10,3,-14v19,-7,74,-20,120,-19xm368,-374v-27,0,-76,34,-138,105v-63,72,-94,154,-94,200v0,39,18,52,35,52v15,0,53,-19,96,-67v62,-70,136,-197,136,-254v0,-27,-17,-36,-35,-36","w":466,"k":{"w":6,"v":6,".":24,",":24,"y":6}},"c":{"d":"45,-92v0,-92,115,-316,257,-316v50,0,74,28,74,59v0,40,-39,47,-49,47v-38,-3,-5,-73,-55,-73v-22,0,-53,21,-84,67v-33,49,-64,130,-67,207v-2,50,19,65,43,65v33,0,70,-26,108,-61v10,-1,16,7,13,17v-39,51,-102,94,-165,94v-39,0,-75,-29,-75,-106","w":338},"d":{"d":"391,-377r72,-206v28,-78,21,-76,-14,-99v-2,-4,-1,-10,3,-14v19,-7,75,-20,118,-19r4,6v-18,56,-33,94,-63,177v-99,273,-146,433,-155,463v-4,13,-3,23,6,23v16,0,79,-51,110,-90v9,0,14,4,15,13v-27,55,-118,137,-170,137v-45,-10,-36,-42,-19,-89r35,-99r-4,-1v-69,92,-144,189,-241,189v-39,0,-60,-32,-60,-72v0,-71,54,-190,133,-261v68,-61,131,-89,179,-89v16,0,40,10,51,31xm364,-306v24,-98,-56,-76,-102,-29v-50,51,-167,202,-174,290v0,12,10,21,19,21v15,0,66,-18,135,-89v78,-80,109,-140,122,-193","w":474},"e":{"d":"313,-408v40,0,63,34,63,60v0,76,-99,126,-243,166v-7,17,-14,49,-14,89v0,34,21,57,48,57v36,0,61,-20,105,-58v9,0,15,8,13,17v-61,75,-118,91,-160,91v-60,0,-81,-56,-81,-105v0,-63,40,-185,149,-269v52,-40,91,-48,120,-48xm293,-377v-14,0,-40,9,-76,48v-39,42,-62,88,-74,117v66,-20,97,-39,134,-74v31,-29,39,-59,39,-68v0,-10,-5,-23,-23,-23","w":346},"f":{"d":"168,-394v33,-136,119,-318,272,-321v37,0,77,26,77,66v0,32,-50,83,-57,41v-18,-104,-84,-85,-128,-19v-45,68,-66,138,-93,233r125,0v7,11,5,26,-10,37r-124,0r-115,411v-26,90,-59,141,-79,161v-15,14,-61,54,-120,54v-83,0,-87,-62,-50,-94v8,-6,17,-3,23,4v24,25,54,41,76,41v20,0,41,-7,64,-84v46,-158,81,-330,129,-493r-83,0v-10,-9,-6,-28,7,-37r86,0","w":261,"k":{"'":-88,"}":-72,"]":-96,")":-108,"\"":-88}},"g":{"d":"58,-180v0,-121,153,-281,285,-210v32,7,114,-43,114,11v0,58,-58,17,-92,20v-21,3,-1,25,-2,44v-8,106,-85,201,-198,226v-30,22,-31,54,11,82v23,15,62,38,98,60v58,34,75,65,75,97v0,56,-81,119,-244,119v-73,0,-158,-28,-158,-95v0,-76,117,-124,171,-141v-66,-38,-14,-81,13,-123v-19,-5,-73,-23,-73,-90xm151,236v46,0,132,-20,132,-68v0,-43,-117,-110,-136,-111v-43,-3,-131,50,-131,102v0,47,67,77,135,77xm275,-379v-17,0,-36,7,-67,41v-39,45,-90,133,-90,184v0,21,14,35,30,35v14,0,38,-6,66,-39v56,-66,91,-155,91,-187v0,-21,-15,-34,-30,-34","w":382,"k":{"p":-24,"y":-48,"f":-48}},"h":{"d":"251,-47v13,1,29,19,40,19v15,0,25,-15,40,-50v27,-65,72,-203,72,-258v0,-52,-45,-23,-75,-3v-87,59,-201,238,-248,341v-9,21,-57,15,-47,-14r190,-562v29,-86,28,-81,-10,-105v-2,-5,-2,-9,3,-13v16,-7,82,-21,117,-23r4,6r-175,503r2,2v70,-99,122,-139,158,-165v36,-26,68,-39,95,-39v44,0,60,32,60,67v0,109,-73,251,-143,316v-34,32,-60,39,-73,39v-58,-2,-39,-56,-10,-61","w":475},"i":{"d":"52,-22r127,-303v5,-11,5,-19,-2,-19v-7,0,-33,12,-83,53v-9,-1,-15,-9,-13,-17v55,-63,132,-100,165,-100v22,10,27,13,10,54r-119,284v-4,10,-5,22,3,22v17,0,70,-35,104,-74v10,1,15,8,15,17v-15,29,-118,119,-187,119v-13,-1,-27,-18,-20,-36xm264,-614v23,0,34,16,34,38v0,31,-22,62,-52,62v-26,0,-35,-20,-35,-37v0,-22,14,-63,53,-63","w":278},"j":{"d":"-73,269v-48,0,-67,-32,-67,-54v0,-21,27,-55,44,-29v35,54,77,66,107,-7v52,-128,135,-402,164,-512v2,-9,-4,-13,-8,-13v-23,0,-73,49,-99,77v-8,-1,-15,-9,-16,-17v21,-36,110,-122,174,-122v13,0,25,14,25,29v-23,113,-95,343,-144,473v-34,89,-92,175,-180,175xm255,-614v23,0,34,16,34,38v0,31,-22,62,-52,62v-26,0,-35,-20,-35,-37v0,-22,14,-63,53,-63","w":259},"k":{"d":"188,-267v51,-62,104,-141,210,-141v58,0,72,34,72,55v0,12,-4,39,-41,73v-47,43,-96,77,-160,88v-7,4,-5,13,-3,19v15,42,35,115,69,127v13,0,32,-13,60,-47v9,0,13,4,13,12v-35,75,-85,95,-101,95v-49,0,-75,-101,-92,-147v-18,-50,-55,-40,-74,7r-52,127v-4,9,-8,13,-17,13v-11,0,-40,-3,-32,-26r183,-552v28,-88,25,-82,-11,-100v-6,-3,-7,-10,-2,-15v30,-14,79,-28,122,-36r6,5r-152,443r2,0xm393,-339v0,-12,-9,-26,-32,-26v-68,0,-165,110,-189,160v-4,10,-3,17,10,15v21,-2,77,-19,147,-65v42,-28,64,-63,64,-84","w":448},"l":{"d":"340,-709r-217,644v-8,23,-7,33,4,33v22,0,69,-49,115,-101v10,-1,16,4,16,14v-26,50,-89,133,-172,133v-30,0,-39,-14,-39,-30v0,-16,5,-34,22,-85r159,-473v27,-87,15,-79,-19,-95v-5,-4,-5,-13,0,-16v18,-7,80,-26,126,-30","w":246},"m":{"d":"421,-408v72,0,50,79,29,130r-38,92r3,2v96,-153,172,-224,238,-224v71,1,61,70,40,124v-19,50,-90,213,-93,221v-4,10,-5,19,2,19v17,0,65,-33,94,-68v14,-2,16,6,16,14v-38,64,-117,112,-172,112v-9,0,-24,-13,-24,-25v27,-96,103,-225,126,-336v0,-12,-1,-24,-16,-24v-29,0,-77,51,-131,120v-51,64,-96,143,-157,253v-5,8,-9,12,-18,12v-11,0,-38,-3,-29,-26r99,-262v4,-11,44,-97,8,-97v-28,0,-84,52,-139,120v-52,65,-92,143,-150,253v-5,9,-9,12,-18,12v-11,0,-38,-3,-29,-26v3,-8,110,-300,114,-315v5,-17,4,-28,-7,-28v-33,0,-68,30,-88,55v-10,0,-15,-7,-14,-15v25,-48,78,-93,154,-93v23,0,33,18,24,41r-69,181r3,2v72,-118,166,-224,242,-224","w":729},"n":{"d":"245,-367r-68,175r3,2v71,-108,170,-218,245,-218v72,1,60,70,40,124v-18,50,-90,213,-93,221v-4,10,-5,19,2,19v17,0,65,-33,94,-68v14,-2,16,6,16,14v-38,64,-117,112,-172,112v-9,0,-24,-13,-24,-25v24,-90,105,-231,126,-336v0,-12,-1,-24,-16,-24v-24,0,-83,50,-138,119v-53,66,-94,144,-153,254v-5,9,-9,12,-18,12v-11,0,-38,-3,-29,-26r116,-315v5,-13,5,-23,-3,-23v-23,0,-58,21,-91,50v-8,0,-13,-7,-12,-15v38,-52,99,-93,149,-93v24,0,35,18,26,41","w":501},"o":{"d":"294,-408v71,0,106,53,102,126v-3,55,-32,151,-106,227v-62,64,-116,69,-143,69v-64,0,-106,-47,-100,-133v4,-62,41,-159,109,-224v52,-50,97,-65,138,-65xm287,-378v-20,0,-44,9,-73,48v-58,78,-97,197,-100,250v-2,35,12,64,41,64v94,0,168,-223,172,-296v2,-31,-5,-66,-40,-66","w":406,"k":{"v":12,"w":12,"y":12,"x":6,",":12,".":24}},"p":{"d":"282,-453r-21,61v76,-7,174,2,174,112v0,92,-56,173,-132,237v-63,53,-139,57,-143,57v-9,0,-17,-2,-31,-6v-8,2,-13,8,-16,18r-41,118v-41,105,-9,85,49,97v5,7,5,18,-4,22v-76,0,-151,4,-218,6v-7,-6,-7,-16,0,-22v61,-7,80,-31,97,-78r186,-522v-36,10,-59,20,-101,41v-8,-1,-13,-10,-10,-19v35,-25,75,-42,124,-55r30,-81v13,-11,53,-1,57,14xm253,-362r-104,292v-10,31,14,50,38,50v13,0,69,-14,112,-81v48,-74,59,-144,59,-188v0,-59,-49,-83,-105,-73","w":448,"k":{"w":6,"z":12,"y":12}},"q":{"d":"484,-422r-181,581v-26,74,-5,72,44,75v5,6,4,18,-4,22v-37,2,-153,18,-216,29v-8,-4,-10,-17,-4,-23v58,-16,89,-28,110,-92r115,-357r-2,0v-46,66,-151,201,-249,201v-43,0,-61,-38,-61,-75v0,-34,7,-90,50,-159v59,-96,138,-188,267,-188v27,0,48,25,54,55v14,-23,37,-59,54,-81v9,-2,22,2,23,12xm332,-378v-18,0,-45,10,-78,40v-65,60,-146,206,-146,279v0,16,8,28,22,28v72,-19,169,-122,210,-192v28,-48,28,-77,28,-98v0,-20,-12,-57,-36,-57","w":465},"r":{"d":"189,-408v50,0,32,68,23,95r-43,126r5,0v30,-56,79,-136,105,-169v23,-30,48,-52,73,-52v35,0,40,34,40,49v0,15,-9,44,-21,60v-12,15,-28,16,-32,-5v-10,-55,-28,-46,-56,-9v-60,78,-117,200,-170,315v-9,23,-57,13,-50,-14v11,-41,76,-217,103,-315v3,-11,1,-21,-8,-21v-26,0,-53,28,-71,52v-10,2,-17,-4,-16,-16v32,-54,70,-96,118,-96","w":341,"k":{"c":12,"d":20,"e":20,"l":12,"n":-8,"o":6,"t":-8,"u":-8,"z":-20,"m":-8,"q":18,"v":-18,"w":-18,"a":30,"y":-18,"h":4,"k":6,"f":-8,"x":-18,",":60,".":60}},"s":{"d":"163,-89v6,-29,-82,-151,-72,-181v0,-39,17,-68,60,-105v29,-25,61,-33,74,-33v52,-1,79,65,42,88v-30,-2,-39,-40,-80,-40v-29,0,-41,25,-41,40v0,46,85,142,72,202v2,55,-76,139,-153,132v-53,-5,-80,-51,-46,-86v8,-7,20,-8,27,0v14,14,43,40,69,40v24,0,48,-25,48,-57","w":262},"t":{"d":"337,-346r-109,0r-90,263v-6,19,-1,25,7,25v15,0,81,-52,114,-90v10,0,16,6,14,18v-56,83,-150,144,-192,144v-39,-12,-29,-39,-13,-84r94,-276r-78,0v-2,-10,1,-24,11,-28r80,-14r56,-99v10,-6,30,-4,38,6r-29,98r109,0v7,10,1,30,-12,37","w":284},"u":{"d":"261,-378r-112,292v-9,24,-4,34,7,34v15,0,55,-32,107,-83v91,-90,143,-206,176,-267v21,-4,45,6,49,18v-10,27,-75,191,-117,310v-5,13,-6,25,4,25v19,0,72,-47,98,-78v9,-2,17,2,18,14v-26,48,-122,127,-175,127v-16,0,-31,-19,-25,-35r68,-183r-2,-1v-72,91,-196,219,-250,219v-41,0,-46,-41,-29,-85r92,-243v7,-19,8,-29,0,-29v-19,0,-71,44,-95,69v-10,2,-18,-6,-17,-15v26,-40,126,-119,173,-119v13,0,36,13,30,30","w":496},"v":{"d":"82,-46v0,-89,67,-193,94,-275v7,-19,6,-29,-7,-29v-19,0,-62,36,-93,70v-9,0,-16,-9,-15,-17v32,-45,99,-111,158,-111v23,0,45,29,31,66r-86,223v-11,30,-22,83,19,83v23,0,67,-21,109,-74v76,-95,102,-155,77,-262v2,-21,18,-36,37,-36v32,0,39,37,39,62v0,73,-75,215,-161,287v-76,63,-132,73,-156,73v-26,0,-46,-30,-46,-60","w":426,"k":{"q":6,".":62,",":62,"c":6,"d":6,"e":6,"o":6,"s":6,"a":6}},"w":{"d":"421,-38v70,2,186,-173,186,-263v0,-24,-2,-48,-9,-71v2,-21,18,-36,37,-36v32,0,39,37,39,62v0,56,-32,145,-109,238v-72,87,-151,122,-191,122v-21,0,-56,-26,-56,-57v0,-27,9,-59,17,-85v5,-16,-2,-15,-14,1v-47,64,-130,141,-190,141v-61,0,-55,-70,-34,-127r75,-200v9,-23,11,-37,-3,-37v-20,0,-62,34,-92,70v-9,0,-16,-9,-15,-17v31,-45,96,-111,158,-111v23,0,43,29,30,66v-30,86,-98,187,-98,280v0,12,5,24,22,24v45,0,122,-99,157,-152v39,-59,60,-136,65,-160v7,-33,29,-54,47,-54v24,0,30,19,30,28v0,21,-11,40,-35,90v-36,74,-54,150,-54,204v0,30,13,44,37,44","w":657,"k":{"q":18,".":62,",":62,"c":12,"d":12,"e":12}},"x":{"d":"42,-93v13,4,15,47,28,41v46,-22,86,-87,126,-131r-42,-108v-12,-30,-18,-47,-28,-47v-17,0,-32,31,-45,51v-10,5,-19,-1,-18,-14v18,-55,65,-107,90,-107v17,0,30,14,44,52r43,117v9,-10,18,-23,68,-82v49,-58,81,-87,113,-87v27,0,40,24,40,47v0,35,-25,58,-40,58v-17,0,-16,-46,-35,-42v-18,0,-45,28,-135,137r37,95v14,35,24,57,36,57v14,0,31,-30,47,-59v7,-8,23,-4,21,6v-13,65,-49,123,-87,123v-53,0,-71,-117,-97,-165v-16,16,-46,55,-73,86v-57,68,-89,79,-108,79v-24,0,-36,-18,-36,-36v0,-24,25,-69,51,-71","w":420,"k":{"q":6,"c":6,"d":6,"e":6,"o":6,"y":-12}},"y":{"d":"-60,180v26,0,25,31,55,34v123,-39,168,-209,163,-406v-5,-187,-46,-183,-96,-74v-11,3,-17,-4,-19,-13v21,-67,65,-129,114,-129v51,0,59,100,61,170v1,64,-1,137,-15,220r4,0v59,-77,115,-249,121,-357v2,-29,17,-31,24,-31v14,0,37,6,37,29v0,25,-26,154,-116,305v-49,83,-85,149,-165,240v-74,85,-124,101,-156,101v-63,0,-68,-88,-12,-89","w":366,"k":{"c":6,"d":6,"e":6,"o":6,"q":6,",":72,".":72}},"z":{"d":"278,23v-62,-5,-131,-61,-179,-61v-34,0,-48,18,-58,52v-34,3,-39,-17,-39,-28v0,-14,13,-30,58,-71r243,-226v11,-11,7,-16,-12,-22v-46,-15,-86,-20,-114,-20v-60,0,-47,50,-27,74v0,11,-11,28,-35,28v-25,0,-37,-19,-37,-37v0,-21,19,-48,45,-72v81,-72,151,-13,241,-13v35,0,49,-23,59,-35v15,-2,27,8,25,20v-55,51,-201,175,-326,303r1,3v69,-5,124,56,188,56v41,0,56,-45,36,-94v-10,-25,12,-30,20,-30v28,0,43,29,43,51v0,48,-84,126,-132,122","w":406,"k":{"c":-6,"d":-12,"e":-6,"o":6}},"{":{"d":"86,79v-1,-76,91,-219,91,-294v0,-31,-30,-39,-68,-42v-3,-8,-2,-17,5,-22v91,-6,103,-49,112,-78v21,-65,29,-145,48,-213v33,-115,126,-124,176,-124v1,6,-1,14,-6,18v-124,26,-115,100,-134,200v-18,94,-35,190,-126,206r-1,4v32,8,51,30,51,69v0,88,-93,179,-93,269v0,42,23,61,61,69v1,6,0,14,-5,17v-56,0,-111,-12,-111,-79","w":320,"k":{"J":-30}},"|":{"d":"199,-750r0,1000r-59,0r0,-1000r59,0","w":260},"}":{"d":"325,-615v1,76,-91,219,-91,294v0,31,30,39,68,42v3,8,2,17,-5,22v-91,6,-103,49,-112,78v-21,65,-29,145,-48,213v-33,115,-126,126,-176,124v-1,-6,1,-14,6,-18v124,-26,115,-100,134,-200v18,-94,35,-190,126,-206r1,-4v-32,-8,-51,-30,-51,-69v0,-88,93,-179,93,-269v0,-42,-23,-61,-61,-69v-1,-6,0,-14,5,-17v56,0,111,12,111,79","w":320},"~":{"d":"116,-129v12,-30,45,-101,102,-101r124,0v52,0,64,-15,89,-65v13,-5,29,-1,33,14v-11,29,-41,100,-102,100r-124,0v-52,0,-64,16,-89,66v-13,5,-29,1,-33,-14","w":560},"'":{"d":"271,-686v41,0,39,14,19,63r-66,161v-3,8,-19,8,-23,0r18,-161v6,-55,8,-63,52,-63","w":208},"`":{"d":"167,-604v-5,-13,37,-39,48,-36v10,0,15,10,19,22r46,142v0,8,-12,12,-18,11v-30,-45,-75,-88,-95,-139","w":360}}});
;
// $Id: at-scripts.js,v 1.1.2.2 2009/12/29 02:27:08 jmburnz Exp $

/**
 * Show/Hide block content in block admin.
 */
$(document).ready(function(){
  $('.block-at-admin-content').hide();
  $('a.toggle-block').click(function() {
    var id = $(this).attr('id');
    $('.toggle-' + id).toggle('fast');
    return false;
  });
});

/**
 * Insert WAI-ARIA Landmark Roles (Roles for Accessible Rich Internet Applications)
 *
 * http://www.w3.org/TR/2006/WD-aria-role-20060926/
 * 
 * Due to validation errors with WAI-ARIA roles we use JavaScript to 
 * insert the roles. This is a stop-gap measure while the W3C sort 
 * out the validator.
 *
 * This is a starting point, you can copy this and use it in your subtheme for more granular control.
 */
if (Drupal.jsEnabled) {
  $(document).ready(function() {

    // Set role=banner on #branding wrapper div.
    $("#branding").attr("role","banner");

    // Optionally set role=complementary
    // Set role=complementary on #main-content blocks, sidebars and regions.
    // $(".block").attr("role","complementary");

    // Remove role=complementary from system blocks.
    // $(".block-system, td.block, tr.region, td.region").removeAttr("role","complementary");

    // Set role=main on #main-content div.
    $("#main-content").attr("role","main");

    // Set role=search on search block and box.
    $("#search-theme-form, #search-block-form, #search-form").attr("role","search");

    // Set role=contentinfo on the footer message.
    $("#footer-message").attr("role","contentinfo");

    // Set role=article on nodes.
    // $(".article").attr("role","article");

    // Set role=nav on navigation-like blocks.
    $("#nav, .admin-panel, #breadcrumb, .block-menu, #block-user-1, .local-tasks").attr("role","navigation");
  
  });
}

/**
 * Theme Settings
 */
// Breadcrumb settings
$(document).ready( function() {
  // Hide the breadcrumb details, if no breadcrumb.
  $('#edit-breadcrumb-display-wrapper').change(
    function() {
      div = $('#div-breadcrumb-collapse');
      if ($('#edit-breadcrumb-display').val() == 'no') {
        div.slideUp('slow');
      } else if (div.css('display') == 'none') {
        div.slideDown('slow');
      }
    }
  );
  if ($('#edit-breadcrumb-display').val() == 'no') {
    $('#div-breadcrumb-collapse').css('display', 'none');
  }
  
  $('#edit-breadcrumb-title').change(
    function() {
      checkbox = $('#edit-breadcrumb-trailing');
      if ($('#edit-breadcrumb-title').attr('checked')) {
        checkbox.attr('disabled', 'disabled');
      } else {
        checkbox.removeAttr('disabled');
      }
    }
  );
  $('#edit-breadcrumb-title').change();
});

// Horizontal login block settings
$(document).ready( function() {
  $('#edit-horizontal-login-block-overlabel').attr('disabled', true);
  $('#edit-horizontal-login-block').change(function() {
    if ($(this).is(':checked')) {
      $('#edit-horizontal-login-block-overlabel').attr('disabled', false);
    } else {
      $('#edit-horizontal-login-block-overlabel').attr('disabled', true);
    }
  }); 
});

/**
 * In most instances this will be called using the built in theme settings.
 * However, if you want to use this manually you can call this file
 * in the info file and user the ready function e.g.:
 * 
 * This will set sidebars and the main content column all to equal height:
 *  if (Drupal.jsEnabled) {
 *    $(document).ready(function() {
 *    $('#content-column, .sidebar').equalHeight();
 *   });
 *  }
 *  
 * This will set all blocks in regions (not sidebars) to equal height:
 *  if (Drupal.jsEnabled) {
 *    $(document).ready(function() {
 *    $('#region-name .block-inner').equalHeight();
 *   });
 *  }
 */
jQuery.fn.equalHeight = function () {
  var height = 0;
  var maxHeight = 0;

  // Store the tallest element's height
  this.each(function () {
    height = jQuery(this).outerHeight();
    maxHeight = (height > maxHeight) ? height : maxHeight;
  });

  // Set element's min-height to tallest element's height
  return this.each(function () {
    var t = jQuery(this);
    var minHeight = maxHeight - (t.outerHeight() - t.height());
    var property = jQuery.browser.msie && jQuery.browser.version < 7 ? 'height' : 'min-height';
    
    t.css(property, minHeight + 'px');
 });
};;
// $Id $

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Create a cookie with the given name and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String name The name of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given name.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String name The name of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};;
// $Id $

if (Drupal.jsEnabled) {
  $(document).ready(function() {
    // Which div or page element are we resizing?
    if (text_resize_scope) { // Admin-specified scope takes precedence.
      if ($('#'+text_resize_scope).length > 0) {
        var element_to_resize = $('#'+text_resize_scope); // ID specified by admin
      }
      else if ($('.'+text_resize_scope).length > 0) {
        var element_to_resize = $('.'+text_resize_scope); // CLASS specified by admin
      }
      else {
        var element_to_resize = $(text_resize_scope); // It's just a tag specified by admin
      }
		}
		else { // Look for some default scopes that might exist.
      if ($('DIV.left-corner').length > 0) {
        var element_to_resize = $('DIV.left-corner'); // Main body div for Garland
      }
      else if ($('#content-inner').length > 0) {
        var element_to_resize = $('#content-inner'); // Main body div for Zen-based themes
      }
      else if ($('#squeeze > #content').length > 0) {
        var element_to_resize = $('#squeeze > #content'); // Main body div for Zen Classic
      }
		}
    // Set the initial font size if necessary
    if ($.cookie('text_resize') != null) {
      element_to_resize.css('font-size', parseFloat($.cookie('text_resize')) + 'px');
      //alert( "Should be size: " + $.cookie('text_resize'));
    }
    else {
      //alert('Couldn\'t find text_resize cookie.');
    }
    if (text_resize_line_height_allow) {
      //alert('line height adjustment allowed! The current line-height is '+parseFloat(element_to_resize.css('line-height'), 10));
			// Set the initial line height if necessary
      if ($.cookie('text_resize_line_height') != null) {
        element_to_resize.css('line-height', parseFloat($.cookie('text_resize_line_height')) + 'px');
      }
		}
    // Changer links will change the text size when clicked
    $('a.changer').click(function() {
      // Set the current font size of the specified section as a variable
      var currentFontSize = parseFloat(element_to_resize.css('font-size'), 10);
			//alert('currentFontSize = '+currentFontSize);
			// Set the current line-height
			var current_line_height = parseFloat(element_to_resize.css('line-height'), 10);
			//alert('current_line_height = '+current_line_height);
      // javascript lets us choose which link was clicked, by ID
      if (this.id == 'text_resize_increase') {
        var new_font_size = currentFontSize * 1.2;
        if (text_resize_line_height_allow) { var new_line_height = current_line_height * 1.2; }
        // Allow resizing as long as font size doesn't go above text_resize_maximum.
        if (new_font_size <= text_resize_maximum) {
          $.cookie('text_resize', new_font_size, { path: '/' });
          if (text_resize_line_height_allow) { $.cookie('text_resize_line_height', new_line_height, { path: '/' }); }
          var allow_change = true;
        }
        else {
					$.cookie('text_resize', text_resize_maximum, { path: '/' });
          if (text_resize_line_height_allow) { $.cookie('text_resize_line_height', text_resize_line_height_max, { path: '/' }); }
          var reset_size_max = true;
				}
      }
      else if (this.id == 'text_resize_decrease') {
        var new_font_size = currentFontSize * 0.8;
        if (text_resize_line_height_allow) { var new_line_height = current_line_height * 0.8; }
        if (new_font_size >= text_resize_minimum) {
          // Allow resizing as long as font size doesn't go below text_resize_minimum.
          $.cookie('text_resize', new_font_size, { path: '/' });
          if (text_resize_line_height_allow) { $.cookie('text_resize_line_height', new_line_height, { path: '/' }); }
          var allow_change = true;
        }
        else {
          // If it goes below text_resize_minimum, just leave it at text_resize_minimum.
          $.cookie('text_resize', text_resize_minimum, { path: '/' });
					if (text_resize_line_height_allow) { $.cookie('text_resize_line_height', text_resize_line_height_min, { path: '/' }); }
          var reset_size_min = true;
        }
      }
      else if (this.id == 'text_resize_reset') {
        $.cookie('text_resize', text_resize_minimum, { path: '/' });
				if (text_resize_line_height_allow) { $.cookie('text_resize_line_height', text_resize_line_height_min, { path: '/' }); }
        var reset_size_min = true;
      }
      // jQuery lets us set the font size value of the main text div
      if (allow_change == true) {
        element_to_resize.css('font-size', new_font_size + 'px'); // Add 'px' onto the end, otherwise ems are used as units by default
				if (text_resize_line_height_allow) { element_to_resize.css('line-height', new_line_height + 'px'); }
        return false;
      }
      else if (reset_size_min == true) {
        element_to_resize.css('font-size', text_resize_minimum + 'px');
				if (text_resize_line_height_allow) { element_to_resize.css('line-height', text_resize_line_height_min + 'px'); }
        return false;
      }
      else if (reset_size_max == true) {
        element_to_resize.css('font-size', text_resize_maximum + 'px');
				if (text_resize_line_height_allow) { element_to_resize.css('line-height', text_resize_line_height_max + 'px'); }
        return false;
      }
    });
  });
};


/** Loading Cookisetter js */
$.getScript('/images/jquery.cookie.js');
$.getScript('/images/cookie_setter.js');
